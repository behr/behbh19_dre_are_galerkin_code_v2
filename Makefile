#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2018-2020
#

# REFERENCES:
#
# https://de.mathworks.com/help/matlab/matlab_external/custom-linking-to-required-api-libraries.html
# https://de.mathworks.com/matlabcentral/answers/377799-compiling-mex-files-without-the-mex-command
# https://de.mathworks.com/matlabcentral/answers/63267-problem-in-specifying-dependent-library-files-by-using-mex-switches#answer_74974
#

###############################################################################
# MATLABROOT                                                                  #
###############################################################################
HOSTNAME=$(shell hostname)
ifeq ($(HOSTNAME), jack)
MWROOT?=/vol1/MPI_MATLAB/R2018a
else ifeq ($(HOSTNAME), pc1184)
MWROOT?=/afs/mpi-magdeburg.mpg.de/usr/ubuntu16.04/MATLAB/R2018a
else ifeq ($(MWROOT),)
$(error ERROR NO MWROOT given: make MWROOT=<MATLABROOT>)
endif


###############################################################################
# C AND FORTRAN COMPILER AND LINKER                                           #
###############################################################################
CC=gcc
FC=gfortran
LD=gcc

# WINDOWS ONLY
DLLTOOL=dlltool


###############################################################################
# C AND FORTRAN COMPILER FLAGS AND LINK FLAGS                                 #
###############################################################################
CFLAGS=-std=c99 -O3 -march=native -Wall -Wpedantic -Werror -Wextra
#CFLAGS=-std=c99 -O3 -march=native -Wall -Wpedantic -Wextra
FFLAGS=-O3 -fdefault-integer-8
OPENMP=-fopenmp


###############################################################################
# OS SPECIFIC                                                                 #
###############################################################################
ifeq ($(OS),Windows_NT)
MV=move
DEL=del
OBJ=obj
MEXEXT=mexw64
else
MV=mv -f
DEL=rm -f
OBJ=o
UNAME_S:=$(shell uname -s)
ifeq ($(UNAME_S),Linux)
MEXEXT=mexa64
endif
ifeq ($(UNAME_S),Darwin)
MEXEXT=mexmaci64
endif
endif


###############################################################################
# VARIABLES TO SOURCE AND INCLUDE DIRECTORY                                   #
###############################################################################
MEXTOOLS_SRC=MEXTOOLS/src
MEXTOOLS_INC=MEXTOOLS/include
MEXTOOLS_HEADER=$(wildcard $(MEXTOOLS_INC)/*.h)
CFLAGS+=-I"MEXTOOLS/include"


###############################################################################
# MATLAB SPECIFIC FLAGS                                                       #
###############################################################################
MATLABMEX=-DMATLAB_MEX_FILE -DMATLAB_DEFAULT_RELEASE=R2018a
MATLABMEX+=-DMX_COMPAT_64 -DMATLAB_MEXCMD_RELEASE=R2018a -DUSE_MEX_CMD
CFLAGS+=-fexceptions -fPIC -fno-omit-frame-pointer
FFLAGS+=-fexceptions -fPIC -fno-omit-frame-pointer
ifeq ($(OS),Windows_NT)
# Windows
LDFLAGS=-flto -Wl,--no-undefined -shared
CFLAGS+=-I"$(MWROOT)\extern\include"
FFLAGS+=-fno-underscoring
LDFLAGS+=-static-libgcc -static
LDLIBS+=-L"MEXTOOLS\def"
LDLIBS+=-llibmwamd -llibmwumfpack -llibmwcholmod -llibmwma57 -llibmkl
LDLIBS+=-L"$(MWROOT)\extern\lib\win64\mingw64"
LDLIBS+=-llibmx -llibmex -llibmat -llibmwblas -llibmwlapack
LDLIBS+=-m64 -lgfortran
LINKEXPORTER=-Wl,"$(MWROOT)\extern\lib\win64\mingw64\exportsmexfileversion.def"
else
# Linux
ifeq ($(UNAME_S),Linux)
LDFLAGS=-flto -Wl,--no-undefined -shared
CFLAGS+=-I"$(MWROOT)/extern/include"
LDFLAGS+=-Wl,--as-needed
LDLIBS=-Wl,-rpath-link,$(MWROOT)/bin/glnxa64 -L"$(MWROOT)/bin/glnxa64"
LDLIBS+=-lmwamd -lmwumfpack -lmwcholmod  -lmwma57 -l:mkl.so
LDLIBS+=-Wl,-rpath-link,$(MWROOT)/extern/bin/glnxa64 -L"$(MWROOT)/extern/bin/glnxa64"
LDLIBS+=-lmx -lmex -lmat -lmwblas -lmwlapack -leng
LDLIBS+=-lm -lgfortran
LINKEXPORTER=-Wl,--version-script,"$(MWROOT)/extern/lib/glnxa64/c_exportsmexfileversion.map"
endif

# OSX
ifeq ($(UNAME_S),Darwin)
LDFLAGS=-flto -Wl,-twolevel_namespace -undefined error -arch x86_64 -shared
CFLAGS+=-I"$(MWROOT)/extern/include"
LDLIBS=-Wl,-rpath,$(MWROOT)/bin/maci64 -L"$(MWROOT)/bin/maci64"
LDLIBS+=-Wl,-rpath,$$ORIGIN -L.
LDLIBS+=-lmwamd -lmwumfpack -lmwcholmod  -lmwma57 -lmkl
LDLIBS+=-Wl,-rpath,$(MWROOT)/extern/bin/maci64 -L"$(MWROOT)/extern/bin/maci64"
LDLIBS+=-lmx -lmex -lmat -lmwblas -lmwlapack
LDLIBS+=-lm -lgfortran
LINKEXPORTER=-Wl,-exported_symbols_list,"$(MWROOT)/extern/lib/maci64/c_exportsmexfileversion.map"
endif
endif


###############################################################################
# DEBUG OPTION                                                                #
###############################################################################
ifeq ($(DEBUG),1)
CFLAGS+=-g3 -DDEBUG
FFLAGS+=-g3 -DDEBUG
endif


###############################################################################
# DEFAULT TARGET AND PARALLEL BUILDS                                          #
###############################################################################
.DEFAULT_GOAL := all
.NOTPARALLEL:


###############################################################################
# WINDOWS ONLY: TARGETS AND RULES FOR LIB FILES FOR IMPLICIT LINKING          #
# AGAINTS DLL LIBRARIES                                                       #
###############################################################################
ifeq ($(OS),Windows_NT)
# IMPLICIT LINK LIBRARIES
IMPLICIT_LINK_DEFS=$(wildcard MEXTOOLS/def/*.def)
IMPLICIT_LINK_LIBS=$(IMPLICIT_LINK_DEFS:.def=.lib)

# RULE FOR libmkl.lib
MEXTOOLS/def/libmkl.lib:MEXTOOLS/def/libmkl.def
	$(DLLTOOL) -v --input-def $< --dllname $(MWROOT)/bin/win64/mkl.dll -l $@

# RULE FOR other implicit link libraries
MEXTOOLS/def/%.lib:MEXTOOLS/def/%.def
	$(DLLTOOL) -v --input-def $< --dllname $(MWROOT)/bin/win64/$*.dll -l $@

all:$(IMPLICIT_LINK_LIBS)
endif


###############################################################################
# TARGETS AND RULES FOR c_mexapi_version                                      #
###############################################################################
# c_mexapi_version OBJECT FILES
ifeq ($(OS),Windows_NT)
C_MEXAPI_VERSION_CSRC=$(MWROOT)/extern/version/c_mexapi_version.c
else
C_MEXAPI_VERSION_CSRC=$(MWROOT)/extern/version/c_mexapi_version.c
endif
C_MEXAPI_VERSION_OBJS=$(MEXTOOLS_SRC)/c_mexapi_version.$(OBJ)

# RULE FOR c_mexapi_version.$(OBJ)
$(MEXTOOLS_SRC)/c_mexapi_version.$(OBJ): $(C_MEXAPI_VERSION_CSRC)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX)


###############################################################################
# TARGETS AND RULES FOR BANDED/CHOLMOD/MA57/MISC/QRTOOLS                      #
###############################################################################
# BANDED OBJECT FILES
BANDED_CSRC=$(wildcard $(MEXTOOLS_SRC)/BANDED/*.c)
BANDED_OBJS=$(BANDED_CSRC:.c=.$(OBJ))

# CHOLMOD OBJECT FILES
CHOLMOD_CSRC=$(wildcard $(MEXTOOLS_SRC)/CHOLMOD/*.c)
CHOLMOD_OBJS=$(CHOLMOD_CSRC:.c=.$(OBJ))	\
	$(MEXTOOLS_SRC)/CHOLMOD/cholmod_l_solve_omp.$(OBJ)

# ENGINE INTERFACE OBJECT FILES
ENGINE_CSRC=$(wildcard $(MEXTOOLS_SRC)/ENGINE_INTERFACE/*.c)
ENGINE_OBJS=$(ENGINE_CSRC:.c=.$(OBJ))

# RULE FOR ENGINE INTERFACE OBJECT FILE
$(MEXTOOLS_SRC)/ENGINE_INTERFACE/%.$(OBJ): $(MEXTOOLS_SRC)/ENGINE_INTERFACE/%.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(OPENMP) $(MATLABMEX)

# MA57 OBJECT FILES
MA57_OBJS=	\
	$(MEXTOOLS_SRC)/MA57/MA57AD.$(OBJ)	\
	$(MEXTOOLS_SRC)/MA57/MA57BD.$(OBJ)	\
	$(MEXTOOLS_SRC)/MA57/MA57DD.$(OBJ)	\
	$(MEXTOOLS_SRC)/MA57/MA57DD_omp.$(OBJ)

# RULE FOR MA57DD_omp OBJECT FILE
$(MEXTOOLS_SRC)/MA57/MA57DD_omp.$(OBJ): $(MEXTOOLS_SRC)/MA57/MA57DD.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(OPENMP) $(MATLABMEX)

# RULES FOR MA57{AD,BD,DD} OBJECT FILES
$(MEXTOOLS_SRC)/MA57/MA57%.$(OBJ): $(MEXTOOLS_SRC)/MA57/MA57%.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX)


# MISC OBJECT FILES
MISC_CSRC=$(wildcard $(MEXTOOLS_SRC)/MISC/*.c)
MISC_OBJS=$(MISC_CSRC:.c=.$(OBJ))

# QRTOOLS OBJECT FILES
QRTOOLS_CSRC=$(wildcard $(MEXTOOLS_SRC)/QRTOOLS/*.c)
QRTOOLS_OBJS=$(QRTOOLS_CSRC:.c=.$(OBJ))

# RULE FOR cholmod_solve_omp.$(OBJ)
%_omp.$(OBJ): %.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(OPENMP) $(MATLABMEX)

# RULE FOR BANDED/CHOLMOD/MISC/QRTOOLS OBJECT FILES
%.$(OBJ): %.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX)


###############################################################################
# TARGETS AND RULES FOR UMFPACK                                               #
###############################################################################
# UMFPACK OBJECT FILES
UMFPACK_OBJS =	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_error.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_report_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_report_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_report_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_report_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_free_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_free_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_free_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_free_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_solve.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_solve.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_solve_omp.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_solve_omp.$(OBJ)

# RULE FOR umfpack_error.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_error.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_error.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX)

# RULE FOR umfpack_dl_report_{symbolic,numeric}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_report_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_report.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX) -DUMFPACK_FUNC=umfpack_dl_report_$*

# RULE FOR umfpack_zl_report_{symbolic,numeric}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_report_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_report.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX) -DUMFPACK_FUNC=umfpack_zl_report_$*

# RULE FOR umfpack_dl_free_{symbolic,numeric}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_free_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_free.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX) -DUMFPACK_FUNC=umfpack_dl_free_$*

# RULE FOR umfpack_zl_free_{symbolic,numeric}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_free_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_free.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX) -DUMFPACK_FUNC=umfpack_zl_free_$*

# RULE FOR umfpack_dl_solve_omp.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_solve_omp.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_solve.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(OPENMP) $(MATLABMEX) -DUMFPACK_COMPLEX=0

# RULE FOR umfpack_zl_solve_omp.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_solve_omp.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_solve.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(OPENMP) $(MATLABMEX) -DUMFPACK_COMPLEX=1

# RULE FOR umfpack_dl_{symbolic,numeric,solve}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_%.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX) -DUMFPACK_COMPLEX=0

# RULE FOR umfpack_zl_{symbolic,numeric,solve}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_%.c $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX) -DUMFPACK_COMPLEX=1


###############################################################################
# TARGETS AND RULES FOR MEXTOOLS_CALL                                         #
###############################################################################
# MEXTOOLS_CALL OBJECT FILES
MEXTOOLS_CALL_CSRC=$(MEXTOOLS_SRC)/MEXTOOLS_CALL/mextools_call.c
MEXTOOLS_CALL_OBJ=$(MEXTOOLS_SRC)/MEXTOOLS_CALL/mextools_call.$(OBJ)
MEXTOOLS_CALL_EXE=MEXTOOLS/mextools_call.$(MEXEXT)

# MEXTOOLS_CALL DEPENDECIES
MEXTOOLS_CALL_DEPS =	\
	$(C_MEXAPI_VERSION_OBJS) $(BANDED_OBJS) $(CHOLMOD_OBJS) $(ENGINE_OBJS)	\
	$(MA57_OBJS) $(QRTOOLS_OBJS) $(UMFPACK_OBJS) $(MISC_OBJS)

# RULE FOR MEXTOOLS_CALL_OBJ
$(MEXTOOLS_CALL_OBJ): $(MEXTOOLS_CALL_CSRC) $(MEXTOOLS_HEADER)
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX)

# RULE FOR MEXTOOLS_CALL_EXE
$(MEXTOOLS_CALL_EXE): $(MEXTOOLS_CALL_OBJ) $(MEXTOOLS_HEADER) $(MEXTOOLS_CALL_DEPS) $(IMPLICIT_LINK_LIBS)
	$(LD) $(LDFLAGS) $(LINKEXPORTER) $(MEXTOOLS_CALL_OBJ) $(MEXTOOLS_CALL_DEPS) $(OPENMP) $(LDLIBS) -o $@

all: $(MEXTOOLS_CALL_EXE)


###############################################################################
# TARGETS AND RULES FOR MDM_SYM_DRE_STEP                                      #
###############################################################################
# MDM_SYM_DRE_STEP OBJECT FILES
MDM_SYM_DRE_STEP_FSRC=$(wildcard $(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/*.f)
MDM_SYM_DRE_STEP_FOBJS=$(MDM_SYM_DRE_STEP_FSRC:.f=.$(OBJ))
MDM_SYM_DRE_STEP_CSRC=$(wildcard $(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/*.c)
MDM_SYM_DRE_STEP_COBJS=$(MDM_SYM_DRE_STEP_CSRC:.c=.$(OBJ))
MDM_SYM_DRE_STEP_EXE=MODIFIED_DAVISON_MAKI_DRE/MDM_SYM_DRE_STEP.$(MEXEXT)

# MDM_SYM_DRE_STEP DEPENDECIES
MDM_SYM_DRE_STEP_DEPS=$(C_MEXAPI_VERSION_OBJS) $(MISC_OBJS)	\
	$(MDM_SYM_DRE_STEP_FOBJS)	$(MDM_SYM_DRE_STEP_COBJS)

# RULE FOR FORTRAN SOURCES FOR MDM_SYM_DRE_STEP
$(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/%.$(OBJ): $(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/%.f
	$(FC) -c $< -o $@ $(FFLAGS) $(MATLABMEX)

# RULE FOR C SOURCES FOR MDM_SYM_DRE_STEP
$(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/%.$(OBJ): $(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/%.c
	$(CC) -c $< -o $@ $(CFLAGS) $(MATLABMEX)

# RULE FOR MDM_SYM_DRE_STEP MEX
$(MDM_SYM_DRE_STEP_EXE): $(MDM_SYM_DRE_STEP_DEPS) $(MEXTOOLS_HEADER) $(IMPLICIT_LINK_LIBS)
	$(LD) $(LDFLAGS) $(LINKEXPORTER) $(MDM_SYM_DRE_STEP_DEPS) $(LDLIBS) -o $@

all: $(MDM_SYM_DRE_STEP_EXE)


###############################################################################
# CLEAN TARGET                                                                #
###############################################################################
.PHONY: clean

ifeq ($(OS),Windows_NT)
clean:
	$(DEL) $(subst /,\,$(MEXTOOLS_CALL_DEPS))
	$(DEL) $(subst /,\,$(MEXTOOLS_CALL_OBJ))
	$(DEL) $(subst /,\,$(MEXTOOLS_CALL_EXE))
	$(DEL) $(subst /,\,$(MDM_SYM_DRE_STEP_EXE))
	$(DEL) $(subst /,\,$(MDM_SYM_DRE_STEP_DEPS))
	$(DEL) $(subst /,\,$(IMPLICIT_LINK_LIBS))
else
clean:
	$(DEL) $(MEXTOOLS_CALL_DEPS) $(MEXTOOLS_CALL_OBJ) $(MEXTOOLS_CALL_EXE)	\
	$(DEL) $(MDM_SYM_DRE_STEP_EXE) $(MDM_SYM_DRE_STEP_DEPS)
endif

