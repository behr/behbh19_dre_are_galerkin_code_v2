function ret = mylcm(v)
%MYLCM      Computes the least common multiple of a integer vector v.
%
%   RET = MYLCM(V) compute the least common multiple of a numeric vector
%   V. V must be a numeric vector with positive integer values only.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% check inputs
validateattributes(v, {'numeric'}, {'real', 'integer', '>', 0, 'finite', 'nonnan', 'vector'}, mfilename, inputname(1));

%% compute the least common multiple
ret = 1;

for i = 1:numel(v)
    ret = lcm(ret, v(i));
end

end
