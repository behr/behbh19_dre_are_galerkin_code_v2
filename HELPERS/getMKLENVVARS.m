function [mkl_debug_cpu_type, mkl_enable_instructions] = getMKLENVVARS()
%GETMKLVARS    Return specific environment variables which have performance
% influence.
%
%   [MKL_DEBUG_CPU_TYPE, MKL_ENABLE_INSTRUCTIONS] = getMKLVARS() retrieves
%   specific environment variables which have performance influcence in
%   connection with the internal MATLB MKL library.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

% default values
mkl_debug_cpu_type = getenv('MKL_DEBUG_CPU_TYPE');
mkl_enable_instructions = getenv('MKL_ENABLE_INSTRUCTIONS');
end
