function v = vec(A)
%VEC    Unrolls a matrix to a vector by stacking columns.
%
%   V = VEC(A) unrolls a matrix A to a vector V by stacking columns.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%
validateattributes(A, {'numeric'}, {'2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(1));
v = A(:);
end
