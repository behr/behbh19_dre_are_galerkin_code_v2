classdef GitInfo
    %GITINFO    Get information about git repository.
    %
    %   GITINFO properties:
    %       branch          - set private, Current Branch
    %       sha             - set private, ID of current commit
    %       remote_url      - set private, URL of git repoistory
    %
    %   GITINFO methods:
    %       GitInfo         - Constructor for GitInfo object
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % current branch
        branch;
        % ID of current commit
        sha;
        % URL of git repository
        remote_url;
    end

    methods
        function obj = GitInfo()
            %GITINFO  Holds information about git repository.
            %
            %   git = GITINFO() Holds information about git repository.
            %
            %   See also EXAMPLE_DAVISON_MAKI_FAIL and EXAMPLE_EIG_DECAY_ARE.
            %
            %   Author: Maximilian Behr

            % default values
            branch = '';
            try
                [~, branch] = system('git rev-parse --abbrev-ref HEAD');
            catch
            end

            sha = '';
            try
                [~, sha] = system('git rev-parse HEAD');
            catch
            end

            remote_url = '';
            try
                [~, remote_url] = system('git config --get remote.origin.url');
            catch
            end

            % set data
            obj.branch = strtrim(branch);
            obj.sha = strtrim(sha);
            obj.remote_url = strtrim(remote_url);
        end
    end
end
