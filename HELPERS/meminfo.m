function [totalmem, usedmem, freemem] = meminfo()
%MEMINFO    Get memory infomation on a unix machine.
%
%   [TOTALMEM, USEDMEM, FREEMEM] = MEMINFO() returns the total, used and
%   free amount of memory in gigabytes on a unix machine. The command
%   uses free command. If the machine is not unix or something fails
%   all output values are set to 0.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

% set default values
totalmem = 0;
usedmem = 0;
freemem = 0;

try
    if isunix
        [~, w] = unix('free ');
        % get second line of output
        w = splitlines(w);
        w = w{2};
        stats = str2double(regexp(w, '[0-9]*', 'match'));
        totalmem = stats(1) / 1e6;
        usedmem = stats(2) / 1e6;
        freemem = (stats(3) + stats(end)) / 1e6;
    end
catch
end
