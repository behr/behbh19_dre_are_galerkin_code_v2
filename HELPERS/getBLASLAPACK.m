function [blas, lapack] = getBLASLAPACK()
%GETBLASLAPACK    Return BLAS and LAPACK version information.
%
%   [BLAS, LAPACK] = getBLASLAPACK() return BLAS and LAPACK version
%   information from the version command. If the version command call
%   fails, BLAS or LAPACK might be empty char arrays.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

% default values
blas = '';
lapack = '';

% get blas information
try
    blas = version('-blas');
    blas = regexprep(blas, '[\n\r]+', ' ');
    blas = erase(blas, '|');
    blas = erase(blas, ',');
    blas = strtrim(blas);
catch
end

% get lapack information
try
    lapack = version('-lapack');
    lapack = regexprep(lapack, '[\n\r]+', ' ');
    lapack = erase(lapack, '|');
    lapack = erase(lapack, ',');
    lapack = strtrim(lapack);
catch
end
