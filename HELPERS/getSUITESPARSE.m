function [ver_umfpack, ver_cholmod, ver_amd, ver_colamd, ver_symamd, ver_metis, ver_suitesparseqr] = getSUITESPARSE()
%GETSUITESPARSE    Return SUITESPARSE version information.
%
%   [VER_UMFPACK, VER_CHOLMOD, VER_AMD, VER_COLAMD, VER_SYMAMD, VER_METIS, VER_SUITESPARSEQR] = GETSUITESPARSE()
%   return SuiteSparse version
%   information by fetching the verbose output of the solvers.
%   If a command call fails empty char arrays are returned.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

% default values
blas = '';
lapack = '';

% default values from spparms
default_spumoni = spparms('spumoni');

%% capture output
spparms('spumoni',2);

A = sparse([1 2 3; 4 5 6; 7 8 9]); %#ok<*NASGU>
A2 = sparse([1 2; 4 5; 7 8]);

str_umfpack = evalc('[L,U,P,Q,R] = lu(A);');
str_cholmod = evalc('chol(A2''*A2);');
str_amd = evalc('amd(A);');
str_colamd = evalc('colamd(A);');
str_symamd = evalc('symamd(A);');
str_metis = evalc('dissect(A);');
str_suitesparseqr = evalc('qr(A2);');

%% extract versions
ver_umfpack = '';
try
    match = regexpi(str_umfpack,'UMFPACK[,]* V[0-9]*\.*[0-9]*\.*[0-9]*','match');
    ver_umfpack = match{1};
catch
end

ver_cholmod = '';
try
    match = regexpi(str_cholmod,'CHOLMOD[,]* version [0-9]*\.*[0-9]*\.*[0-9]*','match');
    ver_cholmod = match{1};
catch
end

ver_amd = '';
try
    match = regexpi(str_amd,'AMD[,]* version [0-9]*\.*[0-9]*\.*[0-9]*','match');
    ver_amd = match{1};
catch
end

ver_colamd = '';
try
    match = regexpi(str_colamd,'colamd[,]* version [0-9]*\.*[0-9]*\.*[0-9]*','match');
    ver_colamd = match{1};
catch
end

ver_symamd = '';
try
    match = regexpi(str_symamd,'symamd[,]* version [0-9]*\.*[0-9]*\.*[0-9]*','match');
    ver_symamd = match{1};
catch
end

ver_metis = '';
try
    match = regexpi(str_metis,'metis[,]* version [0-9]*\.*[0-9]*\.*[0-9]*','match');
    ver_metis = match{1};
catch
end

ver_suitesparseqr = '';
try
    match = regexpi(str_suitesparseqr,'suitesparseqr[,]* version [0-9]*\.*[0-9]*\.*[0-9]*','match');
    ver_suitesparseqr = match{1};
catch
end

%% reset spumoni
spparms('spumoni', default_spumoni);

