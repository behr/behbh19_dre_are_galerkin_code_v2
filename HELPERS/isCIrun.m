function val = isCIrun()
%ISCIRUNT    Checks some environment variables to figure out if code
% is running on a CI machine.
%
%   VAL = ISCIRUN() If VAL is true code is running on a CI machine.
%
%
%   References:
%
%       https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
%
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

val = false;

%% check some predefined env variables
ci_server = getenv('CI_SERVER');
ci_server_host = getenv('CI_SERVER_HOST');
ci_server_name = getenv('CI_SERVER_NAME');
gitlab_ci = getenv('GITLAB_CI');
ci_project_name = getenv('CI_PROJECT_NAME');

if ~isempty(ci_server) || ~isempty(ci_server_host) || ~isempty(ci_server_name) || ~isempty(gitlab_ci) || ~isempty(ci_project_name)
    val = true;
end

end
