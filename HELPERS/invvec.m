function A = invvec(v, n, m)
%INVVEC    Reshapes a n*m vector to a n-x-m matrix.
%
%   A = INVVEC(V, N, M) reshapes a N*M vector V to a  n-x-m matrix A.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%
validateattributes(v, {'numeric'}, {'vector', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(1));
validateattributes(n, {'numeric'}, {'scalar', 'nonsparse', 'finite', 'nonnan', '>=', 1}, mfilename, inputname(2));
validateattributes(m, {'numeric'}, {'scalar', 'nonsparse', 'finite', 'nonnan', '>=', 1}, mfilename, inputname(3));
assert(numel(v) == n*m, 'Dimensions are wrong.');
A = reshape(v, n, m);
end
