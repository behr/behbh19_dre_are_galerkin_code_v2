classdef test_fronorm < matlab.unittest.TestCase
    %TEST_FRONORM   Test for fronorm_outer and fronorm_outer_diff functions.
    %
    %   TEST_FRONORM calls the functions fronorm_outer and fronorm_outer_diff
    %   with different matrices. The returned norms are compared with
    %   the naive compuation using the norm function.
    %
    %   TEST_FRONORM properties:
    %       n               - double, scalar, positive, integer, rows
    %       p1              - double, scalar, positive, integer, columns
    %       p2              - double, scalar, positive, integer, columns
    %       rank_deficient  - logical, test with rank deficient matrix
    %       abs_tol         - double, scalar, positive, tolerance for absolute error
    %       rel_tol         - double, scalar, positive, tolerance for relative error
    %
    %   TEST_FRONORM methods:
    %       test        - Test fronorm_outer and fornorm_outer_diff.
    %
    %   See also FRONORM_OUTER and FRONORM_OUTER_DIFF.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        % double, scalar, positive, integer, rows
        n = {10, 20, 50, 99, 100, 500};
        % double, scalar, positive, integer, columns
        p1 = {1, 3, 7, 13, 40, 100, 200};
        % double, scalar, positive, integer, columns
        p2 = {2, 3, 7, 18, 22, 99, 157};
        % logical, test with rank deficient matrix
        rank_deficient = {false, true};
    end

    properties
        % double, scalar, positive, tolerance for relative error
        reltol = 1e-13;
        % double, scalar, positive, tolerance for absolute error
        abstol = 1e-7;
        % logical
        verbose = ~isCIrun();
    end

    methods(Test)
        function test(obj, n, p1, p2, rank_deficient)

            %% load data
            rng(1234);
            Q1 = rand(n, p1);
            Q2 = rand(n, p2);
            X1 = rand(p1, p1);
            X2 = rand(p2, p2);

            X1 = 0.5 * (X1 + X1');
            X2 = 0.5 * (X2 + X2');

            if rank_deficient
                % make Q1 rank deficient
                [Q1, S1] = svd(Q1, 0);
                mask = logical(eye(size(S1)));
                S1(mask) = logspace(2, -20, size(S1, 1));
                Q1 = Q1 * S1;

                % make Q2 rank defcient
                [Q2, S2] = svd(Q2, 0);
                mask = logical(eye(size(S2)));
                S2(mask) = logspace(2, -20, size(S2, 1));
                Q2 = Q2 * S2;
            end

            %% compute norms
            fnorm_outer1 = fronorm_outer(Q1, X1);
            fnorm_outer2 = fronorm_outer(Q2, X2);
            fnorm_outer3 = fronorm_outer_diff(Q1, X1, Q2, X2);

            %% compute norms for test
            test_fnorm_outer1 = norm(Q1*X1*Q1', 'fro');
            test_fnorm_outer2 = norm(Q2*X2*Q2', 'fro');
            test_fnorm_outer3 = norm(Q1*X1*Q1'-Q2*X2*Q2', 'fro');

            %% compute error
            abserr1 = abs(test_fnorm_outer1-fnorm_outer1);
            abserr2 = abs(test_fnorm_outer2-fnorm_outer2);
            abserr3 = abs(test_fnorm_outer3-fnorm_outer3);
            relerr1 = abserr1 / test_fnorm_outer1;
            relerr2 = abserr2 / test_fnorm_outer2;
            relerr3 = abserr3 / test_fnorm_outer3;

            %% print results
            if obj.verbose
                fprintf('\n');
                f(1) = fprintf('%s - n = %d, p1 = %d, p2 = %d, rank-deficient = %s\n', datestr(now), n, p1, p2, string(rank_deficient));
                f(2) = fprintf('abs./rel. 2-norm err1 = %.2e/%.2e\n', abserr1, relerr1);
                f(3) = fprintf('abs./rel. 2-norm err2 = %.2e/%.2e\n', abserr2, relerr2);
                f(4) = fprintf('abs./rel. 2-norm err3 = %.2e/%.2e\n', abserr3, relerr3);
                fprintf('%s\n', repmat('-', 1, max(f)));
            end

            %% test
            obj.fatalAssertLessThan(abserr1, obj.abstol);
            obj.fatalAssertLessThan(abserr2, obj.abstol);
            obj.fatalAssertLessThan(abserr3, obj.abstol);
            obj.fatalAssertLessThan(relerr1, obj.reltol);
            obj.fatalAssertLessThan(relerr2, obj.reltol);
            obj.fatalAssertLessThan(relerr3, obj.reltol);
        end
    end
end
