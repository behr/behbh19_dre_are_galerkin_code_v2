function [L_new, D_new, cols_new, cols_old] = truncate_LDLT(L_old, D_old, trunc_rel_tol)
%TRUNCATE_LDLT    Truncate a real low-rank LDL' representation.
%
%   [L_new, D_new, cols_new, cols_old] = TRUNCATE_LDLT(L, D, trunc_rel_tol)
%   trucates a real low-rank LDL' representation. D must be a symmetric
%   matrix.
%
%   The matrix D_new is a real symmetric matrix.
%   cols_new is the number of columns of L_new.
%   cols_old is the number of columns of L_old.
%
%   See also: truncate_ZZT.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

%% check input arguments
validateattributes(L_old, {'double'}, {'real', 'nonnan', 'finite', '2d'}, mfilename, inputname(1));
cols_old = size(L_old, 2);
validateattributes(D_old, {'double'}, {'real', 'nonnan', 'finite', '2d', 'square', 'size', [cols_old, cols_old]}, mfilename, inputname(2));
validateattributes(trunc_rel_tol, {'double'}, {'real', 'square', '>', 0, 'scalar'}, mfilename, inputname(3));

%% truncate
[Q, R] = qr(L_old, 0);
RD_oldRT = R * D_old * R';
RD_oldRT = 1 / 2 * (RD_oldRT + RD_oldRT');
[V, D] = eig(RD_oldRT, 'vector');
tol = trunc_rel_tol * sqrt(max(abs(D)));
idx = abs(D) >= tol;
L_new = Q * V(:, idx);
D_new = diag(D(idx));
cols_new = size(L_new, 1);
end
