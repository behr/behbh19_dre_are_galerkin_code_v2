function [SPSD, Q, D] = projection_spsd(S)
%PROJECTION_SPSD    Project a double symmetric matrix onto the set of symmetric
% positive semidefinite matrices.
%
%   [SPSD, Q, D] = PROJECTION_SPSD(S) project a double symmetric matrix S onto the set
%   of symmetric positive semidefinite matrices. SPSD is the best approximation
%   of S with respect to Frobenius and 2-norm to the set of symmetric
%   matrices. The columns of Q are the eigenvectors corresponding to
%   the eigenvalues D. D is a vector of sorted eigenvalues of S.,
%
%   References:
%   https://math.stackexchange.com/questions/2776803/matrix-projection-onto-positive-semi-definite-cone-with-respect-to-the-spectral
%   https://math.stackexchange.com/questions/2436094/find-the-matrix-projection-of-a-symmetric-matrix-onto-the-set-of-symmetric-posit
%   Boyd, Stephen and Vandenberghe, Lieven, Convex optimization, Cambridge University Press, 2004, p.399
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

%% check input arguments
validateattributes(S, {'double'}, {'real', 'nonnan', 'finite', '2d', 'square'}, mfilename, inputname(1));

%% symmetrize and compute spectral decomposition and sort
S = 1 / 2 * (S + S');
[Q, D] = eig(S, 'vector');
[D, I] = sort(D, 'descend');
Q = Q(:, I);

%% project
Dpos = diag(D(D > 0));
sDpos = size(Dpos, 1);
SPSD = Q(:, 1:sDpos) * Dpos * Q(:, 1:sDpos)';
end
