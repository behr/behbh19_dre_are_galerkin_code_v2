classdef test_MDM_SYM_DRE_STEP < matlab.unittest.TestCase
    %TEST_MDM_SYM_DRE_STEP   Test for MDM_SYM_DRE_STEP MEX function.
    %
    %   TEST_MDM_SYM_DRE_STEP tests the functionality of the
    %   MDM_SYM_DRE_STEP MEX function.
    %
    %   TEST_MDM_SYM_DRE_STEP  properties:
    %       n         - order of matrix
    %       arg_check - activate/deactivate argument check
    %       bs        - blocksize
    %       trf       - factorization method
    %       mm        - matrix multiplication method
    %       LUcpy     - additional copy of LU decomposition
    %       Lsolve    - variant for solving with L
    %       Usolve    - variant for solving with U
    %       ldUV      - increase of the leading dimension of U and V
    %
    %   TEST_MDM_SYM_DRE_STEP  methods:
    %       test_MDM_SYM_DRE_STEP_MEX   - Test for MDM_SYM_DRE_STEP.
    %
    %   See also MDM_SYM_DRE_STEP.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        % size of matrix
        n = {1, 3, 7, 32, 47, 51, 64, 125, 128};
        % arg_check
        %arg_check = {0, 1};
        arg_check = {1};
        % blocksize
        bs = {0, 1, 3, 16, 23, 43};
        %bs = {0};
        % factorization method
        %trf = {1, 2, 3, 4, 5, 6, 7, 8};
        trf = {1};
        % matrix multiplication method
        %mm = {1, 2, 3};
        mm = {3};
        % copy diagonal blocks of LU decomposition
        LUcpy = {0, 1, 2, 3};
        %LUcpy = {0};
        % row block solve with L
        Lsolve = {0, 1, 2, 3};
        %Lsolve = {0};
        % column block solve with U
        Usolve = {0, 1, 2, 3};
        %Usolve = {0};
        % ldUV
        %ldUV = {-1, 0};
        ldUV = {-1};
    end

    properties
        abstol = 1e-8;
        reltol = 1e-10;
        steps = 5;
        verbose = ~isCIrun();
        %verbose = false;
        randseed = 1;
    end

    methods(Test)
        function test_MDM_SYM_DRE_STEP_MEX(obj, n, arg_check, bs, trf, mm, LUcpy, Lsolve, Usolve, ldUV)

            %% set random seed
            rng(obj.randseed);

            %% if blocksize is larger than n, we can skip the test.
            if (bs > n)
              return
            end

            %% generate random test matrix
            A = rand(n, n);
            G = rand(n, n);
            G = G * G';
            Q = rand(n, n);
            Q = Q * Q';
            X0 = rand(n, n);
            X0 = X0' * X0;

            %% compute the matrix exponential and check the norm
            nrm1exphH = inf;
            h = 1;
            while (nrm1exphH > 1e+1)
                h = h / 2;
                exp_hH = expm(h*[-A, G; Q, A']);
                nrm1exphH = norm(exp_hH, 1);
            end

            exp_hH11T = exp_hH(1:n, 1:n)';
            exp_hH12T = exp_hH(1:n, n+1:end)';
            exp_hH21T = exp_hH(n+1:end, 1:n)';
            exp_hH22T = exp_hH(n+1:end, n+1:end)';

            %% perform some steps with MDM_SYM_DRE_STEP
            Xk = X0;
            for i = 1:obj.steps
                Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, arg_check, bs, trf, mm, LUcpy, Lsolve, Usolve, ldUV);
            end

            %% compute solution for comparison
            condU = [];
            nrm1X = [];
            Xk2 = X0;
            for i = 1:obj.steps
                UT = exp_hH11T + Xk2 * exp_hH12T;
                VT = exp_hH21T + Xk2 * exp_hH22T;
                Xk2 = linsolve(UT, VT);
                Xk2 = 1 / 2 * (Xk2 + Xk2');
                condU(end+1) = cond(UT); %#ok<*AGROW>
                nrm1X(end+1) = norm(Xk2, 1);
            end
            maxcondU = max(condU);
            maxnrm1X = max(nrm1X);

            %% test symmetry and other properties
            is_symmetricX = issymmetric(Xk);
            is_realX = isreal(Xk);

            %% compare solutions
            abserr_nrm1 = norm(Xk-Xk2, 1);
            relerr_nrm1 = abserr_nrm1 / norm(Xk2, 1);
            is_abserr = abserr_nrm1 < obj.abstol;
            is_relerr = relerr_nrm1 < obj.reltol;

            %% evaluate the checks and print message on error
            failed = ~(is_symmetricX && is_realX && is_abserr && is_relerr);

            if failed || obj.verbose
                fprintf('\n');
                if failed, fprintf('FAILED:\n');
                end
                f(1) = fprintf('n = %4d, steps = %d\n', n, obj.steps);
                f(2) = fprintf('arg_check = %2d, bs = %3d, trf = %d, mm = %d, LUcpy = %d, Lsolve = %d, Usolve = %d, ldUV = %+d\n',  arg_check, bs, trf, mm, LUcpy, Lsolve, Usolve, ldUV);
                f(3) = fprintf('maxcondU = %.2e, maxnrm1X = %.2e, nrm1expH = %.2e\n', maxcondU, maxnrm1X, nrm1exphH);
                f(4) = fprintf('abstol = %.2e, reltol = %.2e\n', obj.abstol, obj.reltol);
                f(5) = fprintf('X is symmetric = %d\n', is_symmetricX);
                f(6) = fprintf('X is real = %d\n', is_realX);
                f(7) = fprintf('abs./rel. 1-norm error = %.2e/%.2e\n', abserr_nrm1, relerr_nrm1);
                if(n<=10 && failed)
                  fprintf('EXPECTED:\n');
                  disp(Xk2);
                  fprintf('GOT:\n');
                  disp(Xk);
                end
                fprintf('%s\n', repmat('-', 1, max(f) - 1));
            end

            % check
            obj.fatalAssertTrue(is_symmetricX);
            obj.fatalAssertTrue(is_realX);
            obj.fatalAssertTrue(is_abserr);
            obj.fatalAssertTrue(is_relerr);
        end
    end
end
