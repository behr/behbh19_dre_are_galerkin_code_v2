%PARAMETERS_MDM_SYM_DRE_STEP
%
%   Script for experimental determining optimal parameters for
%   MDM_SYM_DRE_STEP mex file.
%
%   Parameters:
%     * trf
%       The LU factorization method.
%       Feasible values: 1, 2, 3, 4, 5, 6, 7, 8.
%
%     * mm
%       The matrix matrix product method
%       Feasible values: 1, 2, 3, 4, 5.
%
%     * LUcpy
%       Perform an additional copy of part of the LU factorization.
%       Feasible values: 0, 1, 2, 3.
%
%     * Lsolve
%       Variant for solving with the lower triangular matrix L.
%       Feasible values: 0, 1, 2, 3.
%
%     * Usolve
%       Variant for solving with the upper triangular matrix U.
%       Feasible values: 0, 1, 2, 3.
%
%     * ldUV2P
%       Increase of the leading dimension of U and V.
%       Feasible values: -1, 0, 1, ...
%
%   This script may run a long time. You may specifiy a smaller range
%   for the parameters.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all; %clc

%% parameters to test, adapt to your needs
% copy of the diagonal blocks of LU decomposition
LUcpys = [0, 1, 2, 3];
% row block solve with L
Lsolves = [0, 1, 2, 3];
% column block solve with U
Usolves = [0, 1, 2, 3];
% ldUV2P
ldUV2Ps = [-1, 0];

%% verbosity
verbose = ~isCIrun();

%% diary logging file
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
diary(fullfile(mydir, 'parameters_MDM_SYM_DRE_STEP.log'));

%% instances for determining optimal block size and factorization
if ~isCIrun()
  instances = {};
  %for n = unique([32:1:512])
  %for n = unique([4:4:256, 256:8:512, 512:16:1024])
  %for n = unique([32:1:512, 516:4:1024])
  %for n = unique([169])
  %for n = unique([1, 2, 3, 4])
  %for n = unique([1:1:128])
  %for n = unique([32,64,128,256])
  %for n = unique([64,128,256])

    % blocksizes and runs of timeit
    if n <= 32
      bss = 2.^(5:8);
      trfs = [1, 2, 3, 4, 5, 6, 7, 8];
      mms = [1, 2, 3];
      runs = 3;
    elseif n <= 64
      bss = 2.^(5:8);
      trfs = [1, 2, 3, 4, 5, 6, 7, 8];
      mms = [1, 2, 3];
      runs = 3;
    elseif n <= 128
      bss = 2.^(5:8);
      trfs = [1, 2, 3, 4, 5, 6, 7, 8];
      mms = [3]; %#ok<*NBRAK>
      runs = 2;
    elseif n <= 256
      bss = 2.^(3:8);
      trfs = [1, 3, 6, 7, 8];
      mms = [3];
      runs = 2;
    elseif n <= 512
      bss = 2.^(6:8);
      trfs = [1, 3, 6, 7, 8];
      mms = [3];
      runs = 2;
    else
      bss = 2.^(6:8);
      trfs = [1, 3, 6, 7, 8];
      mms = [3];
      runs = 1;
    end
    bss = unique([0, bss(bss < n)]);

    % create parameter set for instance
    instances{end+1} = struct('n', n, 'bss', bss, 'trfs', trfs, 'mms', mms, 'LUcpys', LUcpys, 'Lsolves', Lsolves, 'Usolves', Usolves, 'ldUV2Ps', ldUV2Ps, 'runs', runs); %#ok<SAGROW>
  end
else
  instances = { ...
    struct('n', 64, 'bss', 2.^(2:5), 'trfs', [1], 'mms', [3], 'LUcpys', [0], 'Lsolves', [1], 'Usolves', [1], 'ldUV2Ps', [-1], 'runs', 1), ...
    struct('n', 128, 'bss', 2.^(4:6), 'trfs', [1], 'mms', [3], 'LUcpys', [0], 'Lsolves', [1], 'Usolves', [1], 'ldUV2Ps', [-1], 'runs', 1), ...
    struct('n', 128, 'bss', 2.^(5:6), 'trfs', [1], 'mms', [3], 'LUcpys', [0], 'Lsolves', [1], 'Usolves', [1], 'ldUV2Ps', [-1], 'runs', 1), ...
    };
end

%% warm up the machine using dgemm
maxNumCompThreads('automatic');
if verbose
  fprintf('%s\n', datestr(now));
  fprintf('######## This script may run several hours #######\n');
  fprintf('# warmup the machine using dgemm and copy operations #\n');
  fprintf('!!!PLEASE CLOSE ALL OTHER APPLICATIONS TO GET REASONABLE RESULTS!!!\n');
end
if ~isCIrun()
  b = rand(500, 500);
  for i = 1:5000
    t = tic();
    t = toc(t);
    a = b;
    c = a;
    a = a + b * c;
  end
else
  b = rand(20, 20);
  for i = 1:1000
    t = tic();
    t = toc(t);
    a = b;
    c = a;
    a = a + b * c;
  end
end

%% determine optimal blocksize experimentally for different matrix sizes
% [n, blocksize, factorization, multiplication, est. time per step, speedup]
optimal_parameters = [];

for idx_instance = 1:numel(instances)

  % get parameters to test
  instance = instances{idx_instance};
  n = instance.n;
  bss = instance.bss;
  trfs = instance.trfs;
  mms = instance.mms;
  LUcpys = instance.LUcpys;
  Lsolves = instance.Lsolves;
  Usolves = instance.Usolves;
  ldUV2Ps = instance.ldUV2Ps;
  runs = instance.runs;

  % create some data for MDM_SYM_DRE_STEP
  Xk = zeros(n, n);
  exp_hH11 = eye(n, n);
  exp_hH12 = zeros(n, n);
  exp_hH21 = zeros(n, n);
  exp_hH22 = eye(n, n);

  % iterate over blocksizes and measure the time
  if verbose
    fprintf('%s\n', datestr(now));
    fprintf('# Determine optimal parameters for n = %d, %.1f %%\n', ...
      n, idx_instance/numel(instances)*100);
  end

  %% naive method for comparison
  f = @() naive_mdm_sym_dre_step(exp_hH11, exp_hH12, exp_hH21, exp_hH22, Xk);
  wtruns = zeros(1, runs);
  for r = 1:runs
    wtruns(r) = timeit(f, 1);
  end
  wt_naive = median(wtruns);

  %% build matrix with all parameter combinations
  parameter_set = {bss, trfs, mms, LUcpys, Lsolves, Usolves, ldUV2Ps};
  c = cell(1, numel(parameter_set));
  [c{:}] = ndgrid(parameter_set{:});
  parameter_matrix = cell2mat(cellfun(@(v)v(:), c, 'UniformOutput', false));

  % We can remove some parameters if blocksize is n.
  % The unblocked code is called in this case and the parameters
  % LUcpy, Lsolve and Usolve have no influence.
  % Thus we only need to vary trf, mm and ldUV2P.
  % We set Lulocs, Lsolve and Usolve to some default values and remove
  % duplicated columns.
  parameter_matrix(parameter_matrix(:,1) == 0, 4:6) = 0;
  parameter_matrix = unique(parameter_matrix, 'rows');

  %% iterate over all parameters and save wall time
  wt = zeros(size(parameter_matrix, 1), 1);

  for i = 1:size(parameter_matrix, 1)
    parameter_cell = num2cell(parameter_matrix(i, :));
    f = @() MDM_SYM_DRE_STEP(exp_hH11, exp_hH12, exp_hH21, exp_hH22, Xk, 0, parameter_cell{:});
    wtruns = zeros(1, runs);
    for r = 1:runs
      wtruns(r) = timeit(f, 1);
    end
    wt(i) = median(wtruns);
    if verbose
      fprintf(['n= %d, bs=%4d, trf=%d, mm=%d, LUcpy=%d, Lsolve=%d, Usolve=%d, ldUV2P=%+d, ', ...
        'est. time per call = %.4e, naive = %.4e, ', ...
        'est. speedup = %.4f, runs of timeit = %d\n'], ...
        n, parameter_cell{:}, wt(i), wt_naive, wt_naive/wt(i), runs);
    end
  end

  %% determine optimal blocksize and factorization
  [~, idx] = sort(wt);
  wt = wt(idx);
  parameter_matrix = parameter_matrix(idx, :);
  s = min(50, numel(wt));
  if verbose
    fprintf('# Best %d parameters for n = %d:\n', s, n);
    for i = 1:s
      parameter_cell = num2cell(parameter_matrix(i, :));
      fprintf(['n = %d, bs=%4d, trf=%d, mm=%d, LUcpy=%d, Lsolve=%d, Usolve=%d, ldUV2P=%+d, ', ...
        'est. time per call = %.4e, naive = %.4e, est. speedup = %.4f\n'], ...
        n, parameter_cell{:}, wt(i), wt_naive, wt_naive/wt(i));
    end
    fprintf('-------------------------------------------------\n');
  end

  % save parameters, est. time per step and est. speedup
  parameter_cell = num2cell(parameter_matrix(1, :));
  if isempty(optimal_parameters)
    optimal_parameters = [n, parameter_cell{:}, wt(1), wt_naive / wt(1)];
  else
    optimal_parameters(end+1, :) = [n, parameter_cell{:}, wt(1), wt_naive / wt(1)]; %#ok<SAGROW>
  end
end

%% write results to log and mtx file
timenow = datestr(datetime('now'));
hostname = getComputerName();
user = getUserName();
mycpuinfo = cpuinfo();
[totalmem, usedmem, freemem] = meminfo();
[blas, lapack] = getBLASLAPACK();
[ver_umfpack, ver_cholmod, ver_amd, ver_colamd, ver_symamd, ver_metis, ver_suitesparseqr] = getSUITESPARSE();
gitinfo = GitInfo();
[mkl_debug_cpu_type, mkl_enable_instructions] = getMKLENVVARS();

comment = char( ...
  sprintf('Optimal parameters for MDM_SYM_DRE_STEP, generated %s', timenow), ...
  sprintf('column 1: size of the matrix (n)'), ...
  sprintf('column 2: optimal block size (bs)'), ...
  sprintf('column 3: optimal LU factorization (trf)'), ...
  sprintf('column 4: optimal matrix multiplication (mm)'), ...
  sprintf('column 5: optimal choice of for additional copy of LU factorization (LUcpy)'), ...
  sprintf('column 6: optimal variant for solving with L (Lsolve)'), ...
  sprintf('column 7: optimal variant for solving with U (Usolve)'), ...
  sprintf('column 8: optimal choice of leading dimension for U and V (ldUV2P)'), ...
  sprintf('column 9: expected time per call of MDM_SYM_DRE_STEP'), ...
  sprintf('column 10: expected speedup over naive implmentation'), ...
  sprintf('Host = %s', hostname), ...
  sprintf('User = %s', user), ...
  sprintf('Computer = %s', computer()), ...
  sprintf('CPU-Name = %s', mycpuinfo.Name), ...
  sprintf('CPU-Clock = %s', mycpuinfo.Clock), ...
  sprintf('CPU-Cache = %s', mycpuinfo.Cache), ...
  sprintf('CPU-NumProcessors = %d', mycpuinfo.NumProcessors), ...
  sprintf('CPU-OSType = %s', mycpuinfo.OSType), ...
  sprintf('CPU-OSVersion = %s', mycpuinfo.OSVersion), ...
  sprintf('Total Mem GB = %e', totalmem), ...
  sprintf('Used Mem GB = %e', usedmem), ...
  sprintf('Free Mem GB = %e', freemem), ...
  sprintf('MATLAB = %s', version), ...
  sprintf('MATLAB INTERNAL BLAS = %s', blas), ...
  sprintf('MATLAB INTERNAL UMFPACK = %s', ver_umfpack), ...
  sprintf('MATLAB INTERNAL CHOLMOD = %s', ver_cholmod), ...
  sprintf('MATLAB INTERNAL AMD = %s', ver_amd), ...
  sprintf('MATLAB INTERNAL COLAMD = %s', ver_colamd), ...
  sprintf('MATLAB INTERNAL SYMAMD = %s', ver_symamd), ...
  sprintf('MATLAB INTERNAL METIS = %s', ver_metis), ...
  sprintf('MATLAB INTERNAL SUITESPARSEQR = %s', ver_suitesparseqr), ...
  sprintf('MATLAB MAXNUMCOMPTHREADS = %d', maxNumCompThreads()), ...
  sprintf('ENV MKL_DEBUG_CPU_TYPE = %s', mkl_debug_cpu_type), ...
  sprintf('ENV MKL_ENABLE_INSTRUCTIONS = %s', mkl_enable_instructions), ...
  sprintf('git sha = %s', gitinfo.sha), ...
  sprintf('git url = %s', gitinfo.remote_url), ...
  sprintf('git branch = %s', gitinfo.branch), ...
  sprintf('mfilename = %s', mfilename) ...
  );
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mmwrite(fullfile(mydir, 'parameters_MDM_SYM_DRE_STEP.mtx'), optimal_parameters, comment);

%% turn off diary
diary('off');

%% local function for estimating the speedup
function Xk = naive_mdm_sym_dre_step(exp_hH11, exp_hH12, exp_hH21, exp_hH22, Xk)
  UT = exp_hH11 + Xk * exp_hH12;
  VT = exp_hH21 + Xk * exp_hH22;
  Xk = linsolve(UT, VT);
  Xk = 1 / 2 * (Xk + Xk');
end
