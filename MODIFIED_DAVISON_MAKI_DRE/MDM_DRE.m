classdef MDM_DRE < Solver_DRE
    %MDM_DRE  Implements modified Davison-Maki method for autonomous nonsymmetric
    %   differential Riccati equations:
    %
    %   Autonomous nonsymmetric differential Riccati equation:
    %
    %   \dot{X} = M22 X - X M11 - X M12 X + M21, X(0) = M0       (NDRE)
    %
    %   The class solves the NDRE using the modified Davison-Maki method.
    %
    %   MDM_DRE methods:
    %       MDM_DRE - Constructor.
    %
    %   MDM_DRE properties:
    %       M11         - set private, double, dense, n-x-n matrix M11 of DRE
    %       M12         - set private, double, dense, n-x-m matrix M12 of DRE
    %       M21         - set private, double, dense, m-x-n matrix M21 of DRE
    %       M22         - set private, double, dense, m-x-m matrix M22 of DRE
    %       M0          - set private, double, dense, m-x-n matrix M0 of DRE
    %       exp_hH11    - set private, submatrix (1,1) of matrix exponential
    %       exp_hH12    - set private, submatrix (1,2) of matrix exponential
    %       exp_hH21    - set private, submatrix (2,1) of matrix exponential
    %       exp_hH22    - set private, submatrix (2,2) of matrix exponential
    %       nrm1_exp_hH - set private, the 1-norm of the matrix exponential of h*H
    %       Xk          - set private, current approximation for X(t)
    %
    %   See also SOLVER_DRE, SOLVER_DRE_OPTIONS and MDM_DRE_OPTIONS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % set private, double, dense, n-x-n matrix M11 of DRE
        M11
        % set private, double, dense, n-x-m matrix M12 of DRE
        M12
        % set private, double, dense, m-x-n matrix M21 of DRE
        M21
        % set private, double, dense, m-x-m matrix M22 of DRE
        M22
        % set private, double, dense, m-x-n matrix M0 of DRE
        M0
        % set private, submatrix (1,1) of matrix exponential
        exp_hH11
        % set private, submatrix (1,2) of matrix exponential
        exp_hH12
        % set private, submatrix (2,1) of matrix exponential
        exp_hH21
        % set private, submatrix (2,2) of matrix exponential
        exp_hH22
        % set private, the 1-norm of the matrix exponential of h*H
        nrm1_exp_hH
        % set private, current approximation for X(t)
        Xk
    end

    methods(Access = public)
        function obj = MDM_DRE(M11, M12, M21, M22, M0, opt, h, T)
            %MDM_DRE  Constructor of class.
            %
            %   MOD = MDM_DRE(M11, M12, M21, M22, M0, OPT, H, T) creates an
            %   instance of the class. OPT must be an instance
            %   of MDM_DRE_options class. The matrices
            %   M11, M12, M21, M22, M0 must be double, dense and of suiteable size.
            %   H is the step size and T the final time.
            %
            %   See also SOLVER_DRE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(M0, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(5));
            [m, n] = size(M0);
            validateattributes(M11, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [n, n]}, mfilename, inputname(1));
            validateattributes(M12, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [n, m]}, mfilename, inputname(2));
            validateattributes(M21, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [m, n]}, mfilename, inputname(3));
            validateattributes(M22, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [m, m]}, mfilename, inputname(4));
            validateattributes(M0, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [m, n]}, mfilename, inputname(5));
            validateattributes(opt, {'MDM_DRE_options'}, {}, mfilename, inputname(6));

            %% check input options
            assert(~opt.symmetric, 'symmetric has to be false in options.');

            %% set values to attributes
            obj.options = copy(opt);
            obj.M11 = M11;
            obj.M12 = M12;
            obj.M21 = M21;
            obj.M22 = M22;
            obj.M0 = M0;
            obj.Xk = M0;

            %% set integration parameters
            obj.t = 0;
            obj.h = h;
            obj.T = T;
            obj.validate_step_size();
        end
    end

    methods(Access = protected)
        function m_prepare(obj)
            %M_PREPARE  Prepare the solver for iteration.
            %
            %   MOD.M_PREPARE() computes the matrix exponential h*H and
            %   its 1-norm if the 1-norm is too large a warning is printed
            %   or an error message it returned.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr

            %% compute the matrix exponential
            n = size(obj.M0, 2);
            exp_hH = expm(obj.h*full([obj.M11, obj.M12; obj.M21, obj.M22]));
            obj.exp_hH11 = exp_hH(1:n, 1:n);
            obj.exp_hH12 = exp_hH(1:n, n+1:end);
            obj.exp_hH21 = exp_hH(n+1:end, 1:n);
            obj.exp_hH22 = exp_hH(n+1:end, n+1:end);

            %% check if matrix exponential is too large
            nrm1exphH = norm(exp_hH, 1);
            %fprintf('%s - %s, h = %.2e, ||expm(h*H)||_1 = %.2e.\n', datestr(now), upper(class(obj)), obj.h, nrm1exphH);
            if obj.options.warning_bound_expmH <= nrm1exphH && nrm1exphH < obj.options.error_bound_expmH
                warning('%s:1-norm of matrix exponential is large %.2e.', upper(class(obj)), nrm1exphH);
            end
            if obj.options.error_bound_expmH <= nrm1exphH || isinf(nrm1exphH) || isnan(nrm1exphH)
                error('%s:1-norm of matrix exponential is too large %.2e, use smaller step sizes.', upper(class(obj)), nrm1exphH);
            end
            obj.nrm1_exp_hH = nrm1exphH;
        end

        function m_time_step(obj)
            %M_TIME_STEP  Time steppping function for solver.
            %
            %   MOD.M_TIME_STEP() performes one time step using the
            %   modified Davison Maki algorithm.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr

            %% perform time step
            U = obj.exp_hH11 + obj.exp_hH12 * obj.Xk;
            V = obj.exp_hH21 + obj.exp_hH22 * obj.Xk;
            obj.Xk = V / U;

            %% update time
            obj.t = obj.t + obj.h;
        end

        function Xt = m_get_solution(obj)
            %M_GET_SOLUTION  Return the current approximation as a matrix Xt.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr
            Xt = obj.Xk;
        end
    end
end
