classdef MDM_SYM_DRE < Solver_DRE
    %MDM_SYM_DRE      Implements modified Davison-Maki method for autonomous
    %   symmetric differential Riccati equations:
    %
    %   M' \dot{X} M = A' X M + M' X A - M' X S X M + Q, X(0) = X0 (DRE)
    %
    %   The class solves the symmetric DRE using the modified Davison Maki method:
    %
    %   MDM_SYM_DRE methods:
    %       MDM_SYM_DRE   - Constructor.
    %
    %   MDM_SYM_DRE properties:
    %       M           - set private, double, dense, n-x-n matrix M of DRE or empty for identity
    %       A           - set private, double, dense, n-x-n matrix A of DRE
    %       S           - set private, double, dense, symmetric, n-x-n matrix S of DRE
    %       Q           - set private, double, dense, symmetric, n-x-n matrix Q of DRE
    %       X0          - set private, double, dense, symmetric, n-x-n matrix X0 of DRE
    %       exp_hH11T   - set private, transpose submatrix (1,1) of matrix exponential
    %       exp_hH12T   - set private, transpose submatrix (1,2) of matrix exponential
    %       exp_hH21T   - set private, transpose submatrix (2,1) of matrix exponential
    %       exp_hH22T   - set private, transpose submatrix (2,2) of matrix exponential
    %       nrm1_exp_hH - set private, the 1-norm of the matrix exponential of h*H
    %       Xk          - set private, current approximation for X(t)
    %       optimal_parameters - set private, optimal paramters for MDM_SYM_DRE_STEP
    %
    %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % set private, double, dense, n-x-n matrix M of DRE or empty for identity
        M
        % set private, double, dense, n-x-n matrix A of DRE
        A
        % set private, double, dense, symmetric, n-x-n matrix S of DRE
        S
        % set private, double, dense, symmetric, n-x-n matrix Q of DRE
        Q
        % set private, double, dense, symmetric, n-x-n matrix X0 of DRE
        X0
        % set private, transpose submatrix (1,1) of matrix exponential
        exp_hH11T
        % set private, transpose submatrix (1,2) of matrix exponential
        exp_hH12T
        % set private, transpose submatrix (2,1) of matrix exponential
        exp_hH21T
        % set private, transpose submatrix (2,2) of matrix exponential
        exp_hH22T
        % set private, the 1-norm of the matrix exponential of h*H
        nrm1_exp_hH
        % set private, current approximation for X(t)
        Xk
        % set private, optimal paramters for MDM_SYM_DRE_STEP
        optimal_parameters;
    end

    properties(Constant)
        % constant, argument check for MDM_SYM_DRE_STEP mex function
        arg_check= 0;
    end

    methods(Access = public)
        function obj = MDM_SYM_DRE(A, M, S, Q, X0, opt, h, T)
            %MDM_SYM_DRE  Constructor of class.
            %
            %   MOD = MDM_SYM_DRE(A, M, S, Q, X0, OPT, H, T)
            %   creates an instance of the class. OPT must be an instance
            %   of MDM_DRE_options class.
            %   The matrices A, M, S, Q and X0 must be dense, double, square
            %   and of same size. If M is empty then the identity
            %   matrix is used. M must be nonsingular. S, Q and X0 must
            %   be symmetric.
            %   H is the step size and T the final time.
            %
            %   See also SOLVER_DRE and MDM_DRE.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(A, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square'}, mfilename, inputname(1));
            n = size(A, 1);
            validateattributes(S, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square', 'size', [n, n]}, mfilename, inputname(3));
            validateattributes(Q, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square', 'size', [n, n]}, mfilename, inputname(4));
            validateattributes(X0, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square', 'size', [n, n]}, mfilename, inputname(5));
            validateattributes(opt, {'MDM_DRE_options'}, {}, mfilename, inputname(6));
            assert(issymmetric(S), 'Matrix %s must be symmetric', inputname(3));
            assert(issymmetric(Q), 'Matrix %s must be symmetric', inputname(4));
            assert(issymmetric(X0), 'Matrix %s must be symmetric', inputname(5));

            if ~isempty(M)
                validateattributes(M, {'double'}, {'real', '2d', 'square', 'nonsparse', 'finite', 'nonnan', 'square', 'size', [n, n]}, mfilename, inputname(2));
            end

            %% check input options
            assert(opt.symmetric, 'symmetric has to be true in options.');

            %% set values to attribute
            obj.options = copy(opt);
            obj.M = M;
            obj.A = A;
            obj.S = S;
            obj.Q = Q;
            obj.X0 = X0;
            obj.Xk = X0;

            %% set integration parameters
            obj.t = 0;
            obj.h = h;
            obj.T = T;
            obj.validate_step_size();

            %% load parameters file for MDM_SYM_DRE_STEP MEX function
            if obj.options.use_mex
                if ~(exist('parameters_MDM_SYM_DRE_STEP.mtx','file') == 2)
                    warning('parameter file: parameters_MDM_SYM_DRE_STEP.mtx does not exist.');
                    warning('Execute the script parameters_MDM_SYM_DRE_STEP.m first');
                    obj.optimal_parameters = {};
                else
                    parameters = mmread('parameters_MDM_SYM_DRE_STEP.mtx');
                    [~,idx] = min(abs(parameters(:,1)-n));
                    obj.optimal_parameters = num2cell(parameters(idx,2:(end-2)));
                end
            end
        end
    end

    methods(Access = protected)
        function m_prepare(obj)
            %M_PREPARE  Prepare the solver for iteration.
            %
            %   MOD.M_PREPARE() computes the matrix exponential h*H and
            %   its 1-norm if the 1-norm is too large a warning is printed
            %   or an error message it returned.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr

            %% transform
            if isempty(obj.M)
                A2 = obj.A;
                Q2 = obj.Q;
            else
                dM = decomposition(obj.M);
                A2 = obj.A / dM;
                Q2 = dM' \ obj.Q / dM;
                Q2 = 1 / 2 * (Q2 + Q2');
            end

            %% compute the matrix exponential
            n = size(obj.X0, 2);
            exp_hH = expm(obj.h*full([-A2, obj.S; Q2, A2']));
            obj.exp_hH11T = exp_hH(1:n, 1:n)';
            obj.exp_hH12T = exp_hH(1:n, n+1:end)';
            obj.exp_hH21T = exp_hH(n+1:end, 1:n)';
            obj.exp_hH22T = exp_hH(n+1:end, n+1:end)';

            %% check if matrix exponential is too large
            nrm1exphH = norm(exp_hH, 1);
            %fprintf('%s - %s, h = %.2e, ||expm(h*H)||_1 = %.2e.\n', datestr(now), upper(class(obj)), obj.h, nrm1exphH);
            if obj.options.warning_bound_expmH <= nrm1exphH && nrm1exphH < obj.options.error_bound_expmH
                warning('%s:1-norm of matrix exponential is large %.2e.', upper(class(obj)), nrm1exphH);
            end
            if obj.options.error_bound_expmH <= nrm1exphH || isinf(nrm1exphH) || isnan(nrm1exphH)
                error('%s:1-norm of matrix exponential is too large %.2e, use smaller step sizes.', upper(class(obj)), nrm1exphH);
            end
            obj.nrm1_exp_hH = nrm1exphH;
        end

        function m_time_step(obj)
            %M_TIME_STEP  Time steppping function for solver.
            %
            %   MOD.M_TIME_STEP() performes one time step using the
            %   modified Davison Maki algorithm.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr

            %% perform time step
            if obj.options.use_mex
                 obj.Xk = MDM_SYM_DRE_STEP(obj.exp_hH11T, obj.exp_hH12T, obj.exp_hH21T, obj.exp_hH22T, obj.Xk, obj.arg_check, obj.optimal_parameters{:});
            else
                UT = obj.exp_hH11T + obj.Xk*obj.exp_hH12T;
                VT = obj.exp_hH21T + obj.Xk*obj.exp_hH22T;
                obj.Xk = linsolve(UT, VT);
                obj.Xk = 1 / 2 * (obj.Xk + obj.Xk');
            end

            %% update time
            obj.t = obj.t + obj.h;
        end

        function Xt = m_get_solution(obj)
            %M_GET_SOLUTION  Return the current approximation as a matrix Xt.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr
            Xt = obj.Xk;
        end
    end
end
