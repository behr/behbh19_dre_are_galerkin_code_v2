classdef MDM_FAC_SYM_DRE < Solver_DRE
    %MDM_FAC_SYM_DRE    Implements modified Davison-Maki method for autonomous
    %   factorized symmetric differential Riccati equations:
    %
    %   M' \dot{X} M = A' X M + M X A - M' X B B' X M + C' C, X(0) = Z0 Z0' (DRE)
    %
    %  The class solves the DRE using the modified Davison Maki method.
    %
    %   MDM_FAC_SYM_DRE methods:
    %       MDM_FAC_SYM_DRE   - Constructor.
    %
    %   MDM_FAC_SYM_DRE properties:
    %       M           - set private, double, dense, n-x-n matrix M of DRE or empty for identity
    %       A           - set private, double, dense, n-x-n matrix A of DRE
    %       B           - set private, double, dense, symmetric, n-x-b matrix B of DRE
    %       C           - set private, double, dense, symmetric, c-x-n matrix C of DRE
    %       Z0          - set private, double, dense, symmetric, n-x-z0 matrix Z0 of DRE
    %       exp_hH11    - set private, submatrix (1,1) of matrix exponential
    %       exp_hH12    - set private, submatrix (1,2) of matrix exponential
    %       exp_hH21    - set private, submatrix (2,1) of matrix exponential
    %       exp_hH22    - set private, submatrix (2,2) of matrix exponential
    %       nrm1_exp_hH - set private, the 1-norm of the matrix exponential of h*H
    %       Xk          - set private, current approximation for X(t)
    %       optimal_parameters - set private, optimal paramters for MDM_SYM_DRE_STEP
    %
    %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % set private, double, dense, n-x-n matrix M of DRE or empty for identity
        M
        % set private, double, dense, n-x-n matrix A of DRE
        A
        % set private, double, dense, symmetric, n-x-b matrix B of DRE
        B
        % set private, double, dense, symmetric, c-x-n matrix C of DRE
        C
        % set private, double, dense, symmetric, n-x-z0 matrix Z0 of DRE
        Z0
        % set private, submatrix (1,1) of matrix exponential
        exp_hH11T
        % set private, submatrix (1,2) of matrix exponential
        exp_hH12T
        % set private, submatrix (2,1) of matrix exponential
        exp_hH21T
        % set private, submatrix (2,2) of matrix exponential
        exp_hH22T
        % set private, the 1-norm of the matrix exponential of h*H
        nrm1_exp_hH
        % set private, current approximation for X(t)
        Xk
        % set private, optimal paramters for MDM_SYM_DRE_STEP
        optimal_parameters;
    end

    methods(Access = public)
        function obj = MDM_FAC_SYM_DRE(A, M, B, C, Z0, opt, h, T)
            %MDM_FAC_SYM_DRE  Constructor of class.
            %
            %   MOD = MDM_FAC_SYM_DRE(A, M, B, C, Z0, opt)
            %   creates an instance of the class. OPT must be an instance
            %   of MDM_DRE_options class.
            %   The matrices A, M, B, C, Z0 must be dense, double
            %   and of suiteable size. If M is empty then the identity
            %   matrix is used. M must be nonsingular.
            %   H is the step size and T the final time.
            %
            %   See also SOLVER_DRE and MDM_DRE.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(A, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square'}, mfilename, inputname(1));
            n = size(A, 1);
            validateattributes(B, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [n, NaN]}, mfilename, inputname(3));
            validateattributes(C, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [NaN, n]}, mfilename, inputname(4));
            validateattributes(Z0, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [n, NaN]}, mfilename, inputname(5));
            assert(size(B, 1) >= size(B, 2), '%s has oversized number of columns', inputname(3));
            assert(size(C, 1) <= size(C, 2), '%s has oversized number of rows', inputname(4));
            assert(size(Z0, 1) >= size(Z0, 2), '%s has oversized number of columns', inputname(5));
            validateattributes(opt, {'MDM_DRE_options'}, {}, mfilename, inputname(6));

            if ~isempty(M)
                validateattributes(M, {'double'}, {'real', '2d', 'square', 'nonsparse', 'finite', 'nonnan', 'square', 'size', [n, n]}, mfilename, inputname(2));
            end

            %% check input options
            assert(opt.symmetric, 'symmetric has to be true in options.');

            %% set values to attributes
            obj.options = copy(opt);
            obj.M = M;
            obj.A = A;
            obj.B = B;
            obj.C = C;
            obj.Z0 = Z0;
            obj.Xk = Z0*Z0';

            %% set integration parameters
            obj.t = 0;
            obj.h = h;
            obj.T = T;
            obj.validate_step_size();

            %% load parameters file for MDM_SYM_DRE_STEP MEX function
            if obj.options.use_mex
                if ~(exist('parameters_MDM_SYM_DRE_STEP.mtx','file') == 2)
                    warning('parameter file: parameters_MDM_SYM_DRE_STEP.mtx does not exist.');
                    warning('Execute the script parameters_MDM_SYM_DRE_STEP.m first');
                    obj.optimal_parameters = {};
                else
                    parameters = mmread('parameters_MDM_SYM_DRE_STEP.mtx');
                    [~,idx] = min(abs(parameters(:,1)-n));
                    obj.optimal_parameters = num2cell(parameters(idx,2:7));
                end
            end

        end
    end

    methods(Access = protected)
        function m_prepare(obj)
            %M_PREPARE  Prepare the solver for iteration.
            %
            %   MOD.M_PREPARE() computes the matrix exponential h*H and
            %   its 1-norm if the 1-norm is too large a warning is printed
            %   or an error message it returned.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr

            %% transform
            if isempty(obj.M)
                A2 = obj.A;
                C2 = obj.C;
            else
                dM = decomposition(obj.M);
                A2 = obj.A / dM;
                C2 = obj.C / dM;
            end

            %% compute the matrix exponential
            n = size(obj.A, 2);
            exp_hH = expm(obj.h*full([-A2, obj.B*obj.B'; C2'*C2, A2']));
            obj.exp_hH11T = exp_hH(1:n, 1:n)';
            obj.exp_hH12T = exp_hH(1:n, n+1:end)';
            obj.exp_hH21T = exp_hH(n+1:end, 1:n)';
            obj.exp_hH22T = exp_hH(n+1:end, n+1:end)';

            %% check if matrix exponential is too large
            nrm1exphH = norm(exp_hH, 1);
            %fprintf('%s - %s, h = %.2e, ||expm(h*H)||_1 = %.2e.\n', datestr(now), upper(class(obj)), obj.h, nrm1exphH);
            if obj.options.warning_bound_expmH <= nrm1exphH && nrm1exphH < obj.options.error_bound_expmH
                warning('%s:1-norm of matrix exponential is large %.2e.', upper(class(obj)), nrm1exphH);
            end
            if obj.options.error_bound_expmH <= nrm1exphH || isinf(nrm1exphH) || isnan(nrm1exphH)
                error('%s:1-norm of matrix exponential is too large %.2e, use smaller step sizes.', upper(class(obj)), nrm1exphH);
            end
            obj.nrm1_exp_hH = nrm1exphH;
        end

        function m_time_step(obj)
            %M_TIME_STEP  Time steppping function for solver.
            %
            %   MOD.M_TIME_STEP() performes one time step using the
            %   modified Davison Maki algorithm.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr

            %% perform time step
            if obj.options.use_mex
                obj.Xk = MDM_SYM_DRE_STEP(obj.exp_hH11T, obj.exp_hH12T, obj.exp_hH21T, obj.exp_hH22T, obj.Xk, obj.optimal_parameters{:});
            else
                UT = obj.exp_hH11T + obj.Xk*obj.exp_hH12T;
                VT = obj.exp_hH21T + obj.Xk*obj.exp_hH22T;
                obj.Xk = linsolve(UT, VT);
                obj.Xk = 1 / 2 * (obj.Xk + obj.Xk');
            end

            %% update time
            obj.t = obj.t + obj.h;
        end

        function Xt = m_get_solution(obj)
            %M_GET_SOLUTION  Return the current approximation as a matrix Xt.
            %
            %   See also SOLVER_DRE and MDM_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr
            Xt = obj.Xk;
        end
    end
end
