function Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21,T, exp_hH22T, Xk, ARG_CHECK, BS, TRF, MM, LULOC, LSOLVE, USOLVE, LDUV2P) %#ok<*INUSD>
%MDM_SYM_DRE_STEP    Interface to MDM_SYM_DRE_STEP routine.
%
%  # Purpose:
%
%  A more efficient implementation of a time step for modified Davison-Maki
%  method for symmetric real differential Riccti equations.
%  The implementation is designed for matrices of medium size.
%  For tiny matrics (n <= 36) the implementation may performed
%  bad. For larger matrices (n >= 1024) the implementation
%  is also not optimal.
%
%  # Description:
%
%  The real symmetric differential Riccati equation is given by
%
%      \dot{X} = A' X + X A - X S X + Q, X(0) = X0.
%
%  The matrix A, S, Q, X0 must be double, dense, square and of same size.
%  Moreover it is assumed the S, Q and X0 are symmetric.
%
%  The modified Davison-Maki method for the real symmetric differential
%  Riccati equation is given by the following iteration:
%
%  1. Approximate matrix exponential of the Hamiltonian for given step size h.
%  Divide the matrix exponential into 4 n-x-n blocks and transpose them.
%
%      ( exp_hH11  exp_hH12 )         ( -h A  h S  )
%      (                    )  = expm (            )
%      ( exp_hH21  exp_hH22 )         (  h Q  h A' )
%
%      exp_hH11T = exp_hH11'
%      exp_hH12T = exp_hH12'
%      exp_hH21T = exp_hH21'
%      exp_hH22T = exp_hH22'
%
%  2. Time Stepping until desired time:
%
%      Xk = X0
%      t=0
%
%      while t + h < T
%          UT = exp_hH11T + Xk * exp_hH12T    (i)
%          VT = exp_hH21T + Xk * exp_hH22T    (ii)
%          Xk = UT \ VT                       (iii)
%          Xk = 1 / 2 * (Xk + Xk')            (iv)
%          // update time
%          t = t + h
%      end
%
%  The time step operations (i)-(iv) are performed by MDM_SYM_DRE_STEP.
%  MDM_SYM_DRE_STEP can be called from MATLAB via:
%
%  # Calling Sequences:
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk)
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, ARG_CHECK)
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, ARG_CHECK,
%  BS)
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, ARG_CHECK,
%  BS, TRF)
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, ARG_CHECK,
%  BS, TRF, MM)
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, ARG_CHECK,
%  BS, TRF, MM, LULOC)
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, ARG_CHECK,
%  BS, TRF, MM, LULOC, LBRS)
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, ARG_CHECK,
%  BS, TRF, MM, LULOC, LBRS, UBCS)
%
%  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk, ARG_CHECK,
%  BS, TRF, MM, LULOC, LSOLVE, USOLVE, LDUV2P)
%
%  The matrices exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T and Xk
%  must be double, dense and of size n-by-n.
%  Xk must be symmetric.
%  MDM_SYM_DRE_STEP performs only some input argument checks.
%
%  Deactivate input argument check ARG_CHECK:
%  0: No input argument checks are performed.
%  nonzero: Input argument checks are performed.
%  default: 0.
%
%  Tuning parameters:
%
%    The blocksize BS.
%    BS is the blocksize >= 0.
%    If BS is 0 then an unblocked variant is used.
%    64, 128, or 256 are good values depending on your machine.
%    default:
%      If n <= 128, then 0.
%      If 128 < n <= 256; then 64.
%      If 256 < n; then 128.
%
%    The LU factorization TRF.
%    1: dgetrf
%    2: dgetrf2
%    3: ReLAPACK_dgetrf with crossover size 24
%    4: ReLAPACK_dgetrf with crossover size 48
%    5: dgetf2
%    6: Crout LU (Variant from LAPACK)
%    7: Left Looking LU (Variant from LAPACK)
%    8: Toledos recursive LU (Variant from LAPACK)
%    TRF must be either 1, 2, 3, 4, 5, 6, 7 or 8.
%    8 is usually a bad choice.
%    1 is the better choice for larger matrices.
%    3 or 4 is usually the better choice for small matrices.
%    5 is good for tiny matrices.
%    default:
%      If n <= 32, then 5.
%      If 32  < n <= 128, then 7.
%      If 128 < n <= 256, then 3.
%      If 256 < n, then 1.
%
%    The multiplication method MM for matrix matrix products.
%    1: dsymm
%    2: dgemm
%    3: dgemm with transpose flag for the first matrix
%    MM must be either 1, 2 or 3.
%    default: 3.
%
%    The data locality option LUCPY.
%    0: No additional copy is performed.
%    1: The diagonal blocks of the LU factorization are copied
%    2: The row blocks of the LU factorization are copied.
%    3: The row blocks of the LU factorization are transposed copied.
%    A good choice is to do no additional copies, therefore choose 0.
%    default: 0
%
%    Variant for solving with L option LSOLVE.
%    0: Solve with L block by block in row-wise fashion.
%    1: Solve with L block by block in column-wise fashion.
%    2: Solve with L for row blocks.
%    3: Solve with L for column blocks.
%    default:
%      If n <= 128, then 0.
%      If 128 < n <= 256, then 3.
%      If 256 < n <= 512, then 2.
%      If 512 < n, then 1.
%
%    Variant for solving with U option USOLVE.
%    0: Solve with U block by block in row-wise fashion.
%    1: Solve with U block by block in column-wise fashion.
%    2: Solve with U for row blocks.
%    3: Solve with U for column blocks.
%    default:
%      If n <= 128, then 0.
%      If 128 < n <= 256, then 3.
%      If 256 < n <= 512, then 2.
%      If 512 < n, then 1.
%
%    Leading dimension for the U and V factorization LDUV2P.
%    LDUV2P must be an scalar greater equals -1.
%    -1: The leading dimension is chosen to be the next multiple of the
%    blocksize.
%    Otherwise the leading dimension of U and V is chosen
%    to be the next multiple of 2^(LDUV2P).
%    Values larger than 8 may reduce the performance drastically.
%    Chose simply -1 this leads usually to a good performance.
%    default: -1
%
%    For more documentation see the source files in MEXTOOLS directory.
%
%    The optimal tuning parameters for your machine can be determined
%    experimentally using the paramters_MDM_SYM_DRE_STEP script.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
help MDM_SYM_DRE_STEP
error ('MDM_SYM_DRE_STEP mexFunction not found.');
end
