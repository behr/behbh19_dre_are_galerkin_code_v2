classdef MDM_DRE_options < copy_handle
    %MDM_DRE_OPTIONS    Options class for MDM_DRE solver.
    %
    %   MDM_DRE_OPTIONS properties:
    %       symmetric   - set private, logical, symmetric solution, default: false
    %       use_mex     - set private, logical, use the MDM_SYM_DRE_STEP MEX function, default: true
    %
    %   See also SOLVER_DRE_OPTIONS and MDM_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = public)
        % set private, logical, symmetric equation or not, default: false
        symmetric
        % set private, logical, use the MDM_SYM_DRE_STEP MEX function, default: true
        use_mex
    end

    properties(SetAccess = immutable)
        % set immutable, warning if 1-norm of matrix exponential gets too large
        warning_bound_expmH = 1e+8;
        % set immutable, error if 1-norm of matrix exponential gets too large
        error_bound_expmH = 1e+10;
    end

    methods(Access = public)
        function obj = MDM_DRE_options(symmetric, use_mex)
            %MDM_DRE_OPTIONS  Constructor of class.
            %
            %   MOD_OPT = MDM_DRE_OPTIONS(SYMMETRIC, USE_MEX)
            %   Creates an instance of the class.
            %   SYMMETRIC is a logical.
            %
            %   MOD_OPT = MDM_DRE_OPTIONS(SYMMETRIC, USE_MEX)
            %   MOD_OPT = MDM_DRE_OPTIONS(SYMMETRIC)
            %
            %   See also SOLVER_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr
            obj.symmetric = false;
            obj.use_mex = true;
            if nargin >= 1
                obj.symmetric = symmetric;
            end

            if nargin >= 2
                obj.use_mex = use_mex;
            end
        end
    end

    methods
        % setter methods
        function set.symmetric(obj, in_symmetric)
            validateattributes(in_symmetric, {'logical'}, {}, mfilename);
            obj.symmetric = in_symmetric;
        end

        function set.use_mex(obj, in_use_mex)
            validateattributes(in_use_mex, {'logical'}, {}, mfilename);
            obj.use_mex = in_use_mex;
        end
    end
end
