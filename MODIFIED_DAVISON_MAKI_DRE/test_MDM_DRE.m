classdef test_MDM_DRE < matlab.unittest.TestCase
    %TEST_MDM_DRE     Test for MDM_DRE, MDM_FAC_SYM_DRE and MDM_SYM_DRE.
    %
    %   TEST_MDM_DRE tests the solvers with the results from the ode15s.
    %
    %   TEST_MDM_DRE properties:
    %       problem_MDM_DRE         - Problem set for MDM_DRE solver.
    %       problem_MDM_SYM_DRE     - Problem set for MDM_SYM_DRE solver.
    %       problem_MDM_FAC_SYM_DRE - Problem set for MDM_FAC_SYM_DRE solver.
    %
    %   TEST_MDM_DRE methods:
    %       test_problem_MDM_DRE            - Test MDM_DRE using problems from problem_MDM_DRE.
    %       test_problem_MDM_SYM_DRE        - Test MDM_SYM_DRE using problems from problem_MDM_SYM_DRE.
    %       test_problem_MDM_FAC_SYM_DRE    - Test MDM_FAC_SYM_DRE using problems from problem_MDM_FAC_SYM_DRE.
    %
    %   See also MDM_DRE, MDM_FAC_SYM_DRE and MDM_SYM_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        problem_MDM_DRE = { ...
            struct('randseed', 1, 'n', 5, 'm', 4, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8), ...
            struct('randseed', 1, 'n', 4, 'm', 5, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8), ...
            struct('randseed', 1, 'n', 1, 'm', 3, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8), ...
            struct('randseed', 1, 'n', 3, 'm', 1, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8), ...
            };

        problem_MDM_SYM_DRE = { ...
            struct('randseed', 1, 'n', 5, 'mass', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', false), ...
            struct('randseed', 1, 'n', 5, 'mass', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', true), ...
            struct('randseed', 1, 'n', 4, 'mass', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', false), ...
            struct('randseed', 1, 'n', 4, 'mass', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', true), ...
            struct('randseed', 1, 'n', 1, 'mass', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', false), ...
            struct('randseed', 1, 'n', 1, 'mass', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', true), ...
            struct('randseed', 1, 'n', 3, 'mass', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', false), ...
            struct('randseed', 1, 'n', 3, 'mass', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', true), ...
            };

        problem_MDM_FAC_SYM_DRE = { ...
            struct('randseed', 1, 'n', 5, 'b', 2, 'c', 3, 'z0', 2, 'mass', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', false), ...
            struct('randseed', 1, 'n', 5, 'b', 2, 'c', 3, 'z0', 2, 'mass', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', true), ...
            struct('randseed', 1, 'n', 4, 'b', 2, 'c', 3, 'z0', 2, 'mass', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', false), ...
            struct('randseed', 1, 'n', 4, 'b', 2, 'c', 3, 'z0', 2, 'mass', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', true), ...
            struct('randseed', 1, 'n', 6, 'b', 3, 'c', 1, 'z0', 1, 'mass', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', false), ...
            struct('randseed', 1, 'n', 6, 'b', 3, 'c', 1, 'z0', 1, 'mass', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', true), ...
            struct('randseed', 1, 'n', 3, 'b', 1, 'c', 1, 'z0', 1, 'mass', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', false), ...
            struct('randseed', 1, 'n', 3, 'b', 1, 'c', 1, 'z0', 1, 'mass', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8, 'use_mex', true), ...
            };
    end

    properties
       verbose = ~isCIrun();
    end

    methods(Test)
        function test_problem_MDM_DRE(obj, problem_MDM_DRE) %#ok<*INUSL>

            %% load data
            if obj.verbose
                fprintf('\n');
            end
            randseed = problem_MDM_DRE.randseed;
            n = problem_MDM_DRE.n;
            m = problem_MDM_DRE.m;
            T = problem_MDM_DRE.T;
            h = problem_MDM_DRE.h;
            abs_tol = problem_MDM_DRE.abs_tol;
            rel_tol = problem_MDM_DRE.rel_tol;

            %% create data
            rng(randseed);
            M11 = rand(n, n);
            M12 = rand(n, m);
            M21 = rand(m, n);
            M22 = rand(m, m);
            M0 = rand(m, n);

            %% create solver
            opt = MDM_DRE_options(false);
            mdm = MDM_DRE(M11, M12, M21, M22, M0, opt, h, T);
            mdm.prepare();

            %% solve with ode15s
            ode_opt = odeset('AbsTol', 1e-13, 'RelTol', 1e-13);
            sol_ode15s = ode15s(@(t, X) vec(M22*invvec(X, m, n)-invvec(X, m, n)*M11-invvec(X, m, n)*M12*invvec(X, m, n)+M21), [0, T], vec(M0), ode_opt);

            %% solve with MDM_DRE and compare solutions
            while mdm.t < T

                %% compare solutions
                mdm.time_step();
                X_mdm = mdm.get_solution();
                X_ode15s = invvec(deval(sol_ode15s, mdm.t), m, n);
                abs_err = norm(X_mdm-X_ode15s);
                rel_err = abs_err / norm(X_ode15s);

                if obj.verbose
                    f1 = fprintf('t = %.2e, abs./rel. 2-norm error = %.2e/%.2e\n', mdm.t, abs_err, rel_err);
                end

                %% check error
                obj.fatalAssertTrue(abs_err < abs_tol);
                obj.fatalAssertTrue(rel_err < rel_tol);
            end

            if obj.verbose
                fprintf('%s\n', repmat('-', 1, f1 - 1));
            end
        end

        function test_problem_MDM_SYM_DRE(obj, problem_MDM_SYM_DRE) %#ok<*INUSL>

            %% load data
            if obj.verbose
                fprintf('\n');
            end
            randseed = problem_MDM_SYM_DRE.randseed;
            n = problem_MDM_SYM_DRE.n;
            mass = problem_MDM_SYM_DRE.mass;
            T = problem_MDM_SYM_DRE.T;
            h = problem_MDM_SYM_DRE.h;
            abs_tol = problem_MDM_SYM_DRE.abs_tol;
            rel_tol = problem_MDM_SYM_DRE.rel_tol;
            use_mex = problem_MDM_SYM_DRE.use_mex;

            %% create data
            rng(randseed);
            A = rand(n, n);
            if mass
                M = rand(n, n);
            else
                M = [];
            end
            S = rand(n, n);
            Q = rand(n, n);
            X0 = rand(n, n);
            S = 1 / 2 * (S + S');
            Q = 1 / 2 * (Q + Q');
            X0 = 1 / 2 * (X0 + X0');

            %% create solver
            opt = MDM_DRE_options(true, use_mex);
            mdm = MDM_SYM_DRE(A, M, S, Q, X0, opt, h, T);
            mdm.prepare();

            %% solve with ode15s
            ode_opt = odeset('AbsTol', 1e-13, 'RelTol', 1e-13);
            if mass
                sol_ode15s = ode15s(@(t, X) vec(M' \ A'*invvec(X, n, n)+invvec(X, n, n)*A/M-invvec(X, n, n)*S*invvec(X, n, n)+M' \ Q/M), [0, T], vec(X0), ode_opt);
            else
                sol_ode15s = ode15s(@(t, X) vec(A'*invvec(X, n, n)+invvec(X, n, n)*A-invvec(X, n, n)*S*invvec(X, n, n)+Q), [0, T], vec(X0), ode_opt);
            end

            %% solve with MDM_DRE and compare solutions
            while mdm.t < T

                %% compare solutions
                mdm.time_step();
                X_mdm = mdm.get_solution();
                X_ode15s = invvec(deval(sol_ode15s, mdm.t), n, n);
                abs_err = norm(X_mdm-X_ode15s);
                rel_err = abs_err / norm(X_ode15s);
                if obj.verbose
                    f1 = fprintf('t = %.2e, abs./rel. 2-norm error = %.2e/%.2e\n', mdm.t, abs_err, rel_err);
                end

                %% check error
                obj.fatalAssertTrue(abs_err < abs_tol);
                obj.fatalAssertTrue(rel_err < rel_tol);
            end

            if obj.verbose
                fprintf('%s\n', repmat('-', 1, f1 - 1));
            end

        end

        function test_problem_MDM_FAC_SYM_DRE(obj, problem_MDM_FAC_SYM_DRE) %#ok<*INUSL>

            %% load data
            if obj.verbose
                fprintf('\n');
            end
            randseed = problem_MDM_FAC_SYM_DRE.randseed;
            n = problem_MDM_FAC_SYM_DRE.n;
            b = problem_MDM_FAC_SYM_DRE.b;
            c = problem_MDM_FAC_SYM_DRE.c;
            z0 = problem_MDM_FAC_SYM_DRE.z0;
            mass = problem_MDM_FAC_SYM_DRE.mass;
            T = problem_MDM_FAC_SYM_DRE.T;
            h = problem_MDM_FAC_SYM_DRE.h;
            abs_tol = problem_MDM_FAC_SYM_DRE.abs_tol;
            rel_tol = problem_MDM_FAC_SYM_DRE.rel_tol;
            use_mex = problem_MDM_FAC_SYM_DRE.use_mex;

            %% create data
            rng(randseed);
            A = rand(n, n);
            if mass
                M = rand(n, n);
            else
                M = [];
            end
            B = rand(n, b);
            C = rand(c, n);
            Z0 = rand(n, z0);

            %% create solver
            opt = MDM_DRE_options(true, use_mex);
            mdm = MDM_FAC_SYM_DRE(A, M, B, C, Z0, opt, h, T);
            mdm.prepare();

            %% solve with ode15s
            ode_opt = odeset('AbsTol', 1e-13, 'RelTol', 1e-13);
            if mass
                dM = decomposition(M);
                sol_ode15s = ode15s(@(t, X) vec(dM' \ A'*invvec(X, n, n)+invvec(X, n, n)*A/dM-(invvec(X, n, n) * B)*(B' * invvec(X, n, n))+dM' \ C'*C/dM), [0, T], vec(Z0*Z0'), ode_opt);
            else
                sol_ode15s = ode15s(@(t, X) vec(A'*invvec(X, n, n)+invvec(X, n, n)*A-(invvec(X, n, n) * B)*(B' * invvec(X, n, n))+C'*C), [0, T], vec(Z0*Z0'), ode_opt);
            end

            %% solve with MDM_DRE and compare solutions
            while mdm.t < T

                %% compare solutions
                mdm.time_step();
                X_mdm = mdm.get_solution();
                X_ode15s = invvec(deval(sol_ode15s, mdm.t), n, n);
                abs_err = norm(X_mdm-X_ode15s);
                rel_err = abs_err / norm(X_ode15s);
                if obj.verbose
                    f1 = fprintf('t = %.2e, abs./rel. 2-norm error = %.2e/%.2e\n', mdm.t, abs_err, rel_err);
                end

                %% check error
                obj.fatalAssertTrue(abs_err < abs_tol);
                obj.fatalAssertTrue(rel_err < rel_tol);
            end

            if obj.verbose
                fprintf('%s\n', repmat('-', 1, f1 - 1));
            end
        end
    end
end
