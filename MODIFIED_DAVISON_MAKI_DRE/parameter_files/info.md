# Parameter Files

The files contains the experimentally determined optimal tuning parameters for `MDM_SYM_DRE_STEP`.

The machines:
 * `mechtild` with `Intel Xeon Silver 4110 (Skylake)`, `MKL_ENABLE_INSTRUCTIONS=AVX2` and 16 cores
 * `mechtild` with `Intel Xeon Gold 6130 (Skylake)` and 32 cores
