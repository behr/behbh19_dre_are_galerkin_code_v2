---
title: Invariant Galerkin Ansatz Spaces and Davison Maki Methods for the Numerical Solution of Differential Riccati Equations
authors: Maximilian Behr, Peter Benner, Jan Heiland
arXiv: https://arxiv.org/abs/1910.13362
Zenodo: https://doi.org/10.5281/zenodo.3763367
Journal: Elsevier: Applied Mathematics and Computation
https://www.journals.elsevier.com/applied-mathematics-and-computation
---


[CMESS]: https://gitlab.mpi-magdeburg.mpg.de/mess/cmess-releases
[MATLAB]: https://de.mathworks.com/
[CSCBIB]: https://gitlab.mpi-magdeburg.mpg.de/csc-administration/csc-bibfiles
[GPLV2]: https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
[BGPLV2]: https://img.shields.io/badge/License-GPL%20v2-blue.svg
[GPLV3]: https://www.gnu.org/licenses/gpl-3.0
[BGPLV3]: https://img.shields.io/badge/License-GPL%20v3-blue.svg
[GITLAB_PREPRINT_CODE]: https://gitlab.mpi-magdeburg.mpg.de/behr/behbh19_dre_are_galerkin_code
[ZENODO_PAPER_BADGE]: https://zenodo.org/badge/doi/10.5281/zenodo.3763367.svg
[ZENODO_PAPER_URL]: https://doi.org/10.5281/zenodo.3763367
[ZENODO2_PAPER_BADGE]: https://zenodo.org/badge/doi/10.5281/zenodo.4452411.svg
[ZENODO2_PAPER_URL]: https://doi.org/10.5281/zenodo.4452411
[GITLAB_PAPER_CODE]: https://gitlab.mpi-magdeburg.mpg.de/behr/behbh19_dre_are_galerkin_code_v2
[DRESPLIT]: http://www.tonystillfjord.net
[MPIMD]: https://www.mpi-magdeburg.mpg.de
[JOURNAL]: https://www.journals.elsevier.com/applied-mathematics-and-computation
[ARXIV]: https://arxiv.org/abs/1910.13362


# **The latest version of the Code is here [![ZENODO2_PAPER_BADGE]][ZENODO2_PAPER_URL]**


# BehBH19_DRE_ARE_GALERKIN_CODE_V2
`Matlab` Code for the paper.


**License:**

[![License: GPL v2][BGPLV2]][GPLV2] [![License: GPL v3][BGPLV3]][GPLV3]

**Copyright 2019-2020 by Maximilian Behr (MPI Magdeburg)**

[![ZENODO_PAPER_BADGE]][ZENODO_PAPER_URL]

The [MATLAB][MATLAB] Code is licensed under `GPL v2` or later.
See [LICENSE](LICENSE) for details.

# Requirements

[MATLAB][MATLAB] `2018a` or later is supported.

# Description of the Directories and Files

|   Directory / File                    |   Description                                                                                             |
|:-------------------------------------:|:---------------------------------------------------------------------------------------------------------:|
|   /CODE/MATLAB                        |   Codes                                                                                                   |
|   /ABSTACT                            |   Abstract Classes for DRE solver                                                                         |
|   /add_to_path.m                      |   [MATLAB][MATLAB] script to adapth the path variable                                                     |
|   /ARE                                |   RADI Algebraic Riccati Equation solver                                                                  |
|   /ARE_GALERKIN_DRE                   |   Algebraic Riccati Equation approach for DRE                                                             |
|   /clear_results.sh                   |   Bash script, clear results produced by scripts                                                          |
|   /BANDED                             |   Interface to Banded Solvers                                                                             |
|   /COMPARE_REFERENCE_DRE              |   Compare DRE solvers                                                                                     |
|   /data                               |   Test Matrices                                                                                           |
|   /data_reference_solution            |   Place precomputed refrences solutions here                                                              |
|   /DATALOGGER                         |   Class creates Logfiles from numerical experiments                                                       |
|   /EXAMPLES/EXAMPLE_DAVISON_MAKI_FAIL |   Example Pictures for Davison-Maki Method                                                                |
|   /EXAMPLES/EXAMPLE_EIG_DECAY_ARE     |   Example Pictures eigenvalue decay ARE                                                                   |
|   /EXAMPLES/EXAMPLE_EIG_DECAY_DRE     |   Example Pictures eigenvalues decay DRE                                                                  |
|   /EXAMPLES/EXAMPLE_ENTRIES_DECAY_DRE |   Example Pictures decay of coefficients of DRE                                                           |
|   /EXAMPLES/PLOT_NUMERICAL_RANKS      |   Plot numerical ranks of the numerical solution of DRE                                                   |
|   /EXPERIMENTAL                       |   Some experimental code                                                                                  |
|   /HELPERS                            |   Functions for miscallenous tasks                                                                        |
|   /lint_my_code.m                     |   [MATLAB][MATLAB] script to lint the code                                                                |
|   /Makefile                           |   Makefile for MEX files                                                                                  |
|   /MEXTOOLS                           |   C sources of MEX files                                                                                  |
|   /mmread                             |   `mtx` read and write functions                                                                          |
|   /MODIFIED_DAVISON_MAKI_DRE          |   Modified Davison Maki Solvers for DRE                                                                   |
|   /OLD                                |   old scripts for compiling MEX files                                                                     |
|   /QRTOOLS                            |   More fine grained acces to LAPACK QR routines, `DGEQRF` and `DGEQRT`                                    |
|   /REFERENCE_SOLUTION_DRE             |   Create / Read a reference solution of DRE and store it on disk                                          |
|   /run_my_tests.m                     |   [MATLAB][MATLAB] script to run the unit tests                                                           |
|   /SOLUTIONFORMULA_DRE                |   a dense DRE solver based on the analytic solution formla                                                |
|   /SPARSE_DIRECT                      |   interface to `UMFPACK`, `CHOLMOD` and `MA57` sparse direct solver shipped with [MATLAB][MATLAB]         |
|   /SPLITTING2_DRE                     |   partly reimplementation of [DRESPLIT][DRESPLIT] codes from Tony Stillfjord                              |

# Compile the MEX-files
Some functionalities depend on `MEX` files:
 * `BANDED` interface to LAPACK banded and tridiagonal linear solvers
 * `QRTOOLS` more fine grained access to LAPACK `DGEQRF` and `DGEQRT` routines
 * `MDM_SYM_DRE_STEP` more faster time stepping for modified-Davison-Maki method
 * `SPARSE_DIRECT` interface to `UMFPACK`, `CHOLMOD` and `MA57`.

Change to the directory `CODE/MATLAB` and use `make` command.
You have to specify the [MATLAB][MATLAB] root directory using the `MWROOT`
variable.
The `Makefile` should work under `Linux`, `Windows` and `Mac OSX`.
There is no test on a regular basis for the `Makefile` on `Windows` and `Mac OSX`.
You need `MATLAB 2018a` or later and a `C` and `Fortran` compiler.
It is recommended to use `gcc` and `gfortran` version `5.0` or later.

## Hints for `Linux`
The `gcc` and `gfortran` compiler should be available.

```bash
# build command
  make MWROOT=/afs/mpi-magdeburg.mpg.de/usr/ubuntu16.04/MATLAB/R2018a all

# clean command
  make MWROOT=/afs/mpi-magdeburg.mpg.de/usr/ubuntu16.04/MATLAB/R2018a clean
```


## Hints for `Mac OSX`
You have to install `gcc` and `gfortran`. You can use the packagemanager `brew`
for that

```bash
    brew install gcc
```

There seems to be currently a problem, which occurs when linking against
a shared library with nonstandard library name. A simple workaround
is the create a symlink.

```bash
# create a symbolic link
    ln -s /Applications/MATLAB_R2019b.app/bin/maci64/mkl.dylib libmkl.dylib
# build command
    make  MWROOT=/Applications/MATLAB_R2019b.app
```

You may have to modifiy the variable `CC`, `FC` and `LD` in `Makefile` to
`gcc-9`, `gfortran-9` and `gcc-9`, depending which compiler version `brew` installed.

## Hints for `Windows`
On Windows there is only support for `MinGW` `C` and `Fortran` compiler.
In addtion you need on `Windows` the `dlltool` to generate implicit link
libraries from the [MATLAB][MATLAB]  internal `ddl` libraries.

 * Download and install [`MinGW-w64`](https://sourceforge.net/projects/mingw-w64/)
 * Use architecture `x86_64` and threads `posix` in installation dialog.
 * The `C` compiler `gcc`, `Fortran` compiler `gfortran` and the `dlltool` should be installed now.
 * Open a terminal and change to `CODE/MATLAB/` directory and execute the `make` command.
 * You have to specify the [MATLAB][MATLAB] root directory using the `MWROOT` variable.
 * Once building is completed open [MATLAB][MATLAB], call the script `add_to_path`.
 * Create test matrices with `data2mat`.
 * Finally you may want to run all system tests via `run_my_tests`.

```bash
# build command
  mingw32-make MWROOT="C:\Progra~1\MATLAB\R2019b" all

# clean command
  mingw32-make MWROOT="C:\Progra~1\MATLAB\R2019b" clean
```


# Convert `*.mtx` files from `*.mat` files

The `data` directory contains the test matrices as `*.mtx` files. These
files have to be converted to `*.mat` files. You can
simply call the script

```
    data2mat
```
to do this. The results will be written to disk.

# Run [MATLAB][MATLAB] tests

* open [MATLAB][MATLAB]
* change to `CODE/MATLAB` directory
* execute `add_to_path.m` script this add the necessary directories to the [MATLAB][MATLAB] path
* execute `data2mat` script to convert `*.mtx` files  to `*.mat` files
* call `run_my_tests`

# ARE Galerkin based approach

All codes for the galerkin based approached are in `CODE/MATLAB/ARE_GALERKIN_DRE`.

# Create optimal parameter file for `MDM_SYM_DRE_STEP`

The function `MDM_SYM_DRE_STEP` can be optimized by determining
optimal tuning parameters for your machine.
You can adapt and execute the script `run_parameters_MDM_SYM_DRE_STEP`
to generate a optimal set of tuning parameters.
The optimal parameters are writte to a file `parameters_MDM_SYM_DRE_STEP`.
The solver `MDM_SYM_DRE` tries to load the file if exists otherwise
a default tuning parameter configuration is used.
Precomputed parameters files for the silver and gold nodes
of the Mechthild HPC Cluster of the Max Planck Insitute Magdeburg
can be found in `MODIFIED_DAVISON_MAKI_DRE/parameter_files`.

# Generate the reference solutions
The function `compare_reference_dre` needs
a reference solution to perform comparision.
The reference solution can be generated
by executing the SLURM jobfile `run_reference_solution_DRE_gold.job`.
You have to execute the script with input arguments.

```bash
  sbatch run_reference_solution_DRE_gold.job <INSTANCE> <INITIAL_CONDITION>
```

Possible value for `<INSTANCE>` and `<INITIAL_CONDITION>`:

 * `<INSTANCE>`:
    * `"rail_371"`
    * `"rail_1357"`
    * `"rail_5177"`
    * `"conv_diff_3_6400"`
    * `"flow_v0"`
    * `"cookie"`
 * `<INITIAL_CONDITION>`:
    * `0` for `Z0 = zeros(n, 1)`
    * `1` for `Z0 = ones(n, 1)`
    * `2` for `Z0 = transpose(C)`
    * `3` for `Z0 = [ones(n,1),transpose(C)]`
