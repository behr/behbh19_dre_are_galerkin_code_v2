#!/bin/bash
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2019-2020
#

# submit_all DIRECTORY search for all job files recursively in DIRECTOY and submit them.
# source submit_all.sh



submit_all(){
    if [[ $# -eq 0 ]] ; then
        echo 'usage: submit_all DIRECTORY'
    else
        JOBS=$(find $1 -name "*.job")
        for JOB in ${JOBS[@]}; do
            JOBDIR=$(dirname $JOB)
            echo "Submit Job $JOB in $JOBDIR"
            sbatch --workdir=$JOBDIR $JOB
        done
    fi
}

export -f submit_all
