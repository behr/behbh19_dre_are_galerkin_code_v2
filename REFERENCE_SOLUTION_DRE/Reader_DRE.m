classdef Reader_DRE < custom_handle
    %READER_DRE    Class reads a mat binary files that
    %   contain a precompouted solution of a differential Riccati equation
    %   from create_reference_solution_dre function.
    %
    %   READER_DRE methods:
    %       read_reference_solution_dre - Constructor.
    %       get_solution_at             - Return the loaded solution at time t.
    %
    %   READER_DRE properties:
    %       mymat       - set private, string, path to mat file which contains the solution
    %       inmemory    - set private, logical, if true the load command is used to load the reference solution, default false
    %       mymatfile   - set private, matfile object to mymat
    %       store_at    - set private, array of time points where the solution is available
    %       name        - set private, name of the loaded solution
    %
    %   If you have enough memory you can set inmemory true.
    %   The full solution is then loaded into memory and the get_solution_at
    %   method is probably faster. Otherwise only parts of the mat file are
    %   loaded on demand.
    %
    %   See also CREATE_REFERENCE_SOLUTION_DRE and RUN_REFERENCE_SOLUTION_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %

    properties(SetAccess = private)
        % set private, string, path to mat file which contains the solution
        mymat
        % set private, logical, if true the load command is used to load the reference solution, default false
        inmemory
        % set private, matfile object to mymat
        mymatfile
        % set private, array of time points where the solution is available
        store_at
        % set private, name of the loaded solution
        name
    end

    methods(Access = public)
        function obj = Reader_DRE(mymat, inmemory)
            %READER_DRE    Constructor of class.
            %
            %   READER = READER_DRE(MYMAT)
            %   creates an instance of the class. MYMAT must be a string
            %   of the path of the mat file, which contains the reference
            %   solution.
            %   READER = READER_DRE(MYMAT, INMEMORY)
            %   If INMEMORY is true the full reference solution is loaded
            %   into memory. You can use this option if enough main memory
            %   is available on your system. The get_solution_at method
            %   is then faster. If INMEMORY is false, only parts of the
            %   solution are loaded on demand.
            %
            %   See also RUN_REFERENCE_SOLUTION_DRE.
            %
            %   Author: Maximilian Behr

            %% check if file exists
            assert(exist(mymat, 'file') == 2, 'File %s does not exists or is not a mat binary file.', mymat)
            if nargin < 2
                obj.inmemory = false;
            else
                validateattributes(inmemory, {'logical'}, {}, mfilename, inputname(2));
                obj.inmemory = inmemory;
            end


            %% load file and check
            fprintf('%s %s\n', datestr(now), mfilename);
            obj.mymat = mymat;
            if obj.inmemory
                obj.mymatfile = load(mymat);
            else
                obj.mymatfile = matfile(mymat, 'Writable', false);
            end

            %% check the loaded file
            fprintf('%s %s - Check consistency of reference solution and field names.\n', datestr(now), mfilename);
            fields_check = {'name', 'mfilename', 'solver_name', 'datetime', 'store_at', 'T', 'solver_options', 'solver_options_struct', 'gitinfo', 'cpu', 'h', 'L_1', 'D_1'};
            if obj.inmemory
                p = fieldnames(obj.mymatfile);
            else
                p = properties(obj.mymatfile);
            end

            for i = 1:length(fields_check)
                field_check = fields_check{i};
                assert(any(strcmp(p, field_check)), 'Could not read field %s from %s.', field_check, mymat);
            end

            %% get some fields
            obj.store_at = obj.mymatfile.store_at;
            assert(~isempty(obj.store_at), 'store_at seems is empty.');
            obj.name = obj.mymatfile.name;

            %% check consistency
            for i = 1:length(obj.store_at)
                Lvar = sprintf('L_%d', i);
                Dvar = sprintf('D_%d', i);
                assert(any(strcmp(p, Lvar)), 'Could not read field %s from %s.', Lvar, mymat);
                assert(any(strcmp(p, Dvar)), 'Could not read field %s from %s.', Dvar, mymat);
            end

            %% print info about loaded file
            fprintf('%s %s - Information about generated reference solution file %s\n', datestr(now), mfilename, obj.mymat);
            fprintf('\t name                = %s\n', obj.mymatfile.name);
            fprintf('\t solver_name         = %s\n', obj.mymatfile.solver_name);
            fprintf('\t datetime            = %s\n', obj.mymatfile.datetime);
            fprintf('\t T                   = %f\n', obj.mymatfile.T);
            fprintf('\t h                   = %d\n', obj.mymatfile.h);
            if length(obj.store_at) >= 3
                fprintf('\t store_at            = [%f, %f, ..., %f]\n', obj.store_at(1), obj.store_at(2), obj.store_at(end));
            end
            fprintf('\t length(store_at)    = %d\n', length(obj.store_at));
            fprintf('\t hostname            = %s\n', obj.mymatfile.hostname);
            mycpuinfo = obj.mymatfile.cpu;
            fprintf('\t CPU-Name            = %s\n', mycpuinfo.Name);
            fprintf('\t CPU-Clock           = %s\n', mycpuinfo.Clock);
            fprintf('\t CPU-Cache           = %s\n', mycpuinfo.Cache);
            fprintf('\t CPU-NumProcessors   = %d\n', mycpuinfo.NumProcessors);
            fprintf('\t CPU-OSType          = %s\n', mycpuinfo.OSType);
            fprintf('\t CPU-OSVersion       = %s\n', mycpuinfo.OSVersion);

            mygitinfo = obj.mymatfile.gitinfo;
            fprintf('\t Project remote-url  = %s\n', mygitinfo.remote_url);
            fprintf('\t Project git-sha     = %s\n', mygitinfo.sha);
            fprintf('\t Project git-branch  = %s\n', mygitinfo.branch);
            fprintf('\t version             = %s\n', obj.mymatfile.version);
        end

        function [L, D] = get_solution_at(obj, t)
            %GET_SOLUTION_AT    Return the solution at a specific time point.
            %
            %   [L,D] = READER.GET_SOLUTION_AT(T) returns the approximation
            %   L*D*L' of X(T).
            %
            %   See also RUN_REFERENCE_SOLUTION_DRE.
            %
            %   Author: Maximilian Behr

            validateattributes(t, {'double'}, {'real', 'scalar', '>=', 0, 'finite', 'nonnan'}, mfilename, inputname(2));
            assert(ismember(t, obj.store_at), 'Reference Solution not available for t = %.2e', t);
            idx = find(obj.store_at == t);
            assert(length(idx) == 1, 'Cannot find index for t = %.2e', t);
            Lfield = sprintf('L_%d', idx);
            Dfield = sprintf('D_%d', idx);
            fprintf('%s %s - Load %s and %s at time t = %.2e\n', datestr(now), mfilename, Lfield, Dfield, t);
            L = obj.mymatfile.(Lfield);
            D = obj.mymatfile.(Dfield);
        end

        function [A, E, B, C, Z0] = get_system_matrices(obj)
            %GET_SYSTEM_MATRICES    Return the system matrices A, E, B and C
            %   which define the Differential Riccati Equation.
            %
            %   [A, E, B, C, Z0] = READER.GET_SYSTEM_MATRICES()
            %   return the matrices which define the Differential Riccati Equation.
            %
            %
            %   Author: Maximilian Behr
            A = obj.mymatfile.A;
            E = obj.mymatfile.M;
            B = obj.mymatfile.B;
            C = obj.mymatfile.C;
            Z0 = obj.mymatfile.Z0;
        end

    end
end
