function create_reference_solution_DRE(name, A, M, B, C, Z0, split_opt, h, T, store_at)
%CREATE_REFERENCE_SOLUTION_DRE  Computes a reference solution for a
%   differential Riccati equation. The reference solution is written to
%   a mat file and can be later loaded using read_reference_solution.
%   The differential Riccati equation is given by
%
%       M'\dot{X}M = A' X M + M' X A - M' X B B' X M + C' C, X(0) = Z0 Z0'.
%
%   CREATE_REFERENCE_SOLUTION_DRE(NAME, A, M, B, C, Z0, SPLIT_OPT, H, T, STORE_AT)
%   NAME must be a string and should name your instance. If M is empty
%   then the identity matrix is used. SOLVER_OPTIONS must be an
%   instance of Splitting2_DRE_options. H is the step size and T the final
%   time. STORE_AT is a sorted vector, which contains the time points
%   where the solution should be stored.
%
%   See also CREATE_REFERENCE_SOLUTION_DRE, RUN_REFERENCE_SOLUTION_DRE and
%   READ_REFERENCE_SOLUTION_DRE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% some input parameter checks
validateattributes(name, {'char'}, {}, mfilename, inputname(1));
validateattributes(A, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(2));
assert(issparse(A), '%s must be sparse', inputname(2));
n = size(A, 1);
if ~isempty(M)
    validateattributes(M, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan', 'size', [n, n]}, mfilename, inputname(3));
    assert(issparse(M), '%s must be sparse', inputname(3))
end
validateattributes(B, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [n, NaN]}, mfilename, inputname(4));
validateattributes(C, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [NaN, n]}, mfilename, inputname(5));
validateattributes(Z0, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [n, NaN]}, mfilename, inputname(6));
validateattributes(split_opt, {'Splitting2_DRE_options'}, {}, mfilename, inputname(7));
validateattributes(h, {'numeric'}, {'real', '>', 0, 'scalar', 'finite', 'nonnan'}, mfilename, inputname(8));
validateattributes(T, {'numeric'}, {'real', '>', 0, 'scalar', 'finite', 'nonnan'}, mfilename, inputname(9));
assert(mod(T, h) == 0, 'Desired step size h = %.2e must be a multiple of final time T = %.2e\n', h, T);
validateattributes(store_at, {'numeric'}, {'real', '>=', 0, '<=', T, 'vector', 'increasing', 'nonnan', 'finite'}, mfilename, inputname(10));
assert(~any(mod(store_at, h)), 'store_at must only contain multiples of stepsize');

%% create necessary filenames and create directory for results
mybasename = sprintf('%s', name);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mkdir(mydir);

mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mydat = fullfile(mydir, sprintf('%s.dat', mybasename));
mymat = fullfile(mydir, sprintf('%s.mat', mybasename));
mymatfile = matfile(mymat, 'Writable', true);

%% delete old diary and create new one
delete(mydiary);
diary(mydiary);

%% generate and prepare solver for reference solution
fprintf('%s - Prepare %s splitting scheme for reference solution.\n', datestr(now), char(split_opt.splitting));
solver = Splitting2_DRE(A, M, B, C, Z0, split_opt, h, T);
solver.prepare();

%% solve and store solution
for i_store = 1:length(store_at)

    % integrate to desired time with reference solver
    t = store_at(i_store);
    fprintf('%s - Integrate with %s solver, %s, h = %.2e, t = %.4e, T = %.4e, %6.2f %%.\n', datestr(now), char(split_opt.splitting), name, h, t, T, t/T*100);
    solver.time_step_until(t);

    % get solution from refrence solver
    [L, D] = solver.get_solution(); %#ok<ASGLU>

    % store solution
    fprintf('%s - Store solution at t = %.4e as low-rank pair (L_%d,D_%d), size(L) = %d-x-%d.\n', datestr(now), solver.t, i_store, i_store, size(L));
    eval(sprintf('mymatfile.L_%d = L;', i_store));
    eval(sprintf('mymatfile.D_%d = D;', i_store));
end

%% Write to file dat file
fprintf('%s - Start writing data to dat file\n', datestr(now));

dlogger = Datalogger(mydat);
dlogger.add_to_preamble('mfilename', mfilename);
dlogger.add_to_preamble('name', name);
dlogger.add_to_preamble('T', T);
dlogger.add_to_preamble('h', h);
dlogger.add_to_preamble('Splitting', char(split_opt.splitting));
dlogger.add_to_preamble('IRK', char(split_opt.irkmethod));
dlogger.add_to_preamble('Quad', char(split_opt.quadrule));
dlogger.add_to_preamble('time_solver', solver.wtime_time_step_until);

header = 'index,t';
dlogger.set_header(header);

index = 1:length(store_at);
datatable = [index(:), store_at(:)];
dlogger.set_table(datatable);

dlogger.write();
fprintf('%s - Finished writing dat file\n\n', datestr(now));

%% Write to mat file
% data and solver
mymatfile.name = name;
mymatfile.A = A;
mymatfile.B = B;
mymatfile.C = C;
mymatfile.M = M;
mymatfile.Z0 = Z0;
mymatfile.solver_options = split_opt;
mymatfile.solver_options_struct = struct(split_opt);
mymatfile.T = T;
mymatfile.h = h;
mymatfile.solver_name = char(split_opt.splitting);
mymatfile.store_at = store_at;
mymatfile.datetime = datestr(datetime('now'));

% meta info
hostname = getComputerName();
user = getUserName();
timenow = datestr(datetime('now'));
gitinfo = GitInfo();
mycpuinfo = cpuinfo();
[blas, lapack] = getBLASLAPACK();
[ver_umfpack, ver_cholmod, ver_amd, ver_colamd, ver_symamd, ver_metis, ver_suitesparseqr] = getSUITESPARSE();
[mkl_debug_cpu_type, mkl_enable_instructions] = getMKLENVVARS();

[totalmem, usedmem, freemem] = meminfo();
mymatfile.mfilename = mfilename;
mymatfile.hostname = hostname;
mymatfile.cpu = mycpuinfo;
mymatfile.totalmem = totalmem;
mymatfile.usedmem = usedmem;
mymatfile.freemem = freemem;
mymatfile.user = user;
mymatfile.timenow = timenow;
mymatfile.version = version();
mymatfile.blas = blas;
mymatfile.lapack = lapack;
mymatfile.ver_umfpack = ver_umfpack;
mymatfile.ver_cholmod = ver_cholmod;
mymatfile.ver_amd = ver_amd;
mymatfile.ver_colamd = ver_colamd;
mymatfile.ver_symamd = ver_symamd;
mymatfile.ver_metis = ver_metis;
mymatfile.ver_suitesparseqr = ver_suitesparseqr;
mymatfile.mkl_debug_cpu_type = mkl_debug_cpu_type;
mymatfile.mkl_enable_instructions = mkl_enable_instructions;
mymatfile.gitinfo = struct(gitinfo);
mymatfile.maxnumcompthreads = maxNumCompThreads();

%% turn diary off
diary('off');

end
