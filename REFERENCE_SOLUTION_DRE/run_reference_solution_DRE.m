%RUN_REFERENCE_SOLUTION_DRE     Calls create_reference_solution_dre.
%
%   See also READ_REFERENCE_SOLUTION and CREATE_REFERENCE_SOLUTION_DRE.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% problem and parameters
instance = 'rail_5177';
data = load(instance);
A = data.A;
E = data.E;
B = full(data.B);
C = full(data.C);
Z0 = zeros(size(A, 1), 1);
n = size(A, 1);
T = 10;
k = -2;
store_at = 0:1/2:T;

%% splitting solvers
split = Splitting2_t.SPLITTING_SYMMETRIC8;
split_opt = Splitting2_DRE_options(split);
split_opt.decomposition_type = Decomposition_t.DECOMPOSITION_BANDED;

%% create refernce solution
name = sprintf('%s_%s_k%d', instance, char(split), k);
create_reference_solution_DRE(name, A, E, B, C, Z0, split_opt, 2^k, T, store_at);
