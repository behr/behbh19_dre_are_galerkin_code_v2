classdef test_SolutionFormula_DRE < matlab.unittest.TestCase
    %TEST_SOLUTIONFORMULA_DRE     Test for SolutionFormula_DRE.
    %
    %   TEST_SOLUTIONFORMULA_DRE tests the solver with the results from the ode15s.
    %
    %   TEST_SOLUTIONFORMULA_DRE properties:
    %       problem_SolutionFormula_DRE - Problem set for SolutionFormula_DRE solver.
    %
    %   TEST_SOLUTIONFORMULA_DRE methods:
    %       test_problem_SolutionFormula_DRE   - Test SolutionFormula_DRE using problems from problem_SolutionFormula_DRE.
    %
    %   See also SOLUTIONFORMULA_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        % no mass, minus sign
        % no mass, plus sign
        % mass, minus sign
        % mass, plus sign
        problem_SolutionFormula_DRE = { ...
            struct('randseed', 1, 'n', 5, 'b', 2, 'c', 3, 'mass', false, 'mpsign', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8); ...
            struct('randseed', 1, 'n', 4, 'b', 2, 'c', 3, 'mass', false, 'mpsign', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8); ...
            struct('randseed', 1, 'n', 5, 'b', 2, 'c', 3, 'mass', false, 'mpsign', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8); ...
            struct('randseed', 1, 'n', 4, 'b', 2, 'c', 3, 'mass', false, 'mpsign', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8); ...
            struct('randseed', 1, 'n', 5, 'b', 2, 'c', 3, 'mass', true, 'mpsign', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8); ...
            struct('randseed', 1, 'n', 4, 'b', 2, 'c', 3, 'mass', true, 'mpsign', false, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8); ...
            struct('randseed', 1, 'n', 5, 'b', 2, 'c', 3, 'mass', true, 'mpsign', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8); ...
            struct('randseed', 1, 'n', 4, 'b', 2, 'c', 3, 'mass', true, 'mpsign', true, 'T', 1, 'h', 2^(-3), 'abs_tol', 1e-8, 'rel_tol', 1e-8); ...
            };
    end

    properties
        verbose = ~isCIrun();
    end

    methods(Test)
        function test_problem_SolutionFormula_DRE(obj, problem_SolutionFormula_DRE) %#ok<*INUSL>

            %% load data
            if obj.verbose
                fprintf('\n');
            end
            randseed = problem_SolutionFormula_DRE.randseed;
            n = problem_SolutionFormula_DRE.n;
            b = problem_SolutionFormula_DRE.b;
            c = problem_SolutionFormula_DRE.c;
            mass = problem_SolutionFormula_DRE.mass;
            mpsign = problem_SolutionFormula_DRE.mpsign;
            T = problem_SolutionFormula_DRE.T;
            h = problem_SolutionFormula_DRE.h;
            abs_tol = problem_SolutionFormula_DRE.abs_tol;
            rel_tol = problem_SolutionFormula_DRE.rel_tol;

            %% create data
            rng(randseed);
            A = rand(n, n);
            A = A - 2 * n * eye(n, n);
            if mass
                M = diag(1:n) + 1e-1 * rand(n, n);
            else
                M = [];
            end
            B = rand(n, b);
            C = rand(c, n);
            X0 = rand(n, n);
            X0 = X0' * X0;

            if mpsign
                scale = 1;
            else
                scale = -1;
            end

            % cannot find a suitable test case (which does not blow up) if mass matrix and mpsign is true, try X0=0
            if mpsign && mass
                X0 = zeros(size(X0));
            end

            %% create solver
            opt = SolutionFormula_DRE_options(mpsign);
            sol = SolutionFormula_DRE(A, M, B, C, X0, opt, h, T);
            sol.prepare();

            %% solve with ode15s
            ode_opt = odeset('AbsTol', 1e-13, 'RelTol', 1e-13);
            if mass
                dM = decomposition(M);
                sol_ode15s = ode15s(@(t, X) vec(dM' \ A'*invvec(X, n, n)+invvec(X, n, n)*A/dM+scale*(invvec(X, n, n) * B)*(B' * invvec(X, n, n))+dM' \ C'*C/dM), [0, T], vec(X0), ode_opt);
            else
                sol_ode15s = ode15s(@(t, X) vec(A'*invvec(X, n, n)+invvec(X, n, n)*A+scale*(invvec(X, n, n) * B)*(B' * invvec(X, n, n))+C'*C), [0, T], vec(X0), ode_opt);
            end

            %% solve with MDM_DRE and compare solutions
            if obj.verbose
                fprintf('mass = %d, plus = %d\n', mass, mpsign);
            end
            while sol.t < T

                %% compare solutions
                sol.time_step();
                X_mdm = sol.get_solution();
                X_ode15s = invvec(deval(sol_ode15s, sol.t), n, n);
                abs_err = norm(X_mdm-X_ode15s);
                rel_err = abs_err / norm(X_ode15s);
                if obj.verbose
                    f = fprintf('t = %.2e, abs./rel. 2-norm error = %.2e/%.2e\n', sol.t, abs_err, rel_err);
                end

                %% check error
                obj.fatalAssertTrue(abs_err < abs_tol);
                obj.fatalAssertTrue(rel_err < rel_tol);
            end
            if obj.verbose
                fprintf('%s\n', repmat('-', 1, f - 1));
            end
        end
    end
end
