classdef SolutionFormula_DRE_options < copy_handle
    %SOLUTIONFORMULA_DRE_OPTIONS  Options class for solution_formula_dre.
    %
    %   SOLUTIONFORMULA_OPTIONS properties:
    %       mp_sign - set private, logical, if true plus in front of quadratic term, otherwise minus, default: false
    %
    %   See also SOLUTION_FORMULA_DRE_OPTIONS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess =  public)
        % set private, logical, if true plus in front of quadratic term, otherwise minus
        mp_sign
    end

    methods(Access = public)
        function obj = SolutionFormula_DRE_options(mp_sign)
            %SOLUTIONFORMULA_DRE_OPTIONS  Constructor of class.
            %
            %   SOL_OPT = SOLUTIONFORMULA_DRE_OPTIONS(MP_SIGN)
            %   creates an instance of the class. MP_SIGN is a logical
            %
            %   See also SOLUTIONFORMULA_DRE.
            %
            %   Author: Maximilian Behr
            obj.mp_sign = false;
            if nargin >= 1
                obj.mp_sign = mp_sign;
            end
        end
    end

    methods
        % setter methods
        function set.mp_sign(obj, in_mp_sign)
            validateattributes(in_mp_sign, {'logical'}, {}, mfilename);
            obj.mp_sign = in_mp_sign;
        end
    end
end
