classdef SolutionFormula_DRE < Solver_DRE
    %SOLUTIONFORMULA_DRE  Implements a solver based on the solution formula for
    %
    %   dense symmetric differential Riccati equation:
    %
    %   M'\dot{X}M = A' X M + M X A -/+ M' X B*B' X M + C'*C, X(0) = X0    (DRE)
    %
    %   The class implements a solver based on the solution formula.
    %   M must be nonsingular. and X0 must be symmetric.
    %   The theoretical assumption is that the algebraic Riccati equation
    %
    %       0 = A' X M + M X A' -/+ M' X B*B' X M + C'*C
    %
    %   has a unique stabilizing symmetric positive semidefinite solution.
    %
    %   The iteration follows from the explicit solution formula.
    %   See [1, Thm. 4.3] or their references in.
    %
    %   SOLUTIONFORMULA_DRE methods:
    %       solutionformula_dre    - Constructor.
    %       prepare                 - Compute solution of ARE, ALE and matrix exponential.
    %       time_step               - Perform a time step.
    %       get_solution            - Return the approximation at current time.
    %
    %   SOLUTIONFORMULA_DRE properties:
    %       M       - double, dense, n-x-n matrix M of DRE or empty for identity
    %       A       - double, dense, n-x-n matrix A of DRE
    %       B       - double, dense, n-x-b matrix B of DRE
    %       C       - double, dense, c-x-n matrix C of DRE
    %       X0      - double, dense, n-x-n matrix X0 of DRE
    %       Xinf    - stabilizing solution of ARE
    %       XL      - solution of Lyapunov equation
    %       expmCL  - matrix exponential of closed loop matrix
    %       Xk      - current matrix approximation
    %
    %   References:
    %   [1] Maximilian Behr and Peter Benner and Jan Heiland,
    %       Invariant Galerkin Ansatz Spaces and Davison-Maki Methods for
    %       the Numerical Solution of Differential Riccati Equations,
    %       2019, arXiv:1910.13362
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % double, dense, n-x-n matrix M of DRE or empty for identity
        M
        % double, dense, n-x-n matrix A of DRE
        A
        % double, dense, n-x-b matrix B of DRE
        B
        % double, dense, c-x-n matrix C of DRE
        C
        % double, dense, n-x-n matrix X0 of DRE
        X0
        % stabilizing solution of ARE
        Xinf
        % solution of Lyapunov equation
        XL
        % closed loop matrix
        Acl
        % matrix exponential of closed loop matrix
        expmAcl
        % XL - expm(Acl)*XL*expm(Acl)'
        XLmexpmAcl
        % current approximation
        Xk
    end

    methods(Access = public)
        function obj = SolutionFormula_DRE(A, M, B, C, X0, opt, h, T)
            %SOLUTIONFORMULA_DRE  Constructor of class.
            %
            %   SOL = SOLUTIONFORMULA_DRE(M, A, S, Q, X0, OPT, h, T)
            %   creates an instance of the class. OPT must be an instance
            %   of SolutionFormula_DRE_options class.
            %   The matrices M, A, S, Q, X0 must be dense, double
            %   and of square. M must be nonsingular.
            %   S, Q and X0 must be symmetric. If M is empty the identity
            %   is used. The algebraic Riccati equation
            %   H is the step size and T the final time.
            %
            %   See also SOLUTIONFORMULA_DRE_OPTIONS.
            %
            %   References:
            %   [1, Thm. 4.3]
            %       Maximilian Behr and Peter Benner and Jan Heiland,
            %       Invariant Galerkin Ansatz Spaces and Davison-Maki Methods for
            %       the Numerical Solution of Differential Riccati Equations,
            %       2019, arXiv:1910.13362
            %
            %   Author: Maximilian Behr

            %% check input matrices
            validateattributes(A, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square'}, mfilename, inputname(1));
            n = size(A, 1);
            validateattributes(B, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [n, NaN]}, mfilename, inputname(3));
            validateattributes(C, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'size', [NaN, n]}, mfilename, inputname(4));
            assert(size(B, 1) >= size(B, 2), '%s has oversized number of columns', inputname(3));
            assert(size(C, 1) <= size(C, 2), '%s has oversized number of rows', inputname(4));
            validateattributes(X0, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square', 'size', [n, n]}, mfilename, inputname(5));
            assert(issymmetric(X0), '%s must be symmetric', inputname(5));
            validateattributes(opt, {'SolutionFormula_DRE_options'}, {}, mfilename, inputname(6));

            if ~isempty(M)
                validateattributes(M, {'double'}, {'real', '2d', 'nonsparse', 'finite', 'nonnan', 'square', 'size', [n, n]}, mfilename, inputname(2));
            end

            %% set values to attributes
            obj.options = copy(opt);
            obj.M = M;
            obj.A = A;
            obj.B = B;
            obj.C = C;
            obj.X0 = X0;
            obj.Xk = obj.X0;

            %% set integration parameters
            obj.t = 0;
            obj.h = h;
            obj.T = T;
            obj.validate_step_size();
        end
    end

    methods(Access = protected)
        function m_prepare(obj)
            %M_PREPARE  Prepare the solver for iteration.
            %
            %   SOL.M_PREPARE() computes the stabilizing solution of
            %   algebraic Riccati and Lyapunov equation, as well as
            %   the matrix exponential.
            %
            %   See also SOLUTIONFORMULA_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr

            % plus/minus in front of quadratic term
            if obj.options.mp_sign
                scale = 1;
            else
                scale = -1;
            end
            b = size(obj.B, 2);

            %% solve Riccati
            obj.Xinf = care(obj.A, obj.B, obj.C'*obj.C, -scale*eye(b, b), [], obj.M);

            %% solve Lyapunov equation
            if ~isempty(obj.M)
                % maybe this can be done better, without taking inverse
                obj.Acl = obj.A/obj.M + scale*obj.B * (obj.B' * obj.Xinf);
            else
                obj.Acl = obj.A + scale*obj.B * (obj.B' * obj.Xinf);
            end

            RHS = -scale*obj.B * obj.B';
            obj.XL = lyap(obj.Acl, RHS);

            %% compute matrix exponential
            obj.expmAcl = expm(obj.h*obj.Acl);

            %% compute iteration matrix for solution formula
            obj.XLmexpmAcl = obj.XL - obj.expmAcl * obj.XL * obj.expmAcl';
        end

        function m_time_step(obj)
            %M_TIME_STEP  Time steppping function for solver.
            %
            %   SOL.M_TIME_STEP() performes one time step using the solution.
            %
            %   See also SOLUTIONFORMULA_DRE.
            %
            %   Author: Maximilian Behr

            %% time step with solution formula and make solution symmetric
            n = size(obj.Xk, 1);
            temp = obj.Xinf - obj.Xk;
            obj.Xk = obj.Xinf - obj.expmAcl' * (temp / (eye(n, n) - obj.XLmexpmAcl * temp)) * obj.expmAcl;
            obj.Xk = 1 / 2 * (obj.Xk + obj.Xk');

            %% update time
            obj.t = obj.t + obj.h;
        end

        function Xt = m_get_solution(obj)
            %M_GET_SOLUTION  Return the current approximation as a matrix Xt.
            %
            %   See also SOLUTIONFORMULA_DRE.
            %
            %   Author: Maximilian Behr
            Xt = obj.Xk;
        end
    end
end
