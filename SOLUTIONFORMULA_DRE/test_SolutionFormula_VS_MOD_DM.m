classdef test_SolutionFormula_VS_MOD_DM < matlab.unittest.TestCase
    %TEST_SOLUTIONFORMULA_VS_MOD_DM     Test for SolutionFormula_DRE and MOD_FAC_SYM_DRE.
    %
    %   TEST_SOLUTIONFORMULA_VS_MOD_DM compares both solvers using benchmark
    %       examples.
    %
    %   TEST_SOLUTIONFORMULA_VS_MOD_DM properties:
    %       problem - Problem set for comparison.
    %
    %   TEST_SOLUTIONFORMULA_VS_MOD_DM methods:
    %       test   - Compare SolutionFormula_DRE and MOD_FAC_SYM_DRE solver.
    %
    %   See also TEST_SOLUTIONFORMULA_VS_MOD_DM
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        problem = {
            % zero initial conditions
            struct('instance', 'beam',      'T', 2^-2,  'h_mdm', 2^-8,  'h_sol', 2^-4,  'abs_tol', 1e-7,    'rel_tol', 1e-8,    'Z0', '@(n) zeros(n, 1)', 'print_mod', 5) ...
            struct('instance', 'rail_371',  'T', 2^0,   'h_mdm', 2^-4 , 'h_sol', 2^-2,  'abs_tol', 1e+2,    'rel_tol', 1e-7,    'Z0', '@(n) zeros(n, 1)', 'print_mod', 5) ...
            % eye as initial condition, solution gets usually large in norm
            struct('instance', 'beam',      'T', 2^-2,  'h_mdm', 2^-8,  'h_sol', 2^-4,  'abs_tol', 1e-7,    'rel_tol', 1e-8,    'Z0', '@(n) eye(n, 2)', 'print_mod', 5) ...
            struct('instance', 'rail_371',  'T', 2^0,   'h_mdm', 2^-4 , 'h_sol', 2^-2,  'abs_tol', 1e+2,    'rel_tol', 1e-7,    'Z0', '@(n) eye(n, 2)', 'print_mod', 5) ...
            % ones as initial condition
            struct('instance', 'beam',      'T', 2^-2,  'h_mdm', 2^-8,  'h_sol', 2^-4,  'abs_tol', 1e-5,    'rel_tol', 1e-8,    'Z0', '@(n) ones(n, 3)', 'print_mod', 5) ...
            struct('instance', 'rail_371',  'T', 2^0,   'h_mdm', 2^-4 , 'h_sol', 2^-2,  'abs_tol', 1e+2,    'rel_tol', 1e-7,    'Z0', '@(n) ones(n, 3)', 'print_mod', 5) ...
            };
    end

    properties
        verbose = ~isCIrun();
    end

    methods(Test)
        function test_problem_SolutionFormula_DRE(obj, problem) %#ok<*INUSL>

            %% load problem instance
            if obj.verbose
                fprintf('\n');
            end
            instance = problem.instance;
            data = load(instance);
            A = full(data.A);
            B = full(data.B);
            C = full(data.C);
            n = size(A, 1);
            if ~isfield(data,'E')
                M = [];
            else
                M = full(data.E);
            end
            Z0_str = problem.Z0;
            Z0_fh = str2func(Z0_str);
            Z0 = Z0_fh(n);

            %% tolerances
            T = problem.T;
            h_mdm = problem.h_mdm;
            h_sol = problem.h_sol;
            abs_tol = problem.abs_tol;
            rel_tol = problem.rel_tol;

            %% check time steps modified davison maki must be the solver with smallest time steps
            assert(h_mdm<=h_sol, 'Step size for MDM_FAC_SYM_DRE must be smaller than solution formula based solver for this test.');

            %% create solver MDM_FAC_SYM_DRE
            symmetric = true;
            mdm_opt = MDM_DRE_options(symmetric);
            mdm = MDM_FAC_SYM_DRE(A, M, B, C, Z0, mdm_opt, h_mdm, T);
            mdm.prepare();

            %% create solver SolutionFormula_DRE
            sol_opt = SolutionFormula_DRE_options(false);
            sol = SolutionFormula_DRE(A, M, B, C, Z0*Z0', sol_opt, h_sol, T);
            sol.prepare();

            %% solve and compare solutions
            if obj.verbose
                fprintf('Test %s, %s, T = %.2e.\n', instance, Z0_str, T);
            end
            print_mod = problem.print_mod;
            print_mod_counter = 0;
            while sol.t < T

                % time step with solution formula (coarser step size)
                sol.time_step();

                % time steps with davison maki (finer step size)
                while mdm.t < sol.t
                    mdm.time_step();
                end

                % solvers should be synchronized
                assert(abs(sol.t - mdm.t) < eps, 'solvers are not synchronized');

                % get solutions and compare solutions
                X_mod_dm = mdm.get_solution();
                X_sol = sol.get_solution();
                abs2_diff = abs(eigs(@(x) X_mod_dm*x - X_sol*x, n, 1));
                nrm2_X_mdm = abs(eigs(@(x) X_mod_dm*x, n, 1));
                rel2_diff = abs2_diff / nrm2_X_mdm;

                % print intermediate results
                percent = mdm.t/T*100;

                if percent >= print_mod_counter && obj.verbose
                    f1 = fprintf('t = %.2e, %6.2f%%, mdm / sol step size = %.2e / %.2e, 2-norm X_mdm = %.2e, abs./rel. 2-norm diff = %.2e / %.2e\n', ...
                        mdm.t, mdm.t/T*100, mdm.h, sol.h, nrm2_X_mdm, abs2_diff, rel2_diff);
                    print_mod_counter = print_mod_counter + print_mod;
                end

                % check tolerance for absolute and relative error
                obj.fatalAssertTrue(abs2_diff < abs_tol);
                obj.fatalAssertTrue(rel2_diff < rel_tol);
            end
            if obj.verbose
                fprintf('%s\n', repmat('-', 1, f1 - 1));
            end
        end
    end
end
