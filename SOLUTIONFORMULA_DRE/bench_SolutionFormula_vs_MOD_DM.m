%BENCH_SOLUTIONFORMULA_VS_MOD_DM
%   Compare the solvers solution_formula_dre and modified_davison_maki_dre solvers.
%
%   RUN_SOLUTION_FORMULA_VS_MOD_DM compares the solution formula based
%   iteration and the modified davison maki iteration.
%   Several slicot benchmark examples are used.
%
%   See also SOLUTION_FORMULA_DRE and SYM_MODIFIED_DM.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% problems
verbose = ~isCIrun();
if isCIrun()
    problems = {
        % eye as initial condition, solution gets usually large in norm
        struct('instance', 'beam',             'T', 2^2,       'k_mod_dm', -8,     'ks_solution_formula', -6:2:2,      'abs_tol', 1e-4,    'rel_tol', 1e-5, 'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        };
else
    problems = {
        % benchmark examples from slicot, rail example and fss example
        struct('instance', 'beam',             'T', 2^2,       'k_mod_dm', -8,     'ks_solution_formula', -6:2:2,      'abs_tol', 1e-7,    'rel_tol', 1e-8,  'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'build',            'T', 2^3,       'k_mod_dm', -4 ,    'ks_solution_formula', -4:1:2,      'abs_tol', 1e-8,    'rel_tol', 1e-8,  'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'CDplayer',         'T', 2^(-4),    'k_mod_dm', -15 ,   'ks_solution_formula', -12:2:-6,    'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'eady',             'T', 2^(-10),   'k_mod_dm', -20 ,   'ks_solution_formula', -18:2:-12,   'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'fom',              'T', 2^(-6),    'k_mod_dm', -12 ,   'ks_solution_formula', -10:2:-8,    'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'heat-cont',        'T', 2^5,       'k_mod_dm', -8 ,    'ks_solution_formula', -4:2:4,      'abs_tol', 1e-10,   'rel_tol', 1e-10, 'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'iss',              'T', 2^5,       'k_mod_dm', -6 ,    'ks_solution_formula', -4:2:4,      'abs_tol', 1e-9,    'rel_tol', 1e-7,  'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'pde',              'T', 2^(-5),    'k_mod_dm', -14 ,   'ks_solution_formula', -12:2:-6,    'abs_tol', 1e-12,   'rel_tol', 1e-12, 'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'random',           'T', 2^(-10),   'k_mod_dm', -18 ,   'ks_solution_formula', -16:2:-12,   'abs_tol', 1e-8,    'rel_tol', 1e-8,  'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'rail_371',         'T', 2^8,       'k_mod_dm', -2 ,    'ks_solution_formula', 0:2:4,       'abs_tol', 1e-6,    'rel_tol', 1e-10, 'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'fss_K50M2Q2_100',  'T', 2^4,       'k_mod_dm', -8 ,    'ks_solution_formula', -4:2:0,      'abs_tol', 1e-8,    'rel_tol', 1e-10, 'X0', '@(n) zeros(n, n)', 'print_mod', 5) ...
        struct('instance', 'beam',             'T', 2^2,       'k_mod_dm', -8,     'ks_solution_formula', -6:2:2,      'abs_tol', 1e-6,    'rel_tol', 1e-8,  'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'build',            'T', 2^3,       'k_mod_dm', -4 ,    'ks_solution_formula', -4:1:2,      'abs_tol', 1e-8,    'rel_tol', 1e-8,  'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'CDplayer',         'T', 2^(-4),    'k_mod_dm', -15 ,   'ks_solution_formula', -12:2:-6,    'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'eady',             'T', 2^(-10),   'k_mod_dm', -20 ,   'ks_solution_formula', -18:2:-12,   'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'fom',              'T', 2^(-6),    'k_mod_dm', -12 ,   'ks_solution_formula', -10:2:-8,    'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'heat-cont',        'T', 2^5,       'k_mod_dm', -8 ,    'ks_solution_formula', -4:2:4,      'abs_tol', 1e-9,    'rel_tol', 1e-10, 'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'iss',              'T', 2^5,       'k_mod_dm', -6 ,    'ks_solution_formula', -4:2:4,      'abs_tol', 1e-9,    'rel_tol', 1e-7,  'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'pde',              'T', 2^(-5),    'k_mod_dm', -14 ,   'ks_solution_formula', -12:2:-6,    'abs_tol', 1e-12,   'rel_tol', 1e-12, 'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'random',           'T', 2^(-10),   'k_mod_dm', -18 ,   'ks_solution_formula', -16:2:-12,   'abs_tol', 1e-8,    'rel_tol', 1e-8,  'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'rail_371',         'T', 2^8,       'k_mod_dm', -2 ,    'ks_solution_formula', 0:2:4,       'abs_tol', 1e-6,    'rel_tol', 1e-10, 'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'fss_K50M2Q2_100',  'T', 2^4,       'k_mod_dm', -8 ,    'ks_solution_formula', -4:2:0,      'abs_tol', 1e-8,    'rel_tol', 1e-10, 'X0', '@(n) eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'beam',             'T', 2^2,       'k_mod_dm', -8,     'ks_solution_formula', -6:2:2,      'abs_tol', 1e+0,     'rel_tol', 1e-7,    'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'build',            'T', 2^3,       'k_mod_dm', -4 ,    'ks_solution_formula', -4:1:2,      'abs_tol', 1e-8,     'rel_tol', 1e-8,    'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'CDplayer',         'T', 2^(-4),    'k_mod_dm', -15 ,   'ks_solution_formula', -12:2:-6,    'abs_tol', 1e-9,     'rel_tol', 1e-11,   'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'eady',             'T', 2^(-10),   'k_mod_dm', -20 ,   'ks_solution_formula', -18:2:-12,   'abs_tol', 1e-10,    'rel_tol', 1e-12,   'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'fom',              'T', 2^(-6),    'k_mod_dm', -12 ,   'ks_solution_formula', -10:2:-8,    'abs_tol', 1e-10,    'rel_tol', 1e-11,   'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'heat-cont',        'T', 2^5,       'k_mod_dm', -8 ,    'ks_solution_formula', -4:2:4,      'abs_tol', 1e-9,     'rel_tol', 1e-9,    'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'iss',              'T', 2^5,       'k_mod_dm', -6 ,    'ks_solution_formula', -4:2:4,      'abs_tol', 1e-7,     'rel_tol', 1e-7,    'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'pde',              'T', 2^(-5),    'k_mod_dm', -14 ,   'ks_solution_formula', -12:2:-6,    'abs_tol', 1e-12,    'rel_tol', 1e-12,   'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'random',           'T', 2^(-10),   'k_mod_dm', -18 ,   'ks_solution_formula', -16:2:-12,   'abs_tol', 1e-6,     'rel_tol', 1e-8,    'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'rail_371',         'T', 2^8,       'k_mod_dm', -2 ,    'ks_solution_formula', 0:2:4,       'abs_tol', 1e-6,     'rel_tol', 1e-10,   'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'fss_K50M2Q2_100',  'T', 2^4,       'k_mod_dm', -8 ,    'ks_solution_formula', -4:2:0,      'abs_tol', 1e-8,     'rel_tol', 1e-10,   'X0', '@(n) 100*eye(n, n)', 'print_mod', 5) ...
        struct('instance', 'beam',             'T', 2^2,       'k_mod_dm', -8,     'ks_solution_formula', -6:2:2,      'abs_tol', 1e-7,    'rel_tol', 1e-8,  'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'build',            'T', 2^3,       'k_mod_dm', -4 ,    'ks_solution_formula', -4:1:2,      'abs_tol', 1e-8,    'rel_tol', 1e-8,  'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'CDplayer',         'T', 2^(-4),    'k_mod_dm', -15 ,   'ks_solution_formula', -12:2:-6,    'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'eady',             'T', 2^(-10),   'k_mod_dm', -20 ,   'ks_solution_formula', -18:2:-12,   'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'fom',              'T', 2^(-6),    'k_mod_dm', -12 ,   'ks_solution_formula', -10:2:-8,    'abs_tol', 1e-10,   'rel_tol', 1e-12, 'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'heat-cont',        'T', 2^5,       'k_mod_dm', -8 ,    'ks_solution_formula', -4:2:4,      'abs_tol', 1e-9,    'rel_tol', 1e-10, 'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'iss',              'T', 2^5,       'k_mod_dm', -6 ,    'ks_solution_formula', -4:2:4,      'abs_tol', 1e-7,    'rel_tol', 1e-7,  'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'pde',              'T', 2^(-5),    'k_mod_dm', -14 ,   'ks_solution_formula', -12:2:-6,    'abs_tol', 1e-12,   'rel_tol', 1e-12, 'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'random',           'T', 2^(-10),   'k_mod_dm', -18 ,   'ks_solution_formula', -16:2:-12,   'abs_tol', 1e-8,    'rel_tol', 1e-8,  'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'rail_371',         'T', 2^8,       'k_mod_dm', -2 ,    'ks_solution_formula', 0:2:4,       'abs_tol', 1e-6,    'rel_tol', 1e-10, 'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        struct('instance', 'fss_K50M2Q2_100',  'T', 2^4,       'k_mod_dm', -8 ,    'ks_solution_formula', -4:2:0,      'abs_tol', 1e-8,    'rel_tol', 1e-10, 'X0', '@(n) ones(n, n)', 'print_mod', 5) ...
        };
end

%% log structure for results
log = cell(0);

%% benchmark iterate over benchmark examples
time_total = tic;
for problem_i = 1:length(problems)

    %% load problem instance
    problem = problems{problem_i};
    if verbose
        fprintf('\n');
        fprintf('----------------------------------------------\n');
    end
    instance = problem.instance;
    data = load(instance);
    A = full(data.A);
    B = full(data.B);
    C = full(data.C);
    n = size(A, 1);
    X0_str = problem.X0;
    X0_fh = str2func(X0_str);
    X0 = X0_fh(n);

    %% tolerance and time data
    abs_tol = problem.abs_tol;
    rel_tol = problem.rel_tol;
    T = problem.T;
    k_mod_dm = problem.k_mod_dm;
    ks_solution_formula = problem.ks_solution_formula;

    %% get print interval
    print_mod = problem.print_mod;

    %% check time steps modified davison maki must be the solver with smallest time steps
    assert(all(k_mod_dm<=ks_solution_formula),'step size for modified davison maki must be smaller than solution formula based solver.');

    %% solve are to monitor convergence
    RHS = C'*C;
    Xinf = care(A, B, RHS);

    %% iterate over different step sizes for solution formula
    for k_solution_formula = ks_solution_formula

        % iterate
        t = 0;
        if verbose
            fprintf('instance = %s, X0 = %s, T = %.8f\n', instance, X0_str, T);
        end

        % create modified davison maki solver
        mdm_opt = MDM_DRE_options(true);
        mdm = MDM_SYM_DRE(A, [], B*B', C'*C, X0, mdm_opt, 2^k_mod_dm, T);
        mdm.prepare();

        % create solution formula based solver
        solformula_dre_opt = SolutionFormula_DRE_options(false);
        solformula = SolutionFormula_DRE(A, [], B, C, X0, solformula_dre_opt, 2^k_solution_formula, T);
        solformula.prepare();

        % reset data
        % store absolute, relative error and time
        log_time = [];
        log_abs2_diff = [];
        log_rel2_diff = [];
        log_nrm2_X_mod_dm = [];
        print_mod_counter = 0;

        time_test = tic;
        while solformula.t < T

            % time step with solution formula (coarser step size)
            solformula.time_step();

            % time steps with davison maki (finer step size)
            while mdm.t < solformula.t
                mdm.time_step();
            end

            % solvers should be synchronized
            assert(abs(solformula.t - mdm.t)<eps, 'solvers are not synchronized');

            % get solutions and compare solutions
            X_mod_dm = mdm.get_solution();
            X_sol = solformula.get_solution();
            abs2_diff = abs(eigs(@(x) X_mod_dm*x - X_sol*x, n, 1));
            nrm2_X_mod_dm = abs(eigs(@(x) X_mod_dm*x, n, 1));
            rel2_diff = abs2_diff / nrm2_X_mod_dm;

            % monitor convergence
            conv_stat = abs(eigs(@(x) X_mod_dm*x - Xinf*x, n, 1));

            % print intermediate results
            percent = mdm.t/T*100;

            if percent >= print_mod_counter && verbose
                fprintf('t =% 12.8f, %6.2f %%, mod_dm / sol_formula step size = %f / %f, norm X_mod = %.2e, abs./rel. 2-norm diff = %.2e / %.2e, conv. to stat. point = %.2e\n', ...
                    mdm.t, mdm.t/T*100, mdm.h, solformula.h,nrm2_X_mod_dm, abs2_diff, rel2_diff, conv_stat);
                print_mod_counter = print_mod_counter + print_mod;
            end

            % check tolerance for absolute and relative error
            assert(abs2_diff < abs_tol, 'abs2_diff = %.2e, abs_tol = %.2e', abs2_diff, abs_tol);
            assert(rel2_diff < rel_tol, 'rel2_diff = %.2e, rel_tol = %.2e', rel2_diff, rel_tol);

            % log the error
            log_time = [log_time; mdm.t]; %#ok<*AGROW>
            log_abs2_diff = [log_abs2_diff; abs2_diff];
            log_rel2_diff = [log_rel2_diff; rel2_diff];
            log_nrm2_X_mod_dm = [log_nrm2_X_mod_dm; nrm2_X_mod_dm];
        end
        time_test = toc(time_test);
        if verbose
            fprintf('Test Runtime: %s, %s %f secs.\n', instance, X0_str, time_test);
            fprintf('Computational Time: %s, Step Size = %f, %f secs.\n', upper(class(mdm)),  mdm.h, mdm.wtime_prepare + mdm.wtime_time_step);
            fprintf('Computational Time: %s, Step Size = %f, %f secs.\n', upper(class(solformula)), solformula.h, solformula.wtime_prepare + solformula.wtime_time_step);
            fprintf('----------------------------------------------\n\n');
        end
        % log data for this run
        log{end+1} = struct(...
            'problem', instance, 'X0', X0_str, 'T', T, 'time', log_time, ...
            'abs2_diff', log_abs2_diff, 'rel2_diff', log_rel2_diff, ...
            'nrm2_X_mod_dm', log_nrm2_X_mod_dm, ...
            'davison_maki_time', mdm.wtime_prepare + mdm.wtime_time_step, 'davison_maki_h', mdm.h,...
            'solution_formula_time', solformula.wtime_prepare + solformula.wtime_time_step, 'solution_formula_h', solformula.h ...
            ); %#ok<*SAGROW>
    end
end
time_total = toc(time_total);
if verbose
    fprintf('Total Test Runtime: %f secs.\n', time_total);
end

%% print and write logging information
llog = length(log);
problem = cell(llog,1);
X0 = cell(llog,1);
T = cell(llog,1);
max_abs2_diff = cell(llog,1);
mean_abs2_diff = cell(llog,1);
max_rel2_diff = cell(llog,1);
mean_rel2_diff = cell(llog,1);
max_nrm2_X_mod_dm = cell(llog,1);
mean_nrm2_X_mod_dm = cell(llog,1);
dm_time = cell(llog,1);
dm_h = cell(llog,1);
sol_time = cell(llog,1);
sol_h = cell(llog,1);
for i = 1:llog
    % extract data
    problem{i}= log{i}.problem;
    X0{i} = log{i}.X0;
    T{i} = log{i}.T;
    max_abs2_diff{i} = max(log{i}.abs2_diff);
    mean_abs2_diff{i} = mean(log{i}.abs2_diff);
    max_rel2_diff{i} = max(log{i}.rel2_diff);
    mean_rel2_diff{i} = mean(log{i}.rel2_diff);
    max_nrm2_X_mod_dm{i} = max(log{i}.nrm2_X_mod_dm);
    mean_nrm2_X_mod_dm{i} = mean(log{i}.nrm2_X_mod_dm);
    dm_time{i} = log{i}.davison_maki_time;
    dm_h{i} = log{i}.davison_maki_h;
    sol_time{i} = log{i}.solution_formula_time;
    sol_h{i} = log{i}.solution_formula_h;
end
tt = table(problem, X0, T, max_abs2_diff, mean_abs2_diff, max_rel2_diff, mean_rel2_diff, max_nrm2_X_mod_dm, mean_nrm2_X_mod_dm, dm_time, dm_h, sol_time, sol_h);
if verbose
    disp(tt);
end
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
writetable(tt, fullfile(mydir, sprintf('%s_results.dat', mfilename)));
