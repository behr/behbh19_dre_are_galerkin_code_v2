classdef ShiftedSolver < custom_handle
    %ShiftedSolver   Implements a direct solver for linear shifted systems
    %   based on UMFPACK and CHOLMOD.
    %
    %   SHIFTEDSOLVER properties:
    %       A                       - set private, double real or complex sparse n-x-n matrix A
    %       E                       - set private, double real or complex sparse n-x-n matrix E
    %       use_numerical           - set private, logical, use numerical values for symbolic analysis
    %       logicalAE               - set private, logical, sparsity pattern of A|E
    %       isrealAE                - set private, logical, true if A and E are real
    %       isdiagAE                - set private, logical, true if A and E are diagonal
    %       istridiagAE             - set private, logical, true if A and E are tridiagonal
    %       istriulAE               - set private, logical, true if A and E are upper or lower triangular
    %       ishermitianAE           - set private, logical, true if A and E are hermitian
    %       umfpack_symbolic_real   - set private, symbolic factorization of A|E for real systems
    %       umfpack_symbolic_cpx    - set private, symbolic factorization of A|E for complex systems
    %       cholmod_symbolic        - set private, symbolic factorization of A|E
    %
    %   The option use_numerical uses also the numerical for symbolic
    %   factorization. This might be faster for UMFPACK and CHOLMOD.
    %   For MA57 this has no effect, default: false.
    %
    %   SHIFTEDSOLVER methods:
    %       ShiftedSolver   - Constructor.
    %       solve           - Solve the system (A + shift * E) x = b.
    %
    %   See also RADI, RADI_OPTIONS, TEST_SHIFTEDSOLVER.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % set private, double real or complex sparse n-x-n matrix A
        A
        % set private, double real or complex sparse n-x-n matrix E
        E
        % set private, logical, use numerical values for symbolic analysis
        use_numerical
        % set private logical, sparsity pattern of A|E
        logicalAE
        % set private logical, true if A and E are real
        isrealAE
        % set private, logical, true if A and E are diagonal
        isdiagAE
        % set private, logical, true if A and E are tridiagonal
        istridiagAE
        % set private, logical, true if A and E are upper or lower triangular
        istriulAE
        % set private logical, true if A and E are hermitian
        ishermitianAE
        % set private, symbolic factorization of A|E for real systems
        umfpack_symbolic_real
        % set private, symbolic factorization of A|E for real systems
        umfpack_symbolic_cpx
        % set private, symbolic factorization of A|E
        cholmod_symbolic
        % set private, symbolic analysis MA57AD
        MA57_keep
        % set private, symbolic analysis MA57AD
        MA57_lfact
        % set private, symbolic analysis MA57AD
        MA57_lifact
        % set private, number of nonzero elements of A|E
        MA57_nnzAE
    end

    methods

        function obj = ShiftedSolver(A, E, use_numerical)
            %SHIFTEDSOLVER  Sparse direct solver for shifted linear systems.
            %
            %   SHIFTEDSOL = SHIFTEDSOLVER(A, E) creates an instance of the
            %   class. A and E must be double real or complex sparse
            %   n-x-n matrices. If E is empty or the constructor
            %   is called with one argument only the identity is used for E.
            %   The solver is based on UMFPACK and CHOLMOD and reuses
            %   the symbolic factorization.
            %
            %   See also TEST_SHIFTEDSOLVER.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(A, {'double'}, {'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
            assert(issparse(A), '%s must be sparse', inputname(1));
            n = size(A, 1);
            assert(n >= 1, 'The order of the matrix must be at least 1');
            obj.A = A;

            if nargin < 2 || isempty(E)
                obj.E = speye(n);
            else
                validateattributes(E, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan', 'size', [n, n]}, mfilename, inputname(2));
                assert(issparse(E), '%s must be sparse', inputname(2));
                obj.E = E;
            end

            obj.use_numerical = false;
            if nargin >= 3
                validateattributes(use_numerical, {'logical'}, {}, mfilename, inputname(3));
                obj.use_numerical = use_numerical;
            end


            %% set other field
            obj.logicalAE = obj.A | obj.E;
            obj.isrealAE = isreal(obj.A) && isreal(obj.E);
            obj.isdiagAE = isdiag(obj.A) && isdiag(obj.E);
            [klA, kuA] = bandwidth(obj.A);
            [klE, kuE] = bandwidth(obj.E);
            obj.istridiagAE = all([klA, kuA, klE, kuE] <= 1);
            obj.istriulAE = (istril(obj.A) && istril(obj.E)) || (istriu(obj.A) && istriu(obj.E));
            obj.ishermitianAE = ishermitian(obj.A) && ishermitian(obj.E);

            %% symbolic analysis
            obj.umfpack_symbolic_real = [];
            obj.umfpack_symbolic_cpx = [];
            obj.cholmod_symbolic = [];
            obj.MA57_keep = [];
            obj.MA57_lfact = [];
            obj.MA57_lifact = [];
        end

        function delete(obj)
            %DELETE  Free symbolic factorizations.
            %
            %   Author: Maximilian Behr
            if obj.cholmod_symbolic
                cholmod_l_free_factor(obj.cholmod_symbolic);
                obj.cholmod_symbolic = 0;
            end

            if obj.umfpack_symbolic_real
                umfpack_dl_free_symbolic(obj.umfpack_symbolic_real);
                obj.umfpack_symbolic_real = 0;
            end

            if obj.umfpack_symbolic_cpx
                umfpack_zl_free_symbolic(obj.umfpack_symbolic_cpx);
                obj.umfpack_symbolic_cpx = 0;
            end
        end


        function x = solve(obj, shift, b)
            %SOLVE  Solve the linear system (A + shift*E)x = b.
            %
            %   X = SHIFTEDSOL.SOLVE(SHIFT, B) solve the shifted
            %   system (A + SHIFT*E)X = B. SHIFT must be a scalar.
            %
            %   See also PERMUTATION_T, RADI, TEST_RADI.
            %
            %   Author: Maximilian Behr
            validateattributes(shift, {'double'}, {'scalar', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(2));
            n = size(obj.A, 1);
            validateattributes(b, {'double'}, {'2d', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(3));
            if issparse(b)
                b = full(b);
            end
            nrhs = size(b, 2);

            %% compute A + shift*E
            AshiftE = obj.A + shift * obj.E;

            %% diagonal, tridiagonal or lower / upper triangular use backslash
            if obj.isdiagAE || obj.istridiagAE || obj.istriulAE
                x = AshiftE \ b;
                return;
            end

            %% check if candidate for cholesky
            scale = 1;
            cholesky_candidate = false;
            if isreal(shift) && obj.ishermitianAE
                diagAshiftE = diag(AshiftE);
                if isreal(diagAshiftE)
                    if all(diagAshiftE < 0)
                        cholesky_candidate = true;
                        scale = -1;
                    elseif all(diagAshiftE > 0)
                        cholesky_candidate = false;
                    end
                end
            end

            %% try cholmod
            if cholesky_candidate

                % cholmod symbolic is already computed as obj.ishermitianAE must be true
                cholmod_numeric = 0;
                try
                    % compute symbolic if necessary

                    if isempty(obj.cholmod_symbolic)
                        if obj.use_numerical
                            obj.cholmod_symbolic = cholmod_l_analyze(AshiftE);
                        else
                            obj.cholmod_symbolic = cholmod_l_analyze(obj.logicalAE);
                        end
                    end

                    if scale == 1
                        cholmod_numeric = cholmod_l_factorize(AshiftE, obj.cholmod_symbolic);
                    else
                        cholmod_numeric = cholmod_l_factorize(-AshiftE, obj.cholmod_symbolic);
                    end

                    if nrhs > 4
                        x = cholmod_l_solve_omp(cholmod_numeric, b);
                    else
                        x = cholmod_l_solve(cholmod_numeric, b);
                    end
                    cholmod_l_free_factor(cholmod_numeric);

                    if ~(scale == 1)
                        x = -x;
                    end
                    return;
                catch ME
                    warning(ME.message);
                end

                %% free cholmod numeric
                if cholmod_numeric
                    cholmod_l_free_factor(cholmod_numeric);
                end
            end

            %% try MA57 for real symmetric systems
            if obj.ishermitianAE && obj.isrealAE && isreal(shift)

                % compute symbolic if necessary, use_numerical options not available for MA57

                if isempty(obj.MA57_keep) || isempty(obj.MA57_lfact) || isempty(obj.MA57_lifact)
                    n = size(obj.A, 1);
                    [irn, jcn, vals] = find(triu(AshiftE));
                    irn = int64(irn);
                    jcn = int64(jcn);
                    [obj.MA57_keep, obj.MA57_lfact, obj.MA57_lifact] = MA57AD(n, irn, jcn);
                    obj.MA57_nnzAE = numel(irn);
                else
                    [irn, jcn, vals] = find(triu(AshiftE));
                    irn = int64(irn);
                    jcn = int64(jcn);
                    n = size(AshiftE, 1);
                end

                % necessary condition for unchanged sparsity pattern
                if numel(irn) == obj.MA57_nnzAE
                    try
                        % numerical factorization
                        [fact, ifact] = MA57BD(n, vals, obj.MA57_keep, obj.MA57_lfact, obj.MA57_lifact);

                        % solve
                        if nrhs == 1
                            x = MA57DD(vals, irn, jcn, fact, ifact, b);
                        else
                            x = MA57DD_omp(vals, irn, jcn, fact, ifact, b);
                        end

                        return
                    catch ME
                        warning(ME.message);
                    end
                end
            end


            %% umfpack: compute symbolic factorization if necessary
            if isreal(AshiftE) && isempty(obj.umfpack_symbolic_real)
                if obj.use_numerical
                    obj.umfpack_symbolic_real = umfpack_dl_symbolic(AshiftE);
                else
                    obj.umfpack_symbolic_real = umfpack_dl_symbolic(obj.logicalAE);
                end
            end

            if ~isreal(AshiftE) && isempty(obj.umfpack_symbolic_cpx)
                if obj.use_numerical
                    obj.umfpack_symbolic_cpx = umfpack_zl_symbolic(AshiftE);
                else
                    obj.umfpack_symbolic_cpx = umfpack_zl_symbolic(obj.logicalAE);
                end
            end

            %% umfpack: try to solve the system
            if isreal(AshiftE)
                try
                    umfpack_numeric = 0;
                    umfpack_numeric = umfpack_dl_numeric(AshiftE, obj.umfpack_symbolic_real);
                    if nrhs > 1
                        x = umfpack_dl_solve_omp(AshiftE, umfpack_numeric, b);
                    else
                        x = umfpack_dl_solve(AshiftE, umfpack_numeric, b);
                    end
                    umfpack_dl_free_numeric(umfpack_numeric);
                    return;
                catch ME
                    warning(ME.message);
                    if umfpack_numeric
                        umfpack_dl_free_numeric(umfpack_numeric);
                    end
                end
            else
                try
                    umfpack_numeric = 0;
                    umfpack_numeric = umfpack_zl_numeric(AshiftE, obj.umfpack_symbolic_cpx);
                    if nrhs > 1
                        x = umfpack_zl_solve_omp(AshiftE, umfpack_numeric, b);
                    else
                        x = umfpack_zl_solve(AshiftE, umfpack_numeric, b);
                    end
                    umfpack_zl_free_numeric(umfpack_numeric);
                    return;
                catch ME
                    warning(ME.message);
                    if umfpack_numeric
                        umfpack_zl_free_numeric(umfpack_numeric);
                    end
                end
            end

            %% warning if everything has failed -> switch back to mldivide
            warning('%s:UMFPACK and CHOLMOD failed, switch back to mldivide.', upper(class(obj)));
            x = AshiftE \ b;

        end
    end
end
