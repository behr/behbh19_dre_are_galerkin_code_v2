classdef test_decomposition_sparse_direct < matlab.unittest.TestCase
    %TEST_DECOMPOSITION_SPARSE_DIRECT   Test for decomposition_sparse_direct class.
    %
    %   TEST_DECOMPOSITION_SPARSE_DIRECT tests the functionality of the
    %   decomposition_sparse_direct class.
    %
    %   TEST_DECOMPOSITION_SPARSE_DIRECT properties:
    %       instance    - problem instance
    %       modifier    - modifes the matrices
    %
    %   TEST_DECOMPOSITION_SPARSE_DIRECT methods:
    %       test_decomp_sparse_direct - Test for decomposition_sparse_direct.
    %
    %   See also DECOMPOSITION_SPARSE_DIRECT.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        instance = {'rail_371', 'rail_1357', 'conv_diff_1_100', 'CDplayer', 'fom', 'flow_v0', 'flow_v05', 'chip_v0', 'chip_v01'};
        nrhs = {1, 3, 5, 7};
        modifier = {@(x) x, @(x) -x, @(x) triu(x), @(x) tril(x), @(x) x + x'};
    end

    properties
        abstol = 1e-6;
        reltol = 1e-8;
        max_order_diff = 3;
        verbose = ~isCIrun();
        randseed = 1;
    end

    methods(Test)
        function test_decomp_sparse_direct(obj, instance, nrhs, modifier)

            %% load instance
            data = load(instance);
            A = data.A;
            n = size(A, 1);
            A = modifier(A);

            %% skip some tests on ciRun
            if any(strcmp({'flow_v0', 'flow_v05', 'chip_v0', 'chip_v01'}, instance)) && isCIrun
                return;
            end

            %% set random seed
            rng(obj.randseed);
            b = rand(n, nrhs);
            nrmb = norm(b);

            %% solve with shiftedsolver
            dA = decomposition_sparse_direct(A);
            x = dA \ b;
            abs2res = norm(A*x-b);
            rel2res = abs2res / nrmb;
            abs2res_order = log10(abs2res);

            %% backslash solution for comparison
            x_backslash = A \ b;
            abs2res_backslash = norm(A*x_backslash-b);
            rel2res_backslash = abs2res_backslash / nrmb;
            abs2res_backslash_order = log10(abs2res_backslash);

            %% print
            is_abstol = abs2res < obj.abstol;
            is_reltol = rel2res < obj.reltol;
            failed = ~(is_abstol && is_reltol);
            if failed || obj.verbose
                fprintf('\n');
                if failed
                    fprintf('FAILED:\n');
                end
                f(1) = fprintf('%s, %s, nrhs = %d\n', instance, func2str(modifier), nrhs);
                f(2) = fprintf('is_abstol = %d\n', is_abstol);
                f(3) = fprintf('is_reltol = %d\n', is_reltol);
                f(4) = fprintf('DECOMPOSITION_SPARSE_DIRECT:    abs./rel. 2-norm res = %.2e / %.2e\n', abs2res, rel2res);
                f(5) = fprintf('BACKSLASH:                      abs./rel. 2-norm res = %.2e / %.2e\n', abs2res_backslash, rel2res_backslash);
                f(6) = fprintf('                                abs./rel. 2-norm tol = %.2e / %.2e\n', obj.abstol, obj.reltol);
                fprintf('%s\n', repmat('-', 1, max(f)-1));
            end

            %% check
            obj.fatalAssertLessThan(abs2res, obj.abstol);
            obj.fatalAssertLessThan(rel2res, obj.reltol);
            obj.fatalAssertLessThan(abs(abs2res_order-abs2res_backslash_order), obj.max_order_diff);
        end
    end
end
