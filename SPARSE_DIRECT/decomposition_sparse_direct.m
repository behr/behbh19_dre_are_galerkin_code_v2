classdef decomposition_sparse_direct < custom_handle
    %DECOMPOSITION_SPARSE_DIRECT   Implements a sparse direct solver
    %   for linear systems based on UMFPACK, CHOLMOD and MA57.
    %   The solver is chosen based on the properties of the matrix A.
    %
    %   DECOMPOSITION_SPARSE_DIRECT properties:
    %       A                   - set private, double real or complex sparse n-x-n matrix A
    %       umfpack_numeric     - set private, numeric factorization of A
    %       cholmod_numeric     - set private, numeric factorization of A
    %
    %   DECOMPOSITION_SPARSE_DIRECT methods:
    %       decomposition_sparse_direct     - Constructor.
    %       mldivide                        - The backslash operator.
    %
    %   See also TEST_DECOMPOSITION_SPARSE_DIRECT.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % set private, size of A
        n = 0;
        % set private, double real or complex sparse n-x-n matrix A
        A = [];
        % set private, UMFPACK numeric factorization of A
        umfpack_numeric = [];
        % set private, CHOLMOD numeric factorization of A
        cholmod_numeric = [];
        % set private, scale
        scale = 1;
        % set private, MA57 values of coordinate storage of A
        MA57_vals = [];
        % set private, MA57 row indices of coordinate storage of A
        MA57_irn = [];
        % set private, MA57 column indices of coordinate storage of A
        MA57_jcn = [];
        % set private, MA57 factorization of A
        MA57_fact = [];
        % set private, MA57 factorization of A
        MA57_ifact = [];
        % set private, MATLABs decomposition object
        MATLAB_decomp = [];
    end

    methods

        function obj = decomposition_sparse_direct(A)
            %DECOMPOSITION_SPARSE_DIRECT  Sparse direct solver for linear systems.
            %
            %   decomp = DECOMPOSITION_SPARSE_DIRECT(A) creates an instance of the
            %   class. A must be double real or complex sparse
            %   n-x-n matrices.
            %
            %   See also TEST_DECOMPOSITION_SPARSE_DIRECT.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(A, {'double'}, {'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
            %assert(issparse(A), '%s must be sparse', inputname(1));
            obj.n = size(A, 1);
            assert(obj.n >= 1, 'The order of the matrix must be at least 1');
            obj.A = A;

            %% check matrix properties
            [kl, ku] = bandwidth(obj.A);

            % check diagonal, tridiagonal, lower/upper triangular
            if (kl <=1 && ku <= 1) || (kl == 0) || (ku == 0) || ~issparse(obj.A)
                obj.MATLAB_decomp = decomposition(obj.A);
                return;
            end

            %% try cholesky with A and -A
            ishermitianA = ishermitian(obj.A);
            diagA = diag(obj.A);

            if isreal(diagA) && all(diagA > 0)
                cholmod_symbolic = 0;
                try
                    cholmod_symbolic = cholmod_l_analyze(obj.A);
                    obj.cholmod_numeric = cholmod_l_factorize(obj.A);
                    obj.scale = 1;
                    return;
                catch ME
                    %warning(ME.message);
                    if cholmod_symbolic
                        cholmod_l_free_factor(cholmod_symbolic);
                        cholmod_symbolic = 0; %#ok<*NASGU>
                    end
                    if obj.cholmod_numeric
                        cholmod_l_free_factor(obj.cholmod_numeric);
                        obj.cholmod_numeric = 0;
                    end
                end
            elseif isreal(diagA) && all(diagA < 0)
                cholmod_symbolic = 0;
                mA = -obj.A;
                cholmod_symbolic = 0;
                try
                    cholmod_symbolic = cholmod_l_analyze(mA);
                    obj.cholmod_numeric = cholmod_l_factorize(mA);
                    obj.scale = -1;
                    return;
                catch ME
                    %warning(ME.message);
                    if cholmod_symbolic
                        cholmod_l_free_factor(cholmod_symbolic);
                        cholmod_symbolic = 0;
                    end
                    if obj.cholmod_numeric
                        cholmod_l_free_factor(obj.cholmod_numeric);
                        obj.cholmod_numeric = 0;
                    end
                end
            end

            %% symmetric and indefinite use MA57
            if isreal(obj.A) && ishermitianA
                [obj.MA57_irn, obj.MA57_jcn, obj.MA57_vals] = find(triu(obj.A));
                obj.MA57_irn = int64(obj.MA57_irn);
                obj.MA57_jcn = int64(obj.MA57_jcn);
                n = size(A, 1);
                [keep, lfact, lifact] = MA57AD(n, obj.MA57_irn, obj.MA57_jcn);
                [obj.MA57_fact, obj.MA57_ifact] = MA57BD(n, obj.MA57_vals, keep, lfact, lifact);
                return;
            end

            %% use umfpack
            if isreal(obj.A)
                symbolic = umfpack_dl_symbolic(obj.A);
                obj.umfpack_numeric = umfpack_dl_numeric(obj.A, symbolic);
                umfpack_dl_free_symbolic(symbolic);
            else
                symbolic = umfpack_zl_symbolic(obj.A);
                obj.umfpack_numeric = umfpack_zl_numeric(obj.A, symbolic);
                umfpack_zl_free_symbolic(symbolic);
            end
            return;
        end

        function delete(obj)
            %DELETE  Free symbolic factorizations.
            %
            %   Author: Maximilian Behr
            if obj.cholmod_numeric
                cholmod_l_free_factor(obj.cholmod_numeric);
                obj.cholmod_numeric = 0;
            end

            if obj.umfpack_numeric
                if isreal(obj.A)
                    umfpack_dl_free_numeric(obj.umfpack_numeric);
                    obj.umfpack_numeric = 0;
                else
                    umfpack_zl_free_numeric(obj.umfpack_numeric);
                    obj.umfpack_numeric = 0;
                end
            end
        end

        function x = mldivide(da, b)
            %\  Matrix left divide for decomposition_sparse_direct using.
            %
            %   X = DA \ B, where DA is a decomposition_sparse_direct object,
            %   solve the linear system A*X = B,
            %   where DA = DECOMPOSITION_SPARSE_DIRECT(A).
            %
            %   See also TEST_DECOMPOSITION_SPARSE_DIRECT.
            %
            %   Author: Maximilian Behr
            validateattributes(da, {'decomposition_sparse_direct'}, {}, mfilename, inputname(1));
            validateattributes(b, {'double'}, {'2d', 'nonempty', 'finite', 'nonnan', 'nrows', da.n}, mfilename, inputname(2));
            if issparse(b)
                b = full(b);
            end
            nrhs = size(b, 2);

            %% solve A*x = b with MATLAB decomposition
            if ~isempty(da.MATLAB_decomp)
                x = da.MATLAB_decomp \ b;
                return;
            end

            %% CHOLMOD
            if ~isempty(da.cholmod_numeric)
                if nrhs > 4
                    x = cholmod_l_solve_omp(da.cholmod_numeric, b);
                else
                    x =  cholmod_l_solve(da.cholmod_numeric, b);
                end

                if da.scale == -1
                    x = -x;
                end
                return;
            end

            %% MA57
            if ~isempty(da.MA57_vals)
                if nrhs >= 2
                    x = MA57DD_omp(da.MA57_vals, da.MA57_irn, da.MA57_jcn, da.MA57_fact, da.MA57_ifact, b);
                else
                    x = MA57DD(da.MA57_vals, da.MA57_irn, da.MA57_jcn, da.MA57_fact, da.MA57_ifact, b);
                end
                return;
            end

            %% UMFPACK
            if ~isempty(da.umfpack_numeric)
                if isreal(da.A)
                    if nrhs >= 2
                        x = umfpack_dl_solve_omp(da.A, da.umfpack_numeric, b);
                    else
                        x = umfpack_dl_solve(da.A, da.umfpack_numeric, b);
                    end
                else
                    if nrhs >= 2
                        x = umfpack_zl_solve_omp(da.A, da.umfpack_numeric, b);
                    else
                        x = umfpack_zl_solve(da.A, da.umfpack_numeric, b);
                    end
                end
            end
        end
    end
end
