%TRY_UMFPACK    Playaround script for banded solvers.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all;
clc

%% data
instance = 'cookie';
data = load(instance);
A = data.A;
E = speye(size(A));
B = data.B;
C = data.C;
A = (5 + 2i) * E - 2^(-2) * A;
n = size(A, 1);
RHS = rand(n, 100);

%% compare solvers
% compute decomposition
dA = decomposition(A);

% compute umfpack decomposition
symbolic = umfpack_zl_symbolic(A);
numeric = umfpack_zl_numeric(A, symbolic);

for nrhs = 1:25:250
    RHS = rand(n, nrhs);
    f = @() dA \ RHS;
    t_decomp = timeit(f);
    f = @() umfpack_zl_solve(A, numeric, RHS);
    t_umf = timeit(f, 1);
    f = @() umfpack_zl_solve_omp(A, numeric, RHS);
    t_umf_omp = timeit(f, 1);
    fprintf('decomp. / UMF / UMF_OMP: RHS = %d, time = %.2e / %.2e / %.2e , speedup = %.2e / %.2e\n', ...
        nrhs, t_decomp, t_umf, t_umf_omp, t_decomp/t_umf, t_decomp/t_umf_omp);
end
umfpack_zl_free_symbolic(symbolic);
umfpack_zl_free_numeric(numeric);
