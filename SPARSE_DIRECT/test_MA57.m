classdef test_MA57 < matlab.unittest.TestCase
    %TEST_MA57   Test for the MA57 interface.
    %
    %   TEST_MA57 tests the functionality of the
    %   MA57 interface.
    %
    %   TEST_MA57 properties:
    %       instance        - problem instance
    %       nrhs            - number of right hand side
    %       cpx_rhs         - complex or real right hand side
    %
    %   TEST_MA57 methods:
    %       test_MA57_interface - Test for MA57 interface.
    %
    %   See also MA57AD, MA57BD, MA57DD AND MA57DD_omp.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        instance = {'rail_1357', 'filter2D', 'flow_v0', 'GasSensor'};
        nrhs = {1, 5, 10};
        cpx_rhs = {false, true};
        shift = {-1, 0, 1};
    end

    properties
        abstol = 1e-6;
        reltol = 1e-8;
        verbose = ~isCIrun();
        randseed = 1;
    end

    methods(Test)
        function test_MA57_interface(obj, instance, nrhs, cpx_rhs, shift)

            %% skip some tests on ci run for speed up
            if isCIrun() && any(strcmp({'GasSensor'}, instance))
                return;
            end

            %% load instance
            data = load(instance);
            A = data.A + shift*data.E;
            n = size(A, 1);

            %% set random seed
            rng(obj.randseed);
            b = rand(n, nrhs);
            if cpx_rhs
                b = b + 1i * rand(n, nrhs);
            end
            normb = norm(b);

            %% pattern of the symmetric A
            [irn, jcn, vals] = find(triu(A));
            irn = int64(irn);
            jcn = int64(jcn);
            n = size(A, 1);

            %% symbolic analysis MA57AD
            [keep, lfact, lifact] = MA57AD(n, irn, jcn);

            %% numeric factorization MA57BD
            [fact, ifact] = MA57BD(n, vals, keep, lfact, lifact);

            %% solve MA57DD and MA57DD_omp
            x = MA57DD(vals, irn, jcn, fact, ifact, b);
            x_omp = MA57DD_omp(vals, irn, jcn, fact, ifact, b);

            %% check residual
            abs2res = norm(A*x-b);
            rel2res = abs2res / normb;
            is_abstol = abs2res < obj.abstol;
            is_reltol = rel2res < obj.reltol;

            abs2res_omp = norm(A*x_omp-b);
            rel2res_omp = abs2res_omp / normb;
            is_abstol_omp = abs2res < obj.abstol;
            is_reltol_omp = rel2res < obj.reltol;

            failed = ~(is_abstol && is_reltol && is_abstol_omp && is_reltol_omp);

            %% backslash solution for comparison
            x_back = A\b;
            abs2res_backslash = norm(A*x_back-b);
            rel2res_backslash = abs2res_backslash / normb;

            %% print on failure and check
            if failed || obj.verbose
                fprintf('\n');
                if failed
                    fprintf('FAILED:\n');
                end
                f(1) = fprintf('%s, nrhs = %d, cpx_rhs = %d, shift = %+.2e\n', instance, nrhs, cpx_rhs, shift);
                f(2) = fprintf('is_abstol     = %d\n', is_abstol);
                f(3) = fprintf('is_reltol     = %d\n', is_reltol);
                f(4) = fprintf('is_abstol_omp = %d\n', is_abstol_omp);
                f(5) = fprintf('is_reltol_omp = %d\n', is_reltol_omp);
                f(6) = fprintf('MA57:         abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res, rel2res);
                f(6) = fprintf('MA57 OMP:     abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_omp, rel2res_omp);
                f(7) = fprintf('BACKSLASH:    abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_backslash, rel2res_backslash);
                f(8) = fprintf('              abs./rel. 2-norm tol      = %.2e/%.2e\n', obj.abstol, obj.reltol);
                fprintf('%s\n', repmat('-', 1, max(f)-1));
            end

            %% check
            obj.fatalAssertLessThan(abs2res, obj.abstol);
            obj.fatalAssertLessThan(rel2res, obj.reltol);
            obj.fatalAssertLessThan(abs2res_omp, obj.abstol);
            obj.fatalAssertLessThan(rel2res_omp, obj.reltol);

        end
    end
end
