classdef test_cholmod < matlab.unittest.TestCase
    %TEST_CHOLMOD   Test for the CHOLMOD interface.
    %
    %   TEST_CHOLMOD tests the functionality of the
    %   CHOLMOD interface.
    %
    %   TEST_CHOLMOD properties:
    %       instance        - problem instance
    %       nrhs            - number of right hand side
    %       cpx_rhs         - complex or real right hand side
    %       cpx_A           - complex or real matrix A
    %       logical_A       - use logical A for symbolic analysis
    %
    %   TEST_CHOLMOD methods:
    %       test_cholmod_interface - Test for CHOLMOD interface.
    %       test_cholmod_print - Test for CHOLMOD print routines.
    %
    %   See also CHOLMOD_L_ANALYZE, CHOLMOD_L_FACTORIZE AND CHOLMOD_L_SOLVE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        instance = {'rail_1357', ...
            'filter2D', ...
            'flow_v0', ...
            'GasSensor', ...
            };
        instance_report = {'rail_371'};
        nrhs = {1, 5, 10};
        cpx_rhs = {false, true};
        cpx_A = {false, true};
        logical_A = {false, true};
    end

    properties
        abstol = 1e-6;
        reltol = 1e-8;
        verbose = ~isCIrun();
        randseed = 1;
    end

    methods(Test)
        function test_cholmod_interface(obj, instance, nrhs, cpx_rhs, cpx_A, logical_A)

            %% skip some tests on ci run for speed up
            if isCIrun() && any(strcmp({'GasSensor'}, instance))
                return;
            end

            %% load instance
            data = load(instance);
            A = -data.A;
            n = size(A, 1);

            %% set random seed
            rng(obj.randseed);
            b = rand(n, nrhs);
            if cpx_rhs
                b = b + 1i * rand(n, nrhs);
            end
            normb = norm(b);

            if cpx_A
                A = (1 + 2i) * A;
                A = A + A';
            end

            %% symbolic, numeric factorization
            if logical_A
                symbolic = cholmod_l_analyze(logical(real(A)) | logical(imag(A)));
            else
                symbolic = cholmod_l_analyze(A);
            end

            numeric = cholmod_l_factorize(A, symbolic);

            %% solve
            x = cholmod_l_solve(numeric, b);
            x_omp = cholmod_l_solve_omp(numeric, b);

            %% check residual
            abs2res = norm(A*x-b);
            rel2res = abs2res / normb;
            is_abstol = abs2res < obj.abstol;
            is_reltol = rel2res < obj.reltol;

            abs2res_omp = norm(A*x_omp-b);
            rel2res_omp = abs2res_omp / normb;
            is_abstol_omp = abs2res < obj.abstol;
            is_reltol_omp = rel2res < obj.reltol;

            failed = ~(is_abstol && is_reltol && is_abstol_omp && is_reltol_omp);

            %% backslash solution for comparison
            abs2res_backslash = norm(A*(A \ b)-b);
            rel2res_backslash = abs2res_backslash / normb;

            %% print on failure and check
            if failed || obj.verbose
                fprintf('\n');
                if failed
                    fprintf('FAILED:\n');
                end
                f(1) = fprintf('%s, nrhs = %d, cpx_rhs = %d, cpx_A = %d, logical_A = %d\n', instance, nrhs, cpx_rhs, cpx_A, logical_A);
                cholmod_l_print_factor(symbolic);
                cholmod_l_print_factor(numeric);
                f(2) = fprintf('is_abstol     = %d\n', is_abstol);
                f(3) = fprintf('is_reltol     = %d\n', is_reltol);
                f(4) = fprintf('is_abstol_omp = %d\n', is_abstol_omp);
                f(5) = fprintf('is_reltol_omp = %d\n', is_reltol_omp);
                f(6) = fprintf('CHOLMOD:      abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res, rel2res);
                f(6) = fprintf('CHOLMOD OMP:  abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_omp, rel2res_omp);
                f(7) = fprintf('BACKSLASH:    abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_backslash, rel2res_backslash);
                f(8) = fprintf('              abs./rel. 2-norm tol      = %.2e/%.2e\n', obj.abstol, obj.reltol);
                fprintf('%s\n', repmat('-', 1, max(f)-1));
            end

            %% check
            obj.fatalAssertLessThan(abs2res, obj.abstol);
            obj.fatalAssertLessThan(rel2res, obj.reltol);
            obj.fatalAssertLessThan(abs2res_omp, obj.abstol);
            obj.fatalAssertLessThan(rel2res_omp, obj.reltol);

            %% free
            cholmod_l_free_factor(symbolic);
            cholmod_l_free_factor(numeric);

        end

        function test_cholmod_report(obj, cpx_A)

            %% small random sparse matrix the routine only prints stuff
            rng(obj.randseed);
            n = 2;
            A = sprand(n, n, 0.1);
            if cpx_A
                A = (1 + 2i) * A;
            end
            A = A + A';
            A = A + (n * n + 1) * speye(n, n);

            %% if no verbosity is wanted we skip
            if ~obj.verbose
                return;
            else
                fprintf('\n');
            end

            %% symbolic, numeric factorization, print and free
            symbolic = cholmod_l_analyze(A);
            numeric = cholmod_l_factorize(A, symbolic);

            cholmod_l_print_factor(symbolic);
            cholmod_l_print_factor(numeric);

            for print_level = 0:2
                cholmod_l_print_factor(symbolic, print_level);
                cholmod_l_print_factor(numeric, print_level);
            end

            cholmod_l_free_factor(symbolic);
            cholmod_l_free_factor(numeric);

        end
    end
end
