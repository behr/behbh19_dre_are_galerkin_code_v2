%TRY_ShiftedSolver    Playaround script for ShiftedSolver class.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all

%% data
instance = 'chip_v0';
data = load(instance);
A = data.A;
E = data.E;
n = size(A, 1);
shift = -100;

AshiftE = A + shift*E;

%% cholmod stuff
shifted_solver = ShiftedSolver(A, E);
for nrhs = 1:2
    B = rand(n, nrhs);

    f = @() shifted_solver.solve(shift, B);
    wtime_shifted = timeit(f, 1);

    f = @() AshiftE\B;
    wtime_backslash = timeit(f, 1);

    X_shifted = shifted_solver.solve(shift, B);
    abs2res_shifted = norm(AshiftE * X_shifted - B);
    rel2res_shifted = abs2res_shifted / norm(B);

    X_backslash = AshiftE \ B;
    abs2res_backslash = norm(AshiftE * X_backslash - B);
    rel2res_backslash = abs2res_backslash / norm(B);

    fprintf('NRHS = %d\n', nrhs);
    fprintf('SHIFTEDSOLVER: abs./rel. residual = %.2e/%.2e\n', abs2res_shifted, rel2res_shifted);
    fprintf('BACKSLASH:     abs./rel. residual = %.2e/%.2e\n', abs2res_backslash, rel2res_backslash);
    fprintf('SHIFTEDSOLVER: wall time = %.2e\n', wtime_shifted);
    fprintf('BACKSLASH:     wall time = %.2e\n', wtime_backslash);

end