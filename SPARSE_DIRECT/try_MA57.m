%TRY_MA57    Playaround script for MA57 interface.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all

%% data
instance = 'rail_20209';
data = load(instance);
A = data.A + data.E;
n = size(A, 1);
nrhs = 2;
RHS = rand(n, nrhs);

%% pattern of the symmetric A
[irn, jcn, vals] = find(triu(A));
irn = int64(irn);
jcn = int64(jcn);
n = size(A, 1);

%% symbolic analysis MA57AD
[keep, lfact, lifact] = MA57AD(n, irn, jcn);

%% numeric factorization MA57BD
[fact, ifact] = MA57BD(n, vals, keep, lfact, lifact);

%% solve MA57DD and MA57DD_omp
X = MA57DD(vals, irn, jcn, fact, ifact, RHS);
abs2res = norm(A*X-RHS);
rel2res = abs2res / norm(RHS);

X_omp = MA57DD_omp(vals, irn, jcn, fact, ifact, RHS);
abs2res_omp = norm(A*X_omp-RHS);
rel2res_omp = abs2res_omp / norm(RHS);

spparms('spumoni', 2);
abs2res_backslash = norm(A*(A\RHS) - RHS);
rel2res_backslash =  abs2res_backslash / norm(RHS);
spparms('spumoni', 0);

%% measure time
f = @() A\RHS;
wtime_backslash = timeit(f, 1);

f = @() MA57AD(n, irn, jcn);
wtime_MA57AD = timeit(f, 3);

f = @() MA57BD(n, vals, keep, lfact, lifact);
wtime_MA57BD = timeit(f, 2);

f = @() MA57DD(vals, irn, jcn, fact, ifact, RHS);
wtime_MA57DD = timeit(f, 1);

f = @() MA57DD_omp(vals, irn, jcn, fact, ifact, RHS);
wtime_MA57DD_omp = timeit(f, 1);


%% print results
fprintf('MA57DD:        abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res, rel2res);
fprintf('MA57DD_omp:    abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_omp, rel2res_omp);
fprintf('BACKSLASH:     abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_backslash, rel2res_backslash);
fprintf('TIME MA57AD:       %.2e\n', wtime_MA57AD);
fprintf('TIME MA57BD:       %.2e\n', wtime_MA57BD);
fprintf('TIME MA57DD:       %.2e\n', wtime_MA57DD);
fprintf('TIME MA57DD_omp:   %.2e\n', wtime_MA57DD_omp);
fprintf('TIME MA57:         %.2e\n', wtime_MA57AD + wtime_MA57BD + wtime_MA57DD);
fprintf('TIME MA57 (omp):   %.2e\n', wtime_MA57AD + wtime_MA57BD + wtime_MA57DD_omp);
fprintf('TIME BACKSLASH:    %.2e\n', wtime_backslash);
