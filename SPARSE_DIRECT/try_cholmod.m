%TRY_CHOLMOD    Playaround script for CHOLMOD interface.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all

%% data
instance = 'rail_20209';
data = load(instance);
A = data.E;
n = size(A, 1);
nrhs = 100;
RHS = rand(n, nrhs);

%% cholmod stuff

% symbolic
symbolic = cholmod_l_analyze(A);
cholmod_l_print_factor(symbolic);

% numeric
numeric = cholmod_l_factorize(A, symbolic);
cholmod_l_print_factor(numeric);

% solve, residual and time seq
X = cholmod_l_solve(numeric, RHS);
res = norm(A*X-RHS);
relres = res / norm(RHS);
f = @() cholmod_l_solve(numeric, RHS);
wtime = timeit(f, 1);
fprintf('seq: abs. / rel. 2-norm residual %.2e/%.2e\n', res, relres);
fprintf('seq: wtime = %.3e\n', wtime);

% solve, residual and time omp
divs = 4:2:32;
for div = divs
    X = cholmod_l_solve_omp(numeric, RHS, div);
    res = norm(A*X-RHS);
    relres = res / norm(RHS);
    f = @() cholmod_l_solve_omp(numeric, RHS, div);
    wtime = timeit(f, 1);
    fprintf('div = %d\n', div);
    fprintf('omp: abs. / rel. 2-norm residual %.2e/%.2e\n', res, relres);
    fprintf('omp: wtime = %.3e\n', wtime);
end

% free
cholmod_l_free_factor(numeric);
cholmod_l_free_factor(symbolic);
