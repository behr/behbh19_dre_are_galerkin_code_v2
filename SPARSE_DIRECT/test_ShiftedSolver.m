classdef test_ShiftedSolver < matlab.unittest.TestCase
    %TEST_SHIFTEDSOLVER   Test for ShiftedSolver class.
    %
    %   TEST_SHIFTEDSOLVER tests the functionality of the ShiftedSolver
    %   class.
    %
    %   TEST_SHIFTEDSOLVER properties:
    %       instance    - problem instance
    %       shift       - shift parameter
    %       modifier    - modifes the matrices
    %       nrhs        - number of right hand side
    %
    %   TEST_SHIFTEDSOLVER methods:
    %       test_ShiftedSol    - Test for ShiftedSolver.
    %
    %   See also SHIFTEDSOLVER, PERMUTATION_T, RADI and RADI_OPTIONS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        %instance = {'rail_371', 'rail_1357', 'conv_diff_1_100', 'fss_K10M2Q2_20', 'build', 'flow_v0', 'flow_v05'};
        instance = {'rail_79841'};
        nrhs = {1, 3, 5, 7};
        modifier = {@(x) x, @(x) -x};
        use_numerical = {false, true}
    end

    properties
        abstol = 1e-8;
        reltol = 1e-10;
        max_order_diff = 3;
        verbose = ~isCIrun();
        randseed = 1;
        shifts = [rand(1,3), (1+1i)*rand(1,3), 1i*rand(1,3), (-1+1i)*rand(1,3), -1*rand(1,3)];
    end

    methods(Test)
        function test_ShiftedSol(obj, instance, nrhs, modifier, use_numerical)

            %% load instance
            data = load(instance);
            A = data.A;
            n = size(A, 1);

            if isfield(data, 'E')
                E = data.E;
            else
                E = speye(size(A));
            end

            A = modifier(A);
            E = modifier(E);

            %% set random seed
            rng(obj.randseed);
            b = rand(n, nrhs);
            nrmb = norm(b);

            %% solve with shiftedsolver
            shiftedsol = ShiftedSolver(A, E, use_numerical);
            for shift = obj.shifts
                wtime_shifted = tic();
                x = shiftedsol.solve(shift, b);
                wtime_shifted = toc(wtime_shifted);

                %% compute residuals
                AshiftE = A + shift * E;
                abs2res = norm(AshiftE*x-b);
                rel2res = abs2res / nrmb;
                abs2res_order = log10(abs2res);

                %% backslash solution for comparison
                wtime_backslash = tic();
                x_backslash = AshiftE \ b;
                wtime_backslash = toc(wtime_backslash);
                abs2res_backslash = norm(AshiftE*x_backslash-b);
                rel2res_backslash = abs2res_backslash / nrmb;
                abs2res_backslash_order = log10(abs2res_backslash);

                %% print
                is_abstol = abs2res < obj.abstol;
                is_reltol = rel2res < obj.reltol;
                failed = ~(is_abstol && is_reltol);
                if failed || obj.verbose
                    fprintf('\n');
                    if failed
                        fprintf('FAILED:\n');
                    end
                    f(1) = fprintf('%s, %s, use_numerical = %d, shift = %.2e + %.2e*i, nrhs = %d\n', ...
                        instance, func2str(modifier), use_numerical, real(shift), imag(shift), nrhs);
                    f(2) = fprintf('is_abstol = %d\n', is_abstol);
                    f(3) = fprintf('is_reltol = %d\n', is_reltol);
                    f(4) = fprintf('SHIFTEDSOLVER: abs./rel. 2-norm res = %.2e / %.2e\n', abs2res, rel2res);
                    f(5) = fprintf('BACKSLASH:     abs./rel. 2-norm res = %.2e / %.2e\n', abs2res_backslash, rel2res_backslash);
                    f(6) = fprintf('               abs./rel. 2-norm tol = %.2e / %.2e\n', obj.abstol, obj.reltol);
                    f(7) = fprintf('SHIFTEDSOLVER: wall time = %.2e\n', wtime_shifted);
                    f(8) = fprintf('BACKSLASH:     wall time = %.2e\n', wtime_backslash);
                    fprintf('%s\n', repmat('-', 1, max(f)-1));
                end

                %% check
                obj.fatalAssertLessThan(abs2res, obj.abstol);
                obj.fatalAssertLessThan(rel2res, obj.reltol);
                obj.fatalAssertLessThan(abs(abs2res_order - abs2res_backslash_order), obj.max_order_diff);
            end
        end
    end
end
