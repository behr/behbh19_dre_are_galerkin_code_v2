classdef test_umfpack < matlab.unittest.TestCase
    %TEST_UMFPACK   Test for the UMFPACK interface.
    %
    %   TEST_UMFPACK tests the functionality of the
    %   UMFPACK interface.
    %
    %   TEST_UMFPACK properties:
    %       instance        - problem instance
    %       nrhs            - number of right hand side
    %       cpx_rhs         - complex or real right hand side
    %       cpx_A           - complex or real matrix A
    %       logical_A       - use logical A for symbolic analysis
    %
    %   TEST_UMFPACK methods:
    %       test_umfpack_interface - Test for UMFPACK interface.
    %       test_umfpack_report - Test for UMFPACK report routines.
    %
    %   See also UMFPACK_DL_SYMBOLIC, UMFPACK_ZL_SYMBOLIC,
    %   UMFPACK_DL_NUMERIC, UMFPACK_ZL_NUMERIC, UMFPACK_DL_SOLVE
    %   UMFPACK_ZL_SOLVE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        instance = {'rail_1357', ...
            'conv_diff_1_6400', 'conv_diff_2_6400', 'conv_diff_3_6400', 'conv_diff_4_6400', ...
            'fss_K200M5Q2_400', ...
            'beam', 'build', 'CDplayer', ...
            'filter2D', ...
            };
        instance_report = {'rail_371'};
        nrhs = {1, 5, 10};
        cpx_rhs = {false, true};
        cpx_A = {false, true};
        logical_A = {false, true};
    end

    properties
        abstol = 1e-6;
        reltol = 1e-8;
        verbose = ~isCIrun();
        randseed = 1;
    end

    methods(Test)
        function test_umfpack_interface(obj, instance, nrhs, cpx_rhs, cpx_A, logical_A)

            %% load instance
            data = load(instance);
            A = data.A;
            n = size(A, 1);

            %% set random seed
            rng(obj.randseed);
            b = rand(n, nrhs);
            if cpx_rhs
                b = b + 1i * rand(n, nrhs);
            end
            normb = norm(b);

            if cpx_A
                A = (1 + 2i) * A;
            end

            %% symbolic, numeric factorization, solve and free
            if isreal(A)
                if logical_A
                    symbolic = umfpack_dl_symbolic(logical(A));
                else
                    symbolic = umfpack_dl_symbolic(A);
                end
                numeric = umfpack_dl_numeric(A, symbolic);
                x = umfpack_dl_solve(A, numeric, b);
                x_omp = umfpack_dl_solve_omp(A, numeric, b);
                umfpack_dl_free_symbolic(symbolic);
                umfpack_dl_free_numeric(numeric);
            else
                if logical_A
                    symbolic = umfpack_zl_symbolic(logical(real(A)) | logical(imag(A)));
                else
                    symbolic = umfpack_zl_symbolic(A);
                end
                numeric = umfpack_zl_numeric(A, symbolic);
                x = umfpack_zl_solve(A, numeric, b);
                x_omp = umfpack_zl_solve_omp(A, numeric, b);
                umfpack_zl_free_symbolic(symbolic);
                umfpack_zl_free_numeric(numeric);
            end

            %% check residual
            abs2res = norm(A*x-b);
            rel2res = abs2res / normb;
            is_abstol = abs2res < obj.abstol;
            is_reltol = rel2res < obj.reltol;

            abs2res_omp = norm(A*x_omp-b);
            rel2res_omp = abs2res_omp / normb;
            is_abstol_omp = abs2res < obj.abstol;
            is_reltol_omp = rel2res < obj.reltol;

            failed = ~(is_abstol && is_reltol && is_abstol_omp && is_reltol_omp);

            %% backslash solution for comparison
            abs2res_backslash = norm(A*(A \ b)-b);
            rel2res_backslash = abs2res_backslash / normb;

            %% print on failure and check
            if failed || obj.verbose
                fprintf('\n');
                if failed
                    fprintf('FAILED:\n');
                end
                f(1) = fprintf('%s, nrhs = %d, cpx_rhs = %d, cpx_A = %d, logical_A = %d\n', instance, nrhs, cpx_rhs, cpx_A, logical_A);
                f(2) = fprintf('is_abstol     = %d\n', is_abstol);
                f(3) = fprintf('is_reltol     = %d\n', is_reltol);
                f(4) = fprintf('is_abstol_omp = %d\n', is_abstol_omp);
                f(5) = fprintf('is_reltol_omp = %d\n', is_reltol_omp);
                f(6) = fprintf('UMFPACK:      abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res, rel2res);
                f(7) = fprintf('UMFPACK OMP:  abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_omp, rel2res_omp);
                f(8) = fprintf('BACKSLASH:    abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_backslash, rel2res_backslash);
                f(9) = fprintf('              abs./rel. 2-norm tol      = %.2e/%.2e\n', obj.abstol, obj.reltol);
                fprintf('%s\n', repmat('-', 1, max(f-1)));
            end

            %% check
            obj.fatalAssertLessThan(abs2res, obj.abstol);
            obj.fatalAssertLessThan(rel2res, obj.reltol);
            obj.fatalAssertLessThan(abs2res_omp, obj.abstol);
            obj.fatalAssertLessThan(rel2res_omp, obj.reltol);
        end

        function test_umfpack_report(obj, cpx_A)

            %% small random sparse matrix the routine only prints stuff
            rng(obj.randseed);
            n = 2;
            A = sprand(n, n, 0.1) + (n * n + 1) * speye(n, n);

            if cpx_A
                A = (1 + 2i) * A;
            end

            %% if no verbosity is wanted we skip
            if ~obj.verbose
                return;
            else
                fprintf('\n');
            end

            %% symbolic, numeric factorization, report and free
            if isreal(A)
                symbolic = umfpack_dl_symbolic(A);
                numeric = umfpack_dl_numeric(A, symbolic);
                umfpack_dl_report_numeric(numeric);
                umfpack_dl_report_symbolic(symbolic);
                for print_level = 0:6
                    fprintf('print_level = %d\n', print_level);
                    umfpack_dl_report_numeric(numeric, print_level);
                    umfpack_dl_report_symbolic(symbolic, print_level);
                end
                umfpack_dl_free_symbolic(symbolic);
                umfpack_dl_free_numeric(numeric);
            else
                symbolic = umfpack_zl_symbolic(A);
                numeric = umfpack_zl_numeric(A, symbolic);
                umfpack_zl_report_numeric(numeric);
                umfpack_zl_report_symbolic(symbolic);
                for print_level = 0:6
                    fprintf('print_level = %d\n', print_level);
                    umfpack_zl_report_numeric(numeric, print_level);
                    umfpack_zl_report_symbolic(symbolic, print_level);
                end
                umfpack_zl_free_symbolic(symbolic);
                umfpack_zl_free_numeric(numeric);
            end
        end
    end
end
