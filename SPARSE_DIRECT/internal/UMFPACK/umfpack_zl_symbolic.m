function symbolic = umfpack_zl_symbolic(A)
%UMFPACK_ZL_SYMBOLIC    Interface to umfpack_zl_symbolic routine of
%  UMFPACK.
%
%  # Purpose:
%
%  Computes the symbolic factorization of a complex double square sparse matrix A.
%  A may be also sparse logical.
%
%  # Calling Sequence:
%
%    SYMBOLIC = UMFPACK_ZL_SYMBOLIC(A)
%
%  Internal function.
%
%  See also UMFPACK_ZL_REPORT_SYMBOLIC, UMFPACK_ZL_FREE_SYMBOLIC.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(1, 1);
nargoutchk(1, 1);
validateattributes(A, {'double', 'logical'}, {'square', '2d', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
assert(issparse(A) && (~isreal(A) || islogical(A)), '%s must be complex or logical sparse', inputname(1));
symbolic = mextools_call('umfpack_zl_symbolic', A);
