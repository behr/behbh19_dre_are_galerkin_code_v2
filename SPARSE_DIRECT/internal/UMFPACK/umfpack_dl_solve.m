function X = umfpack_dl_solve(A, numeric, B)
%UMFPACK_DL_SOLVE    Interface to umfpack_dl_solve routine of
%  UMFPACK
%
%  # Purpose:
%
%  Solve the linear system A*X = B using numeric factorization of the
%  complex double matrix A.
%
%  # Calling Sequence:
%
%    X = UMFPACK_DL_SOLVE(A, NUMERIC, B)
%
%  Internal function.
%
%  See also UMFPACK_DL_SOLVE_OMP.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(3, 3);
nargoutchk(1, 1);
validateattributes(A, {'double'}, {'real', 'square', '2d', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
assert(issparse(A), '%s must be sparse', inputname(1));
n = size(A, 1);
validateattributes(numeric, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(2), 2);
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(3), 3);
X = mextools_call('umfpack_dl_solve', A, numeric, B);
