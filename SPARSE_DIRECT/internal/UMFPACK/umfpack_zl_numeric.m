function numeric = umfpack_zl_numeric(A, symbolic)
%UMFPACK_ZL_NUMERIC    Interface to umfpack_zl_numeric routine of
%  UMFPACK.
%
%  # Purpose:
%
%  Computes the numerical factorization of a complex square sparse matrix A.
%
%  # Calling Sequence:
%
%    NUMERIC = UMFPACK_ZL_NUMERIC(A, SYMBOLIC)
%
%  Internal function.
%
%  See also UMFPACK_ZL_REPORT_NUMERIC, UMFPACK_ZL_FREE_NUMERIC.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(2, 2);
nargoutchk(1, 1);
validateattributes(A, {'double'}, {'square', '2d', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
assert(issparse(A) && ~isreal(A), '%s must be complex sparse', inputname(1));
validateattributes(symbolic, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(2), 2);
numeric = mextools_call('umfpack_zl_numeric', A, symbolic);
