function umfpack_zl_free_numeric(numeric)
%UMFPACK_ZL_FREE_NUMERIC    Interface to umfpack_zl_free_numeric routine of
%  UMFPACK.
%
%  # Purpose:
%
%  Clear memory of a numerical factorization.
%
%  # Calling Sequence:
%
%    UMFPACK_ZL_FREE_NUMERIC(NUMERIC)
%
%  Internal function.
%
%  See also UMFPACK_ZL_NUMERIC.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(1, 1);
nargoutchk(0, 0);
validateattributes(numeric, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1));
mextools_call('umfpack_zl_free_numeric', numeric);
