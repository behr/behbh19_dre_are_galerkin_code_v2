function umfpack_dl_report_numeric(numeric, print_level)
%UMFPACK_DL_REPORT_NUMERIC    Interface to umfpack_dl_report_numeric routine of
%  UMFPACK.
%
%  # Purpose:
%
%  Print information about numerical factorization.
%  PRINT_LEVEL must be a nonnegative double real scalar integer
%  smaller equals 6.
%
%  # Calling Sequence:
%
%    UMFPACK_DL_REPORT_NUMERIC(NUMERIC)
%    UMFPACK_DL_REPORT_NUMERIC(NUMERIC, PRINTLEVEL)
%
%  Internal function.
%
%  See also UMFPACK_DL_NUMERIC.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(1, 2);
nargoutchk(0, 0);
validateattributes(numeric, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1), 1);
if nargin == 1
    mextools_call('umfpack_dl_report_numeric', numeric);
else
    validateattributes(print_level, {'numeric'}, {'scalar', 'real', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0, '<=', 6}, mfilename, inputname(2), 2);
    mextools_call('umfpack_dl_report_numeric', numeric, print_level);
end
