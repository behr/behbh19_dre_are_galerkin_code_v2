function X = MA57DD(VALS, IRN, JCN, FACT, IFACT, B)
%MA57BD    Interface to MA57DD routine of MA57.
%
%  # Purpose:
%
%  Solve the linear system A X = B using the numeric
%  factoriuation FACT and IFACT of A obtained from
%  MA57BD. VALS, IRN and JCN is the coordinate storage
%  of the upper triagonaluar part of A.
%  A must be real and symmetric. B is the right hand
%  side and must be double real or complex dense.
%
%  # Calling Sequence:
%
%    X = MA57DD(VALS, IRN, JCN, FACT, IFACT, B)
%
%  Internal function.
%
%  See also MA57AD, MA57BD and TEST_MA57.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(6, 6);
nargoutchk(1, 1);
validateattributes(VALS, {'double'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
NE = length(VALS);
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(6), 6);
N = size(B, 1);
validateattributes(IRN, {'int64'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 1, '<=', N, 'numel', NE}, mfilename, inputname(2), 2);
validateattributes(JCN, {'int64'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 1, '<=', N, 'numel', NE}, mfilename, inputname(3), 3);
validateattributes(FACT, {'double'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(4), 4);
validateattributes(IFACT, {'int64'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 1}, mfilename, inputname(5), 5);
X = mextools_call('MA57DD', VALS, IRN, JCN, FACT, IFACT, B);
end
