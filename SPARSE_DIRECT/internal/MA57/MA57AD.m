function [KEEP, LFACT, LIFACT] = MA57AD(N, IRN, JCN)
%MA57AD    Interface to MA57AD routine of MA57.
%
%  # Purpose:
%
%  Perform symbolic analysis using the sparsity pattern
%  given in coordinate storage format. N is the order of the
%  matrix. IRN and JCN are row and column indices of the
%  nonzero entries of a symmetric sparse matrix.
%  IRN and JCN must hold the indices of the nonzero
%  entries in the upper triangular. MA57 is for symmetric matrices.
%  IRN and JCN must be of class int64.
%
%  # Calling Sequence:
%
%    [KEEP, LFACT, LIFACT] = MA57AD(N, IRN, JCN)
%
%  Internal function.
%
%  See also MA57BD, MA57DD and TEST_MA57.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(3, 3);
nargoutchk(3, 3);
validateattributes(N, {'numeric'}, {'scalar', 'nonempty', 'finite', 'nonnan', '>=', 1}, mfilename, inputname(1), 1);
validateattributes(IRN, {'int64'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 1, '<=', N}, mfilename, inputname(2), 2);
validateattributes(JCN, {'int64'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 1, '<=', N}, mfilename, inputname(3), 3);
assert(all(IRN <= JCN), '%s and %s do not represent the sparsity patter of the upper triangular part', inputname(2), inputname(3));
assert(length(IRN) == length(JCN), '%s and %s must be of same length', inputname(2), inputname(3));
[KEEP, LFACT, LIFACT] = mextools_call('MA57AD', N, IRN, JCN);
end
