function [FACT, IFACT] = MA57BD(N, VALS, KEEP, LFACT, LIFACT)
%MA57BD    Interface to MA57BD routine of MA57.
%
%  # Purpose:
%
%  Perform numerical factorization using information
%  from the symbolic analysis. N is the order of the
%  symmetric matrix and VALS contains the values of the
%  upper triangular of of the symmetri matrix.
%  KEEP, LFACT and LIFACT are returned by MA57AD and must be
%  unchanged.
%
%  # Calling Sequence:
%
%    [FACT, IFACT] = MA57BD(N, VALS, KEEP, LFACT, LIFACT)
%
%  Internal function.
%
%  See also MA57AD, MA57DD and TEST_MA57.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(5, 5);
nargoutchk(2, 2);
validateattributes(N, {'numeric'}, {'scalar', 'nonempty', 'finite', 'nonnan', '>=', 1}, mfilename, inputname(1), 1);
validateattributes(VALS, {'double'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(2), 2);
validateattributes(KEEP, {'int64'}, {'vector', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(3), 3);
validateattributes(LFACT, {'int64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 1}, mfilename, inputname(4), 4);
validateattributes(LIFACT, {'int64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 1}, mfilename, inputname(5), 5);
[FACT, IFACT] = mextools_call('MA57BD', N, VALS, KEEP, LFACT, LIFACT);
end
