function X = cholmod_l_solve_omp(numeric, B, divs)
%CHOLMOD_L_SOLVE_OMP    OpenMP parallelized interface to cholmod_l_solve
%  routine of CHOLMOD.
%
%  # Purpose:
%
%  Solves the linear system A*X = B using the NUMERIC factorization
%  of A. B must be a dense double real or complex matrix of suiteable size.
%  DIVS must be a positive double real integer scalar larger equals 4.
%  The default value is 6. DIVS is the number of columns of B each
%  OpenMP threads is using to solve the system A*X = B.
%  Depending on your number of cores, the NUMERIC factorization
%  and the number of columns of B the values has impact on performance.
%  The default value is 6. Usually a value larger than 12 does not make
%  sense and may lead to performance loss.
%  The OpenMP parallelism uses all available cores.
%  Attention: Then OpenMP environment variable OMP_NUM_THREADS
%  has no effect.
%
%  # Calling Sequence:
%
%    X = CHOLMOD_L_SOLVE_OMP(NUMERIC, B)
%    X = CHOLMOD_L_SOLVE_OMP(NUMERIC, B, DIVS)
%
%  Internal function.
%
%  See also CHOLMOD_L_ANALYZE, CHOLMOD_L_FACTORIZE and CHOLMOD_L_SOLVE_OMP.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(2, 3);
nargoutchk(1, 1);
validateattributes(numeric, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1), 1);
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(2), 2);
if nargin == 2
    X = mextools_call('cholmod_l_solve_omp', numeric, B);
else
    validateattributes(divs, {'numeric'}, {'scalar', 'real', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 4}, mfilename, inputname(3), 3);
    X = mextools_call('cholmod_l_solve_omp', numeric, B, divs);
end
