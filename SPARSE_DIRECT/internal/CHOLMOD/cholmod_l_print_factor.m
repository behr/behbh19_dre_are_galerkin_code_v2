function cholmod_l_print_factor(fac, print_level)
%CHOLMOD_L_PRINT_FACTOR    Interface to cholmod_l_print_factor routine of
%  CHOLMOD.
%
%  # Purpose:
%
%  Print information of a numeric or symbolic factorization.
%
%  # Calling Sequence:
%
%    CHOLMOD_L_PRINT_FACTOR(FAC)
%    CHOLMOD_L_PRINT_FACTOR(FAC, PRINT_LEVEL)
%
%  PRINT_LEVEL is a nonnegative double scalar integer.
%  PRINT_LEVEL controls the verbosity and can be 0, 1 or 2.
%
%  Internal function.
%
%  See also CHOLMOD_L_ANALYZE and CHOLMOD_L_FACTORIZE.
%
%   Author: Maximilian Behr

% This program is print software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the print Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(1, 2);
nargoutchk(0, 0);
validateattributes(fac, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1), 1);
if nargin == 1
    mextools_call('cholmod_l_print_factor', fac);
else
    validateattributes(print_level, {'numeric'}, {'scalar', 'real', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0, '<=', 2}, mfilename, inputname(2), 2);
    mextools_call('cholmod_l_print_factor', fac, print_level);
end
