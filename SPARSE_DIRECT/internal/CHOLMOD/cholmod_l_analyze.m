function symbolic = cholmod_l_analyze(A, NMETHODS)
%CHOLMOD_L_ANALYZE    Interface to cholmod_l_analyze routine of
%  CHOLMOD.
%
%  # Purpose:
%
%  Compute a symbolic factorization of a sparse double real or complex
%  square matrix A. %  A may be also sparse logical.
%  NMETHODS is a nonnegative scalar integer
%  value. NMETHODS controls the symbolic analysis and defines the
%  the fill-in reducing permutation methods tried during symbolic analysis.
%  The maximum value for NMETHODS is 9 and the minimum value is 0.
%  The default is 0.
%  A high value takes usually more time, but may lead to smaller
%  Cholesky factors. Compare also the CHOLMOD User Guide for the
%  the NMETHODS parameter in cholmod_common struct.
%  Usually there is no need to choose a specific value, you can
%  simply use the default 0.
%
%  # Calling Sequence:
%
%    SYMBOLIC = CHOLMOD_L_ANALYZE(A)
%    SYMBOLIC = CHOLMOD_L_ANALYZE(A, NMETHODS)
%
%  Internal function.
%
%  See also CHOLMOD_L_PRINT_FACTOR and CHOLMOD_L_FREE_FACTOR.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(1, 2);
nargoutchk(1, 1);
validateattributes(A, {'double', 'logical'}, {'square', '2d', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
assert(issparse(A), '%s must be sparse', inputname(1));
if nargin == 1
    symbolic = mextools_call('cholmod_l_analyze', A);
else
    validateattributes(NMETHODS, {'numeric'}, {'scalar', 'real', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0, '<=', 9}, mfilename, inputname(2), 2);
    symbolic = mextools_call('cholmod_l_analyze', A, NMETHODS);
end
