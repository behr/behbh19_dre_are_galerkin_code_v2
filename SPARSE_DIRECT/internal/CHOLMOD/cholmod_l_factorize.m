function numeric = cholmod_l_factorize(A, symbolic)
%CHOLMOD_L_FACTORIZE    Interface to cholmod_l_factorize routine of
%  CHOLMOD.
%
%  # Purpose:
%
%  Compute the numerical factorization of a sparse double real or complex
%  square matrix A using the SYMBOLIC factorization.
%  A must be hermitian. Internally the routine only
%  uses the upper triangular part and does not check if A is
%  actually hermitian. The matrix A must be positive or negative
%  definit. If not the routine throws an error.
%
%  # Calling Sequence:
%
%    NUMERIC = CHOLMOD_L_FACTORIZE(A, SYMBOLIC)
%
%  Internal function.
%
%  See also CHOLMOD_L_ANALYZE, CHOLMOD_L_PRINT_FACTOR and CHOLMOD_L_FREE_FACTOR.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(2, 2);
nargoutchk(1, 1);
validateattributes(A, {'double'}, {'square', '2d', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
assert(issparse(A), '%s must be sparse', inputname(1));
validateattributes(symbolic, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(2), 2);
numeric = mextools_call('cholmod_l_factorize', A, symbolic);
