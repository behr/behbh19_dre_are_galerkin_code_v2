function X = cholmod_l_solve(numeric, B)
%CHOLMOD_L_SOLVE    Interface to cholmod_l_solve routine of
%  CHOLMOD.
%
%  # Purpose:
%
%  Solves the linear system A*X = B using the NUMERIC factorization
%  of A.
%
%  # Calling Sequence:
%
%    X = CHOLMOD_L_SOLVE(NUMERIC, B)
%
%  Internal function.
%
%  See also CHOLMOD_L_ANALYZE, CHOLMOD_L_FACTORIZE and CHOLMOD_L_SOLVE_OMP.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(2, 2);
nargoutchk(1, 1);
validateattributes(numeric, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1), 1);
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(2), 2);
X = mextools_call('cholmod_l_solve', numeric, B);
