classdef (Abstract) Solver_DRE < custom_handle
    %SOLVER_DRE     Base class for deriving differential Riccati equation solvers.
    %
    %   SOLVER_DRE implements functionality to measure number
    %   of function calls and wall clock time.
    %
    %   SOLVER_DRE is a base class for single-step methods with constant
    %   step size.
    %
    %   SOLVER_DRE properties:
    %       t                       - double, scalar, nonnegative, current time
    %       h                       - double, scalar, positive, step size
    %       T                       - double, scalar, positive, final time
    %       options                 - SOLVER_DRE_options, options of solver
    %       calls_prepare           - number of prepare calls
    %       wtime_prepare           - elapsed time of all prepare calls
    %       calls_time_step         - number of time_step calls
    %       wtime_time_step         - elapsed time of all time_step calls
    %       calls_time_step_until   - number of time_step_until calls
    %       wtime_time_step_until   - elapsed time of all time_step_until calls
    %       calls_get_solution      - number of get_solution calls
    %       wtime_get_solution      - elapsed time of all get_solution calls
    %
    %   SOLVER_DRE methods:
    %       prepare             - Prepare function for solver.
    %       time_step           - Time step function for solver.
    %       time_step_until     - Time step function for solver.
    %       get_solution        - Return the approximation at the current time.
    %       validate_step_size  - Validation step sizes settings.
    %       print_status        - Print status information about ths solver.
    %
    %   SOLVER_DRE methods to implement:
    %       m_prepare       - Prepare function for solver.
    %       m_time_step     - Time step function for solver.
    %       m_get_solution  - Returns the approximation at current time.
    %
    %   See also ARE_GALERKIN_DRE, MODIFIED_DAVISON_MAKI_DRE and SPLITTING2_DRE.
    %
    %   Author: Maximilian Behr

    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = protected)
        % double, scalar, nonnegative, current time
        t = 0;
        % double, scalar, positive, step size
        h = 1;
        % double, scalar, positive, final time
        T = 2;
        % options of solver
        options;
        % number of prepare calls
        calls_prepare = int64(0);
        % elapsed time of all prepare calls
        wtime_prepare = 0;
        % number of time_step calls
        calls_time_step = int64(0);
        % elapsed time of all time_step calls
        wtime_time_step = 0;
        % number of time_step_until calls
        calls_time_step_until = int64(0);
        % elapsed time of all time_step_until calls
        wtime_time_step_until = 0;
        % number of get_solution calls
        calls_get_solution = int64(0);
        % elapsed time of all get_solution calls
        wtime_get_solution = 0;
    end

    methods(Abstract, Hidden = true, Access = protected)
        %M_PREPARE  Prepare function for solver.
        %
        %   M_PREPARE function is called before the time stepping
        %   functions are used.
        %
        %   Author: Maximilian Behr
        m_prepare(obj);

        %M_TIME_STEP  Time step function for solver.
        %
        %   M_TIME_STEP performs one time step.
        %
        %   Author: Maximilian Behr
        m_time_step(obj);

        %M_GET_SOLUTION  Returns the approximation at current time.
        %
        %   M_GET_SOLUTION returns the approximation at current time.
        %
        %   Author: Maximilian Behr
        m_get_solution(obj);
    end

    methods(Sealed = true)
        function prepare(obj)
            %PREPARE  Prepare function for solver.
            %
            %   PREPARE wraps m_prepare to measure elapsed time and counts
            %   the number of calls of m_prepare.
            %
            %   Author: Maximilian Behr
            temp = tic;
            obj.m_prepare();
            obj.wtime_prepare = obj.wtime_prepare + toc(temp);
            obj.calls_prepare = obj.calls_prepare + 1;
        end

        function time_step(obj)
            %TIME_STEP  Time step function for solver.
            %
            %   TIME_STEP wraps m_time_step to measure elapsed time and counts
            %   the number of calls of m_time_step.
            %
            %   Author: Maximilian Behr

            % variant below may cause problems in final step due to accuracy
            %assert(obj.t+obj.h <= obj.T, 'Cannot make time step, final Time reached, t = %f, h = %f, T = %f.', obj.t, obj.h, obj.T);
            assert(obj.t+obj.h <= obj.T+obj.h/4, 'Cannot make time step, final Time reached, t = %f, h = %f, T = %f.', obj.t, obj.h, obj.T);
            temp = tic;
            obj.m_time_step();
            obj.wtime_time_step = obj.wtime_time_step + toc(temp);
            obj.calls_time_step = obj.calls_time_step + 1;
        end

        function time_step_until(obj, t)
            %TIME_STEP_UNTIL  Time step function for solver.
            %
            %   TIME_STEP_UNTIL calls m_time_step until the desired time
            %   t has reached. The time t must be a multiple of the
            %   step size. The elapsed time is measured.
            %
            %   Author: Maximilian Behr
            validateattributes(t, {'numeric'}, {'real', '>=', 0, 'scalar', 'finite', 'nonnan'}, mfilename, inputname(2));
            assert(obj.t <= t, 'Desired t = %e is smaller the actual t = %e.', t, obj.t);
            assert(t <= obj.T, 'Desired t = %e is lager than the final T = %e.', t, obj.T);
            assert(mod(t, obj.h) == 0, 'Desired t = %e must be a multiple of h = %e\n', t, obj.h);
            temp = tic;
            %while obj.t + obj.h <= t, does not work due to floating point numbers
            while obj.t + obj.h <= (t + obj.h / 4)
                obj.m_time_step();
            end
            obj.wtime_time_step_until = obj.wtime_time_step_until + toc(temp);
            obj.calls_time_step_until = obj.calls_time_step_until + 1;
            assert(abs(t-obj.t) <= obj.t*100*eps+eps, 'Desired t = %e not exactly reached. Current time = %e reached. \n', t, obj.t);
        end

        function varargout = get_solution(obj)
            %GET_SOLUTION  Return the approximation at the current time.
            %
            %   GET_SOLUTION returns the approximation at the current time.
            %
            %   Author: Maximilian Behr
            temp = tic;
            [varargout{1:nargout}] = obj.m_get_solution();
            obj.wtime_get_solution = obj.wtime_get_solution + toc(temp);
            obj.calls_get_solution = obj.calls_get_solution + 1;
        end

        function validate_step_size(obj)
            %VALIDATE_STEP_SIZE     Checks if the properties h and T have valid values.
            %
            %   VALIDATE_STEP_SIZE checks if the properties h and T have valid values.
            %
            %   Author: Maximilian Behr
            validateattributes(obj.h, {'numeric'}, {'real', '>', 0, 'scalar', 'finite', 'nonnan'}, mfilename);
            validateattributes(obj.T, {'numeric'}, {'real', '>', 0, 'scalar', 'finite', 'nonnan'}, mfilename);
            assert(mod(obj.T, obj.h) == 0, 'Desired step size h = %e must be a multiple of final time T = %e\n', obj.h, obj.T);
        end
    end

    methods(Access = public)
        function print_status(obj)
            %PRINT_STATUS  Print status information about ths solver.
            %
            %   Author: Maximilian Behr
            fprintf('%s %s: t = %7.6f, h = %7.6f, T = %7.6f, %.2f%%\n', datestr(now), upper(class(obj)), obj.t, obj.h, obj.T, (obj.t / obj.T)*100);
        end
    end
end
