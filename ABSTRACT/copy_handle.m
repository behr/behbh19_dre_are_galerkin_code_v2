classdef copy_handle < custom_handle & matlab.mixin.Copyable
    %COPY_HANDLE     A subclass of custom handle with predefined deep copy
    %   function.
    %
    %   Author: Maximilian Behr

    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    methods(Access = protected)
        % Override copyElement method
        function cpy = copyElement(obj)
            classname = class(obj);
            constructor = str2func(classname);
            cpy = constructor();
            metaobj = metaclass(obj);
            props = metaobj.PropertyList;
            for i = 1:numel(props)
                if ~strcmp(props(i).SetAccess, 'immutable')
                    if isa(obj.(props(i).Name),'handle')
                        cpy.(props(i).Name) = copy(obj.(props(i).Name));
                    else
                        cpy.(props(i).Name) = obj.(props(i).Name);
                    end
                end
            end
        end
    end
end
