classdef Datalogger < custom_handle
    %DATALOGGER     Writing data to logfiles.
    %
    %   DATALOGGER properties:
    %       logfile         - set private, filename of logfile
    %       table_precision - set private, double, scalar precision for writing numerical data
    %       preamble        - set private, cell array, holds data for preamble of logfile
    %       header          - set private, char array, header of tabular numerical for logfile
    %       table           - set private, double or vpa, tabular of numerical data for logfile
    %
    %   DATALOGGER methods:
    %       Datalogger      - Constructor.
    %       add_to_preamble - Add data to preamble.
    %       set_header      - Set header of tabular data.
    %       set_table       - Set tabular data for writing.
    %       write           - Write data to file.
    %
    %   Author: Maximilian Behr
    
    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % set private, filename of logfile
        logfile
        % set private, numeric scalar, precision for writing numerical data
        table_precision
        % set private, cell array, holds data for preamble of logfile
        preamble
        % set private, char array, header of tabular numerical for logfile
        header
        % set private, matrix, tabular of numerical data for logfile
        table
    end
    
    methods(Access = public)
        function obj = Datalogger(logfile, precision)
            %DATALOGGER  Creates a Datalogger.
            %
            %   dlog = DATALOGGER(LOGFILE) creates a datalogger with
            %   LOGFILE as file for logging.
            %
            %   dlog = DATALOGGER(LOGFILE, PRECISION) creates a datalogger
            %   with LOGFILE as file for logging. PRECISION is the numerical
            %   precision for writing numerical data to the LOGFILE.
            %
            %   See also EXAMPLE_DAVISON_MAKI_FAIL and EXAMPLE_EIG_DECAY_ARE.
            %
            %   Author: Maximilian Behr
            validateattributes(logfile, {'char', 'string'}, {}, mfilename, inputname(1));
            obj.logfile = logfile;
            
            if nargin == 2
                validateattributes(precision, {'numeric'}, {'>', 0, 'finite', 'scalar', 'nonnan'}, mfilename, inputname(2));
                obj.table_precision = precision;
            else
                obj.table_precision = 16;
            end
            
            obj.preamble = {};
            obj.header = '';
            obj.table = [];
            
            %% set default information ot preamble
            obj.set_default_preamble();
        end
        
        function add_to_preamble(obj, name, value)
            %ADD_TO_PREAMBLE  Add name value pair to preamble.
            %
            %   DLOGGER.ADD_TO_PREAMBLE(NAME, VALUE) add a NAME/VALUE pair to
            %   the preamble.
            %
            %   Author: Maximilian Behr
            validateattributes(name, {'char', 'string'}, {}, mfilename, inputname(1));
            
            % check for suitable print format string
            identifier = '%s';
            if isinteger(value) || islogical(value)
                identifier = '%d';
            end
            if isnumeric(value)
                if mod(value, 1) == 0
                    identifier = '%d';
                else
                    identifier = '%.2e';
                end
            end
            
            % add to preamble
            obj.preamble{end+1} = struct('name', name, 'identifier', identifier, 'value', value);
        end
        
        function set_header(obj, header)
            %SET_HEADER  Set header to the Datalogger.
            %
            %   DLOGGER.SET_HEADER() write collected data from DLOGGER
            %   to the logfile.
            %
            %   Author: Maximilian Behr
            validateattributes(header, {'char', 'string'}, {}, mfilename, inputname(1));
            obj.header = header;
        end
        
        function set_table(obj, table)
            %SET_TABLE  Set table to the Datalogger.
            %
            %   DLOGGER.SET_TABLE() write collected data from DLOGGER
            %   to the logfile.
            %
            %   Author: Maximilian Behr
            obj.table = table;
        end
        
        function write(obj)
            %WRITE  Write collected data to file.
            %
            %   DLOGGER.WRITE() write collected data from DLOGGER
            %   to the logfile.
            %
            %   Author: Maximilian Behr
            
            % write preamble
            first = true;
            for i = 1:length(obj.preamble)
                str = obj.preamble{i};
                if first
                    dlmwrite(obj.logfile, sprintf(strcat('%s=', str.identifier), str.name, str.value), 'delimiter', '');
                else
                    dlmwrite(obj.logfile, sprintf(strcat('%s=', str.identifier), str.name, str.value), 'delimiter', '', '-append');
                end
                first = false;
            end
            
            % write header for table
            dlmwrite(obj.logfile, obj.header, 'delimiter', '', '-append');
            
            % write table
            if ~isa(obj.table,'sym')
            %if true
                dlmwrite(obj.logfile, obj.table, 'delimiter', ',', '-append', 'precision', obj.table_precision);
            else
                % dlmwrite is horrible slow for vpa / symbolic matrices
                
                % store and set number of digits
                digi = digits();
                digits(obj.table_precision);
                
                % convert to matrix of string with desired
                tablestring = char(vpa(obj.table));
                
                % restore digits
                digits(digi);
                
                % extract rows
                tablestring = extractBetween(tablestring,'[',']');
                
                % write to file
                fid = fopen(obj.logfile, 'a');
                for i = 1:size(tablestring, 1)
                   temp = tablestring{i};
                   temp = strrep(temp,'[','');
                   temp = strrep(temp,']','');
                   temp = strrep(temp,', ',',');
                   %disp(temp);
                   fprintf(fid,'%s\n', temp);
                end
                fclose(fid);
            end
        end
    end
    
    methods(Access = private)
        function set_default_preamble(obj)
            %SET_DEFAULT_PREAMBLE  Set default preamble.
            %
            %   SET_DEFAULT_PREAMBLE() set information about machine, matlab
            %   memory, git, host and user.
            %
            %   Author: Maximilian Behr
            
            % get information
            timenow = datestr(now);
            hostname = getComputerName();
            user = getUserName();
            mycpuinfo = cpuinfo();
            [totalmem, usedmem, freemem] = meminfo();
            gitinfo = GitInfo();
            [blas, lapack] = getBLASLAPACK();
            
            % general
            obj.add_to_preamble('Time', timenow);
            obj.add_to_preamble('Host', hostname);
            obj.add_to_preamble('User', user);
            obj.add_to_preamble('date', datestr(datetime('now')));
            
            % cpu and hardware
            obj.add_to_preamble('CPU-Name', mycpuinfo.Name);
            obj.add_to_preamble('CPU-Clock', mycpuinfo.Clock);
            obj.add_to_preamble('CPU-Cache', mycpuinfo.Cache);
            obj.add_to_preamble('CPU-NumProcessors', mycpuinfo.NumProcessors);
            obj.add_to_preamble('CPU-OSType', mycpuinfo.OSType);
            obj.add_to_preamble('CPU-OSVersion', mycpuinfo.OSVersion);
            obj.add_to_preamble('Total Mem GB', totalmem);
            obj.add_to_preamble('Used Mem GB', usedmem);
            obj.add_to_preamble('Free Mem GB', freemem);
            
            % software
            obj.add_to_preamble('Version', version());
            obj.add_to_preamble('MATLAB INTERNAL BLAS', blas);
            obj.add_to_preamble('MATLAB INTERNAL LAPACK', lapack);
            [ver_umfpack, ver_cholmod, ver_amd, ver_colamd, ver_symamd, ver_metis, ver_suitesparseqr] = getSUITESPARSE();
            obj.add_to_preamble('MATLAB INTERNAL UMFPACK', ver_umfpack);
            obj.add_to_preamble('MATLAB INTERNAL CHOLMOD', ver_cholmod);
            obj.add_to_preamble('MATLAB INTERNAL AMD', ver_amd);
            obj.add_to_preamble('MATLAB INTERNAL COLAMD', ver_colamd);
            obj.add_to_preamble('MATLAB INTERNAL SYMAMD', ver_symamd);
            obj.add_to_preamble('MATLAB INTERNAL METIS', ver_metis);
            obj.add_to_preamble('MATLAB INTERNAL SUITESPARSQR', ver_suitesparseqr);
            [mkl_debug_cpu_type, mkl_enable_instructions] = getMKLENVVARS();
            obj.add_to_preamble('ENV MKL_DEBUG_CPU_TYPE', mkl_debug_cpu_type);
            obj.add_to_preamble('ENV MKL_ENABLE_INSTRUCTIONS', mkl_enable_instructions);
            obj.add_to_preamble('MATLAB MAXNUMCOMPTHEADS', maxNumCompThreads);
            
            % git
            obj.add_to_preamble('Project remote-url', gitinfo.remote_url);
            obj.add_to_preamble('Project git-sha', gitinfo.sha);
            obj.add_to_preamble('Project git-branch', gitinfo.branch);
            
            % MEX-M.E.S.S. and C-M.E.S.S. information
            try
                obj.add_to_preamble('C-M.E.S.S. git-branch', mess_git_branch());
                obj.add_to_preamble('C-M.E.S.S. git-id', mess_git_id());
                mess_ver = sprintf('%d.%d.%d', mess_version_major(), mess_version_minor(), mess_version_patch());
                obj.add_to_preamble('C-M.E.S.S. version', mess_ver);
            catch %#ok<*CTCH>
            end
        end
    end
end
