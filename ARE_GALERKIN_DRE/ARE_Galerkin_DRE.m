classdef ARE_Galerkin_DRE < Solver_DRE
    %ARE_GALERKIN_DRE   Implements a Galerkin solver for the symmetric autonomous
    %   differential Riccati equation based on the solution of the algebraic
    %   Riccati equation.
    %
    %   Differential Riccati Equation:
    %
    %       \dot{X}    = A' X   +    X A -    X B B' X   + C'C, X(0)=0  (DRE)
    %       E'\dot{X}E = A' X E + E' X A - E' X B B' X E + C'C, X(0)=0  (GDRE)
    %
    %   The class first solves the algebraic Riccati equation:
    %
    %       0 = A' X      +    X A   -    X B B' X   + C'C    (ARE)
    %       0 = A' X E    + E' X A   - E' X B B' X E + C'C    (GARE)
    %
    %   for X = Z Z' and Z is a low-rank factor of the solution.
    %   The trialspace for X is then formed from the compact truncated singular
    %   value decomposition of Z.
    %
    %   ARE_GALERKIN_DRE private properties:
    %       A                   - set private, double, n-x-n sparse matrix A
    %       E                   - set private, double, n-x-n sparse matrix E or empty for identity
    %       B                   - set private, double, n-x-b dense matrix B
    %       C                   - set private, double, c-x-n dense matrix C
    %       Z                   - set private, double, n-x-z dense low-rank solution of the ARE/GARE
    %       trunc_Z             - set private, double, n-x-z dense truncated low-rank solution of the ARE/GARE
    %       trunc_S             - set private, double, z-x-z dense singular values of trunc_Z
    %       trunc_S2            - set private, double, z-x-z dense squared singular values of trunc_Z
    %       trunc_Q             - set private, double, n-x-z dense orthogonal matrix of singular vectors of trunc_Z
    %       trunc_abs2res_are   - set private, double, scalar, positive, abs. 2-norm residual of trunc_Z for ARE/GARE
    %       trunc_rel2res_are   - set private, double, scalar, positive, rel. 2-norm residual of trunc_Z for ARE/GARE
    %       proj_A              - set private, double, z-x-z dense projected matrix A
    %       proj_B              - set private, double, z-x-z dense projected matrix B
    %       proj_C              - set private, double, z-x-z dense projected matrix C
    %       proj_X0             - set private, double, z-x-z dense projection initial condition
    %       RADI_stat           - set private, RADI_status instance
    %       mdm_dre             - set private, MDM_SYM_DRE instance, solver for projected system
    %
    %   ARE_GALERKIN_DRE methods:
    %       are_galerkin_dre    - Constructor.
    %
    %   ARE_GALERKIN_DRE protected methods:
    %       m_prepare                               - Prepare the solver, solve ARE, compute truncated svd and setup small-scale system.
    %       m_time_step                             - Perform a time step.
    %       m_get_solution                          - Return the approximation QXQ' at current time.
    %
    %   See also SOLVER_DRE, ARE_GALERKIN_DRE_OPTIONS and TEST_ARE_GALERKIN_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % set private, double, n-x-n sparse matrix A
        A
        % set private, double, n-x-n sparse matrix E or empty for identity
        E
        % set private, double, n-x-b dense matrix B
        B
        % set private, double, c-x-n dense matrix C
        C
        % set private, double, n-x-z dense low-rank solution of the ARE/GARE
        Z
        % set private, double, n-x-z dense truncated low-rank solution of the ARE/GARE
        trunc_Z
        % set private, double, z-x-z dense singular values of trunc_Z
        trunc_S
        % set private, double, z-x-z dense squared singular values of trunc_Z
        trunc_S2
        % set private, double, n-x-z dense orthogonal matrix of singular vectors of trunc_Z
        trunc_Q
        % set private, double, scalar, positive, abs. 2-norm residual of trunc_Z for ARE/GARE
        trunc_abs2res_are
        % set private, double, scalar, positive, rel. 2-norm residual of trunc_Z for ARE/GARE
        trunc_rel2res_are
        % set private, double, z-x-z dense projected matrix A
        proj_A
        % set private, double, z-x-z dense projected matrix B
        proj_B
        % set private, double, z-x-z dense projected matrix C
        proj_C
        % set private, double, z-x-z dense projection initial condition
        proj_X0
        % set private, double, 1-norm of matrix exponential of projected hamiltonian
        nrm1_exp_hH
        % set private, RADI_status instance
        RADI_stat
    end

    properties(Access = private)
        % private, MDM_SYM_DRE solver
        mdm_solver = []
        % private, MDM_DRE_options
        mdm_solver_opt = []
    end

    methods

        function obj = ARE_Galerkin_DRE(A, E, B, C, opt, h, T)
            %ARE_GALERKIN_DRE  Constructor of class.
            %
            %   GAL = ARE_GALERKIN_DRE(A, M, B, C, OPT, H, T) creates an
            %   instance of the class. OPT must be an instance
            %   of ARE_Galerkin_DRE options class. The matrices A and M must be
            %   double sparse and square. If M is empty then the
            %   identity matrix is used. B and C must be real
            %   and dense. H is the step size and T the final time.
            %
            %   See also ARE_GALERKIN_DRE_OPTIONS and SOLVER_DRE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(A, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
            assert(issparse(A), '%s must be sparse', inputname(1));
            n = size(A, 1);
            if ~isempty(E)
                validateattributes(E, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan', 'size', [n, n]}, mfilename, inputname(2));
                assert(issparse(E), '%s must be sparse', inputname(2))
            end
            validateattributes(B, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [n, NaN]}, mfilename, inputname(3));
            validateattributes(C, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [NaN, n]}, mfilename, inputname(4));
            validateattributes(opt, {'ARE_Galerkin_DRE_options'}, {}, mfilename, inputname(5));

            %% set arguments to properties
            obj.E = E;
            obj.A = A;
            obj.B = B;
            obj.C = C;
            obj.options = copy(opt);

            %% set integration parameters
            obj.t = 0;
            obj.h = h;
            obj.T = T;
            obj.validate_step_size();
        end
    end

    methods(Access = protected)

        function m_prepare(obj)
            %M_PREPARE  Prepare the solver for iteration.
            %
            %   MOD.M_PREPARE() solves the ARE and prepares the
            %   modified Davison Maki solver.
            %
            %   Author: Maximilian Behr

            %% solve algebraic riccati equation
            [obj.Z, obj.RADI_stat] = RADI(obj.A, obj.E, obj.B, obj.C, obj.options.RADI_opt);
            fprintf('%s %s: abs./rel. 2-norm residual   = %.2e/%.2e\n', datestr(now), upper(class(obj)), obj.RADI_stat.abs2res, obj.RADI_stat.rel2res);
            fprintf('%s %s: RADI Iterations             = %d\n', datestr(now), upper(class(obj)), obj.RADI_stat.iter);
            fprintf('%s %s: RADI Time                   = %.2e\n', datestr(now), upper(class(obj)), obj.RADI_stat.time_overall);
            fprintf('%s %s: Size(Z)                     = %d-x-%d\n', datestr(now), upper(class(obj)), size(obj.Z));

            %% perform svd truncation
            [obj.trunc_Z, obj.trunc_Q, obj.trunc_S] = truncate_ZZT(obj.Z, obj.options.trunc_tol);
            tol = obj.trunc_S(1,1)*obj.options.trunc_tol;
            obj.trunc_S2 = obj.trunc_S.^2;
            fprintf('%s %s: Truncated columns of Z from %d to %d with tolerance %.2e\n', ...
                datestr(now), upper(class(obj)), size(obj.Z, 2), size(obj.trunc_Z, 2), tol);
            obj.trunc_abs2res_are = res2_ARE(obj.A, obj.E, obj.B, obj.C, obj.trunc_Z);
            obj.trunc_rel2res_are = obj.trunc_abs2res_are / norm(obj.C)^2;
            fprintf('%s %s: Truncated abs./rel. 2-norm residual = %.2e/%.2e\n', datestr(now), upper(class(obj)), obj.trunc_abs2res_are, obj.trunc_rel2res_are);


            %% options for modified davison maki solver
            symmetric = true;
            use_mex = true;
            obj.mdm_solver_opt = MDM_DRE_options(symmetric, use_mex);

            %% handle non trivial matrix E
            if isempty(obj.E)
                EinvQinf = obj.trunc_Q;
            else
                EinvQinf = obj.E \ obj.trunc_Q;
            end

            %% compute the projected system
            if ismember(obj.options.ansatz, [ARE_Galerkin_DRE_Ansatz_t.CONSTANT_PLUS, ARE_Galerkin_DRE_Ansatz_t.CONSTANT_PLUS_RESIDUAL])
                obj.proj_B = obj.trunc_Q' * obj.B;
                proj_S = obj.proj_B*obj.proj_B';
                temp_proj_A = obj.trunc_Q' * obj.A * EinvQinf;
                obj.proj_A = temp_proj_A - proj_S * obj.trunc_S2;
                obj.proj_C = obj.C*EinvQinf;
                obj.proj_X0 = obj.trunc_S2;

                %% add residual
                if obj.options.ansatz == ARE_Galerkin_DRE_Ansatz_t.CONSTANT_PLUS_RESIDUAL
                    temp1 = temp_proj_A'*obj.trunc_S2;
                    temp2 = obj.trunc_S2*obj.proj_B;
                    proj_Q = -(temp1 + temp1' - temp2*temp2' + obj.proj_C'*obj.proj_C);
                else
                    proj_Q = zeros(size(obj.proj_A));
                end

                %% prepare solver for projected system
                obj.mdm_solver = MDM_SYM_DRE(obj.proj_A, [], -proj_S, proj_Q, obj.proj_X0, obj.mdm_solver_opt, obj.h, obj.T);
                obj.mdm_solver.prepare();
            else
                obj.proj_B = obj.trunc_Q' * obj.B;
                proj_S = obj.proj_B*obj.proj_B';
                obj.proj_A = obj.trunc_Q' * obj.A * EinvQinf;
                obj.proj_C = obj.C*EinvQinf;
                proj_Q = obj.proj_C'*obj.proj_C;
                obj.proj_X0 = zeros(size(obj.proj_A));

                %% prepare solver for projected system
                obj.mdm_solver = MDM_SYM_DRE(obj.proj_A, [], proj_S, proj_Q, obj.proj_X0, obj.mdm_solver_opt, obj.h, obj.T);
                obj.mdm_solver.prepare();
            end

            %% get 1-norm of matrix exponential of projected hamiltonian
            obj.nrm1_exp_hH = obj.mdm_solver.nrm1_exp_hH;
        end

        function m_time_step(obj)
            %M_TIME_STEP  Time steppping function for solver.
            %
            %   MOD.M_TIME_STEP() calls the time_step function
            %   of the modified Davison Maki solver.
            %   The time of the are_galerkin_dre solver is updated
            %   accordingly.
            %
            %   Author: Maximilian Behr

            obj.mdm_solver.time_step();
            obj.t = obj.mdm_solver.t;
        end

        function [Q, Xt] = m_get_solution(obj)
            %M_GET_SOLUTION  Return the current approximation as Q*Xt*Q'.
            %
            %   Author: Maximilian Behr
            if ismember(obj.options.ansatz, [ARE_Galerkin_DRE_Ansatz_t.CONSTANT_PLUS, ARE_Galerkin_DRE_Ansatz_t.CONSTANT_PLUS_RESIDUAL])
                Q = obj.trunc_Q;
                Xt = obj.trunc_S2 - obj.mdm_solver.get_solution();
            else
                Q = obj.trunc_Q;
                Xt = obj.mdm_solver.get_solution();
            end
        end
    end
end
