%TRY_ARE_GALERKIN_DRE    Playaround script for ARE_Galerkin_DRE solver.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% problem data
instance = 'rail_371';
data = load(instance);
A = data.A;
B = data.B;
C = data.C;
E = data.E;
n = size(A, 1);
Z0 = zeros(n, 1);
T = 2^(-4);
h_ref = 2^(-14);
log_mod = 2^3;

%% picture of plot and diary
mypic = sprintf('%s_%s_h_ref%.2e_T_%.2e.fig', mfilename, instance, h_ref, T);
mydiary = sprintf('%s_%s_h_ref%.2e_T_%.2e.log', mfilename, instance, h_ref, T);
diary(mydiary)

%% create options for solver
gal_opt = ARE_Galerkin_DRE_options();
gal = ARE_Galerkin_DRE(A, E, B, C, gal_opt, h_ref, T);
gal.prepare();

%% solve ARE to track convergence to stationary point
radi_opt = RADI_options();
radi_opt.rel2res_tol = 1e-19;
radi_opt.abs2res_tol = 1e-19;
radi_opt.maxiter = 400;
radi_opt.verbose = true;
[Z, stat] = RADI(A, E, B, C, radi_opt);
nrm2C = norm(C)^2;
abs2res = res2_ARE(A, E, B, C, Z);
rel2res = abs2res / nrm2C;
nrm2Xinf = norm(Z)^2;
fprintf('abs./rel. 2-norm residual = %.2e/%.2e, ||Xinf|| = %.2e\n', abs2res, rel2res, nrm2Xinf);

%% integrate and compare
results = [];
i = 0;
temp = tic();
while gal.t < gal.T

    % integrate with fine solver
    t = tic();
    gal.time_step();
    t = toc(t);
    temp2 = toc(temp);

    if mod(i, log_mod) == 0
        fprintf('walltime: time step = %.2e\n', t);

        % get solutions and compare
        [Q, X] = gal.get_solution();

        % compute distance to stationary point
        t = tic();
        nrm2_Xt = two_norm_sym_handle(@(x) Q*(X * (Q' * x)), n);
        diff_Xinf = two_norm_sym_handle(@(x) Z*(Z' * x)-Q*(X * (Q' * x)), n);
        abs2res_Xt = res2_ARE(A, E, B, C, Q, X);
        rel2res_Xt = abs2res_Xt / nrm2C;
        t = toc(t);
        fprintf('walltime: norm computation = %.2e\n', t);

        results(end+1, :) = [gal.t, nrm2_Xt, diff_Xinf, abs2res_Xt, rel2res_Xt]; %#ok<*SAGROW>

        % print results
        est_rem_time = abs((temp2/gal.t)*gal.T - temp2);
        fprintf('%s - %s: T = %.2e, href = %.2e, t = %.2e, %.2f %%, est. rem. time = %.2e\n', ...
            datestr(now), instance, T, h_ref, gal.t, gal.t/T*100, est_rem_time);
        fprintf('\tnorm Xt = %.2e, diff Xinf = %.2e\n', nrm2_Xt, diff_Xinf);
        fprintf('\tabs./rel. 2-norm residual Xt = %e/%e\n', abs2res_Xt, rel2res_Xt);
    end
    i = i + 1;
end

%% plot results
m = 2;
n = 3;

% titles and plot commands
titles = { ...
    '||Xref||', ...
    '||Xt - Xref||', ...
    '||R(Xt)||_2', ...
    '||R(Xt)||_2 / ||C||_2^2', ...
    };

% plot commands
plot_commands = {; ...
    @() semilogy(results(:, 1), results(:, 2)), ...
    @() semilogy(results(:, 1), results(:, 3)), ...
    @() semilogy(results(:, 1), results(:, 4)), ...
    @() semilogy(results(:, 1), results(:, 5)), ...
    };

for i = 1:numel(titles)
    subplot(m, n, i);
    plot_command = plot_commands{i};
    plot_command();
    title(titles{i});
end

hgsave(gcf, mypic);
diary('off');