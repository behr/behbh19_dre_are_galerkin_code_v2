classdef test_ARE_Galerkin_DRE < matlab.unittest.TestCase
    %TEST_ARE_GALERKIN_DRE  Test for ARE_Galerkin_DRE.
    %
    %   TEST_ARE_GALERKIN_DRE compares the following solver:
    %
    %       * ARE_Galerkin_DRE
    %       * MDM_FAC_SYM_DRE
    %       * Splitting2_DRE
    %
    %   See also ARE_GALERKIN_DRE, SPLITTING2_DRE and MDM_FAC_SYM_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %

    properties(TestParameter)
        problem = { ...
            struct('instance', 'conv_diff_1_100', 'use_mdm', true, 'T', 2^(-2), 'h', 2^(-12), 'reltol', 1e-6) ...
            struct('instance', 'conv_diff_2_100', 'use_mdm', true, 'T', 2^(-2), 'h', 2^(-12), 'reltol', 1e-6) ...
            struct('instance', 'conv_diff_3_100', 'use_mdm', true, 'T', 2^(-2), 'h', 2^(-12), 'reltol', 1e-6) ...
            struct('instance', 'conv_diff_4_100', 'use_mdm', true, 'T', 2^(-2), 'h', 2^(-12), 'reltol', 1e-6) ...
            struct('instance', 'rail_371', 'use_mdm', true, 'T', 2^2, 'h', 2^(-4), 'reltol', 1e-6) ...
            struct('instance', 'rail_1357', 'use_mdm', false, 'T', 2^(-5), 'h', 2^(-10), 'reltol', 1e-6) ...
            %struct('instance', 'rail_5177', 'use_mdm', false, 'T', 2^(-5), 'h', 2^(-10), 'reltol', 1e-6) ...
            %struct('instance', 'rail_20209', 'use_mdm', false, 'T', 2^(-5), 'h', 2^(-10), 'reltol', 1e-6) ...
            };
    end

    properties
        verbose = ~isCIrun();
    end

    methods(Test)

        function test(obj, problem)

            %% load data
            instance = problem.instance;
            use_mdm = problem.use_mdm;
            T = problem.T;
            h = problem.h;
            reltol = problem.reltol;
            data = load(instance);
            A = data.A;
            B = full(data.B);
            C = full(data.C);
            n = size(A, 1);
            Z0 = zeros(n, 1);

            if isfield(data, 'E')
                E = data.E;
            else
                E = speye(n, n);
            end

             %% skip some tests on ci run for speed up
            if isCIrun() && any(strcmp({'rail_371', 'rail_1357', 'rail_5177', 'rail_20209', 'rail_79841'}, instance))
               return;
            end

            if obj.verbose
               fprintf('\n');
            end

            %% collect solver in cell array
            solvers = {};

            %% create are_galerkin_dre solvers
            gal_opt = ARE_Galerkin_DRE_options();
            gal_opt.RADI_opt.use_shifted_solver = true;
            gal_opt.ansatz = ARE_Galerkin_DRE_Ansatz_t.CONSTANT_PLUS;
            sol = ARE_Galerkin_DRE(A, E, B, C, gal_opt, h, T);
            solvers{end+1} = struct('solver', sol, 'name', 'gal_const', 'low_rank', true, 'galerkin', true);

            gal_opt.ansatz = ARE_Galerkin_DRE_Ansatz_t.CONSTANT_PLUS_RESIDUAL;
            sol = ARE_Galerkin_DRE(A, E, B, C, gal_opt, h, T);
            solvers{end+1} = struct('solver', sol, 'name', 'gal_const_res', 'low_rank', true, 'galerkin', true);

            gal_opt.ansatz = ARE_Galerkin_DRE_Ansatz_t.STANDARD;
            sol = ARE_Galerkin_DRE(A, E, B, C, gal_opt, h, T);
            solvers{end+1} = struct('solver', sol, 'name', 'gal_std', 'low_rank', true, 'galerkin', true);

            %% create splitting scheme solvers
            split_opt = Splitting2_DRE_options(Splitting2_t.SPLITTING_SYMMETRIC8);
            sol = Splitting2_DRE(A, E, B, C, Z0, split_opt, h, T);
            solvers{end+1} = struct('solver', sol, 'name', 'split', 'low_rank', true, 'galerkin', false);

            split_opt.decomposition_type = Decomposition_t.DECOMPOSITION_BANDED;
            sol = Splitting2_DRE(A, E, B, C, Z0, split_opt, h, T);
            solvers{end+1} = struct('solver', sol, 'name', 'split_banded', 'low_rank', true, 'galerkin', false);

            split_opt.decomposition_type = Decomposition_t.DECOMPOSITION_SPARSE_DIRECT;
            sol = Splitting2_DRE(A, E, B, C, Z0, split_opt, h, T);
            solvers{end+1} = struct('solver', sol, 'name', 'split_sparse_direct', 'low_rank', true, 'galerkin', false);

            %% create modified davison maki solver
            if use_mdm
                mdm_fac_sym_opt = MDM_DRE_options();
                mdm_fac_sym_opt.symmetric = true;
                mdm_fac_sym_opt.use_mex = true;
                sol = MDM_FAC_SYM_DRE(full(A), full(E), B, C, Z0, mdm_fac_sym_opt, h, T);
                solvers{end+1} = struct('solver', sol, 'name', 'mdm_mex', 'low_rank', false, 'galerkin', false);
                mdm_fac_sym_opt.use_mex = false;
                sol = MDM_FAC_SYM_DRE(full(A), full(E), B, C, Z0, mdm_fac_sym_opt, h, T);
                solvers{end+1} = struct('solver', sol, 'name', 'mdm', 'low_rank', false, 'galerkin', false);
            end

            %% solve algebraic riccati equation to monitor convergence
            radi_opt = RADI_options();
            radi_opt.maxiter = 500;
            [Zinf, radi_stat] = RADI(A, E, B, C, radi_opt);
            if obj.verbose
                fprintf('\n');
                fprintf('RADI: iterations                   = %d\n', radi_stat.iter);
                fprintf('RADI: abs./rel. 2-norm residual    = %d\n', radi_stat.abs2res, radi_stat.rel2res);
            end

            %% warm up tic / toc
            for i = 1:100000
                t = toc(tic()); %#ok<NASGU>
            end

            %% prepare the solvers
            for i = 1:numel(solvers)
                sol = solvers{i}.solver;
                fprintf('Prepare solver: %s\n', solvers{i}.name);
                sol.prepare();
                fprintf('Prepare finished: %s\n', solvers{i}.name);
            end

            %% solve and compare solution
            solvers_time = zeros(1, numel(solvers));
            t = 0;
            while t < T

                % perform a time step
                for i = 1:numel(solvers)
                   sol = solvers{i}.solver;
                   wtime = tic();
                   sol.time_step();
                   wtime = toc(wtime);
                   solvers_time(i) = solvers_time(i) + wtime;
                end

                % check if solvers are synchronous
                for i = 2:numel(solvers)
                    sol1 = solvers{i-1}.solver;
                    sol2 = solvers{i}.solver;
                    assert(sol1.t == sol2.t, 'Solvers %s and %s are not synchronous.', solvers{i-1}.name, solvers{i}.name);
                    t = sol1.t;
                end

                % collect solutions
                solutions = {};
                solutions_name = {};
                cols_low_rank_factors = {};
                for i = 1:numel(solvers)
                    if solvers{i}.low_rank
                        [Z , X] = solvers{i}.solver.get_solution();
                        solutions{i} = @(x) Z*X*Z'*x; %#ok<*AGROW>
                        cols_low_rank_factors{i} = size(Z, 2);
                    else
                        X = solvers{i}.solver.get_solution();
                        solutions{i} = @(x) X*x;
                        cols_low_rank_factors{i} = size(X, 2);
                    end
                    solutions_name{i} = solvers{i}.name;
                end

                % compute 2 norm
                nrm2X = cellfun(@(solution) two_norm_sym_handle(solution, n), solutions);

                % compute distance to statioary point
                diff2Xinf = cellfun(@(solution) two_norm_sym_handle(@(x) solution(x) - Zinf*(Zinf' * x), n), solutions);

                % compute 2 norm error
                n_solutions = numel(solutions);
                absnrm2_err = zeros(n_solutions, n_solutions);
                for i = 1:n_solutions
                    for j = 1:n_solutions
                        if j > i
                            handle_i = solutions{i};
                            handle_j = solutions{j};
                            absnrm2_err(i,j) = two_norm_sym_handle(@(x) handle_i(x)-handle_j(x), n);
                        end
                    end
                end
                % diagonal must be zero and the matrix absnrm2_err must be symmetrc
                absnrm2_err = triu(absnrm2_err)' + triu(absnrm2_err);

                %% print results
                if obj.verbose
                    fprintf('%s, instance = %s, h = %.2e, t = %.2e, T = %.2e, %6.2f%%\n', datestr(now), instance, h, t, T, t/T*100);
                    fprintf('\n');
                    first_col = 20;
                    fprintf('%s',repmat(' ', 1, first_col));
                    for i=1:n_solutions
                       other_cols (i) = fprintf('| %-10s ', solutions_name{i});
                    end
                    fprintf('\n');
                    other_cols = max(other_cols, 9);

                    % cols low-rank factors
                    fprintf('%-*s', first_col, 'cols low-rank fac.');
                    for i = 1:n_solutions
                        f = fprintf('| %d', cols_low_rank_factors{i});
                        fprintf('%*s', max(other_cols(i)-f,0),' ');
                    end
                    fprintf('\n');

                    % 2-norm X(t)
                    fprintf('%-*s', first_col, '||X(t)||_2');
                    for i = 1:n_solutions
                       f = fprintf('| %.2e', nrm2X(i));
                       fprintf('%*s', max(other_cols(i)-f,0),' ');
                    end
                    fprintf('\n');

                    % 2-norm X(t) - X_inf
                    fprintf('%-*s', first_col, '||X(t) - X_inf||_2');
                    for i = 1:n_solutions
                        f = fprintf('| %.2e', diff2Xinf(i));
                        fprintf('%*s', max(other_cols(i)-f,0),' ');
                    end
                    fprintf('\n');

                    % wall time solvers
                    fprintf('%-*s', first_col, 'wall time');
                    for i = 1:n_solutions
                        f = fprintf('| %.2e', solvers_time(i));
                        fprintf('%*s', max(other_cols(i)-f,0),' ');
                    end
                    fprintf('\n');

                    % absolute 2-norm diff
                    fprintf('%-*s|\n', first_col, 'abs. 2-norm diff');
                    for i=1:n_solutions
                        fprintf('%-*s', first_col, solutions_name{i});
                        for j=1:n_solutions
                            f = fprintf('| %.2e', absnrm2_err(i,j));
                            fprintf('%*s', max(other_cols(j)-f,0),' ');
                        end
                        fprintf('\n');
                    end

                    % relative 2-norm diff
                    fprintf('%-*s|\n', first_col, 'rel. 2-norm diff');
                    for i=1:n_solutions
                        fprintf('%-*s',  first_col, solutions_name{i});
                        for j=1:n_solutions
                            f = fprintf('| %.2e', absnrm2_err(i,j)/nrm2X(i));
                            fprintf('%*s', max(other_cols(j)-f,0),' ');
                        end
                        fprintf('\n');
                    end
                    fprintf('\n');
                end

                %% check relative tolerance
                for k = 1:n_solutions
                    for i = 1:n_solutions
                        for j = 1:n_solutions
                            obj.fatalAssertLessThan(absnrm2_err(i,j)/nrm2X(k), reltol);
                        end
                    end
                end
            end

            if obj.verbose
                no_of_steps =  round(T/h);
                f = zeros(1, n_solutions + 1);
                f(1) = fprintf('No. of time steps = %d\n', no_of_steps);
                for i = 1:n_solutions
                    sol = solvers{i}.solver;
                    time = solvers_time(i);
                    f(i+1) = fprintf('Time %-25s Class/Measured %.2e/%.2e, avg. time per step = %.2e\n',  ...
                        upper(class(sol)), sol.wtime_time_step, time, time/no_of_steps);
                end
                fprintf('%s\n\n', repmat('-', 1, max(f) - 1));
            end

        end
    end
end
