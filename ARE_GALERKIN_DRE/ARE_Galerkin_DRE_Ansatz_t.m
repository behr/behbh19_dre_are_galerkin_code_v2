%ARE_GALERKIN_DRE_ANSATZ_T   Enumeration for the ansatz used in ARE_Galerkin_DRE
% solver.
%
%   Currently ansatzes
%       - CONSTANT_PLUS
%       - CONSTANT_PLUS_RESIDUAL
%       - STANDARD
%
%   Xinf is the solution of the algebraic Riccati equation.
%
%   CONSTANT_PLUS: X(t) is approximated by Xinf + Q Xtilde(t) Q^T.
%
%   CONSTANT_PLUS_RESIDUAL: X(t) is approximated by Xinf + Q Xtilde(t) Q^T
%   and the residual of Xinf is incorperated to the galerkin equation
%   for Xtilde.
%
%   STANDARD: X(t) is approximated by Q Xtilde(t) Q^T.
%
%   See also ARE_GALERKIN_DRE AND TEST_ARE_GALERKIN_DRE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

classdef (Sealed = true) ARE_Galerkin_DRE_Ansatz_t < int8
    enumeration
        CONSTANT_PLUS(0),
        CONSTANT_PLUS_RESIDUAL(1),
        STANDARD(2),
    end
end
