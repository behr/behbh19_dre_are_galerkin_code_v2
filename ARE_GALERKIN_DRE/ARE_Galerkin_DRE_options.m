classdef ARE_Galerkin_DRE_options < copy_handle
    %ARE_GALERKIN_DRE_OPTIONS    Options class for ARE_GALERKIN_DRE solver.
    %
    %   ARE_GALERKIN_DRE_OPTIONS properties:
    %       trunc_tol   - double, positive, scalar, relative tolerance for compact svd truncation
    %       RADI_opt    - RADI_options instance
    %       ansatz      - ARE_Galerkin_DRE_Ansatz_t instance, the type of ansatz for galerkin, default: CONSTANT_PLUS
    %
    %   See also ARE_GALERKIN_DRE and TEST_ARE_GALERKIN_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = public)
        % double, positive, scalar, relative tolerance for compact svd truncation
        trunc_tol
        % RADI_options instance
        RADI_opt
        % ARE_Galerkin_DRE_Ansatz_t instance, the type of ansatz for galerkin, default: CONSTANT_PLUS
        ansatz
    end

    methods(Access = public)
        function obj = ARE_Galerkin_DRE_options(trunc_tol, RADI_maxiter, RADI_abs2res_tol, RADI_rel2res_tol, RADI_shifts_l, RADI_use_shifted_solver, ansatz, RADI_verbose)
            %ARE_GALERKIN_DRE_OPTIONS Constructor for the options class.
            %
            %   OPT = ARE_GALERKIN_DRE_OPTIONS(TRUNC_TOL, RADI_MAXITER, RADI_ABSRES_TOL, RADI_RELRES_TOL, RADI_SHIFTS_L, RADI_USE_SHIFTED_SOLVER, ANSATZ, RADI_VERBOSE)
            %   OPT = ARE_GALERKIN_DRE_OPTIONS(TRUNC_TOL, RADI_MAXITER, RADI_ABSRES_TOL, RADI_RELRES_TOL, RADI_SHIFTS_L, RADI_USE_SHIFTED_SOLVER, ANSATZ)
            %   OPT = ARE_GALERKIN_DRE_OPTIONS(TRUNC_TOL, RADI_MAXITER, RADI_ABSRES_TOL, RADI_RELRES_TOL, RADI_SHIFTS_L, RADI_USE_SHIFTED_SOLVER)
            %   OPT = ARE_GALERKIN_DRE_OPTIONS(TRUNC_TOL, RADI_MAXITER, RADI_ABSRES_TOL, RADI_RELRES_TOL, RADI_SHIFTS_L)
            %   OPT = ARE_GALERKIN_DRE_OPTIONS(TRUNC_TOL, RADI_MAXITER, RADI_ABSRES_TOL, RADI_RELRES_TOL)
            %   OPT = ARE_GALERKIN_DRE_OPTIONS(TRUNC_TOL, RADI_MAXITER, RADI_ABSRES_TOL)
            %   OPT = ARE_GALERKIN_DRE_OPTIONS(TRUNC_TOL)
            %   OPT = ARE_GALERKIN_DRE_OPTIONS()
            %
            %   The missing property arguments are set to its default values:
            %
            %     * TRUNC_TOL:                      eps
            %     * RADI_opt.MAXITER:               250
            %     * RADI_opt.ABSRES_TOL:            1e-16
            %     * RADI_opt.RELRES_TOL:            1e-14
            %     * RADI_opt.SHIFTS_L:              2
            %     * RADI_opt.USE_SHIFTED_SOLVER:    false
            %     * ANSATZ:                         CONSTANT_PLUS
            %     * RADI_opt.VERBOSE:               false
            %
            %
            %   See also ARE_Galerkin_DRE and ARE_Galerkin_DRE_Ansatz_t.
            %
            %   Author: Maximilian Behr
            obj.trunc_tol = eps;
            obj.RADI_opt = RADI_options();
            obj.RADI_opt.maxiter = 250;
            obj.RADI_opt.abs2res_tol = 1e-16;
            obj.RADI_opt.rel2res_tol = 1e-14;
            obj.RADI_opt.shifts_l = 4;
            obj.RADI_opt.use_shifted_solver = false;
            obj.ansatz = ARE_Galerkin_DRE_Ansatz_t.CONSTANT_PLUS;
            obj.RADI_opt.verbose = false;

            if nargin >= 1
                obj.trunc_tol = trunc_tol;
            end

            if nargin >= 2
                obj.RADI_opt.maxiter = RADI_maxiter;
            end

            if nargin >= 3
                obj.RADI_opt.abs2res_tol = RADI_abs2res_tol;
            end

            if nargin >= 4
                obj.RADI_opt.rel2res_tol = RADI_rel2res_tol;
            end

            if nargin >= 5
                obj.RADI_opt.shifts_l = RADI_shifts_l;
            end

            if nargin >= 6
                obj.RADI_opt.use_shifted_solver = RADI_use_shifted_solver;
            end

            if nargin >= 7
                obj.RADI_opt.use_shifted_solver = ansatz;
            end

            if nargin >= 8
                obj.RADI_opt.verbose = RADI_verbose;
            end
        end
      end

    methods
        % setter methods
        function set.trunc_tol(obj, in_trunc_tol)
            validateattributes(in_trunc_tol, {'double'}, {'real', 'scalar', 'positive', 'finite', 'nonnan'}, mfilename);
            obj.trunc_tol = in_trunc_tol;
        end

        function set.RADI_opt(obj, in_RADI_opt)
            validateattributes(in_RADI_opt, {'RADI_options'}, {}, mfilename);
            obj.RADI_opt = in_RADI_opt;
        end

        function set.ansatz(obj, in_ansatz)
            validateattributes(in_ansatz, {'ARE_Galerkin_DRE_Ansatz_t'}, {}, mfilename);
            obj.ansatz = in_ansatz;
        end
    end
end
