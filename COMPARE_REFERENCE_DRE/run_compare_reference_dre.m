%RUN_COMPARE_REFERENCE_DRE
%
%
%   See also COMPARE_DRE_SOLVERS and RUN_COMPARE_DRE_SOLVERS_REFERENCE_SOLUTION.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all;

%% reader for reference solution
%references = {'rail_1357_SPLITTING_SYMMETRIC8_k-5_T4700_gold', ...
%    'conv_diff_3_6400_SPLITTING_SYMMETRIC8_k-20_T0.125_gold'};
%Ts = {2^0, 2^(-12)};
%hs = {2^(-5), 2^(-16)};

references = {'conv_diff_3_6400_SPLITTING_SYMMETRIC8_k-20_T0.125_gold'};
Ts = {2^(-12)};
hs = {2^(-16)};


for j = 1:numel(references)

    reference = references{j};
    T = Ts{j};
    h = hs{j};
    reader = Reader_DRE(strcat(reference,'.mat'));

    %% system matrices
    [A, E, B, C, Z0] = reader.get_system_matrices();

    %% create solver and call compare_reference_dre
    error_at = reader.mymatfile.store_at;
    error_at = intersect(error_at, h:h:T);

    for i = 0:1
        if i
            solver_opt = Splitting2_DRE_options(Splitting2_t.SPLITTING_SYMMETRIC8);
            solver_opt.decomposition_type = Decomposition_t.DECOMPOSITION_SPARSE_DIRECT;
            solver = Splitting2_DRE(A, E, B, C, [], solver_opt, h, T);
            name = sprintf('%s_vs_%s_h%.2e_T%.2e', reference, char(solver.options.splitting), h, T);
        else
            solver_opt = ARE_Galerkin_DRE_options();
            solver_opt.RADI_opt.use_shifted_solver = true;
            solver_opt.trunc_tol = 10^(-4);
            solver = ARE_Galerkin_DRE(A, E, B, C, solver_opt, h, T);
            name = sprintf('%s_vs_%s_%s_trunc_tol%.2e_h%.2e_T%.2e', reference, class(solver), char(solver.options.ansatz), solver.options.trunc_tol, h, T);
        end

        % call compare_reference_dre
        compare_reference_dre(name, reader, solver, error_at);
    end
end
