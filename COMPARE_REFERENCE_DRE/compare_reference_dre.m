function compare_reference_dre(name, reader, solver, error_at)
%COMPARE_DRE_SOLVERS    Compare two differential Riccati equation solvers.
%
%   The differential Riccati equation is given by
%
%       E' \dot{X} E = A' X E + E' X A - E' X B B' X E + C' C,  X(0)=0.
%
%   COMPARE_DRE_SOLVER(A, E, B, C, reader, SOLVER_TEST_OPT, ERROR_AT, NAME)
%   The matrices A, E, B, C defines the differential Riccati equation.
%   A and E must be doule, real, sparse and quadratic.
%   If E is empty the identity matrix is assumed. B and C must be real
%   and dense.
%   reader must be either an instance of read_reference_solution_dre
%   or an instance of a subclass of solver_dre_options.
%   SOLVER_TEST_OPT must be an instance of a subclass
%   of solver_dre_options. The solver is then created from the given
%   options. ERROR_AT is a vector which contains the time points
%   for comparision both solution. At these points the norm
%   of the difference of both solution and the norm of the solutions
%   itself is computed.
%   ERROR_AT must contain only points multiple of the step size of
%   both solver. NAME is the name of the output directory.
%
%   See also RUN_COMPARE_DRE_SOLVERS and RUN_COMPARE_DRE_SOLVERS_REFERENCE_SOLUTION.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% check input arguments
validateattributes(name, {'char'}, {'scalartext', 'nonempty'}, mfilename, inputname(1), 1);
validateattributes(reader, {'Reader_DRE'}, {}, mfilename, inputname(2), 2);
validateattributes(solver, {'Solver_DRE', 'Splitting2_DRE', 'ARE_Galerkin_DRE'}, {}, mfilename, inputname(3), 3);
T = min(solver.T, reader.mymatfile.T);
validateattributes(error_at, {'double'}, {'increasing', 'row', 'nonempty', 'nonsparse', 'finite', 'nonnan', '>', 0, '<=', T}, mfilename, inputname(3), 3);
assert(all(ismember(error_at, reader.mymatfile.store_at)), 'No comparision possible. error_at must be a subset of store_at.');
assert(~any(mod(error_at, solver.h)), 'error_at must only contain multiples of stepsize');
solver_isGalerkin = isa(solver, 'ARE_Galerkin_DRE');

%% create necessary filenames and create directory for results
mybasename = sprintf('%s', name);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end

mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mydat = fullfile(mydir, sprintf('%s.dat', mybasename));
mkdir(mydir);

%% delete old diary and create new one
delete(mydiary);
diary(mydiary);


%% solve algebraic riccati equation to monitor convergence of the reference solution
[A, E, B, C, ~] = reader.get_system_matrices();
radi_opt = RADI_options();
radi_opt.rel2res_tol = 1e-19;
radi_opt.abs2res_tol = 1e-19;
radi_opt.maxiter = 500;
radi_opt.verbose = true;
[Zinf, ~] = RADI(A, E, B, C, radi_opt);
Zinf = truncate_ZZT(Zinf, eps);
nrm2C = norm(C)^2;
abs2res = res2_ARE(A, E, B, C, Zinf);
rel2res = abs2res / nrm2C;
nrm2Xinf = norm(Zinf)^2;
fprintf('abs./rel. 2-norm residual = %.2e/%.2e, ||Xinf|| = %.2e\n', abs2res, rel2res, nrm2Xinf);

%% generate solver for test solution
fprintf('%s - Prepare solver for comparison.\n', datestr(now));
solver.prepare();

%% solve and compare solutions
datatable = [];
for t_error = error_at

    % intergrate to desired time with solver to test
    dnow = datestr(now);
    fprintf('%s - %s\n', dnow, name);
    temp = tic();
    solver.time_step_until(t_error);
    temp = toc(temp);
    dnow = datestr(now);
    fprintf('%s - Integrate with solver to t = %.2e in %.2e sec, T = %.2e, %6.2f%%\n', dnow, t_error, temp, T, t_error/T*100);

    % get solution
    dnow = datestr(now);
    fprintf('%s - t = %.2e get solutions.\n', dnow, solver.t);
    [Q_reader, X_reader] = reader.get_solution_at(solver.t);
    [Q_solver, X_solver] = solver.get_solution();

    % compute 2-norm error
    dnow = datestr(now);
    fprintf('%s - t = %.2e compare solutions.\n', dnow, solver.t);
    n = size(Q_reader, 1);
    nrm2_Xreader = two_norm_sym_handle(@(x) (Q_reader * (X_reader * (Q_reader' * x))), n);
    nrm2_Xsolver = two_norm_sym_handle(@(x) (Q_solver * (X_solver * (Q_solver' * x))), n);
    absnrm2_err = two_norm_sym_handle(@(x) (Q_solver * (X_solver * (Q_solver' * x)))-(Q_reader * (X_reader * (Q_reader' * x))), n);
    relnrm2_err = absnrm2_err / nrm2_Xreader;

    % compute F-norm error
    nrmF_Xreader = fronorm_outer(Q_reader, X_reader);
    nrmF_Xsolver = fronorm_outer(Q_solver, X_solver);
    absnrmF_err = fronorm_outer_diff(Q_reader, X_reader, Q_solver, X_solver);
    relnrmF_err = absnrmF_err / nrmF_Xreader;

    % compute best Approximation of reference in solution in Frobenius norm
    % if the test solver is a galerkin based solver, i.e. are_galerkin_dre
    nrmF_Xbest = 0;
    absnrmF_err_Xbest = 0;
    relnrmF_err_Xbest = 0;
    nrm2_Xbest = 0;
    absnrm2_err_Xbest = 0;
    relnrm2_err_Xbest = 0;
    if solver_isGalerkin
        temp = Q_solver' * Q_reader;
        X_best = temp * X_reader * temp';
        % 2-norm
        nrm2_Xbest = two_norm_sym_handle(@(x) Q_solver*(X_best * (Q_solver' * x)), n);
        absnrm2_err_Xbest = two_norm_sym_handle(@(x) (Q_solver * (X_best * (Q_solver' * x)))-(Q_reader * (X_reader * (Q_reader' * x))), n);
        relnrm2_err_Xbest = absnrm2_err_Xbest / nrm2_Xreader;
        % F-norm
        nrmF_Xbest = fronorm_outer(Q_solver, X_best);
        absnrmF_err_Xbest = fronorm_outer_diff(Q_solver, X_best, Q_reader, X_reader);
        relnrmF_err_Xbest = absnrmF_err_Xbest / nrmF_Xreader;
    end

    % get size of low rank solution
    cols_reader = size(Q_reader, 2);
    cols_solver = size(Q_solver, 2);

    % compute ||X_ref - X_inf ||_2, distance to stationary point
    diffnrm2_Xinf_reader = two_norm_sym_handle(@(x) (Q_reader * (X_reader * (Q_reader' * x)))-(Zinf * (Zinf' * x)), n);

    % print info
    dnow = datestr(now);
    fprintf('%s - t = %.2e absnrm2_err = %.2e, relnrm2_err = %.2e, nrm2_Xreader = %.2e, nrm2_Xsolver = %.2e.\n', dnow, t_error, absnrm2_err, relnrm2_err, nrm2_Xreader, nrm2_Xsolver);
    fprintf('%s - t = %.2e absnrmF_err = %.2e, relnrmF_err = %.2e, nrmF_Xreader = %.2e, nrmF_Xsolver = %.2e.\n', dnow, t_error, absnrmF_err, relnrmF_err, nrmF_Xreader, nrmF_Xsolver);
    if solver_isGalerkin
        fprintf('%s - Best approx.\n', dnow);
        fprintf('%s - t = %.2e absnrm2_err = %.2e, relnrm2_err = %.2e, nrm2_Xbest = %.2e.\n', dnow, t_error, absnrm2_err_Xbest, relnrm2_err_Xbest, nrm2_Xbest);
        fprintf('%s - t = %.2e absnrmF_err = %.2e, relnrmF_err = %.2e, nrmF_Xbest = %.2e.\n', dnow, t_error, absnrmF_err_Xbest, relnrmF_err_Xbest, nrmF_Xbest);
    end
    fprintf('%s - cols_reader = %d, cols_solver = %d\n', datestr(now), cols_reader, cols_solver);
    fprintf('%s - || X_ref - Xinf ||_2 = %.2e\n', datestr(now), diffnrm2_Xinf_reader);

    % log errors
    datatable(end+1, :) = ...
        [t_error, ...
        absnrm2_err, relnrm2_err, nrm2_Xreader, nrm2_Xsolver, ...
        absnrmF_err, relnrmF_err, nrmF_Xreader, nrmF_Xsolver, ...
        absnrm2_err_Xbest, relnrm2_err_Xbest, nrm2_Xbest, ...
        absnrmF_err_Xbest, relnrmF_err_Xbest, nrmF_Xbest, ...
        cols_reader, cols_solver, diffnrm2_Xinf_reader, ...
        ]; %#ok<AGROW>
end

%% Write to file for tikz and mat
fprintf('%s - Start writing data to tikz file\n', datestr(now));

% meta data
dlogger = Datalogger(mydat);
dlogger.add_to_preamble('mfilename', mfilename);
dlogger.add_to_preamble('name', name);
dlogger.add_to_preamble('n', n);
dlogger.add_to_preamble('T', T);

% data from reader
dlogger.add_to_preamble('reader', class(reader));
dlogger.add_to_preamble('reader_mfilename', reader.mymatfile.mfilename);
dlogger.add_to_preamble('reader_name', reader.mymatfile.name);
dlogger.add_to_preamble('reader_mymat', reader.mymat);
dlogger.add_to_preamble('reader_h', reader.mymatfile.h);
dlogger.add_to_preamble('reader_k', round(log2(reader.mymatfile.h)));
dlogger.add_to_preamble('reader_solver_name', reader.mymatfile.solver_name);

% data from solver
dlogger.add_to_preamble('solver', class(solver));
if isa(solver, 'Splitting2_DRE')
    dlogger.add_to_preamble('solver_splitting', char(solver.options.splitting));
    dlogger.add_to_preamble('solver_irkmethod', char(solver.options.irkmethod));
    dlogger.add_to_preamble('solver_quadrule', char(solver.options.quadrule));
    dlogger.add_to_preamble('solver_qrmethod', char(solver.options.qrmethod));
    dlogger.add_to_preamble('solver_decomposition_type', char(solver.options.decomposition_type));
    dlogger.add_to_preamble('solver_galerkin_ansatz', 'none');
    dlogger.add_to_preamble('solver_galerkin_trunc_tol', 0);
else
    dlogger.add_to_preamble('solver_splitting', 'none');
    dlogger.add_to_preamble('solver_irkmethod', 'none');
    dlogger.add_to_preamble('solver_quadrule', 'none');
    dlogger.add_to_preamble('solver_qrmethod', 'none');
    dlogger.add_to_preamble('solver_decomposition_type', 'none');
    dlogger.add_to_preamble('solver_galerkin_ansatz', char(solver.options.ansatz));
    dlogger.add_to_preamble('solver_galerkin_trunc_tol', solver.options.trunc_tol);
end
dlogger.add_to_preamble('solver_h', solver.h);
dlogger.add_to_preamble('solver_k', round(log2(solver.h)));

wtime_solve_are = 0;
trunc_abs2res_are = 0;
trunc_rel2res_are = 0;
Z_cols = 0;
trunc_Z_cols = 0;
nrm1_exp_hH = 0;
if solver_isGalerkin
    wtime_solve_are = solver.RADI_stat.time_overall;
    trunc_abs2res_are = solver.trunc_abs2res_are;
    trunc_rel2res_are = solver.trunc_rel2res_are;
    Z_cols = size(solver.Z, 2);
    trunc_Z_cols = size(solver.trunc_Z, 2);
    nrm1_exp_hH = solver.nrm1_exp_hH;
end
dlogger.add_to_preamble('solver_wtime_prepare', solver.wtime_prepare);
dlogger.add_to_preamble('solver_wtime_time_step', solver.wtime_time_step);
dlogger.add_to_preamble('solver_wtime_time_step_until', solver.wtime_time_step_until);
dlogger.add_to_preamble('solver_wtime_solve_are', wtime_solve_are);
dlogger.add_to_preamble('solver_trunc_abs2res_are', trunc_abs2res_are);
dlogger.add_to_preamble('solver_trunc_rel2res_are', trunc_rel2res_are);
dlogger.add_to_preamble('solver_Z_cols', Z_cols);
dlogger.add_to_preamble('solver_trunc_Z_cols', trunc_Z_cols);
dlogger.add_to_preamble('solver_nrm1_exp_hH', nrm1_exp_hH);

% data of tracked error
header = 't,';
header = strcat(header, 'ABSNRM2_ERR,RELNRM2_ERR,NRM2_XREADER,NRM2_XSOLVER,');
header = strcat(header, 'ABSNRMF_ERR,RELNRMF_ERR,NRMF_XREADER,NRMF_XSOLVER,');
header = strcat(header, 'ABSNRM2_ERR_XBEST,RELNRM2_ERR_XBEST,NRM2_XBEST,');
header = strcat(header, 'ABSNRMF_ERR_XBEST,RELNRMF_ERR_XBEST,NRMF_XBEST,');
header = strcat(header, 'COLS_READER,COLS_SOLVER,DIFFNRM2_XREADER_XINF');
dlogger.set_header(header);
dlogger.set_table(datatable);
dlogger.write();

fprintf('%s - Finished writing tikz file\n\n', datestr(now));

%% turn diary off
diary('off');

end
