%MAKE_MEX
%
%   Compile script for MEX functions. Adapt the variables
%   below to your needs.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019
%

warning('off');

%% clear all, close all
clearvars, close all                        

%% compiler options adapt to your needs
VERBOSE = '';     % '' or '-v';
DEBUG = '-g';       % '' or '-g';
INCLUDES ='-IMEXTOOLS/include';
%CFLAGS = '-fPIC -std=c99 -O3 -march=native -Wall -Wpedantic -Wextra -Werror';
CFLAGS = '-fPIC -std=c99 -O3 -march=native -Wall -Wpedantic -Wextra';
%CFLAGS_OMP = '-Xpreprocessor -fopenmp -lomp';
CFLAGS_OMP = '-fopenmp';
LDFLAGS_OMP = '-fopenmp';
LDFLAGS = '-flto';
LIBS = '-lmwblas -lmwlapack -lmwamd -lmwumfpack -lmwcholmod -lmwma57';
LIBMKL = '-l":mkl.so"';


%% collect compiler options and remove empty variables
CFLAGS_OMP = sprintf('CFLAGS="%s %s"', CFLAGS, CFLAGS_OMP);
CFLAGS = sprintf('CFLAGS="%s"', CFLAGS);
LDFLAGS_OMP = sprintf('LDFLAGS="%s %s"', LDFLAGS, LDFLAGS_OMP);
LDFLAGS = sprintf('LDFLAGS="%s"', LDFLAGS);

% prepare flags with and without OpenMP support
LIBS = split(LIBS)';
MEXFLAGS = [VERBOSE, DEBUG, INCLUDES, CFLAGS, LDFLAGS, LIBS];
MEXFLAGS = MEXFLAGS(~cellfun('isempty',MEXFLAGS));

MEXFLAGS_OMP = [VERBOSE, DEBUG, INCLUDES, CFLAGS_OMP, LDFLAGS_OMP, LIBS];
MEXFLAGS_OMP = MEXFLAGS_OMP(~cellfun('isempty',MEXFLAGS_OMP));


%% operating system specific
if ispc
    OBJSUFFIX = '.obj';
else
    OBJSUFFIX = '.o';
end

%% helper
msg_compile = @(SRC, OBJ) fprintf('COMPILE:\n\tSRC = %s\n\tOBJ = %s\n', SRC, OBJ);

%% compile BANDED/CHOLMOD/MA57/MISC
FILES_BANDED = dir(fullfile('MEXTOOLS','src','BANDED','*.c'));
FILES_CHOLMOD = dir(fullfile('MEXTOOLS','src','CHOLMOD','*.c'));
FILES_MA57 = dir(fullfile('MEXTOOLS','src','MA57','*.c'));
FILES_MISC = dir(fullfile('MEXTOOLS','src','MISC','*.c'));
FILES_QRTOOLS = dir(fullfile('MEXTOOLS','src','QRTOOLS','*.c'));
FILES_MDM_C = dir(fullfile('MEXTOOLS','src','MDM_SYM_DRE_STEP','MDM_SYM_DRE_STEP_*.c'));
FILES_MDM_C2 = dir(fullfile('MEXTOOLS','src','MDM_SYM_DRE_STEP','ReLAPACK_*.c'));
FILES_MDM_F = dir(fullfile('MEXTOOLS','src','MDM_SYM_DRE_STEP','*.f'));

FILES = [FILES_BANDED; FILES_CHOLMOD; FILES_MA57; FILES_MISC; FILES_QRTOOLS; FILES_MDM_C; FILES_MDM_C2; FILES_MDM_F];
for i = 1:numel(FILES)
    % directory and source file
    DIR = FILES(i).folder;
    FILENAME = FILES(i).name;
    SRC = fullfile(DIR, FILENAME);
    [~, BASENAME, EXT] = fileparts(SRC);
    OBJ = fullfile(DIR, strcat(BASENAME, OBJSUFFIX));
    OUT = strcat(BASENAME, OBJSUFFIX);

    % compile and move to source
    msg_compile(SRC, OBJ);
    if strcmpi('.f', EXT)
        %-fdefault-integer 8 seems to be standard
        mex('-c', MEXFLAGS{:}, SRC);
    else
        mex('-c', '-R2018a', MEXFLAGS{:}, SRC);
    end
    movefile(OUT, OBJ, 'f');

    % addtional compile with openmp
    if any(strcmpi({'cholmod_l_solve.c', 'MA57DD.c'}, FILENAME))
        OBJ = strrep(SRC, '.c', strcat('_omp', OBJSUFFIX));
        msg_compile(SRC, OBJ);
        mex('-c', '-R2018a', MEXFLAGS_OMP{:}, SRC);
        movefile(OUT, OBJ, 'f');
    end
end

%% compile UMFPACK
UMFPACK_COMPILE = {
    struct('src', 'umfpack_error.c', 'obj_base', 'umfpack_error', 'flag', '', 'omp', false), ...
    struct('src', 'umfpack_report.c', 'obj_base', 'umfpack_dl_report_numeric', 'flag', '-DUMFPACK_FUNC=umfpack_dl_report_numeric', 'omp', false) ...
    struct('src', 'umfpack_report.c', 'obj_base', 'umfpack_zl_report_numeric', 'flag', '-DUMFPACK_FUNC=umfpack_zl_report_numeric', 'omp', false), ...
    struct('src', 'umfpack_report.c', 'obj_base', 'umfpack_dl_report_symbolic', 'flag', '-DUMFPACK_FUNC=umfpack_dl_report_symbolic', 'omp', false), ...
    struct('src', 'umfpack_report.c', 'obj_base', 'umfpack_zl_report_symbolic', 'flag', '-DUMFPACK_FUNC=umfpack_zl_report_symbolic', 'omp', false), ...
    struct('src', 'umfpack_free.c', 'obj_base', 'umfpack_dl_free_numeric', 'flag', '-DUMFPACK_FUNC=umfpack_dl_free_numeric', 'omp', false), ...
    struct('src', 'umfpack_free.c', 'obj_base', 'umfpack_zl_free_numeric', 'flag', '-DUMFPACK_FUNC=umfpack_zl_free_numeric', 'omp', false), ...
    struct('src', 'umfpack_free.c', 'obj_base', 'umfpack_dl_free_symbolic', 'flag', '-DUMFPACK_FUNC=umfpack_dl_free_symbolic', 'omp', false), ...
    struct('src', 'umfpack_free.c', 'obj_base', 'umfpack_zl_free_symbolic', 'flag', '-DUMFPACK_FUNC=umfpack_zl_free_symbolic', 'omp', false), ...
    struct('src', 'umfpack_solve.c', 'obj_base', 'umfpack_dl_solve', 'flag', '-DUMFPACK_COMPLEX=0', 'omp', false), ...
    struct('src', 'umfpack_solve.c', 'obj_base', 'umfpack_dl_solve_omp', 'flag', '-DUMFPACK_COMPLEX=0', 'omp', true), ...
    struct('src', 'umfpack_solve.c', 'obj_base', 'umfpack_zl_solve', 'flag', '-DUMFPACK_COMPLEX=1', 'omp', false), ...
    struct('src', 'umfpack_solve.c', 'obj_base', 'umfpack_zl_solve_omp', 'flag', '-DUMFPACK_COMPLEX=1', 'omp', true), ...
    struct('src', 'umfpack_symbolic.c', 'obj_base', 'umfpack_dl_symbolic', 'flag', '-DUMFPACK_COMPLEX=0', 'omp', false), ...
    struct('src', 'umfpack_symbolic.c', 'obj_base', 'umfpack_zl_symbolic', 'flag', '-DUMFPACK_COMPLEX=1', 'omp', false), ...
    struct('src', 'umfpack_numeric.c', 'obj_base', 'umfpack_dl_numeric', 'flag', '-DUMFPACK_COMPLEX=0', 'omp', false), ...
    struct('src', 'umfpack_numeric.c', 'obj_base', 'umfpack_zl_numeric', 'flag', '-DUMFPACK_COMPLEX=1', 'omp', false)
    };

for i = 1:numel(UMFPACK_COMPILE)
    LIBS = UMFPACK_COMPILE{i};
    SRC = fullfile('MEXTOOLS','src','UMFPACK', LIBS.src);
    OBJ = fullfile('MEXTOOLS','src','UMFPACK', strcat(LIBS.obj_base, OBJSUFFIX));
    OUT = strrep(LIBS.src,'.c',OBJSUFFIX);
    OMP = LIBS.omp;
    FLAG = LIBS.flag;
    msg_compile(SRC, OBJ);
    if OMP
        if isempty(FLAG)
            mex('-c', '-R2018a', MEXFLAGS_OMP{:}, SRC);
        else
            mex('-c', '-R2018a', FLAG, MEXFLAGS_OMP{:}, SRC);
        end
    else
        if isempty(FLAG)
            mex('-c', '-R2018a', MEXFLAGS{:}, SRC);
        else
            mex('-c', '-R2018a', FLAG, MEXFLAGS{:}, SRC);
        end
    end
    movefile(OUT, OBJ, 'f');
end

%% compile MEXTOOLS_CALL
SRC = fullfile('MEXTOOLS','src','MEXTOOLS_CALL', 'mextools_call.c');
DEPS_BANDED = dir(fullfile('MEXTOOLS','src','BANDED','*.o'));
DEPS_CHOLMOD = dir(fullfile('MEXTOOLS','src','CHOLMOD','*.o'));
DEPS_MA57 = dir(fullfile('MEXTOOLS','src','MA57','*.o'));
DEPS_MISC = dir(fullfile('MEXTOOLS','src','MISC','*.o'));
DEPS_QRTOOLS = dir(fullfile('MEXTOOLS','src','QRTOOLS','*.o'));
DEPS_UMFPACK = dir(fullfile('MEXTOOLS','src','UMFPACK','*.o'));
DEPS = [DEPS_BANDED; DEPS_CHOLMOD; DEPS_MA57; DEPS_MISC; DEPS_QRTOOLS; DEPS_UMFPACK];
OBJS = cell(1, numel(DEPS));
for i = 1:numel(DEPS)
    OBJ = fullfile(DEPS(i).folder, DEPS(i).name);
    OBJS{i} = OBJ;
end
mex('-R2018a', MEXFLAGS_OMP{:}, OBJS{:}, SRC, '-outdir', fullfile('MEXTOOLS'));

%% compile MDM_SYM_DRE_STEP
SRC = fullfile('MEXTOOLS','src','MDM_SYM_DRE_STEP', 'MDM_SYM_DRE_STEP.c');
DEPS_MDM_SYM_DRE_STEP = dir(fullfile('MEXTOOLS','src','MDM_SYM_DRE_STEP','*.o'));
DEPS_MISC = dir(fullfile('MEXTOOLS','src','MISC','*.o'));
DEPS = [DEPS_MDM_SYM_DRE_STEP; DEPS_MISC];
OBJS = cell(1, numel(DEPS));
for i = 1:numel(DEPS)
    OBJ = fullfile(DEPS(i).folder, DEPS(i).name);
    OBJS{i} = OBJ;
end
mex('-R2018a', MEXFLAGS{:}, OBJS{:}, SRC, '-outdir', fullfile('MODIFIED_DAVISON_MAKI_DRE'));
return;


%% compile using mex
%mex(DEBUG, VERBOSE, CFLAGS, LDFLAGS, LIBBLAS, LIBLAPACK, LIBMKL, 'MDM_SYM_DRE_STEP.c')

%%% OLD CODE NOT NEEDED ANYMORE. MAYBE USEFULL AT LATER POINT IN TIME.
% check operating system
%mname = mfilename;
%mroot = matlabroot();
%assert(ismac() || isunix(), '%s supports only OSX or Unix operating systems.', mname);
%
% find mkl.so or mkl.dylib library and create symbolic link
%
% fprintf('%s: Search for mkl.%s library matlabroot %s.\n', mname, libext, mroot);
% mkllib = dir(fullfile(mroot,strcat('**/mkl.', libext)));
%
% if isempty(mkllib)
%     error(mname, 'Cannot find mkl.{so,dylib} library in matlabroot.');
% end
%
% if length(mkllib) > 1
%     error(mname, 'Found multiple libraries matching the pattern mkl.{so,dylib} in matlabroot.');
% end
%
% mkllib = fullfile(mkllib.folder, mkllib.name);
% assert(isfile(mkllib), '%s mkl.%s not found.', mname, mkllib);
%
% % create symlink in this directory
% [mydir, ~, ~] = fileparts(mfilename('fullpath'));
% linklibmkl = fullfile(mydir, sprintf('libmkl.%s', libext));
%
% % check if symlink exists and delete
% if exist(linklibmkl, 'file')==2
%   delete(linklibmkl);
% end
%
% stat = system(sprintf('ln -s %s %s', mkllib, linklibmkl));
% if stat
%     error(mname, 'Cannot create symbolic link %s', linklibmkl);
% end
% fprintf('%s: Created symlink:%s->%s successfully.\n', mname, linklibmkl, mkllib);
