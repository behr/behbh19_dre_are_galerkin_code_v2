#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2018-2020
#


###############################################################################
# C AND FORTRAN COMPILER                                                      #
###############################################################################
CC=gcc
FC=gfortran


###############################################################################
# C AND FORTRAN COMPILER FLAGS                                                #
###############################################################################
#CFLAGS+=-fPIC -std=c99 -O0 -march=native -Wall -Wpedantic -Wextra -Werror -IMEXTOOLS/include
CFLAGS+=-fPIC -std=c99 -O3 -march=native -Wall -Wpedantic -Wextra -IMEXTOOLS/include
FFLAGS+=-fPIC -O0 -c -fdefault-integer-8

#CFLAGS+=-std=c99 -O0 -march=native -Wall -Wpedantic -Wextra -Werror -IMEXTOOLS/include
#CFLAGS+=-std=c99 -O3 -march=native -Wall -Wpedantic -Wextra -IMEXTOOLS/include
#FFLAGS+=-O0 -c -fdefault-integer-8


FOPENMP=-fopenmp


###############################################################################
# LINK FLAGS                                                                  #
###############################################################################
LDFLAGS=-flto


###############################################################################
# MEX COMPILER SETTINGS                                                       #
###############################################################################
#HOSTNAME=$(shell hostname)
#ifeq ($(HOSTNAME), jack)
# 	MEX?=/vol1/MPI_MATLAB/R2018a/bin/mex
#else
# 	MEX?=mex
#endif
MEX?=mex
MEXCFLAGS=-Wall -Wpedantic -Wextra
MEXFLAGS=-R2018a
MEXLIBS=-lmwblas -lmwlapack -lmwumfpack -lmwcholmod -lmwma57 -lmwamd
MEXLIBS_MKL=-l'":mkl.so"'


###############################################################################
# DEBUG AND VERBOSE OPTIONS                                                   #
###############################################################################
ifdef VERBOSE
MEXFLAGS+=-v
endif

DEFINES=
ifdef DEBUG
MEXFLAGS+= -g
CFLAGS+=-g3 -ggdb
FFLAGS+=-g3 -ggdb
DEFINES=-DDEBUG
endif

###############################################################################
# OS SPECIFIC                                                                 #
###############################################################################
ifeq ($(OS),Windows_NT)
MV=move
DEL=del
OBJ=obj
MEXEXT=mexw64
else
MV=mv -f
DEL=rm -f
OBJ=o
MEXEXT=mexa64
endif



###############################################################################
# DEFAULT TARGET                                                              #
###############################################################################
.DEFAULT_GOAL := all


###############################################################################
# DEACTIVATE PARALLEL BUILDS                                                   #
###############################################################################
.NOTPARALLEL:


###############################################################################
# VARIABLES TO SOURCE AND INCLUDE DIRECTORY                                    #
###############################################################################
MEXTOOLS_SRC=MEXTOOLS/src
MEXTOOLS_INC=MEXTOOLS/include


###############################################################################
# TARGETS AND RULES FOR MISC                                                  #
###############################################################################
# MISC HEADER DEPENDENCIES
MISC_MEX_HEADER=$(MEXTOOLS_INC)/mextools.h $(MEXTOOLS_INC)/qrtools.h

# MISC OBJECT FILES
MISC_MEX_CSRC=$(wildcard $(MEXTOOLS_SRC)/MISC/*.c)
MISC_MEX_OBJS=$(patsubst %.c, %.$(OBJ), $(MISC_MEX_CSRC))

# RULE FOR MISC MEX EXECUTABLES
$(MEXTOOLS_SRC)/MISC/%.$(OBJ): $(MEXTOOLS_SRC)/MISC/%.c $(MISC_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS)" $<
	$(MV) $*.$(OBJ) $@

all: $(MISC_MEX_OBJS)


###############################################################################
# TARGETS AND RULES FOR BANDED                                                #
###############################################################################
# BANDED HEADER DEPENDENCIES
BANDED_MEX_HEADER =	\
$(MEXTOOLS_INC)/mextools.h	\
$(MEXTOOLS_INC)/blas_lapack.h	\
$(MEXTOOLS_INC)/banded.h

# BANDED OBJECT FILES
BANDED_MEX_CSRC=$(wildcard $(MEXTOOLS_SRC)/BANDED/*.c)
BANDED_MEX_OBJS=$(patsubst %.c, %.$(OBJ), $(BANDED_MEX_CSRC))

# RULE FOR BANDED MEX EXECUTABLES
$(MEXTOOLS_SRC)/BANDED/%.$(OBJ): $(MEXTOOLS_SRC)/BANDED/%.c $(BANDED_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS)" $<
	$(MV) $*.$(OBJ) $@

all: $(BANDED_MEX_OBJS)


###############################################################################
# TARGETS AND RULES FOR CHOLMOD                                               #
###############################################################################
# CHOLMOD HEADER DEPENDENCIES
CHOLMOD_MEX_HEADER =	\
$(MEXTOOLS_INC)/mextools.h	\
$(MEXTOOLS_INC)/blas_lapack.h	\
$(MEXTOOLS_INC)/cholmod.h

# CHOLMOD OBJECT FILES
CHOLMOD_MEX_CSRC=$(wildcard $(MEXTOOLS_SRC)/CHOLMOD/*.c)
CHOLMOD_MEX_OBJS= $(patsubst %.c, %.$(OBJ), $(CHOLMOD_MEX_CSRC))	\
	$(MEXTOOLS_SRC)/CHOLMOD/cholmod_l_solve_omp.$(OBJ)

# RULE FOR cholmod_l_solve_omp
$(MEXTOOLS_SRC)/CHOLMOD/cholmod_l_solve_omp.$(OBJ): $(MEXTOOLS_SRC)/CHOLMOD/cholmod_l_solve.c $(CHOLMOD_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS) $(FOPENMP)" $<
	$(MV) cholmod_l_solve.$(OBJ) $@

# RULE FOR cholmod_l_{analyze, factorize, solve, print_factor, free_factor}
$(MEXTOOLS_SRC)/CHOLMOD/%.$(OBJ): $(MEXTOOLS_SRC)/CHOLMOD/%.c $(CHOLMOD_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS)" $<
	$(MV) $*.$(OBJ) $@

all: $(CHOLMOD_MEX_OBJS)


###############################################################################
# TARGETS AND RULES FOR MA57                                                  #
###############################################################################
# MA57 HEADER DEPENDENCIES
MA57_MEX_HEADER =	\
$(MEXTOOLS_INC)/mextools.h	\
$(MEXTOOLS_INC)/blas_lapack.h	\
$(MEXTOOLS_INC)/ma57.h

# MA57 OBJECT FILES
MA57_MEX_CSRC=$(wildcard $(MEXTOOLS_SRC)/MA57/*.c)
MA57_MEX_OBJS=$(patsubst %.c, %.$(OBJ), $(MA57_MEX_CSRC))	\
	$(MEXTOOLS_SRC)/MA57/MA57DD_omp.$(OBJ)

# RULE FOR MA57DD_omp
$(MEXTOOLS_SRC)/MA57/MA57DD_omp.$(OBJ): $(MEXTOOLS_SRC)/MA57/MA57DD.c $(MA57_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS) $(FOPENMP)" $<
	$(MV) MA57DD.$(OBJ) $@

# RULE FOR MA57{AD, BD, DD}
$(MEXTOOLS_SRC)/MA57/%.$(OBJ): $(MEXTOOLS_SRC)/MA57/%.c $(MA57_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS)" $<
	$(MV) $*.$(OBJ) $@

all: $(MA57_MEX_OBJS)


###############################################################################
# TARGETS AND RULES FOR QRTOOLS                                               #
###############################################################################
# QRTOOLS HEADER DEPENDENCIES
QRTOOLS_MEX_HEADER =	\
$(MEXTOOLS_INC)/mextools.h	\
$(MEXTOOLS_INC)/blas_lapack.h	\
$(MEXTOOLS_INC)/qrtools.h

# QRTOOLS OBJECT FILES
QRTOOLS_MEX_CSRC=$(wildcard $(MEXTOOLS_SRC)/QRTOOLS/*.c)
QRTOOLS_MEX_OBJS=$(patsubst %.c, %.$(OBJ), $(QRTOOLS_MEX_CSRC))

# RULE FOR QRTOOLS MEX EXECUTABLES
$(MEXTOOLS_SRC)/QRTOOLS/%.$(OBJ): $(MEXTOOLS_SRC)/QRTOOLS/%.c $(QRTOOLS_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS)" $<
	$(MV) $*.$(OBJ) $@

all: $(QRTOOLS_MEX_OBJS)


###############################################################################
# TARGETS AND RULES FOR UMFPACK                                               #
###############################################################################
# UMFPACK HEADER DEPENDENCIES
UMFPACK_MEX_HEADER =	\
$(MEXTOOLS_INC)/mextools.h	\
$(MEXTOOLS_INC)/blas_lapack.h	\
$(MEXTOOLS_INC)/umfpack.h	\
$(MEXTOOLS_INC)/amd.h

# UMFPACK OBJECT FILES
UMFPACK_MEX_OBJS =	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_error.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_report_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_report_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_report_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_report_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_free_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_free_symbolic.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_free_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_free_numeric.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_solve_omp.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_solve_omp.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_solve.$(OBJ)	\
	$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_solve.$(OBJ)

# RULE FOR umfpack_error.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_error.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_error.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS)"  $<
	$(MV) umfpack_error.$(OBJ) $@

# RULE FOR umfpack_dl_report_{symbolic,numeric}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_report_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_report.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) -DUMFPACK_FUNC=umfpack_dl_report_$* CC="$(CC)" CFLAGS="$(CFLAGS)"  $<
	$(MV) umfpack_report.$(OBJ) $@

# RULE FOR umfpack_zl_report_{symbolic,numeric}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_report_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_report.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) -DUMFPACK_FUNC=umfpack_zl_report_$* CC="$(CC)" CFLAGS="$(CFLAGS)"  $<
	$(MV) umfpack_report.$(OBJ) $@

# RULE FOR umfpack_dl_free_{symbolic,numeric}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_free_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_free.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) -DUMFPACK_FUNC=umfpack_dl_free_$* CC="$(CC)" CFLAGS="$(CFLAGS)"  $<
	$(MV) umfpack_free.$(OBJ) $@

# RULE FOR umfpack_zl_free_{symbolic,numeric}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_free_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_free.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) -DUMFPACK_FUNC=umfpack_zl_free_$* CC="$(CC)" CFLAGS="$(CFLAGS)"  $<
	$(MV) umfpack_free.$(OBJ) $@

# RULE FOR umfpack_dl_solve_omp
$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_solve_omp.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_solve.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) -DUMFPACK_COMPLEX=0 CC="$(CC)" CFLAGS="$(CFLAGS) $(FOPENMP)"  $<
	$(MV) umfpack_solve.$(OBJ) $@

# RULE FOR umfpack_zl_solve_omp
$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_solve_omp.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_solve.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) -DUMFPACK_COMPLEX=1 CC="$(CC)" CFLAGS="$(CFLAGS) $(FOPENMP)"  $<
	$(MV) umfpack_solve.$(OBJ) $@

# RULE FOR umfpack_dl_{symbolic,numeric,solve}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_dl_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_%.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) -DUMFPACK_COMPLEX=0 CC="$(CC)" CFLAGS="$(CFLAGS)"  $<
	$(MV) umfpack_$*.$(OBJ) $@

# RULE FOR umfpack_zl_{symbolic,numeric,solve}.$(OBJ)
$(MEXTOOLS_SRC)/UMFPACK/umfpack_zl_%.$(OBJ): $(MEXTOOLS_SRC)/UMFPACK/umfpack_%.c $(UMFPACK_MEX_HEADER)
	$(MEX) -c $(MEXFLAGS) $(DEFINES) -DUMFPACK_COMPLEX=1 CC="$(CC)" CFLAGS="$(CFLAGS)"  $<
	$(MV) umfpack_$*.$(OBJ) $@

all: $(UMFPACK_MEX_OBJS)


###############################################################################
# TARGETS AND RULES FOR MEXTOOLS_CALL                                         #
###############################################################################
# MEXTOOLS CALL HEADER DEPENDENCIES
MEXTOOLS_CALL_MEX_HEADER=$(wildcard $(MEXTOOLS_INC)/*.h)

# MEXTOOLS CALL OBJECT FILES
MEXTOOLS_CALL_MEX_OBJS =	\
	$(BANDED_MEX_OBJS)	$(CHOLMOD_MEX_OBJS)	\
	$(MA57_MEX_OBJS)	$(QRTOOLS_MEX_OBJS)	\
	$(UMFPACK_MEX_OBJS)	$(MISC_MEX_OBJS)

# MEXTOOLS CALL MEX EXECUTABLE TARGET
MEXTOOLS_CALL_MEX_EXECUTABLES=MEXTOOLS/mextools_call.$(MEXEXT)

# RULE FOR mextools_call
MEXTOOLS/mextools_call.$(MEXEXT): $(MEXTOOLS_SRC)/MEXTOOLS_CALL/mextools_call.c	\
	$(MEXTOOLS_CALL_MEX_HEADER) $(MEXTOOLS_CALL_MEX_OBJS)
	$(MEX) $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS)"	\
		LD="$(CC)"	LDFLAGS="$(LDFLAGS)" $(MEXTOOLS_CALL_MEX_OBJS) $(MEXLIBS) $< -output $@

all: $(MEXTOOLS_CALL_MEX_EXECUTABLES)


###############################################################################
# TARGETS AND RULES FOR MDM_SYM_DRE_STEP                                      #
###############################################################################
# MDM_SYM_DRE_STEP SOURCES
MDM_SYM_DRE_STEP_FDEPS = $(wildcard $(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/*.f)
MDM_SYM_DRE_STEP_CDEPS = \
	$(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/MDM_SYM_DRE_STEP_blocked.c	\
	$(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/MDM_SYM_DRE_STEP_unblocked.c	\
	$(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/MDM_SYM_DRE_STEP_helpers.c	\
	$(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/ReLAPACK_dgetrf.c

# MDM_SYM_DRE_STEP HEADER DEPENDENCIES
MDM_SYM_DRE_STEP_MEX_HEADER =	\
$(MEXTOOLS_INC)/mextools.h	\
$(MEXTOOLS_INC)/blas_lapack.h	\
$(MEXTOOLS_INC)/mdm_sym_dre_step.h

# MDM_SYM_DRE_STEP FORTRAN OBJECT FILES
MDM_SYM_DRE_STEP_FOBJS = $(MDM_SYM_DRE_STEP_FDEPS:.f=.$(OBJ))

# RULE FOR FORTRAN SOURCES FOR MDM_SYM_DRE_STEP
$(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/%.$(OBJ): $(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/%.f
	$(FC) $(DEFINES) $(FFLAGS) -c  $< -o $@

# MDM_SYM_DRE_STEP MEX EXECUTABLE TARGET
MDM_SYM_DRE_STEP_MEX_EXECUTABLES=MODIFIED_DAVISON_MAKI_DRE/MDM_SYM_DRE_STEP.$(MEXEXT)

# RULE FOR MDM_SYM_DRE_STEP MEX
MODIFIED_DAVISON_MAKI_DRE/MDM_SYM_DRE_STEP.$(MEXEXT): $(MEXTOOLS_SRC)/MDM_SYM_DRE_STEP/MDM_SYM_DRE_STEP.c\
	$(MDM_SYM_DRE_STEP_CDEPS) $(MDM_SYM_DRE_STEP_FOBJS) $(MDM_SYM_DRE_STEP_MEX_HEADER)
	$(MEX) $(MEXFLAGS) $(DEFINES) CC="$(CC)" CFLAGS="$(CFLAGS)"	\
	LD="$(CC)" LDFLAGS="$(LDFLAGS) $(MDM_SYM_DRE_STEP_FOBJS) $(MISC_MEX_OBJS)" $(MEXLIBS) $(MEXLIBS_MKL) $<	\
	$(MDM_SYM_DRE_STEP_CDEPS) -output $@

#all: $(MDM_SYM_DRE_STEP_MEX_EXECUTABLES)


###############################################################################
# CLEAN TARGET                                                                #
###############################################################################
.PHONY: clean

clean:
	$(DEL) $(BANDED_MEX_OBJS)  \
	$(DEL) $(CHOLMOD_MEX_OBJS) \
	$(DEL) $(MA57_MEX_OBJS) \
	$(DEL) $(MISC_MEX_OBJS) \
	$(DEL) $(QRTOOLS_MEX_OBJS) \
	$(DEL) $(UMFPACK_MEX_OBJS) \
	$(DEL) $(MEXTOOLS_CALL_MEX_EXECUTABLES) \
	$(DEL) $(MDM_SYM_DRE_STEP_MEX_EXECUTABLES) \
	$(DEL) $(MDM_SYM_DRE_STEP_FOBJS)
