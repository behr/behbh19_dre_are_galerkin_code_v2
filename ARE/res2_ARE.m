function res2 = res2_ARE(A, E, B, C, Z, D)
%RES2_ARE  computes the 2-norm residual of a  low-rank solution
%   of a Riccati equation.
%
%   Riccati equation:
%
%       0 = A' X   +    X A -    X B B' X   + C'C  (standard)
%       0 = A' X E + E' X A - E' X B B' X E + C'C  (generalized)
%
%   RES2 = RES2_ARE(A, E, B, C, Z) computes the 2-norm residual of
%   a low-rank solution factor Z for the standard / generalized algebraic
%   Riccati equation. This means X is approximated by Z*Z'.
%   RES2 = RES2_ARE(A, E, B, C, Z, D) computes the 2-norm residual of
%   a low-rank solution given by Z*D*Z'.
%   A and E must be n-by-n matrices. B/C must be n-by-b / c-by-n.
%   If E is empty the identity is used. If B is empty 0 is used.
%
%   See also MESS_CARE, RES2_LYAP.
%
%   Author: Maximilian Behr (MPI Magdeburg, CSC)

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

%% check input parameters
validateattributes(A, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
assert(issparse(A), '%s must be sparse', inputname(1));
n = size(A, 1);
isemptyE = isempty(E);
if ~isemptyE
    validateattributes(E, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan', 'size', [n, n]}, mfilename, inputname(2));
    assert(issparse(E), '%s must be sparse', inputname(2))
end
isemptyB = isempty(B);
if ~isemptyB
    validateattributes(B, {'double'}, {'nonsparse', 'real', 'finite', 'nonnan', '2d', 'size', [n, NaN]}, mfilename, inputname(3));
end
validateattributes(C, {'double'}, {'nonsparse', 'real', 'finite', 'nonnan', '2d', 'size', [NaN, n]}, mfilename, inputname(4));
validateattributes(Z, {'double'}, {'nonsparse', 'real', 'finite', 'nonnan', '2d', 'size', [n, NaN]}, mfilename, inputname(5));

hasD = false;
if nargin >= 6
    z = size(Z, 2);
    validateattributes(D, {'double'}, {'square', 'nonsparse', 'real', 'finite', 'nonnan', '2d', 'size', [z, z]}, mfilename, inputname(6));
    hasD = true;
end

%% setup opts
opts.isreal = true;
opts.issym = true;

if isemptyE
    if isemptyB
        %res2 = abs(eigs(@(x) A'*(Z*(Z'*x)) + Z*(Z'*(A*x))  + C'*(C*x), n, 1, 'LM',opts));
        if hasD
            res2 = abs(eigs(@(x) lyap_op2(A, C, Z, D, x), n, 1, 'LM', opts));
        else
            res2 = abs(eigs(@(x) lyap_op(A, C, Z, x), n, 1, 'LM', opts));
        end
    else
        %res2 = abs(eigs(@(x) A'*(Z*(Z'*x)) + Z*(Z'*(A*x)) - Z*(Z'*(B*(B'*(Z*(Z'*x))))) + C'*(C*x), n, 1, 'LM',opts));
        if hasD
            res2 = abs(eigs(@(x) ric_op2(A, B, C, Z, D, x), n, 1, 'LM', opts));
        else
            res2 = abs(eigs(@(x) ric_op(A, B, C, Z, x), n, 1, 'LM', opts));
        end
    end
else
    if isemptyB
        %res2 = abs(eigs(@(x) A'*(Z*(Z'*(E*x))) + E'*(Z*(Z'*(A*x))) + C'*(C*x), n, 1, 'LM',opts));
        if hasD
            res2 = abs(eigs(@(x) glyap_op2(A, E, C, Z, D, x), n, 1, 'LM', opts));
        else
            res2 = abs(eigs(@(x) glyap_op(A, E, C, Z, x), n, 1, 'LM', opts));
        end
    else
        %res2 = abs(eigs(@(x) A'*(Z*(Z'*(E*x))) + E'*(Z*(Z'*(A*x))) - E'*(Z*(Z'*(B*(B'*(Z*(Z'*(E*x))))))) + C'*(C*x), n, 1, 'LM',opts));
        if hasD
            res2 = abs(eigs(@(x) gric_op2(A, E, B, C, Z, D, x), n, 1, 'LM', opts));
        else
            res2 = abs(eigs(@(x) gric_op(A, E, B, C, Z, x), n, 1, 'LM', opts));
        end
    end
end
end

% lyapunov
function y = lyap_op(A, C, Z, x)
y = A' * (Z * (Z' * x)) + Z * (Z' * (A * x)) + C' * (C * x);
end

function y = lyap_op2(A, C, Z, D, x)
y = A' * (Z * (D * (Z' * x))) + Z * (D * (Z' * (A * x))) + C' * (C * x);
end

function y = glyap_op(A, E, C, Z, x)
y = A' * (Z * (Z' * (E * x))) + E' * (Z * (Z' * (A * x))) + C' * (C * x);
end

function y = glyap_op2(A, E, C, Z, D, x)
y = A' * (Z * (D * (Z' * (E * x)))) + E' * (Z * (D * (Z' * (A * x)))) + C' * (C * x);
end

% riccati
function y = ric_op(A, B, C, Z, x)
y1 = (Z * (Z' * x));
y = A' * y1 + Z * (Z' * (A * x)) - Z * (Z' * (B * (B' * y1))) + C' * (C * x);
end

function y = ric_op2(A, B, C, Z, D, x)
y1 = (Z * (D * (Z' * x)));
y = A' * y1 + Z * (D * (Z' * (A * x))) - Z * (D * (Z' * (B * (B' * y1)))) + C' * (C * x);
end

function y = gric_op(A, E, B, C, Z, x)
y1 = Z * (Z' * (E * x));
y = A' * y1 + E' * (Z * (Z' * (A * x))) - E' * (Z * (Z' * (B * (B' * y1)))) + C' * (C * x);
end

function y = gric_op2(A, E, B, C, Z, D, x)
y1 = Z * (D * (Z' * (E * x)));
y = A' * y1 + E' * (Z * (D * (Z' * (A * x)))) - E' * (Z * (D * (Z' * (B * (B' * y1))))) + C' * (C * x);
end
