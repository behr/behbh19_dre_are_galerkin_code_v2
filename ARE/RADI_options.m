classdef RADI_options < copy_handle
    %RADI_OPTIONS hold all options for the radi algebraic Riccati equation solver.
    %
    %   RADI_OPTIONS properties:
    %       maxiter             - double, scalar, nonnegative, integer, maximum number of iterations, default: 100
    %       abs2res_tol         - double, scalar, nonegative, tolerance for absolute 2-norm residual, default: 1e-10
    %       rel2res_tol         - double, scalar, nonnegative, tolerance for relative 2-norm residual, default: 1e-10
    %       shifts_l            - double, scalar, nonnegative, integer, number of columns of Z used for residual Hamiltonian projection shifts, default: 4
    %       use_shifted_solver  - logical, use the ShiftedSolver, default: false
    %       verbose             - logical, default: false
    %
    %   The parameter shifts_l controls the size of the projection space
    %   for residual Hamiltonian projection based shift parameter.
    %   The last shifts_l * p columns of Z are used for the projection
    %   space, where p is the number of rows of the matrix C.
    %   If p is inf the whole low-rank factor Z is used for the
    %   projection space.
    %
    %   See also RADI and TEST_RADI.
    %
    % Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = public)
        % double, scalar, nonnegative, integer, maximum number of iterations, default: 100
        maxiter
        % double, scalar, nonegative, tolerance for absolute 2-norm residual, default: 1e-10
        abs2res_tol
        % double, scalar, nonnegative, tolerance for relative 2-norm residual, default: 1e-10
        rel2res_tol
        % double, scalar, nonnegative, integer, number of columns of Z used for residual Hamiltonian projection shifts, default: 4
        shifts_l
        % logical, use the ShiftedSolver, default: false
        use_shifted_solver
        % logical, default: false
        verbose
    end

    methods(Access = public)
        function obj = RADI_options(maxiter, abs2res_tol, rel2res_tol, shifts_l, use_shifted_solver, verbose)
            %RADI_OPTIONS Constructor for the options class.
            %
            %   OPT = RADI_OPTIONS(MAXITER, ABS2RES_TOL, REL2RES_TOL, SHIFTS_L, USE_SHIFTED_SOLVER, VERBOSE)
            %   generates an object of this class holding all available options for the
            %   radi solver.
            %
            %   OPT = RADI_OPTIONS(MAXITER, ABS2RES_TOL, REL2RES_TOL, SHIFTS_L, USE_SHIFTED_SOLVER)
            %   OPT = RADI_OPTIONS(MAXITER, ABS2RES_TOL, REL2RES_TOL, SHIFTS_L)
            %   OPT = RADI_OPTIONS(MAXITER, ABS2RES_TOL, REL2RES_TOL)
            %   OPT = RADI_OPTIONS(MAXITER, ABS2RES_TOL)
            %   OPT = RADI_OPTIONS(MAXITER)
            %   OPT = RADI_OPTIONS()
            %
            %   The missing property arguments are set to its default value.
            %
            %   See also RADI AND TEST_RADI.
            %
            % Author: Maximilian Behr
            obj.maxiter = 100;
            obj.abs2res_tol = 1e-10;
            obj.rel2res_tol = 1e-10;
            obj.shifts_l = 4;
            obj.use_shifted_solver = false;
            obj.verbose = false;

            if nargin >= 1
                obj.maxiter = maxiter;
            end

            if nargin >= 2
                obj.abs2res_tol = abs2res_tol;
            end

            if nargin >= 3
                obj.rel2res_tol = rel2res_tol;
            end

            if nargin >= 4
                obj.shifts_l = shifts_l;
            end

            if nargin >= 5
                obj.use_shifted_solver = use_shifted_solver;
            end

            if nargin >= 6
                obj.verbose = verbose;
            end
        end
    end

    methods
        % setter methods
        function set.maxiter(obj, in_maxiter)
            validateattributes(in_maxiter, {'double'}, {'real', 'scalar', 'integer', 'positive', 'finite', 'nonnan'}, mfilename);
            obj.maxiter = in_maxiter;
        end

        function set.abs2res_tol(obj, in_abs2res_tol)
            validateattributes(in_abs2res_tol, {'double'}, {'real', 'scalar', 'positive', 'finite', 'nonnan'}, mfilename);
            obj.abs2res_tol = in_abs2res_tol;
        end

        function set.rel2res_tol(obj, in_rel2res_tol)
            validateattributes(in_rel2res_tol, {'double'}, {'real', 'scalar', 'positive', 'finite', 'nonnan'}, mfilename);
            obj.rel2res_tol = in_rel2res_tol;
        end

        function set.shifts_l(obj, in_shifts_l)
            if in_shifts_l == inf
                obj.shifts_l = in_shifts_l;
            else
                validateattributes(in_shifts_l, {'double'}, {'real', 'scalar', 'integer', 'positive', 'nonnan'}, mfilename);
                obj.shifts_l = in_shifts_l;
            end
        end

        function set.use_shifted_solver(obj, in_use_shifted_solver)
            validateattributes(in_use_shifted_solver, {'logical'}, {}, mfilename);
            obj.use_shifted_solver = in_use_shifted_solver;
        end

        function set.verbose(obj, in_verbose)
            validateattributes(in_verbose, {'logical'}, {}, mfilename);
            obj.verbose = in_verbose;
        end

    end
end
