classdef RADI_status < copy_handle
    %RADI_STATUS holds information about RADI iteration.
    %
    %   RADI_STATUS properties:
    %       converged           - logical, true if converged otherwise false
    %       iter                - double, scalar, nonnegative, integer number of iterations
    %       abs2res             - double, scalar, absolute 2-norm residual
    %       rel2res             - double, scalar, relative 2-norm residual
    %       abs2res_hist        - double, vector, absolute 2-norm residuals in each iteration
    %       rel2res_hist        - double, vector, relative 2-norm residuals in each iteration
    %       shift_hist          - double, complex, shift parameter used in iteration
    %       time_overall        - double, scalar, overall wall time for RADI iteration
    %       time_linear_solves  - double, scalar, wall time for solving shifted systems
    %       time_shifts         - double, scalar, wall time for shift parameter computation
    %
    %   RADI_STATUS methods:
    %       add_to_status           - Add data to status object.
    %       finalize                - Finalize the status object.
    %       tic_time_overall        - Initialize the timer for overall wall time.
    %       toc_time_overall        - Finalize the timer for overall wall time.
    %       tic_time_linear_solves  - Initialize the timer for shifted system wall time.
    %       toc_time_linear_solves  - Finalize the timer for shifted system wall time.
    %       tic_time_shifts         - Initialize the timer for shift parameter computation wall time.
    %       toc_time_shifts         - Finalize the timer for shift parameter computation wall time.
    %
    %   See also RADI and TEST_RADI.
    %
    % Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = public)
        % logical, true if converged otherwise false
        converged,
        % double, scalar, nonnegative, integer number of iterations taken
        iter
        % double, scalar, absolute 2-norm residual
        abs2res
        % double, scalar, relative 2-norm residual
        rel2res
        % double, vector, absolute 2-norm residuals in each iteration
        abs2res_hist
        % double, vector, relative 2-norm residuals in each iteration
        rel2res_hist
        % double, complex, shift parameter used in iteration
        shift_hist
        % double, scalar, wall time for iteration
        time_overall
        % double, scalar, wall time for solving shifted systems
        time_linear_solves
        % double, scalar, wall time for shift parameter computation
        time_shifts
    end

    % internal variables for wall time measurements
    properties(Access = private)
        tic_time_overall_ = 0;
        tic_time_linear_solves_ = 0;
        tic_time_shifts_ = 0;
        finalized_ = false;
    end

    methods(Access = public)
        function obj = RADI_status()
            %RADI_STATUS Constructor for the options class.
            %
            %   RADI_STAT = RADI_STATUS() generates an object of this class
            %   with preallocated and with default values initialized fields.
            %   MAXITER is the maximum number of iterations defined in
            %   RADI_options.
            %
            %   RADI_STAT = RADI_STATUS()
            %
            %   See also RADI, RADI_OPTIONS AND TEST_RADI.
            %
            % Author: Maximilian Behr
            maxiter_default = 500;
            obj.converged = false;
            obj.iter = 0;
            obj.abs2res = 0;
            obj.rel2res = 0;
            obj.abs2res_hist = zeros(maxiter_default,1);
            obj.rel2res_hist = zeros(maxiter_default,1);
            obj.shift_hist = complex(zeros(maxiter_default,1));
            obj.time_overall = 0;
            obj.time_linear_solves = 0;
            obj.time_shifts = 0;
        end

        function add_to_status(obj, abs2res, rel2res, shift)
            %ADD_TO_STATUS  Add data to status object.
            %
            %   stat.ADD_TO_STATUS(ABS2RES, REL2RES, SHIFT) add the
            %   ABS2RES, REL2RES and SHIFT to the status object.
            %   The internal iteration counter is incremented.
            %
            %   See also RADI.
            %
            %   Author: Maximilian Behr
            check_finalized(obj);
            obj.iter = obj.iter + 1;
            obj.abs2res = abs2res;
            obj.rel2res = rel2res;
            obj.abs2res_hist(obj.iter) = abs2res;
            obj.rel2res_hist(obj.iter) = rel2res;
            obj.shift_hist(obj.iter) = shift;
        end

        function finalize(obj)
            %FINALIZE  Resizes the internal vectors holding status information.
            %
            %   stat.FINALIZE() resizes the internal vectors holding
            %   the status information about the RADI iteration.
            %
            %   See also RADI.
            %
            %   Author: Maximilian Behr
            check_finalized(obj);
            obj.abs2res_hist = obj.abs2res_hist(1:obj.iter);
            obj.rel2res_hist = obj.rel2res_hist(1:obj.iter);
            obj.shift_hist = obj.shift_hist(1:obj.iter);
            obj.finalized_ = true;
        end

        function tic_time_overall(obj)
            check_finalized(obj);
            obj.tic_time_overall_ = tic();
        end

        function toc_time_overall(obj)
            check_finalized(obj);
            if obj.tic_time_overall_
                obj.time_overall = toc(obj.tic_time_overall_);
                obj.tic_time_overall_ = 0;
            else
                error('tic_time_overall_ was not initialized by tic_time_overall.');
            end
        end

        function tic_time_linear_solves(obj)
            check_finalized(obj);
            obj.tic_time_linear_solves_ = tic();
        end

        function toc_time_linear_solves(obj)
            check_finalized(obj);
            if obj.tic_time_linear_solves_
                obj.time_linear_solves = obj.time_linear_solves + toc(obj.tic_time_linear_solves_);
                obj.tic_time_linear_solves_ = 0;
            else
                error('tic_time_linear_solves_ was not initialized by tic_time_linear_solves.');
            end
        end

        function tic_time_shifts(obj)
            check_finalized(obj);
            obj.tic_time_shifts_ = tic();
        end

        function toc_time_shifts(obj)
            check_finalized(obj);
            if obj.tic_time_shifts_
                obj.time_shifts = obj.time_shifts + toc(obj.tic_time_shifts_);
                obj.tic_time_shifts_ = 0;
            else
                error('tic_time_shifts_ was not initialized by tic_time_shifts.');
            end
        end
    end

    methods
        % setter methods
        function set.converged(obj, in_converged)
            check_finalized(obj);
            validateattributes(in_converged, {'logical'}, {}, mfilename);
            obj.converged = in_converged;
        end

        function set.iter(obj, in_iter)
            check_finalized(obj);
            validateattributes(in_iter, {'double'}, {'real', 'scalar', 'nonnegative', 'finite', 'nonnan', 'integer'}, mfilename);
            obj.iter = in_iter;
        end

        function set.abs2res(obj, in_abs2res)
            check_finalized(obj);
            validateattributes(in_abs2res, {'double'}, {'real', 'scalar', 'nonnegative', 'finite', 'nonnan'}, mfilename);
            obj.abs2res = in_abs2res;
        end

        function set.rel2res(obj, in_rel2res)
            check_finalized(obj);
            validateattributes(in_rel2res, {'double'}, {'real', 'scalar', 'nonnegative', 'finite', 'nonnan'}, mfilename);
            obj.rel2res = in_rel2res;
        end

        function set.abs2res_hist(obj, in_abs2res_hist)
            check_finalized(obj);
            validateattributes(in_abs2res_hist, {'double'}, {'real', 'vector', 'nonnegative', 'finite', 'nonnan'}, mfilename);
            obj.abs2res_hist = in_abs2res_hist;
        end

        function set.rel2res_hist(obj, in_rel2res_hist)
            check_finalized(obj);
            validateattributes(in_rel2res_hist, {'double'}, {'real', 'vector', 'nonnegative', 'finite', 'nonnan'}, mfilename);
            obj.rel2res_hist = in_rel2res_hist;
        end

        function set.shift_hist(obj, in_shift_hist)
            check_finalized(obj);
            validateattributes(in_shift_hist, {'double'}, {'vector', 'finite', 'nonnan'}, mfilename);
            obj.shift_hist = in_shift_hist;
        end

        function set.time_overall(obj, in_time_overall)
            check_finalized(obj);
            validateattributes(in_time_overall, {'double'}, {'real', 'scalar', 'nonnegative', 'finite', 'nonnan'}, mfilename);
            obj.time_overall = in_time_overall;
        end

        function set.time_linear_solves(obj, in_time_linear_solves)
            check_finalized(obj);
            validateattributes(in_time_linear_solves, {'double'}, {'real', 'scalar', 'nonnegative', 'finite', 'nonnan'}, mfilename);
            obj.time_linear_solves = in_time_linear_solves;
        end

        function set.time_shifts(obj, in_time_shifts)
            check_finalized(obj);
            validateattributes(in_time_shifts, {'double'}, {'real', 'scalar', 'nonnegative', 'finite', 'nonnan'}, mfilename);
            obj.time_shifts = in_time_shifts;
        end
    end

    methods(Access = private)
        function check_finalized(obj)
            if obj.finalized_
                error('finalized was called, no internal changes possible anymore.');
            end
        end
    end
end
