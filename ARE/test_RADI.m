classdef test_RADI < matlab.unittest.TestCase
    %TEST_RADI   Test for RADI algebraic Riccati equation solver.
    %
    %   TEST_RADI solves the algebraic Riccati equation and
    %   checks the absolute and relative residuals.
    %   Several benchmark examples are used.
    %
    %   See also RADI and RADI_OPTIONS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        instance = {'fom', 'heat-cont', 'cookie', 'pde', 'rail_5177', 'rail_20209', 'conv_diff_3_6400', 'tridiag', 'chip_v0', 'flow_v0', 'filter2D', 'T2DAL', 'T2DAH'};
        isemptyB = {false, true};
        shifts_l = {2, 4};
        use_shifted_solver = {false, true};
    end

    properties
        abs2res_tol = 1e-3;
        rel2res_tol = 1e-3;
        maxiter  = 1200;
        verbose = ~isCIrun();
    end

    methods(Test)

        function test(obj, instance, isemptyB, shifts_l, use_shifted_solver) %#ok<*INUSL>

            if obj.verbose
                fprintf('\n');
            end

            %% load data
            if ~strcmp(instance,'tridiag')
                data = load(instance);
                A = data.A;
                B = full(data.B);
                C = full(data.C);
                n = size(A, 1);

                if isfield(data, 'E')
                    E = data.E;
                else
                    E = [];
                end
            else
                % tridiag is a test example with a nonsymmetric E
                alpha = -1;
                beta = -2;
                gamma = -1;
                n = 500;
                A = gallery('tridiag', n, alpha, beta, gamma);
                E = gallery('tridiag', n, 0, 1, 1);
                B = 2 * ones(n, 1);
                C = 3 * ones(1, n);
            end

            if isemptyB
                B = [];
            end

            %% skip some tests on ci run for speed up
            if isCIrun() && any(strcmp({'chip_v0', 'flow_v0', 'filter2D', 'T2DAH', 'GasSensor'}, instance))
                return;
            end

            %% lyapunov equation does not seems to work very well for T2DAH, T2DAL skip this test
            if any(strcmp({'T2DAL', 'T2DAH'}, instance)) && isemptyB
                return;
            end

            %% create options for radi
            opt = RADI_options();
            opt.maxiter = obj.maxiter;
            opt.abs2res_tol = obj.abs2res_tol / 1e+2; % just to be sure that tolerances are reached
            opt.rel2res_tol = obj.rel2res_tol / 1e+2; % just to be sure that tolerances are reached
            opt.shifts_l = shifts_l;
            opt.use_shifted_solver = use_shifted_solver;
            opt.verbose = obj.verbose;

            %% solve the ARE using RADI
            [Z, stat] = RADI(A, E, B, C, opt);
            z = size(Z, 2);

            %% compute residual
            nrm2RHS = norm(C)^2;
            abs2res_check = res2_ARE(A, E, B , C, Z);
            rel2res_check = abs2res_check / nrm2RHS;

            %% print results
            if obj.verbose
                f(1) = fprintf('instance = %s\n', instance);
                f(2) = fprintf('n = %d, isemptyB = %s, cols Z = %d, shifts_l = %d, use_shifted_solver = %d\n', n, string(isemptyB), z, shifts_l, use_shifted_solver);
                f(3) = fprintf('RADI: converged                      = %s\n', string(stat.converged));
                f(4) = fprintf('RADI: iterations                     = %d\n', stat.iter);
                f(5) = fprintf('RADI: time                           = %f\n', stat.time_overall);
                f(6) = fprintf('RADI: time linear solves             = %f\n', stat.time_linear_solves);
                f(7) = fprintf('RADI: time shifts                    = %f\n', stat.time_shifts);
                f(8) = fprintf('RADI:      abs./rel. 2-norm residual = %.2e/%.2e\n', stat.abs2res, stat.rel2res);
                f(9) = fprintf('Computed:  abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_check, rel2res_check);
                f(10) = fprintf('Tolerance: abs./rel. 2-norm residual = %.2e/%.2e\n', obj.abs2res_tol, obj.rel2res_tol);
                fprintf('%s\n', repmat('-',1, max(f) - 1));
            end

            %% check tolerance
            obj.fatalAssertTrue(stat.converged);
            obj.fatalAssertLessThan(stat.abs2res, obj.abs2res_tol);
            obj.fatalAssertLessThan(abs2res_check, obj.abs2res_tol);
            obj.fatalAssertLessThan(stat.rel2res, obj.rel2res_tol);
            obj.fatalAssertLessThan(rel2res_check, obj.rel2res_tol);
        end
    end
end
