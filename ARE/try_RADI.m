%TRY_RADI    Playaround script for RADI solver.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all; clc

%% data
instance = 'conv_diff_3_6400';
data = load(instance);
A = data.A;
if isfield(data, 'E')
    E = data.E;
else
    E = [];
end
B = data.B;
C = data.C;

%% solve ARE using RADI
opt = RADI_options();
opt.verbose = false;
opt.maxiter = 500;
opt.use_shifted_solver = true;
[Z, stat] = RADI(A, E, B, C, opt);
nrm2RHS = norm(C)^2;

%% print results and truncate
fprintf('Status about RADI process\n');
disp(stat);
abs2res = res2_ARE(A, E, B , C, Z);
rel2res = abs2res / nrm2RHS;

svdZ = svd(Z,0);
tol = svdZ(1,1)*eps;
trunc_Z = truncate_ZZT(Z, tol);
trunc_abs2res = res2_ARE(A, E, B , C, trunc_Z);
trunc_rel2res = trunc_abs2res / nrm2RHS;

fprintf('abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res, rel2res);
fprintf('size(Z) = %d-x-%d\n', size(Z));

fprintf('trunc abs./rel. 2-norm residual = %.2e/%.2e\n', trunc_abs2res, trunc_rel2res);
fprintf('size(trunc_Z) = %d-x-%d\n', size(trunc_Z));

semilogy(1:length(svdZ), svdZ);




