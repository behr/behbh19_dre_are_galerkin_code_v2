function [Z, stat] = RADI(A, E, B, C, options)
%RADI     Rational low-rank ADI method for large-scale symmetric algebraic Riccati
%   equation:
%
%       0 = A' X E + E' X A - E' X B B' X E + C' C (ARE)
%
%   [Z, STAT] = RADI(A, E, B, C, OPTIONS)
%   returns a low-rank approximation of the symmetric
%   positive semidefinite solution X of the ARE. That is
%   Z * Z' approximates X.
%   A, E, B and C must be double matrix. A and E are n-by-n and
%   sparse. If E is empty the identity matrix is used.
%   B and C are dense of size n-by-b and c-by-n.
%   If B is empty the algebraic Lyapunov equation is solved.
%   OPTIONS must be an instance of RADI_options.
%   The method is efficient if b,c is much smaller than n.
%   STAT is an instance of RADI_status and holds information
%   about the iteration process.
%
%   For references see:
%   [1] Benner, P., Kürschner, P., Saak, J.,
%       RADI: A low-rank ADI-type algorithm for large scale algebraic Riccati equations
%       Numerical Algorithms, 2018, https://doi.org/10.1007/s00211-017-0907-5
%
%   See also: TEST_RADI and RADI_OPTIONS.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% check input parameters
validateattributes(A, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
assert(issparse(A), '%s must be sparse', inputname(1));
n = size(A, 1);
isemptyE = isempty(E);
if ~isemptyE
    validateattributes(E, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan', 'size', [n, n]}, mfilename, inputname(2));
    assert(issparse(E), '%s must be sparse', inputname(2));
end
isemptyB = isempty(B);
if ~isemptyB
    validateattributes(B, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [n, NaN]}, mfilename, inputname(3));
end
validateattributes(C, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [NaN, n]}, mfilename, inputname(4));
validateattributes(options, {'RADI_options'}, {}, mfilename, inputname(5));

%% get values from options
abs2res_tol = options.abs2res_tol;
rel2res_tol = options.rel2res_tol;
maxiter = options.maxiter;
shifts_l = options.shifts_l;
use_shifted_solver = options.use_shifted_solver;
verbose = options.verbose;

%% prepare status object
stat = RADI_status();
stat.tic_time_overall();

%% initialize scalar variables
converged = false;
iter = 0;
abs2res = inf;
rel2res = inf;
nrmRHS = norm(C)^2;
rowsC = size(C, 1);

%% initialize matrix variables
R = C';
K = 0;
Y = []; %#ok<*NASGU>
Z = [];
eye_rowsC = eye(rowsC);

%% transpose the matrices
AT = A';
if isemptyE
    ET = speye(n, n);
else
    ET = E';
end

%% setup shifted solver
if use_shifted_solver
    ShiftedSol = ShiftedSolver(AT, ET, true);
end

%% the RADI iteration
while iter <= maxiter

    %% get shift
    stat.tic_time_shifts();
    if iter == 0
        shift = initial_shift(A, E, B, C);
    else
        shift = next_shift(A, E, B, R, K, Z, shifts_l);
    end
    stat.toc_time_shifts();

    %% solve shifted system
    stat.tic_time_linear_solves();
    if iter == 0
        if use_shifted_solver
            V = ShiftedSol.solve(shift, R);
        else
            V = (AT + shift * ET) \ R;
        end

    else
        if ~isempty(B)
            %V = radi_smw(AT+shift*ET, K, B, R);
            if use_shifted_solver
                V = radi_smw(AT, shift, ET, ShiftedSol, K, B, R);
            else
                V = radi_smw(AT, shift, ET, [], K, B, R);
            end

        else
            if use_shifted_solver
                V = ShiftedSol.solve(shift, R);
            else
                V = (AT + shift * ET) \ R;
            end

        end
    end
    stat.toc_time_linear_solves();

    if isreal(shift)
        V = real(V);
        if ~isempty(B)
            VtB = V' * B;
            Yhat = eye_rowsC + (VtB * VtB');
            Ychol = chol(Yhat);
            V = V / Ychol;
            Z = [Z, sqrt(-2*real(shift)) * V]; %#ok<*AGROW>
            ETVYhat = (ET * V) / Ychol';
            R = R + -2 * real(shift) * ETVYhat;
            K = K + -2 * real(shift) * ETVYhat * VtB;
        else
            Z = [Z, sqrt(-2*real(shift)) * V];
            ETV = ET * V;
            R = R + -2 * real(shift) * ETV;
        end
    else
        sa = abs(shift);
        sr = real(shift);
        si = imag(shift);

        V1 = sqrt(-2*sr) * real(V);
        V2 = sqrt(-2*sr) * imag(V);

        if ~isempty(B)
            Vr = V1' * B;
            Vi = V2' * B;

            F1 = [-sr / sa * Vr - si / sa * Vi; si / sa * Vr - sr / sa * Vi];
            F2 = [Vr; Vi];
            F3 = [si / sa * eye_rowsC; sr / sa * eye_rowsC];

            Yhat = blkdiag(eye_rowsC, 1/2*eye_rowsC) - 1 / (4 * sr) * (F1 * F1') - 1 / (4 * sr) * (F2 * F2') - 1 / 2 * (F3 * F3');
            Ychol = chol(Yhat);

            V = [V1, V2] / Ychol;
            Z = [Z, V];
            EVYhat = (ET * V) / Ychol';
            R = R + sqrt(-2*sr) * EVYhat(:, 1:rowsC);
            K = K + EVYhat * F2;
        else
            F3 = [si / sa * eye_rowsC; sr / sa * eye_rowsC];
            Yhat = blkdiag(eye_rowsC, 1/2*eye_rowsC) - 1 / 2 * (F3 * F3');
            Ychol = chol(Yhat);
            V = [V1, V2] / Ychol;
            Z = [Z, V];
            EVYhat = (ET * V) / Ychol';
            R = R + sqrt(-2*sr) * EVYhat(:, 1:rowsC);
        end
    end
    iter = iter + 1;

    %% residual stuff
    abs2res = norm(R)^2;
    rel2res = abs2res / nrmRHS;

    %% print info
    if verbose
        fprintf('iter = %3d, abs. / rel. res = %.2e / %.2e, shift = %.2e + %.2e i\n', iter, abs2res, rel2res, real(shift), imag(shift));
    end

    %% add to status object
    stat.add_to_status(abs2res, rel2res, shift);

    %% check for convergence
    if (abs2res < abs2res_tol) && (rel2res < rel2res_tol)
        converged = true;
        break;
    end

end

%% print warning
if ~converged
    warning('RADI did not converge %d iterations.', maxiter);
end

%% finalize the status object
stat.toc_time_overall();
stat.converged  = converged;
stat.finalize();

end

function shift = initial_shift(A, E, B, C)

%% compute projected hamiltonian
[QC, ~] = qr(C', 0);
Aproj = QC' * A * QC;
if ~isempty(B)
    Bproj = QC' * B;
    Gproj = Bproj * Bproj';
else
    Gproj = zeros(size(Aproj));
end
Cproj = QC' * C';
Rproj = Cproj * Cproj';
Hproj = [Aproj, Gproj; Rproj, -Aproj'];

%% compute eigenvalues
if ~isempty(E)
    Eproj = QC' * E * QC;
    EETproj = blkdiag(Eproj, Eproj');
    [evecs, evals] = eig(Hproj, EETproj, 'vector');
else
    [evecs, evals] = eig(Hproj, 'vector');
end

%% take only the stable ones
idx = real(evals) < 0;
evals = evals(idx);
evecs = evecs(:, idx);
numel_evals = numel(evals);

if numel_evals == 0
    warning(['No stable eigenvalues of projected Hamiltonian.\n', ...
        'Eigenvalues of Hamiltonian occur in quadruples.\n', ...
        'Either all eigenvalues are on imaginary axis or ', ...
        'eigenvalue computation failed.\n', ...
        'Return -1 as shift.'
        ], '');
    shift = -1;
    return;
end

%% find the shift with largest update
n = size(evecs, 1) / 2;
update = zeros(numel_evals, 1);
for i = 1:numel_evals
    r = evecs(1:n, i);
    q = evecs(n+1:end, i);
    nrmq = norm(q);
    if ~isempty(E)
        update(i) = (nrmq / abs(q'*Eproj*r)) * nrmq;
    else
        update(i) = (nrmq / abs(q'*r)) * nrmq;
    end
end
[~, idx] = max(update);
shift = evals(idx);

if imag(shift) < 1e-8
    shift = real(shift);
end
end


function shift = next_shift(A, E, B, R, K, Z, shifts_l)

%% compute projected hamiltonian
Zhat = Z(:, max(1, size(Z, 2)-shifts_l):end);
[Q, ~] = qr(Zhat, 0);
if ~isempty(B)
    Bproj = Q' * B;
    Aproj = Q' * A * Q - (Bproj) * (K' * Q);
    Gproj = Bproj * Bproj';
else
    Aproj = Q' * A * Q;
    Gproj = zeros(size(Aproj));
end

Cproj = Q' * R;
Rproj = Cproj * Cproj';
Hproj = [Aproj, Gproj; Rproj, -Aproj'];

%% compute eigenvalues
if ~isempty(E)
    Eproj = Q' * E * Q;
    EETproj = blkdiag(Eproj, Eproj');
    [evecs, evals] = eig(Hproj, EETproj, 'vector');
else
    [evecs, evals] = eig(Hproj, 'vector');
end

%% take only the stable ones
idx = real(evals) < 0;
evals = evals(idx);
evecs = evecs(:, idx);
numel_evals = numel(evals);

if numel_evals == 0
    warning(['No stable eigenvalues of projected Hamiltonian.\n', ...
        'Eigenvalues of Hamiltonian occur in quadruples.\n', ...
        'Either all eigenvalues are on imaginary axis or ', ...
        'eigenvalue computation failed.\n', ...
        'Return -1 as shift.'
        ], '');
    shift = -1;
    return;
end

%% find the shift with largest update
n = size(evecs, 1) / 2;
update = zeros(numel_evals, 1);
for i = 1:numel_evals
    r = evecs(1:n, i);
    q = evecs(n+1:end, i);
    nrmq = norm(q);
    if ~isempty(E)
        update(i) = (nrmq / abs(r'*Eproj*q)) * nrmq;
    else
        update(i) = (nrmq / abs(r'*q)) * nrmq;
    end

end
[~, idx] = max(update);
shift = evals(idx);

if imag(shift) < 1e-8
    shift = real(shift);
end

end

function x = radi_smw(A, shift, E, ShiftedSol, U, V, y)
eye_p = eye(size(V, 2));
colsy = size(y,2);
if isempty(ShiftedSol)
    AinvyU = (A + shift*E)\([y,U]);
else
    AinvyU = ShiftedSol.solve(shift,[y,U]);
end
Ainvy = AinvyU(:,1:colsy);
AinvU = AinvyU(:,(colsy+1):end);
x = Ainvy + AinvU * linsolve((eye_p - V' * AinvU), (V' * Ainvy));
end