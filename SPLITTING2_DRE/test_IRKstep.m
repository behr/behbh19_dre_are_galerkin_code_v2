classdef test_IRKstep < matlab.unittest.TestCase
    %TEST_IRKSTEP   Test for IRKstep class.
    %
    %   TEST_IRKstep computes a reference solution for
    %
    %       M \dot{x}(t)=Ax(t), x(0)=x0=ones(n, 1).
    %
    %   using the EXPM matlab routine at t = T.
    %
    %   A IRKstep class instance is applied to the problem and compares the
    %   relative and absolute 2 norm difference.
    %   If the prescribed tolerance are reached the next problem is used.
    %   TEST_IRKstep checks the implemented implicit Runge-Kutta methods
    %   using different step sizes and different problems.
    %   The cache functionality is also tested.
    %
    %   See also IRKSTEP AND IRKMETHOD.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        problem = { ...
            struct('instance', 'rail_371', 'T', 1, 'hs', 1./(2.^(2:1:16)), 'reltol', 1e-5, 'abstol', 1e-5) ...
            struct('instance', 'CDplayer', 'T', 2^-8, 'hs', 1./(2.^(8:2:24)), 'reltol', 1e-3, 'abstol', 1e-3) ...
            struct('instance', 'build', 'T', 2^-6, 'hs', 1./(2.^(8:2:24)), 'reltol', 1e-3, 'abstol', 1e-3) ...
            };
        irk = [cellfun(@(s) IRKmethod_GAUSS_LEGENDRE(s), num2cell(2:15), 'UniformOutput', false), ...
            cellfun(@(s) IRKmethod_RADAUIIA(s), num2cell(2:15), 'UniformOutput', false)];
    end

    properties
        verbose = ~isCIrun();
    end

    methods(Test)
        function test_cache(obj, irk) %#ok<*INUSL>

            %% set data
            if obj.verbose
                fprintf('\n');
                fprintf('Check some cache functionality for %s\n', char(irk));
            end
            n = 100;
            A = sprand(n, n, 0.1);
            x0 = rand(n, 2);

            %% create IRKmethod
            irk_step = IRKstep(A, [], irk);

            %% add and delete from cache and check
            irk_step.add_step_size_cache(1);
            irk_step.add_step_size_cache(2.1);
            irk_step.add_step_size_cache(3);
            irk_step.add_step_size_cache(1);
            obj.fatalAssertTrue(all([1, 2.1, 3] == irk_step.get_step_size_cache()));
            irk_step.del_step_size_cache(2.1);
            obj.fatalAssertTrue(all([1, 3] == irk_step.get_step_size_cache()));

            %% step with and without cache and compare
            x_cache = irk_step.step_cache(3, x0);
            x_no_cache = irk_step.step(3, x0);
            obj.fatalAssertTrue(norm(x_cache-x_no_cache) < 1e-12);

            if obj.verbose
                fprintf('\n');
            end
        end

        function test(obj, problem, irk) %#ok<*INUSL>

            %% load data
            if obj.verbose
                fprintf('\n');
            end
            instance = problem.instance;
            data = load(instance);
            A = data.A;
            n = size(A, 1);
            x0 = ones(n, 1);
            M = [];
            if isfield(data, 'E')
                M = data.E;
            end
            T = problem.T;
            reltol = problem.reltol;
            abstol = problem.abstol;
            hs = problem.hs;

            %% compute a reference approximation by using expm
            if ~isempty(M)
                xref = expm(T*(full(M) \ full(A))) * x0;
            else
                xref = expm(T*full(A)) * x0;
            end
            nrm2xref = norm(xref);

            %% compute approximation with step size h
            reltol_reached = false;
            abstol_reached = false;

            %% print information
            if obj.verbose
                f(1) = fprintf('irk = %s, instance = %s, T = %.2e, 2-norm reference = %.2e\n', char(irk), instance, T, nrm2xref);
                f(2) = fprintf('abs./rel. 2-norm tol = %.2e / %.2e\n', abstol, reltol);
            end

            for h = hs

                %% create a IRK solver
                irk_step = IRKstep(A, M, irk);

                %% add step sizes to cache
                irk_step.add_step_size_cache(h);

                %% iterate until t = T
                t = 0;
                xk = x0;
                steps = 0;
                wt = tic();
                while (T - t) > eps
                    xk = irk_step.step(h, xk);
                    t = t + h;
                    steps = steps + 1;
                end
                wt = toc(wt);

                %% compare with reference solution
                absnrm2err = norm(xk-xref);
                relnrm2err = absnrm2err / nrm2xref;

                % print results
                if obj.verbose
                    f(3) = fprintf('h = %.2e, steps = %4d, abs./rel. 2-norm error = %.2e/%.2e, avg. time per step = %.2e\n', h, steps, absnrm2err, relnrm2err, wt/steps);
                end

                % check if tolerance for absolute and relative error
                abstol_reached = absnrm2err < abstol;
                reltol_reached = relnrm2err < reltol;

                % break if first time tolerance is reached
                if abstol_reached && reltol_reached
                    if obj.verbose
                        fprintf('abs./rel. 2-norm tol reached\n');
                    end
                    break
                end

            end

            %% test if reltol was reached at least ones
            obj.fatalAssertTrue(abstol_reached);
            obj.fatalAssertTrue(reltol_reached);
            if obj.verbose
                fprintf('%s\n', repmat('-', 1, max(f) - 1));
            end
        end
    end
end
