%BENCH_SPLITTING2_DRE
%   Benchmark script for Splittgin2_DRE solvers.
%
%   See also SPLITTING2_DRE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% problems and verbosity
verbose = ~isCIrun();
if isCIrun()
    problems = {; ...
        struct('instance', 'rail_371', 'T', 10, 'h', 2^(-2), 'print_mod', 25); ...
        };

    split_solvers = { ...
        struct('split', Splitting2_t.SPLITTING_LIE, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC4, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC6, 'irk', IRKmethod_GAUSS_LEGENDRE(3), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC8, 'irk', IRKmethod_GAUSS_LEGENDRE(4), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        };
else
    problems = {; ...
        struct('instance', 'rail_371', 'T', 10, 'h', 2^(-2), 'print_mod', 25); ...
        struct('instance', 'rail_1357', 'T', 10, 'h', 2^(-2), 'print_mod', 25); ...
        struct('instance', 'rail_5177', 'T', 10, 'h', 2^(-2), 'print_mod', 25); ...
        };

    split_solvers = { ...
        struct('split', Splitting2_t.SPLITTING_LIE, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_LIE, 'irk', IRKmethod_RADAUIIA(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_RADAUIIA(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_RADAUIIA(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC4, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC4, 'irk', IRKmethod_RADAUIIA(3), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC6, 'irk', IRKmethod_GAUSS_LEGENDRE(3), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC6, 'irk', IRKmethod_RADAUIIA(4), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC8, 'irk', IRKmethod_GAUSS_LEGENDRE(4), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC8, 'irk', IRKmethod_RADAUIIA(5), 'quad', QuadRule_GAUSS_LEGENDRE(10))
        };
end


%% benchmark iterate over benchmark examples
if ~isCIrun()
    profile clear
    profile on
end
time_total = tic;
for problem_i = 1:length(problems)

    %% load problem instance
    problem = problems{problem_i};
    if verbose
        fprintf('\n');
        fprintf('----------------------------------------------\n');
    end
    instance = problem.instance;
    data = load(instance);
    A = data.A;
    B = data.B;
    C = data.C;
    if isfield(data, 'E')
        E = data.E;
    else
        E = speye(n, n);
    end

    n = size(A, 1);
    Z0 = zeros(n, 1);
    T = problem.T;
    h = problem.h;

    %% get print interval
    print_mod = problem.print_mod;

    %% iterate over different solvers
    for split_solver_i = 1:length(split_solvers)

        %% create splitting scheme solver
        split_solver = split_solvers{split_solver_i};
        split = split_solver.split;
        irk = split_solver.irk;
        quad = split_solver.quad;
        split_opt = Splitting2_DRE_options(split, irk, quad);
        split_sol = Splitting2_DRE(A, E, B, C, Z0, split_opt, h, T);

        %% solve
        if verbose
            fprintf('instance = %s, T = %.8f\n', instance, T);
            fprintf('splitting = %s, irk = %s, quad = %s\n', split, irk, quad);
        end
        time_test = tic;
        print_mod = problem.print_mod;
        print_mod_counter = 0;
        split_sol.prepare();
        while split_sol.t < T

            % time step with splitting solver
            split_sol.time_step();

            % print intermediate results
            percent = split_sol.t / T * 100;

            if percent >= print_mod_counter && verbose
                [L, D] = split_sol.get_solution();
                fprintf('t =% 12.8f, %6.2f %%, step size = %f, size(L) = %3d-x-%3d\n', split_sol.t, split_sol.t/T*100, split_sol.h, size(L));
                print_mod_counter = print_mod_counter + print_mod;
            end
        end
        time_test = toc(time_test);
        if verbose
            fprintf('Test Runtime: %s, %f secs.\n', instance, time_test);
            fprintf('splitting = %s, irk = %s, quad = %s\n', split, irk, quad);
            fprintf('Computational Time: %s, Step Size = %f, %f secs.\n', upper(class(split_sol)), split_sol.h, split_sol.wtime_prepare+split_sol.wtime_time_step);
            fprintf('----------------------------------------------\n\n');
        end
    end
end
if ~isCIrun()
profile viewer
end
