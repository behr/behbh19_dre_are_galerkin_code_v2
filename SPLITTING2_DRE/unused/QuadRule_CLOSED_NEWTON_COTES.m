%QUADRULE_CLOSED_NEWTON_COTES     Implementation of closed Newton-Cotes
% methods using the abstract QuadRule class.
%
%   See also QUADRULE.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

classdef QuadRule_CLOSED_NEWTON_COTES < QuadRule

    properties(SetAccess=protected)
        name;
    end

    properties(Hidden=true)
        s;
    end


    methods
        function obj = QuadRule_CLOSED_NEWTON_COTES(name)
            %QUADRULE_CLOSED_NEWTON_COTES  Constructor of class.
            %
            %   CLN = QUADRULE_CLOSED_NEWTON_COTES(NAME)
            %   creates an instance of the class.
            %   CLN represents a closed Newton-Cotes method. NAME
            %   must be a string.
            %   The following values for NAME are valid:
            %
            %       - TRAPEZOID
            %       - SIMPSON
            %       - MILNE
            %       - WEDDLE
            %
            %   Author: Maximilian Behr

            %% check input args
            if strcmpi(name, 'TRAPEZOID')
                obj.s = 3;
            elseif strcmpi(name, 'SIMPSON')
                obj.s = 5;
            elseif strcmpi(name, 'MILNE')
                obj.s = 7;
            elseif strcmpi(name, 'WEDDLE')
                obj.s = 9;
            else
                error('%s is not implemented.', name);
            end
            obj.name = upper(name);
        end

        function c = char(obj)
           c = obj.name;
        end

        function p = order(obj)
            p = obj.s;
        end

        function q = use_start_point(obj) %#ok<*MANU>
            q = true;
        end

        function q = use_end_point(obj) %#ok<*MANU>
            q = true;
        end

        function equidistant = is_equidistant(obj)
            equidistant = true;
        end

        function [nodes, weights] = nodes_weights(obj, interval)
            %NODES_WEIGHTS  Nodes and the weights of the quadrature rule.
            %   [NODES, WEIGHTS] = quad.NODES_WEIGHTS() returns
            %   the nodes NODES and the WEIGHTS W of the quadrature rule
            %   for the interval [0,1].
            %
            %   [NODES, WEIGHTS] = quad.NODES_WEIGHTS(INTERVAL) returns
            %   the nodes NODES and the weights WEIGHTS of the
            %   quadrature rule for the interval INTERVAL.
            %
            %   Author: Maximilian Behr

            % check input args
            if nargin == 2
                validateattributes(interval, {'double'}, {'real', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(2));
                assert(length(interval) == 2, 'invalid interval is given');
                assert(interval(1) < interval(2), 'invalid interval is given');
            end

            % nodes and weights
            if obj.s == 3
                nodes = [0, 1];
                weights = [1 / 2, 1 / 2];
            elseif obj.s == 5
                nodes = [0, 1 / 2, 1];
                weights = [1 / 6, 4 / 6, 1 / 6];
            elseif obj.s == 7
                nodes = [0, 1 / 4, 2 / 4, 3 / 4, 1];
                weights = [7 / 90, 32 / 90, 12 / 90, 32 / 90, 7 / 90];
            elseif obj.s == 9
                nodes = [0, 1 / 6, 2 / 6, 3 / 6, 4 / 6, 5 / 6, 1];
                weights = [41 / 840, 216 / 840, 27 / 840, 272 / 840, 27 / 840, 216 / 840, 41 / 840];
            else
                error('Not implemented');
            end

            % transform
            if nargin < 2
                a = 0;
                b = 1;
            else
                a = interval(1);
                b = interval(2);
            end

            nodes = a + (b - a) * nodes;
            weights = (b - a) * weights;

            % assertion is sorted nodes
            assert(issorted(nodes), 'nodes have to be sorted.');
        end
    end
end
