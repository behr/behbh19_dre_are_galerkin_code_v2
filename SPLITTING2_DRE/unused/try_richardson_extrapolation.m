%TRY_RICHARDSON_EXTRAPOLATION
%
%   Some experiments using implicit Runge-Kutta method and Richardson extrapolation.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all;
clc

%% data
%instance = load('rail_5177.mat');
instance = load('beam.mat');
A = instance.A;
n = size(A, 1);
%M = instance.E;
M = eye(n, n);
x0 = instance.C';
T = 1;
irk = IRKmethod_t.GAUSS_LEGENDRE_4;

%% create solvers with step size T/k1 and T/k2
n1 = 8;
n2 = 16;
irk_stepper_k1 = IRKstep(A, M, irk);
irk_stepper_k2 = IRKstep(A, M, irk);
p = irk_stepper_k1.order;
symmetric = irk_stepper_k1.symmetric;

%% compute e^{T*A}*x0
wt = tic();
xk1 = x0;
for i = 1:n1
    xk1 = irk_stepper_k1.step(T/n1, xk1);
end
wt = toc(wt);
fprintf('wtime solver 1 = %.2e\n', wt);

wt = tic();
xk2 = x0;
for i = 1:n2
    xk2 = irk_stepper_k2.step(T/n2, xk2);
end
wt = toc(wt);
fprintf('wtime solver 1 = %.2e\n', wt);

%% compare results
fprintf('||xk1-xk2||_F      = %.2e\n', norm(xk1-xk2, 'fro'));

% richardson extrapolation
if symmetric
    error_est = (xk2 - xk1) ./ ((n2 / n1)^(2 * p) - 1);
else
    error_est = (xk2 - xk1) ./ ((n2 / n1)^(p) - 1);
end
extra_pol = xk2 + error_est;
fprintf('||error_est||_F    = %.2e\n', norm(error_est, 'fro'));

% compute reference solution
xsol = expm(full(M) \ full(A)*T) * x0;
fprintf('||xk1-xsol||_F     = %.2e\n', norm(xk1-xsol, 'fro'));
fprintf('||xk2-xsol||_F     = %.2e\n', norm(xk2-xsol, 'fro'));
fprintf('||extra-xsol||_F   = %.2e\n', norm(extra_pol-xsol, 'fro'));
fprintf('||xsol||_F         = %.2e\n', norm(xsol, 'fro'));
