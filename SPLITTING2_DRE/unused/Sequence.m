classdef Sequence < handle
    %SEQUENCE    Represents different usefull sequences.
    %
    %   SEQUENCE represent different usefull sequences.
    %
    %   The following sequences are implemented:
    %
    %       - Harmonic, a_n = n,            1, 2, 3, 4, 5, 6, ...
    %       - Geomtric, a_n = 2^(n-1),      1, 2, 4, 8, 16, 32, ...
    %       - Bulirsch,
    %           a_n = 1,        if n==1
    %           a_n = 2^(n/2),  if mod(n,2)==0
    %           a_n = 3*2^((n-3)/2), if mod(n,2)==1
    %                                       1, 2, 3, 4, 6, 8, 12, ...
    %       - TwoSquareOne, a_n = 2^(n-1) +1,
    %                                       2, 3, 5, 9, 17, 33, ...
    %
    %   SEQUENCE methods:
    %       SEQUENCE                - Constructor of class.
    %       next                    - Next element of the sequence.
    %
    %   SEQUENCE properties:
    %        type                   - Sequence_t, type of sequence
    %        n                      - double, current index of sequence
    %
    %   See also SEQUENCE_T.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020

    properties(SetAccess = private)
        type %Sequence_t, type of sequence
        n % double, current index of sequence
    end

    methods
        function obj = Sequence(sequence_type)
            %SEQUENCE  Constructor of class.
            %
            %   SEQ = SEQUENCE(SEQUENCE_TYPE)
            %   creates an object which represent the seqence given by
            %   SEQUENCE_TYPE. SEQUENCE_TYPE is an enum from SEQUENCE_T.
            %
            %   See also SEQUENCE_T.
            %
            %   Author: Maximilian Behr

            %% check input args
            assert(isa(sequence_type,'Sequence_t'),'%s: %s has invalid type.',mfilename, inputname(1));
            obj.type = sequence_type;
            obj.n = 1;
        end

        function elem = next(obj)
            %NEXT   Return next element of the sequence.
            %
            %   ELEM = SEQ.NEXT() returns the next element of the
            %   sequence SEQ.
            %
            %   Author: Maximilian Behr

            if obj.type == Sequence_t.HARMONIC
                elem = obj.n;
            elseif obj.type == Sequence_t.GEOMETRIC
                elem = 2^(obj.n-1);
            elseif obj.type == Sequence_t.BULIRSCH
                if obj.n == 1
                    elem = 1;
                elseif mod(obj.n,2)==0
                    elem = 2^(round(obj.n/2));
                else
                    elem = 3*2^(round((obj.n-3)/2));
                end
            elseif obj.type == Sequence_t.TWOSQUARE_ONE
                elem = 2^(obj.n-1) + 1;
            else
                error('Not implemented');
            end
            elem = round(elem);
            obj.n = obj.n+1;
        end
    end
end
