%BUTCHERTABLEAU_PRINT   Prints a ButcherTableau instance.
%
%   BUTCHERTABLEAU_PRINT prints instances of ButcherTableau for
%   copy and paste matrices easily.
%   The tableaus are computed using multiprecision techniques.
%   The precision for printing can be adapted.
%
%   See also ButcherTableau.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear data
clearvars, clc, close all;

%% get tableau, print and copy and paste stuff
prec = 8192;
print_matrix_prec = 20;
file = 'tableau_code.dat';
tableau_dir = 'tableau';

fid = fopen(file, 'w');
% RADAU_IIA
methods = @(stage) sprintf('RADAU_IIA_%d', stage);
for s = 2:15
    method = methods(s);
    tab = ButcherTableau(method, prec);
    %fprintf('stages = %d, condV = %.2e\n',s,cond(tab.V));
    fprintf(fid, 'case IRKmethod_t.%s\n', method);
    print_matrix(tab.A, 'A', print_matrix_prec, fid);
    print_matrix(tab.b, 'b_vec', print_matrix_prec, fid);
    print_matrix(tab.c, 'c_vec', print_matrix_prec, fid);
    print_matrix(tab.d, 'd_vec', print_matrix_prec, fid);
    print_matrix(tab.V, 'V', print_matrix_prec, fid);
    print_matrix(tab.D, 'D', print_matrix_prec, fid);
    print_matrix(tab.invV, 'invV', print_matrix_prec, fid);
    print_matrix(tab.invD, 'invD', print_matrix_prec, fid);

    %mtx files
    write_mtx_matrix(tableau_dir, method, s, 'A', tab.A);
    write_mtx_matrix(tableau_dir, method, s, 'b_vec', tab.b);
    write_mtx_matrix(tableau_dir, method, s, 'c_vec', tab.c);
    write_mtx_matrix(tableau_dir, method, s, 'd_vec', tab.d);
    write_mtx_matrix(tableau_dir, method, s, 'V', tab.V);
    write_mtx_matrix(tableau_dir, method, s, 'D', tab.D);
    write_mtx_matrix(tableau_dir, method, s, 'invV', tab.invV);
    write_mtx_matrix(tableau_dir, method, s, 'invD', tab.invD);
end

% GAUSS_LEGENDRE
methods = @(stage) sprintf('GAUSS_LEGENDRE_%d', stage);
for s = 1:15
    method = methods(s);
    tab = ButcherTableau(method, prec);
    %fprintf('stages = %d, condV = %.2e\n',s,cond(tab.V));
    fprintf(fid, 'case IRKmethod_t.%s\n', method);
    print_matrix(tab.A, 'A', print_matrix_prec, fid);
    print_matrix(tab.b, 'b_vec', print_matrix_prec, fid);
    print_matrix(tab.c, 'c_vec', print_matrix_prec, fid);
    print_matrix(tab.d, 'd_vec', print_matrix_prec, fid);
    print_matrix(tab.V, 'V', print_matrix_prec, fid);
    print_matrix(tab.D, 'D', print_matrix_prec, fid);
    print_matrix(tab.invV, 'invV', print_matrix_prec, fid);
    print_matrix(tab.invD, 'invD', print_matrix_prec, fid);

    %mtx files
    write_mtx_matrix(tableau_dir, method, s, 'A', tab.A);
    write_mtx_matrix(tableau_dir, method, s, 'b_vec', tab.b);
    write_mtx_matrix(tableau_dir, method, s, 'c_vec', tab.c);
    write_mtx_matrix(tableau_dir, method, s, 'd_vec', tab.d);
    write_mtx_matrix(tableau_dir, method, s, 'V', tab.V);
    write_mtx_matrix(tableau_dir, method, s, 'D', tab.D);
    write_mtx_matrix(tableau_dir, method, s, 'invV', tab.invV);
    write_mtx_matrix(tableau_dir, method, s, 'invD', tab.invD);
end
fclose(fid);
%%%% end of script part %%%%

function print_matrix(A, name, digi, fid)
%PRINT_MATRIX prints a matrix A with prec digits after comma for copy and paste.
% The matrix A is converted to double precision first.
% Take care all numbers with absolute value smaller than machine precision
% are set to 0.
%
%   Arguments:
%       A       - matrix
%       name    - name of matrix for printing
%       dig     - digits after comma
%       tol     - all numbers with absolute value smaller than tol are replaced by 0
%       fid     - optional if print to file is wanted
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

% print to file or to console
if nargin <= 3
    fid = 1;
end

[n, m] = size(A);
fprintf(fid, '\n');
fprintf(fid, '%s = zeros(%d, %d);\n', name, n, m);

% convert to double
A = double(A);
A_real = isreal(A);

% post processing replace small numbers by zero as
for i = 1:n
    for j = 1:m
        if abs(A(i, j)) < eps
            fprintf(2, 'replaced entry (%d, %d) = %.2e of %s by zero.\n', i, j, A(i, j), name);
            A(i, j) = 0;
        end

        if ~A_real
            if abs(real(A(i, j))) < eps
                fprintf(2, 'replaced real part (%d, %d) = %.2e of %s by zero.\n', i, j, real(A(i, j)), name);
                A(i, j) = 1i * imag(A(i, j));
            end
            if abs(imag(A(i, j))) < eps
                fprintf(2, 'replaced imag part (%d, %d) = %.2e of %s by zero.\n', i, j, imag(A(i, j)), name);
                A(i, j) = real(A(i, j));
            end
        end
    end
end

%% print
for i = 1:n
    for j = 1:m
        if A_real
            fprintf(fid, '%s(%2d, %2d) = %+.*f;\n', name, i, j, digi, A(i, j));
        else
            fprintf(fid, '%s(%2d, %2d) = %+.*f + %.*fi;\n', name, i, j, digi, real(A(i, j)), digi, imag(A(i, j)));
        end
    end
end
end

function write_mtx_matrix(tableau_dir, solver, stages, name, A)
%WRITE_MTX_MATRIX write a matrix A to tableau directory.
% The matrix A is converted to double precision first.
% Take care all numbers with absolute value smaller than machine precision
% are set to 0.
%
%   Arguments:
%       tableau_dir - directory for tableaus
%       solver      - name of the solver
%       stages      - number of stages
%       name        - name of the matrix
%       A           - matrix itself
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

% convert to double
[n, m] = size(A);
A = double(A);
A_real = isreal(A);

% post processing replace small numbers by zero as
for i = 1:n
    for j = 1:m
        if abs(A(i, j)) < eps
            fprintf('replaced entry (%d, %d) = %.2e of %s by zero.\n', i, j, A(i, j), name);
            A(i, j) = 0;
        end
        if ~A_real
            if abs(real(A(i, j))) < eps
                fprintf('replaced real part (%d, %d) = %.2e of %s by zero.\n', i, j, real(A(i, j)), name);
                A(i, j) = 1i * imag(A(i, j));
            end
            if abs(imag(A(i, j))) < eps
                fprintf('replaced imag part (%d, %d) = %.2e of %s by zero.\n', i, j, imag(A(i, j)), name);
                A(i, j) = real(A(i, j));
            end
        end
    end
end

%% write matrix to mtx file
[blas, lapack] = getBLASLAPACK();
cp = getComputerName();
un = getUserName();
gi = GitInfo();
comment = char( ...
    sprintf('Tableau Matrix %s for %s with %d stages, generated %s', name, solver, stages, date), ...
    sprintf('MATLAB = %s', version), ...
    sprintf('BLAS = %s', blas), ...
    sprintf('LAPACK = %s', lapack), ...
    sprintf('Computer = %s', cp), ...
    sprintf('User = %s', un), ...
    sprintf('git sha = %s', gi.sha), ...
    sprintf('git url = %s', gi.remote_url), ...
    sprintf('mfilename = %s', mfilename) ...
    );
mmwrite(fullfile(tableau_dir, sprintf('%s_%s.mtx', solver, name)), A, comment);
end
