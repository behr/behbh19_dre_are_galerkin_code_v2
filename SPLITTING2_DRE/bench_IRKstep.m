%BENCH_IRKstep
%   Benchmark script for IRKstep.
%
%   See also SPLITTING2_DRE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% problems and verbosity
verbose = ~isCIrun();
if isCIrun()
    problems = {; ...
        struct('instance', 'rail_371', 'T', 10, 'h', 2^(-2), 'print_mod', 25); ...
        };
else
    problems = {; ...
        struct('instance', 'rail_5177', 'T', 2^4, 'h', 2^(-4), 'print_mod', 10); ...
        };
end


%% benchmark iterate over benchmark examples
if ~isCIrun()
    profile clear
    profile on
    spparms('spumoni', 1);
end

for problem_i = 1:length(problems)

    %% load problem instance
    problem = problems{problem_i};
    instance = problem.instance;
    data = load(instance);
    A = data.A;
    if isfield(data, 'E')
        E = data.E;
    else
        E = speye(n, n);
    end

    n = size(A, 1);
    x0 = rand(n, 50);
    T = problem.T;
    h = problem.h;

    %% create IRKmethod
    irk = IRKmethod_GAUSS_LEGENDRE(4);
    irk_step = IRKstep(A, E, irk);
    irk_step.add_step_size_cache(h);

    %% information
    if verbose
        f1 = fprintf('irk = %s, instance = %s, T = %.2e, h = %.2e\n', char(irk), instance, T, h);
    end

    %% iterate until t = T
    print_mod_counter = 0;
    t = 0;
    xk = x0;
    steps = 0;
    wt = tic();
    while (T - t) > eps
        xk = irk_step.step(h, xk);
        t = t + h;
        steps = steps + 1;

        % print intermediate results
        percent = t / T * 100;
        if percent >= print_mod_counter && verbose
            fprintf('T = %.2e, t = %.2e, h = %.2e, %6.2f %%\n', T, t, h, percent);
            print_mod_counter = print_mod_counter + problem.print_mod;
        end
    end
    wt = toc(wt);

    %% print information
    if verbose
        f2 = fprintf('steps = %4d, avg. time per step = %.2e\n', steps, wt/steps);
        fprintf('%s\n', repmat('-', 1, max([f1, f2]) - 1));
    end

end
if ~isCIrun()
    profile viewer
end
