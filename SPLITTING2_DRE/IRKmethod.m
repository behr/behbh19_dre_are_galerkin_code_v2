classdef (Abstract) IRKmethod
    %IRKMETHOD      Class for implicit Runge Kutta data.
    %
    %   Fully implicit Gauss-Legendre Runge-Kutta methods are available
    %   with stages s = 1, ..., 15. The order of these methods 2*s, which is
    %   the maximal achivable order for a Runge-Kutta method.
    %   RadauIIA methods with stages s = 2, ..., 15 are also available.
    %   They have order 2*s-1.
    %
    %   IRKMETHOD properties:
    %       A               - set private, matrix A of Butcher Tableau
    %       b               - set private, vector b of Butcher Tableau
    %       c               - set private, vector c of Butcher Tableau
    %       d               - set private, vector d=inv(A')*b of Butcher Tableau
    %       D               - set private, spectral decomp of Butcher Tableau matrix A = V*D*invV
    %       V               - set private, spectral decomp of Butcher Tableau matrix A = V*D*invV
    %       invD            - set private, spectral decomp of Butcher Tableau matrix A = V*D*invV
    %       invV            - set private, spectral decomp of Butcher Tableau matrix A = V*D*invV
    %
    %   IRKMETHOD methods:
    %       char            - Character array representation of the implicit Runge-Kutta method.
    %       string          - String array representation of the implicit Runge-Kutta method.
    %       order           - Return the global order (convergence order) of the method.
    %       is_symmetric    - Return true if the method is symmetric.
    %       stages          - Return the number of stages.
    %       is_fsal         - Return true if the method has the first same as last property.
    %
    %   See also IRKMETHOD_GAUSS_LEGENDRE, IRKMETHOD_RADAUIIA, IRKSTEP, IRKMETHOD_TABLEAU.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = protected)
        % set private, matrix A of Butcher Tableau
        A
        % set private, vector b of Butcher Tableau
        b
        % set private, vector c of Butcher Tableau
        c
        % set private, vector d=inv(A')*b of Butcher Tableau
        d
        % set private, spectral decomp of Butcher Tableau matrix A = V*D*invV
        V
        % set private, spectral decomp of Butcher Tableau matrix A = V*D*invV
        D
        % set private, spectral decomp of Butcher Tableau matrix A = V*D*invV
        invD
        % set private, spectral decomp of Butcher Tableau matrix A = V*D*invV
        invV
    end

    methods(Abstract)
        %CHAR  Character array representation of the implicit Runge-Kutta method.
        %   Return a character array representation of the
        %   implicit Runge-Kutta method.
        %
        %   C = CHAR(IRK) returns a character array representation of
        %   the implicit Runge-Kutta method IRK.
        %
        %   Author: Maximilian Behr
        c = char(obj)

        %STRING  String array representation of the implicit Runge-Kutta method.
        %   Return a string array representation of the
        %   implicit Runge-Kutta method.
        %
        %   C = STRING(IRK) returns a string array representation of
        %   the implicit Runge-Kutta method IRK.
        %
        %   Author: Maximilian Behr
        c = string(obj)

        %ORDER  Order of the implicit Runge-Kutta method.
        %   The order is the number P such that the global error of the
        %   implicit Runge-Kutta method is smaller equals C*h^P, where
        %   h is the step size.
        %
        %   P = IRK.ORDER() returns the order P of the implicit
        %   Runge-Kutta method IRK.
        %
        %   Author: Maximilian Behr
        p = order(obj)

        %STAGES     Number of stages implicit Runge-Kutta method.
        %
        %   S = IRK.STAGES() returns the number of stages S of the implicit
        %   Runge-Kutta method IRK.
        %
        %   Author: Maximilian Behr
        s = stages(obj)

        %IS_SYMMETRIC     True if implicit Runge-Kutta method is symmetric.
        %
        %   S = IRK.IS_SYMMETRIC() returns true if the implicit Runge-Kutta
        %   method IRK is symmetric.
        %
        %   Author: Maximilian Behr
        q = is_symmetric(obj)

        %IS_FSAL     True if implicit Runge-Kutta method has FSAL property.
        %
        %   S = IRK.IS_FSAL() returns true if the implicit Runge-Kutta
        %   method IRK has the first same as last property otherwise false.
        %
        %   Author: Maximilian Behr
        q = is_fsal(obj)
    end

    methods(Access = public)
        function [A, b, c, d, V, invV, D, invD] = get_tableau(obj)
            %GET_TABLEAU     Return Butcher tableau.
            %
            %   [A, b, c, d, V, invV, D, invD] = IRK.GET_TABLEAU() returns
            %   the Butcher tableau as well as the spectral decomposition of
            %   the tableau matrix A and its spectral decomposition.
            %   d is given by inv(A')*b.
            %
            %   Author: Maximilian Behr
            A = obj.A;
            b = obj.b;
            c = obj.c;
            d = obj.d;
            V = obj.V;
            invV = obj.invV;
            D = obj.invD;
            invD = obj.invD;
        end
    end
end
