%SPLITTING2_T   Enumeration for the splitting scheme method.
%
%   Currently supported splitting schemes:
%       - SPLITTING_LIE
%       - SPLITTING_STRANG
%       - SPLITTING_SYMMETRIC2
%       - SPLITTING_SYMMETRIC4
%       - SPLITTING_SYMMETRIC6
%       -   ...
%       - SPLITTING_SYMMETRIC20
%
%   See also
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

classdef (Sealed = true) Splitting2_t < int8
    enumeration
        SPLITTING_LIE(0),
        SPLITTING_STRANG(1),
        SPLITTING_SYMMETRIC2(2),
        SPLITTING_SYMMETRIC4(4),
        SPLITTING_SYMMETRIC6(6),
        SPLITTING_SYMMETRIC8(8),
        SPLITTING_SYMMETRIC10(10),
        SPLITTING_SYMMETRIC12(12),
        SPLITTING_SYMMETRIC14(14),
        SPLITTING_SYMMETRIC16(16),
        SPLITTING_SYMMETRIC18(18),
        SPLITTING_SYMMETRIC20(20),
    end

    methods(Access = public)
        function p = order(obj)
            %ORDER  Return the order of the splitting scheme.
            %
            %   P = SPLIT.ORDER() returns the order P of the splitting scheme.
            %
            %   Author: Maximilian Behr
            switch obj
                case Splitting2_t.SPLITTING_LIE
                    p = 1;
                case Splitting2_t.SPLITTING_STRANG
                    p = 2;
                case Splitting2_t.SPLITTING_SYMMETRIC2
                    p = 2;
                case Splitting2_t.SPLITTING_SYMMETRIC4
                    p = 4;
                case Splitting2_t.SPLITTING_SYMMETRIC6
                    p = 6;
                case Splitting2_t.SPLITTING_SYMMETRIC8
                    p = 8;
                case Splitting2_t.SPLITTING_SYMMETRIC10
                    p = 10;
                case Splitting2_t.SPLITTING_SYMMETRIC12
                    p = 12;
                case Splitting2_t.SPLITTING_SYMMETRIC14
                    p = 14;
                case Splitting2_t.SPLITTING_SYMMETRIC16
                    p = 16;
                case Splitting2_t.SPLITTING_SYMMETRIC18
                    p = 18;
                case Splitting2_t.SPLITTING_SYMMETRIC20
                    p = 20;
                otherwise
                    error('not implemented');
            end
        end

        function gam = gamma(obj)
            %GAMMA  Return the coefficients gamma of the splitting scheme.
            %
            %   GAM = SPLIT.GAMMA() return the coefficients gamma of the
            %   splitting scheme.
            %
            %   Author: Maximilian Behr
            switch obj
                case Splitting2_t.SPLITTING_LIE
                    warning('No gamma coefficients needed for SPLITTING_LIE. Return empty.\n');
                    gam = [];
                case Splitting2_t.SPLITTING_STRANG
                    warning('No gamma coefficients needed for SPLITTING_STRANG. Return empty.\n');
                    gam = [];
                case Splitting2_t.SPLITTING_SYMMETRIC2
                    gam = 1 / 2;
                case Splitting2_t.SPLITTING_SYMMETRIC4
                    gam = [-1 / 6, 2 / 3];
                case Splitting2_t.SPLITTING_SYMMETRIC6
                    gam = [1 / 48; -8 / 15; 81 / 80];
                case Splitting2_t.SPLITTING_SYMMETRIC8
                    gam = [-1 / 720; 8 / 45; -729 / 560; 512 / 315];
                case Splitting2_t.SPLITTING_SYMMETRIC10
                    gam = [1 / 17280, -32 / 945, 6561 / 8960, -8192 / 2835, 390625 / 145152];
                case Splitting2_t.SPLITTING_SYMMETRIC12
                    gam = [-1 / 604800, 4 / 945, -2187 / 8960, 32768 / 14175, -9765625 / 1596672, 8748 / 1925];
                case Splitting2_t.SPLITTING_SYMMETRIC14
                    gam = [1 / 29030400, -16 / 42525, 19683 / 358400, -524288 / 467775, 244140625 / 38320128, -314928 / 25025, 13841287201 / 1779148800];
                case Splitting2_t.SPLITTING_SYMMETRIC16
                    gam = [-1 / 1828915200, 16 / 637875, -177147 / 19712000, 524288 / 1403325, -6103515625 / 1494484992, 2834352 / 175175, -678223072849 / 26687232000, 8589934592 / 638512875];
                case Splitting2_t.SPLITTING_SYMMETRIC18
                    gam = [1 / 146313216000, -64 / 49116375, 177147 / 157696000, -8388608 / 91216125, 152587890625 / 83691159552, -11337408 / 875875, 33232930569601 / 853991424000, -549755813888 / 10854718875, 22876792454961 / 975822848000];
                case Splitting2_t.SPLITTING_SYMMETRIC20
                    gam = [-1 / 14485008384000, 8 / 147349125, -1594323 / 14350336000, 33554432 / 1915538625, -152587890625 / 251073478656, 6377292 / 875875, -1628413597910449 / 43553562624000, 8796093022208 / 97692469875, -1853020188851841 / 18540634112000, 610351562500 / 14849255421];
                otherwise
                    error('not implemented');
            end
        end
    end
end
