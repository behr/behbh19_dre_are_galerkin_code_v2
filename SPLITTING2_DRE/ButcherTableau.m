classdef ButcherTableau < custom_handle
    % BUTCHERTABLEAU    Computes Butcher Tableaus using multiprecision.
    %
    %   BUTCHERTABLEAU computes the Butcher Tableau for different
    %   implicit Runge-Kutta methods. Currently only Gauss-Legendre
    %   and Radau IIA methods are supported.
    %
    %   BUTCHERTABLEAU methods:
    %       ButcherTableau          - Constructor of class.
    %       print                   - Prints an instance.
    %       gauss_legendre_tableau  - Computes the Butcher tableau for Gauss-Legendre methods.
    %       radau_IIA_tableau       - Computes a Butcher Tableau of a Radau IIA methods with S stages.
    %
    %   BUTCHERTABLEAU properties:
    %        A                      - set private, sym/vpa, matrix A of Butcher tableau
    %        b                      - set private, sym/vpa, vector b of Butcher tableau
    %        c                      - set private, sym/vpa, vector c of Butcher tableau
    %        d                      - set private, sym/vpa, vector d=inv(A')*b of Butcher tableau
    %        V                      - set private, sym/vpa, matrix of eigenvectors of A
    %        invV                   - set private, sym/vpa, inverse matrix of eigenvectors of A
    %        D                      - set private, sym/vpa, vector with eigenvalues of eigenvalues of A
    %        invD                   - set private, sym/vpa, vector of reciprocals of D
    %        prec                   - set private, double, precision for vpa
    %        print_prec = 8;        - set private, double, precision for print method
    %        order                  - set private, double, order of the method
    %        stages                 - set private, double, number of stages
    %        symmetric              - set private, logical, true if method is symmetric
    %        implicit               - set private, logical, true if method is implicit
    %        method                 - set private, string, indiciates the type of the method
    %
    %   See also BUTCHERTABLEAU_PRINT.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    properties(SetAccess = private)
        % set private, sym/vpa, matrix A of Butcher Tableau
        A
        % set private, sym/vpa, vector b of Butcher Tableau
        b
        % set private, sym/vpa, vector c of Butcher Tableau
        c
        % set private, sym/vpa, vector d=inv(A')*b of Butcher Tableau
        d
        % set private, sym/vpa, matrix of eigenvectors of A
        V
        % set private, sym/vpa, inverse matrix of eigenvectors of A
        invV
        % set private, sym/vpa, vector with eigenvalues of eigenvalues of A
        D
        % set private, sym/vpa, vector of reciprocals of D
        invD
        % set private, double, precision for vpa
        prec
        % set private, double, precision for print routine
        print_prec = 8;
        % set private, double, order of the method
        order
        % set private, double, number of stages
        stages
        % set private, logical, true if method is symmetric
        symmetric
        % set private, logical, true if method is implicit
        implicit
        % set private, string, indiciates the type of the method
        method
    end

    methods(Access = public)
        function obj = ButcherTableau(method_str, prec)
            %BUTCHERTABLEAU  Constructor of class.
            %
            %   TAB = BUTCHERTABLEAU(METHOD_STR, PREC)
            %   creates a BUTCHERTABLEAU instance.
            %   METHOD_STR must a string of the form 'GAUSS_LEGENDRE_S'
            %   where S is the number of stages.
            %   Currently only Gauss-Legendre methods are available.
            %   PREC is a scalar which controls the precision for
            %   for multiprecision computations.
            %
            %   For reference see:
            %
            %   Hairer, Wanner;
            %   "Solving Ordinary Differential Equations I",
            %   The Methods of Kuntzmann and Butcher of Order 2s
            %   p.208 Simplifying assumptions B and C
            %
            %   See also ButcherTableau_print.
            %
            %   Author: Maximilian Behr

            %% check prec
            obj.method = method_str;
            validateattributes(prec, {'double'}, {'real', 'square', '>', 0, 'scalar'}, mfilename, inputname(2));
            obj.prec = prec;
            digits(prec);
            sym_vpa = @(x) vpa(x);

            %% set tableau data
            if strcmp(method_str, 'GAUSS_LEGENDRE_2')
                obj.A = sym_vpa(zeros(2, 2));
                obj.b = sym_vpa(zeros(2, 1));
                obj.c = sym_vpa(zeros(2, 1));

                obj.A(1, 1) = sym_vpa(1/4);
                obj.A(1, 2) = sym_vpa(1/4) - sym_vpa(1/6) * sym_vpa(sqrt(3));
                obj.A(2, 1) = sym_vpa(1/4) + sym_vpa(1/6) * sym_vpa(sqrt(3));
                obj.A(2, 2) = sym_vpa(1/4);

                obj.b(1) = sym_vpa(1/2);
                obj.b(2) = sym_vpa(1/2);

                obj.c(1) = sym_vpa(1/2) - sym_vpa(1/6) * sym_vpa(sqrt(3));
                obj.c(2) = sym_vpa(1/2) + sym_vpa(1/6) * sym_vpa(sqrt(3));

                obj.order = 4;
                obj.stages = 2;
                obj.symmetric = true;
                obj.implicit = true;
            elseif strcmp(method_str, 'GAUSS_LEGENDRE_3')
                obj.A = sym_vpa(zeros(3, 3));
                obj.b = sym_vpa(zeros(3, 1));
                obj.c = sym_vpa(zeros(3, 1));

                obj.A(1, 1) = sym_vpa(5/36);
                obj.A(1, 2) = sym_vpa(2/9) - sym_vpa(sqrt(15)/15);
                obj.A(1, 3) = sym_vpa(5/36) - sym_vpa(sqrt(15)/30);
                obj.A(2, 1) = sym_vpa(5/36) + sym_vpa(sqrt(15)/24);
                obj.A(2, 2) = sym_vpa(2/9);
                obj.A(2, 3) = sym_vpa(5/36) - sym_vpa(sqrt(15)/24);
                obj.A(3, 1) = sym_vpa(5/36) + sym_vpa(sqrt(15)/30);
                obj.A(3, 2) = sym_vpa(2/9) + sym_vpa(sqrt(15)/15);
                obj.A(3, 3) = sym_vpa(5/36);

                obj.b(1) = sym_vpa(5/18);
                obj.b(2) = sym_vpa(4/9);
                obj.b(3) = sym_vpa(5/18);

                obj.c(1) = sym_vpa(1/2) - sym_vpa(sqrt(15)/10);
                obj.c(2) = sym_vpa(1/2);
                obj.c(3) = sym_vpa(1/2) + sym_vpa(sqrt(15)/10);

                obj.order = 6;
                obj.stages = 3;
                obj.symmetric = true;
                obj.implicit = true;
            elseif strcmp(method_str, 'GAUSS_LEGENDRE_4')
                sqrt30 = sym_vpa(sqrt(30));
                omega1 = sym_vpa(1/8) - sqrt30 / 144;
                omega2 = sym_vpa(1/2) * sym_vpa(sqrt((15 + 2 * sqrt30)/35));
                omega3 = omega2 * (sym_vpa(1/6) + sqrt30 / 24);
                omega4 = omega2 * (sym_vpa(1/21) + (5 * sqrt30) / 168);
                omega5 = omega2 - 2 * omega3;
                omega1h = sym_vpa(1/8) + sqrt30 / 144;
                omega2h = sym_vpa(1/2) * sym_vpa(sqrt((15 - 2 * sqrt30)/35));
                omega3h = omega2h * (sym_vpa(1/6) - sqrt30 / 24);
                omega4h = omega2h * (sym_vpa(1/21) - (5 * sqrt30) / 168);
                omega5h = omega2h - 2 * omega3h;

                obj.A = sym_vpa(zeros(4, 4));
                obj.b = sym_vpa(zeros(4, 1));
                obj.c = sym_vpa(zeros(4, 1));

                obj.A(1, 1) = omega1;
                obj.A(1, 2) = omega1h - omega3 + omega4h;
                obj.A(1, 3) = omega1h - omega3 - omega4h;
                obj.A(1, 4) = omega1 - omega5;
                obj.A(2, 1) = omega1 - omega3h + omega4;
                obj.A(2, 2) = omega1h;
                obj.A(2, 3) = omega1h - omega5h;
                obj.A(2, 4) = omega1 - omega3h - omega4;
                obj.A(3, 1) = omega1 + omega3h + omega4;
                obj.A(3, 2) = omega1h + omega5h;
                obj.A(3, 3) = omega1h;
                obj.A(3, 4) = omega1 + omega3h - omega4;
                obj.A(4, 1) = omega1 + omega5;
                obj.A(4, 2) = omega1h + omega3 + omega4h;
                obj.A(4, 3) = omega1h + omega3 - omega4h;
                obj.A(4, 4) = omega1;

                obj.c(1) = sym_vpa(1/2) - omega2;
                obj.c(2) = sym_vpa(1/2) - omega2h;
                obj.c(3) = sym_vpa(1/2) + omega2h;
                obj.c(4) = sym_vpa(1/2) + omega2;

                obj.b(1) = 2 * omega1;
                obj.b(2) = 2 * omega1h;
                obj.b(3) = 2 * omega1h;
                obj.b(4) = 2 * omega1;

                obj.order = 8;
                obj.stages = 4;
                obj.symmetric = true;
                obj.implicit = true;
            elseif ~isempty(regexp(method_str, 'GAUSS_LEGENDRE_\d+', 'match'))
                % try to extract the order
                s = regexp(method_str, '\d+', 'match');
                assert(length(s) == 1)
                s = str2double(s{:});
                assert(mod(s, 1) == 0 && s > 0);

                % compute Butcher Tableau
                [obj.A, obj.b, obj.c] = ButcherTableau.gauss_legendre_tableau(s, prec);

                obj.order = 2 * s;
                obj.stages = s;
                obj.symmetric = true;
                obj.implicit = true;
            elseif ~isempty(regexp(method_str, 'RADAU_IIA_\d+', 'match'))
                % try to extract the order
                s = regexp(method_str, '\d+', 'match');
                assert(length(s) == 1)
                s = str2double(s{:});
                assert(mod(s, 1) == 0 && s > 0);
                assert(s > 1, 'A one stage RADAU_IIA scheme is a implicit euler method. This case is handled by GAUSS_LEGENDRE_1.');

                % compute Butcher Tableau
                [obj.A, obj.b, obj.c] = ButcherTableau.radau_IIA_tableau(s, prec);

                obj.order = 2 * s - 1;
                obj.stages = s;
                obj.symmetric = false;
                obj.implicit = true;
            else
                error('%s:Tableau not implemented for %s.\n', upper(class(obj)), char(method_str));
            end

            %% compute d
            obj.d = obj.A' \ obj.b;

            %% compute spectral decomposition of A
            [obj.V, obj.D] = eig(obj.A);
            obj.D = diag(obj.D);
            obj.invV = inv(obj.V);
            obj.invD = 1 ./ obj.D;

        end

        function print(obj)
            %PRINT Print a ButcherTableau instance.
            %
            %   TAB.PRINT() will print a ButcherTableau instance.
            %
            %   Author: Maximilian Behr

            %% convert to lower precision for printing
            digits(obj.print_prec);
            tempA = vpa(obj.A);
            tempb = vpa(obj.b);
            tempc = vpa(obj.c);
            tempd = vpa(obj.d);
            tempV = vpa(obj.V);
            tempinvV = vpa(obj.invV);
            tempD = vpa(obj.D);
            tempinvD = vpa(obj.invD);

            %% print header
            fprintf('%s = \n', inputname(1));
            fprintf('\n');
            fprintf('<a href="">%s</a> with properties:\n', class(obj));
            fprintf('\n');

            %% print properties
            property_indent = max(cellfun(@length, properties(obj)));
            fprintf('%+*s: %s\n', property_indent, 'method', mat2str(obj.method));
            fprintf('%+*s: %d\n', property_indent, 'order', obj.order);
            fprintf('%+*s: %d\n', property_indent, 'stages', obj.stages);
            fprintf('%+*s: %s\n', property_indent, 'symmetric', mat2str(obj.symmetric));
            fprintf('%+*s: %s\n', property_indent, 'implicit', mat2str(obj.implicit));
            fprintf('%+*s: %s\n', property_indent, 'prec', mat2str(obj.prec));
            fprintf('%+*s: %s\n', property_indent, 'print_prec', mat2str(obj.print_prec));

            %% print tableau
            fprintf('\n');
            fprintf('%s (Print with %d digits prec):\n\n', 'Tableau', obj.print_prec);

            width_A = max(arrayfun(@(x) length(char(x)), tempA(:)));
            width_b = max(arrayfun(@(x) length(char(x)), tempc(:)));
            width_c = max(arrayfun(@(x) length(char(x)), tempc(:)));
            width_d = max(arrayfun(@(x) length(char(x)), tempd(:)));
            width_col_left = width_c;
            width_col_right = max([width_A, width_b, width_d]);
            width_table = width_col_left + 3 + width_col_right * obj.stages + 2 * (obj.stages - 1);

            % print header
            fprintf('%+*s | %+*s\n', width_col_left, 'c', (width_col_right * obj.stages + 2 * (obj.stages - 1)), 'A');

            % print c | A
            for i = 1:obj.stages
                % print row
                fprintf('%+*s |', width_col_left, tempc(i));
                for j = 1:obj.stages
                    if j == obj.stages
                        fprintf(' %+*s', width_col_right, tempA(i, j));
                    else
                        fprintf(' %+*s,', width_col_right, tempA(i, j));
                    end
                end
                fprintf('\n');
            end

            % print ---
            for i = 1:width_table, fprintf('-');
            end
            fprintf('\n');

            % print b | b
            fprintf('%+*s |', width_col_left, 'b''');
            for i = 1:obj.stages
                if i == obj.stages
                    fprintf(' %+*s', width_col_right, tempb(i));
                else
                    fprintf(' %+*s,', width_col_right, tempb(i));
                end
            end
            fprintf('\n');

            % print d | d
            fprintf('%+*s |', width_col_left, 'd''');
            for i = 1:obj.stages
                if i == obj.stages
                    fprintf(' %+*s', width_col_right, tempd(i));
                else
                    fprintf(' %+*s,', width_col_right, tempd(i));
                end
            end
            fprintf('\n');

            %% print spectral decomposition
            fprintf('\n');
            fprintf('%s (Print with %d digits prec):\n\n', 'Spectral Decomposition', obj.print_prec);
            fprintf('A =\n\n');
            pretty(tempA);
            fprintf('V =\n\n');
            pretty(tempV)
            fprintf('D =\n\n');
            pretty(tempD);
            fprintf('invV =\n\n');
            pretty(tempinvV);
            fprintf('invD =\n\n');
            pretty(tempinvD);
            fprintf('\n%s:\n\n', 'Nodes');
            fprintf('c =\n\n');
            pretty(tempc);

            fprintf('\n%s:\n\n', 'Weights');
            fprintf('b =\n\n');
            pretty(tempb);
            fprintf('d =\n\n');
            pretty(tempd);

            %% restore precision
            digits(obj.prec);

        end
    end

    methods(Static)
        function [A, b, c] = gauss_legendre_tableau(s, prec)
            %GAUSS_LEGENDRE_TABLEAU  Butcher Tableau of a Gauss-Legendre methods with S stages.
            %
            %   [A, B, C] = GAUSS_LEGENDRE_TABLEAU(S, PREC)
            %   returns the Butcher tableau matrices A, B, C for
            %   a S-stage Gauss-Legendre method.
            %   PREC is a double which controls the digits.
            %   Multiprecision is used.
            %
            %   GAUSS_LEGENDRE_TABLEAU computes the roots of the
            %   S-order Legendre polynomial using Goloub-Welsch algorithm.
            %   The roots are then transformed from the interval [-1,1] to
            %   [0,1]. C of the Butcher Tableau is then given by these
            %   transformed roots. A and B can be uniquely determined by
            %   the simplifiying assumptions.
            %
            %   For reference see:
            %   W. Gander, M. Gander, J. Matrix, F. Kwok;
            %   "Scientific Computing, An introduction using Maple and MATLAB",
            %   2014 Chapter 9.3
            %
            %   Hairer, Wanner;
            %   "Solving Ordinary Differential Equations I",
            %   The Methods of Kuntzmann and Butcher of Order 2s
            %   p.208 Simplifying assumptions B and C
            %
            %   Author: Maximilian Behr

            %% restore digits
            digits_old = digits();
            digits(prec);

            %% Goloub Welsch algorithm
            s = vpa(s);
            beta = 1:(s - 1);
            beta = beta ./ (sqrt(4*beta.^2-1));
            [~, c] = eig(diag(beta, 1)+diag(beta, -1));
            [c, ~] = sort(diag(c));

            %% transform the roots to [0,1]
            c = (c + 1) ./ 2;

            %% compute b from simplifying condition
            Vc = fliplr(vander(c));
            es = (1 ./ (1:s))';
            b = Vc' \ es;

            %% compute A from the simplifying conditions
            Vc2 = [Vc(:, 2:end), c.^s];
            A = (Vc2 * diag(es)) / Vc;

            %% restore digits
            digits(digits_old);
        end

        function [A, b, c] = radau_IIA_tableau(s, prec)
            %RADAU_IIA_TABLEAU  Butcher Tableau of a Radau IIA methods with S stages.
            %
            %   [A, B, C] = RADAU_IIA_TABLEAU(S, PREC)
            %   returns the Butcher tableau matrices A, B, C for
            %   a S-stage RadauIIA method.
            %   PREC is a double which controls the digits.
            %   Multiprecision is used.
            %   S must be larger equals 2.
            %
            %   RADAU_IIA_TABLEAU computes the roots of the
            %   Gauss-Radau quadrature rule on [-1,1] with -1 as endpoint.
            %   The quadrature nodes are computed using Goloub-Welsch
            %   algorithm.
            %   Then the nodes are mirrored at 0, to get the quadrature nodes
            %   with fixed right endpoint 1.
            %   Afterwards the nodes are transformed to [0, 1].
            %   From the simplifing condition B(s) the vector b is computed.
            %   The simplifing conditions C(s) are used to compute A.
            %
            %   For reference see:
            %
            %   SHEN, Jie; TANG, Tao; WANG, Li-Lian.
            %   Spectral methods: algorithms, analysis and applications.
            %   Springer Science & Business Media, 2011.
            %   Ch. 3.2.3 Computation of Nodes and Weights Formula 3.184
            %
            %   Hairer, Wanner;
            %   "Solving Ordinary Differential Equations II",
            %   RADAU IA and RADAU IIA methods.
            %
            %   Author: Maximilian Behr

            %% restore digits
            digits_old = digits();
            digits(prec);

            %% first compute the nodes of Gauss-Radau Qudrature Formula
            % on [-1, 1] using with left endpoint -1.
            s = vpa(s);
            j = 0:(s - 2);
            a = 1 ./ ((2 * j + 1) .* (2 * j + 3));
            j = 1:(s - 2);
            b = sqrt(j.*(j + 1)./(2 * j + 1).^2);
            A = diag(a) + diag(b, 1) + diag(b, -1);
            c = sort(eig(A));
            c = [-1; c];

            %% this would give now the left point fixed
            % for RADAU_IIA we need the right endpoint simply mirror everything
            % for RADAU_IA this can be skipped but then other simplifying
            % conditions has to be used
            c = sort(-c);

            %% transform to [0,1]
            c = (c + 1) ./ 2;

            %% compute b from simplifying condition B(s)
            Vc = fliplr(vander(c));
            es = (1 ./ (1:s))';
            b = Vc' \ es;

            %% compute A from the simplifying conditions
            Vc2 = [Vc(:, 2:end), c.^s];
            A = (Vc2 * diag(es)) / Vc;

            %% restore digits
            digits(digits_old);
        end
    end
end
