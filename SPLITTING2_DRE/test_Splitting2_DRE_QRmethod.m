classdef test_Splitting2_DRE_QRmethod < matlab.unittest.TestCase
    %TEST_SPLITTING_DRE2_QRmethod   Test the different QRmethods for Splitting2_DRE solver.
    %
    %   TEST_SPLITTING2_DRE_QRmethod compares splitting schemes
    %   solvers using internally different QR methods.
    %
    %   See also SPLITTING2_DRE and SPLITTING2_DRE_OPTIONS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        problem = { ...
            struct('instance', 'rail_371', 'T', 2^-1, 'h', 2^-4, 'abs_tol', 1e-2, 'rel_tol', 1e-11, 'abs_tol_orth', 1e-7, 'print_mod', 50) ...
            struct('instance', 'rail_1357', 'T', 2^-1, 'h', 2^-4, 'abs_tol', 1e-2, 'rel_tol', 1e-11, 'abs_tol_orth', 1e-7, 'print_mod', 50) ...
            };

        split = { ...
            Splitting2_t.SPLITTING_LIE ...
            Splitting2_t.SPLITTING_STRANG ...
            Splitting2_t.SPLITTING_SYMMETRIC2 ...
            Splitting2_t.SPLITTING_SYMMETRIC4 ...
            Splitting2_t.SPLITTING_SYMMETRIC6 ...
            Splitting2_t.SPLITTING_SYMMETRIC8 ...
            Splitting2_t.SPLITTING_SYMMETRIC10 ...
            };
    end

    properties
        verbose = ~isCIrun();
    end

    methods(Test)

        function test(obj, problem, split) %#ok<*INUSL>

            %% load data
            if obj.verbose
                fprintf('\n');
            end
            instance = problem.instance;
            T = problem.T;
            h = problem.h;
            abs_tol = problem.abs_tol;
            rel_tol = problem.rel_tol;
            abs_tol_orth = problem.abs_tol_orth;
            data = load(instance);
            A = data.A;
            B = full(data.B);
            C = full(data.C);
            n = size(A, 1);
            Z0 = ones(n, 1);

            if isfield(data, 'E')
                E = data.E;
            else
                E = speye(n, n);
            end

            %% create QR splitting_solver
            split_opt_qr = Splitting2_DRE_options(split);
            split_opt_qr.qrmethod = QRmethod_t.QR;
            split_sol_qr = Splitting2_DRE(A, E, B, C, Z0, split_opt_qr, h, T);
            split_sol_qr.prepare();

            %% create QRF splitting_solver
            split_opt_qrf = Splitting2_DRE_options(split);
            split_opt_qrf.qrmethod = QRmethod_t.QRF;
            split_sol_qrf = Splitting2_DRE(A, E, B, C, Z0, split_opt_qrf, h, T);
            split_sol_qrf.prepare();

            %% create QRT splitting_solver
            split_opt_qrt = Splitting2_DRE_options(split);
            split_opt_qrt.qrmethod = QRmethod_t.QRT;
            split_sol_qrt = Splitting2_DRE(A, E, B, C, Z0, split_opt_qrt, h, T);
            split_sol_qrt.prepare();

            %% solve and compare solution
            if obj.verbose
                f(1) = fprintf('instance = %s, h = %.2e, T = %.2e\n', instance, h, T);
            end
            print_mod = problem.print_mod;
            print_mod_counter = 0;
            while split_sol_qr.t < T

                % time step with QR truncation based splitting solver
                split_sol_qr.time_step();

                % time step with QRF truncation based splitting solver
                split_sol_qrf.time_step();

                % time step with QRT truncation based splitting solver
                split_sol_qrt.time_step();

                % get solutions
                [L_qr, D_qr] = split_sol_qr.get_solution();
                [L_qrf, D_qrf] = split_sol_qrf.get_solution();
                [L_qrt, D_qrt] = split_sol_qrt.get_solution();

                % compare different solutions
                nrm2X_qr = two_norm_sym_handle(@(x) (L_qr * (D_qr * (L_qr' * x))), n);
                abs_err_qr_qrf = two_norm_sym_handle(@(x) (L_qr * (D_qr * (L_qr' * x)))-(L_qrf * (D_qrf * (L_qrf' * x))), n);
                abs_err_qr_qrt = two_norm_sym_handle(@(x) (L_qr * (D_qr * (L_qr' * x)))-(L_qrt * (D_qrt * (L_qrt' * x))), n);
                rel_err_qr_qrf = abs_err_qr_qrf / nrm2X_qr;
                rel_err_qr_qrt = abs_err_qr_qrt / nrm2X_qr;

                % check for orthogonality
                abs_err_orth_qr =  norm(L_qr'*L_qr - eye(size(L_qr, 2)));
                abs_err_orth_qrf =  norm(L_qrf'*L_qrf - eye(size(L_qrf, 2)));
                abs_err_orth_qrt =  norm(L_qrt'*L_qrt - eye(size(L_qrt, 2)));

                % print intermediate results
                percent = split_sol_qr.t / T * 100;
                if percent >= print_mod_counter && obj.verbose
                    f(2) = fprintf('Splitting = %s, t = %.4e, %6.2f%%\n', char(split), split_sol_qr.t, split_sol_qr.t/T*100);
                    f(3) = fprintf('cols: L_qr/L_qrf/L_qrt = %d/%d/%d\n', size(L_qr, 2), size(L_qrf, 2), size(L_qrt, 2));
                    f(4) = fprintf('abs./rel. 2-norm error qr/qrf = %.2e/%.2e\n',  abs_err_qr_qrf, rel_err_qr_qrf);
                    f(5) = fprintf('abs./rel. 2-norm error qr/qrt = %.2e/%.2e\n',  abs_err_qr_qrt, rel_err_qr_qrt);
                    f(6) = fprintf('abs. 2-norm error orth. L_qr  = %.2e\n', abs_err_orth_qr);
                    f(7) = fprintf('abs. 2-norm error orth. L_qrf = %.2e\n', abs_err_orth_qrf);
                    f(8) = fprintf('abs. 2-norm error orth. L_qrt = %.2e\n', abs_err_orth_qrt);
                    print_mod_counter = print_mod_counter + print_mod;
                end

                % check error for tolerances
                obj.fatalAssertLessThan(abs_err_qr_qrf, abs_tol);
                obj.fatalAssertLessThan(abs_err_qr_qrt, abs_tol);
                obj.fatalAssertLessThan(rel_err_qr_qrf, rel_tol);
                obj.fatalAssertLessThan(rel_err_qr_qrt, rel_tol);
                obj.fatalAssertLessThan(abs_err_orth_qr, abs_tol_orth);
                obj.fatalAssertLessThan(abs_err_orth_qrf, abs_tol_orth);
                obj.fatalAssertLessThan(abs_err_orth_qrt, abs_tol_orth);
            end
            if obj.verbose
                f(9) = fprintf('Time QR truncation Splitting Solver:   %.2e, avg. time per step = %.2e\n', split_sol_qr.wtime_time_step, split_sol_qr.wtime_time_step/T*h);
                f(10) = fprintf('Time QRF truncation Splitting Solver:  %.2e, avg. time per step = %.2e\n', split_sol_qrf.wtime_time_step, split_sol_qrf.wtime_time_step/T*h);
                f(11) = fprintf('Time QRT truncation Splitting Solver:  %.2e, avg. time per step = %.2e\n', split_sol_qrt.wtime_time_step, split_sol_qrt.wtime_time_step/T*h);
                fprintf('%s\n', repmat('-', 1, max(f) - 1));
            end
        end
    end
end
