classdef IRKmethod_GAUSS_LEGENDRE < IRKmethod
    %IRKMETHOD_GAUSS_LEGENDRE      Class implements data for Gauss-Legendre
    % Runge-Kutta methods.
    %
    %   Fully implicit Gauss-Legendre Runge-Kutta methods are available
    %   with stages s = 1, ..., 15. The order of these methods 2*s, which is
    %   the maximal achivable order for a Runge-Kutta method.
    %
    %   See also IRKSTEP and IRKMETHOD.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(Hidden = true)
        s;
    end

    methods(Access = public)
        function obj = IRKmethod_GAUSS_LEGENDRE(s)
            %IRKMETHOD_GAUSS_LEGENDRE  Constructor of class.
            %
            %   GL = IRKMETHOD_GAUSS_LEGENDRE(S)
            %   creates an instance of the class.
            %   GL represents a Gauss-Legendre Runge-Kutta method with
            %   S stages. It has order 2*S.
            %   S is a positive real integer double, such that
            %   1<=S<=15.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(s, {'double'}, {'real', '<=', 15, '>=', 1, 'scalar', 'integer'}, mfilename, inputname(1));
            obj.s = s;

            %% read tableau file and set tableau data
            tab_file = @(name) fullfile(fileparts(mfilename('fullpath')), 'tableau', sprintf('GAUSS_LEGENDRE_%d_%s.mtx', obj.s, name));
            assert(isfile(tab_file('A')), 'Tableau data file %s not found.', tab_file('A'));
            assert(isfile(tab_file('b_vec')), 'Tableau data file %s not found.', tab_file('b_vec'));
            assert(isfile(tab_file('c_vec')), 'Tableau data file %s not found.', tab_file('c_vec'));
            assert(isfile(tab_file('d_vec')), 'Tableau data file %s not found.', tab_file('d_vec'));
            assert(isfile(tab_file('V')), 'Tableau data file %s not found.', tab_file('V'));
            assert(isfile(tab_file('D')), 'Tableau data file %s not found.', tab_file('D'));
            assert(isfile(tab_file('invV')), 'Tableau data file %s not found.', tab_file('invV'));
            assert(isfile(tab_file('invD')), 'Tableau data file %s not found.', tab_file('invD'));

            obj.A = mmread(tab_file('A'));
            obj.b = mmread(tab_file('b_vec'));
            obj.c = mmread(tab_file('c_vec'));
            obj.d = mmread(tab_file('d_vec'));
            obj.V = mmread(tab_file('V'));
            obj.D = mmread(tab_file('D'));
            obj.invV = mmread(tab_file('invV'));
            obj.invD = mmread(tab_file('invD'));

        end

        function c = char(obj)
            c = sprintf('GAUSS_LEGENDRE_%d', obj.s);
        end

        function s = string(obj)
            s = char(obj);
        end

        function p = order(obj)
            p = 2 * obj.s;
        end

        function s = stages(obj)
            s = obj.s;
        end

        function q = is_symmetric(obj) %#ok<*MANU>
            q = true;
        end

        function q = is_fsal(obj)
            q = false;
        end
    end
end
