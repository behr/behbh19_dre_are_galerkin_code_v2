classdef QuadRule_CLENSHAW_CURTIS < QuadRule
    %QUADRULE_CLENSHAW_CURTIS     Implementation of Clenshaw-Curtis quadrature
    % methods using the abstract QuadRule class.
    %
    %   Note: I am not sure about the degree.
    %
    %   See also QUADRULE and QUADRULE_GAUSS_LEGENDRE.
    %
    %   Author: Maximilian Behr

    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %

    properties(Hidden = true)
        s;
    end

    methods
        function obj = QuadRule_CLENSHAW_CURTIS(s)
            %QUADRULE_CLENSHAW_CURTIS  Constructor of class.
            %
            %   CL = QUADRULE_CLENSHAW_CURTIS(S)
            %   creates an instance of the class.
            %   CL represents a Clenshaw-Curtis method with
            %   S stages. S is a double integer larger equals 2.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(s, {'double'}, {'real', '>', 1, 'scalar', 'integer', 'finite', 'nonnan'}, mfilename, inputname(1));
            obj.s = s;
        end

        function c = char(obj)
            c = sprintf('CLENSHAW_CURTIS_%d', obj.s);
        end

        function s = string(obj)
            s = string(char(obj));
        end

        function p = order(obj)
            p = obj.s;
        end

        function q = use_start_point(obj) %#ok<*MANU>
            q = true;
        end

        function q = use_end_point(obj) %#ok<*MANU>
            q = true;
        end

        function equidistant = is_equidistant(obj)
            equidistant = false;
        end

        function [nodes, weights] = nodes_weights(obj, interval)
            %NODES_WEIGHTS  Nodes and the weights of the quadrature rule.
            %   [NODES, WEIGHTS] = quad.NODES_WEIGHTS() returns
            %   the nodes NODES and the WEIGHTS W of the quadrature rule
            %   for the interval [0,1].
            %
            %   [NODES, WEIGHTS] = quad.NODES_WEIGHTS(INTERVAL) returns
            %   the nodes NODES and the weights WEIGHTS of the
            %   quadrature rule for the interval INTERVAL. NODES and
            %   WEIGHTS are rowvectors.
            %
            %   Author: Maximilian Behr

            %% check input args
            if nargin == 2
                validateattributes(interval, {'double'}, {'real', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(2));
                assert(length(interval) == 2, 'invalid interval is given');
                assert(interval(1) < interval(2), 'invalid interval is given');
            end

            % ClenShaw-Curtis get number of nodes from order
            if nargin < 2
                [nodes, weights] = QuadRule_CLENSHAW_CURTIS.clenshaw_points(obj.s, [0, 1]);
            else
                [nodes, weights] = QuadRule_CLENSHAW_CURTIS.clenshaw_points(obj.s, interval);
            end

            nodes = reshape(nodes, 1, []);
            weights = reshape(weights, 1, []);

            % assertion is sorted nodes
            assert(issorted(nodes), 'nodes have to be sorted.');
            assert(isrow(nodes), 'nodes have to be a row vector.');
            assert(isrow(weights), 'weights have to be a row vector.');
            assert(length(nodes) == length(weights), 'Number of nodes must be equal to number of weights.');

        end
    end

    methods(Access = public, Static)
        function [nodes, weights] = clenshaw_points(p, interval)
            %CLENSHAW_POINTS      Computes the nodes and weights for Clenshaw-Curtis
            %   quadrature.
            %
            %   [NODES, WEIGHTS] = CLENSHAW_POINTS(P, INTER) compute NODES and WEIGHTS
            %   for Clenshaw-Curtis quadrature on the interval [INTER(1), INTER(2)].
            %   INTER must be a row-vector with 2 columns.
            %
            %   [NODES, WEIGHTS] = CLENSHAW_POINTS(P) compute NODES and WEIGHTS
            %   for Clenshaw-Curits quadrature on the interval [0, 1].
            %
            %   We use Waldvogels method.
            %
            %   Waldvogel; Fast construction of the Fejer and Clenshaw-Curtis
            %   quadrature rules; BIT. Numerical Mathematics;
            %   https://doi.org/10.1007/s10543-006-0045-4
            %
            %   Author: Maximilian Behr

            %% check inputs
            validateattributes(p, {'numeric'}, {'real', 'integer', '>', 0, 'finite', 'nonnan', 'scalar'}, mfilename, inputname(1));
            interval_ = [-1, 1];
            if nargin >= 2
                validateattributes(interval, {'numeric'}, {'real', 'finite', 'nonnan', 'vector', 'nrows', 1, 'ncols', 2}, mfilename, inputname(1));
                assert(interval(1) < interval(2), 'no proper interval given');
                interval_ = interval;
            end

            %% Waldvogels method
            if p == 0
                nodes = [];
                weights = [];
            elseif p == 1
                nodes = 0;
                weights = 2;
            else
                pm1 = p - 1;
                nodes = sin(pi*(-pm1:2:pm1)/(2 * pm1)).';
                c = 2 ./ [1, 1 - (2:2:(p - 1)).^2];
                c = [c, c(floor(p/2):-1:2)];
                weights = ifft(c);
                weights([1, p]) = weights(1) / 2;
            end

            %% transform nodes and weights
            weights = (diff(interval_) / 2) * weights;
            nodes = interval_(2) * (nodes + 1) / 2 + interval_(1) * (1 - nodes) / 2;

        end

    end
end
