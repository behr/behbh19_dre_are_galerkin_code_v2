classdef Splitting2_DRE_options < copy_handle
    %SPLITTING2_DRE_OPTIONS hold all options for SPLITTING2_DRE splitting scheme solver.
    %
    %   SPLITTING2_DRE_OPTIONS properties:
    %       splitting           - Splitting2_t instance, the type of the splitting scheme
    %       irkmethod           - IRKmethod instance, the type of the implicit Runge-Kutta method, default: GAUSS_LEGENDRE of sufficient order
    %       quadrule            - Quadrule instance, the type of the quadrature rule, default: GAUSS_LEGENDRE(20)
    %       trunc_tol           - double, scalar, nonnegative, relative truncation tolerance,  default: 1e-10
    %       spsd_correction     - logical, symmetric positive semidefiniteness correction, replaces the negative values in LDL' format by zero to ensure semidefiniteness, default: true
    %       qrmethod            - QRmethod_t instance, set the desired QR method, default: QRF
    %       decomposition_type  - Decomposition_t instance, set the desired decomposition method, default: DECOMPOSITION_MATLAB
    %       verbose             - Verbose_t instance, control the verbosity of the solver,  default: NONE
    %
    %   If spsd_correction is true, then negative values diagonal of the
    %   real low-rank LDL' approximation are removed.
    %   The L must be orthogonal and D diagonal. This gives the best
    %   positive semidefinite approximation.
    %   After truncation process L is orthogonal and D is diagonal.
    %   The remove of negative values in D ensure the semidefiniteness of
    %   the approximation.
    %   If Lie, Strang or symmetric splitting scheme of order 2 is used,
    %   D should be positive semidefinite in theory as all coefficients
    %   of the method are nonnegative. However due to numerical errors
    %   this property might be lost.
    %   spsd_correction comes with almost zero additional computational
    %   costs.
    %   Strang splitting scheme is an exception as an additional
    %   small eigenvalue problem has to be solved.
    %
    %   The qrmethod member specifies the QR method, which should be used
    %   for truncation. For small examples QRF is recommended. For larger
    %   examples QRT might be the better choice.
    %
    %   decomposition_type specifies the decomposition method, which
    %   should be used for solving the linear system.
    %   The default value is DECOMPOSITION_MATLAB.
    %   The DECOMPOSITION_BANDED might be faster, but usually needs
    %   more main memory.
    %
    % Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = public)
        % Splitting2_t instance, the type of the splitting scheme
        splitting
        % IRKmethod instance, the type of the implicit Runge-Kutta method, default: GAUSS_LEGENDRE of sufficient order
        irkmethod
        % Quadrule instance, the type of the quadrature rule, default: GAUSS_LEGENDRE(20)
        quadrule
        % double, scalar, nonnegative, relative truncation tolerance, default: 1e-10
        trunc_tol
        % logical, symmetric positive semidefinites correction, default: true
        spsd_correction
        % QRmethod_t instance, set the desired QR method, default: QRF
        qrmethod
        % Decomposition_t instance, set the desired decomposition method, default: DECOMPOSITION_MATLAB
        decomposition_type
        % Verbose_t instance, control the verbosity of the solver, default: NONE
        verbose
    end

    methods(Access = public)
        function obj = Splitting2_DRE_options(splitting, irkmethod, quadrule, trunc_tol, spsd_corr, qrmethod, decomposition_type, verbose)
            %SPLITTING2_DRE_OPTIONS Constructor for the options class.
            %
            %   OPT = SPLITTING2_DRE_OPTIONS(SPLITTING, IRKMETHOD, QUADRULE, TRUNC_TOL, SPSD_CORR, QRMETHOD, DECOMPOSITION_TYPE, VERBOSE)
            %   generates an object of this class holding all available options for the
            %   splitting scheme solver.
            %
            %   OPT = SPLITTING2_DRE_OPTIONS(SPLITTING, IRKMETHOD, QUADRULE, TRUNC_TOL, SPSD_CORR, QRMETHOD, DECOMPOSITION_TYPE)
            %   OPT = SPLITTING2_DRE_OPTIONS(SPLITTING, IRKMETHOD, QUADRULE, TRUNC_TOL, SPSD_CORR, QRMETHOD)
            %   OPT = SPLITTING2_DRE_OPTIONS(SPLITTING, IRKMETHOD, QUADRULE, TRUNC_TOL, SPSD_CORR)
            %   OPT = SPLITTING2_DRE_OPTIONS(SPLITTING, IRKMETHOD, QUADRULE, TRUNC_TOL)
            %   OPT = SPLITTING2_DRE_OPTIONS(SPLITTING, IRKMETHOD, QUADRULE)
            %   OPT = SPLITTING2_DRE_OPTIONS(SPLITTING, IRKMETHOD)
            %   OPT = SPLITTING2_DRE_OPTIONS(SPLITTING)
            %
            %   The missing property arguments are set to its default value.
            %
            %   See also SPLITTING2_DRE.
            %
            % Author: Maximilian Behr
            obj.splitting = splitting;
            obj.irkmethod = IRKmethod_GAUSS_LEGENDRE(ceil(order(splitting)/2));
            obj.quadrule = QuadRule_GAUSS_LEGENDRE(20);
            obj.trunc_tol = 1e-10;
            obj.spsd_correction = true;
            obj.qrmethod = QRmethod_t.QRF;
            obj.decomposition_type = Decomposition_t.DECOMPOSITION_MATLAB;
            obj.verbose = Verbose_t.NONE;

            if nargin >= 2
                obj.irkmethod = irkmethod;
            end

            if nargin >= 3
                obj.quadrule = quadrule;
            end

            if nargin >= 4
                obj.trunc_tol = trunc_tol;
            end

            if nargin >= 5
                obj.spsd_correction = spsd_corr;
            end

            if nargin >= 6
                obj.qrmethod = qrmethod;
            end

            if nargin >= 7
                obj.decomposition_type = decomposition_type;
            end

            if nargin >= 8
                obj.verbose = verbose;
            end

        end

        function validate(obj)
            %VALIDATE   validate options structure.
            %
            %   Requirements:
            %       order(splitting) <= order(quadrule)
            %       order(splitting) <= order(irkmethod)
            %
            % validate order of splitting scheme and quadrature rule
            if order(obj.splitting) > order(obj.quadrule)
                error('Quadrature rule %s of order %d reduces order of splitting scheme %s order %d.  Increase the order of your QuadRule.', ...
                    char(obj.quadrule), order(obj.quadrule), char(obj.splitting), order(obj.splitting));
            end

            % validate order of splitting scheme and implicit Runge Kutta method
            if order(obj.splitting) > order(obj.irkmethod)
                error('IRKmethod %s of order %d reduces order of splitting scheme %s order %d. Increase the order of your IRKmethod.', ...
                    char(obj.irkmethod), order(obj.irkmethod), char(obj.splitting), order(obj.splitting));
            end
        end
    end

    methods
        % setter methods
        function set.splitting(obj, in_splitting)
            validateattributes(in_splitting, {'Splitting2_t'}, {}, mfilename);
            obj.splitting = in_splitting;
        end

        function set.irkmethod(obj, in_irkmethod)
            validateattributes(in_irkmethod, {'IRKmethod'}, {}, mfilename);
            obj.irkmethod = in_irkmethod;
        end

        function set.quadrule(obj, in_quadrule)
            validateattributes(in_quadrule, {'QuadRule'}, {}, mfilename);
            obj.quadrule = in_quadrule;
        end

        function set.trunc_tol(obj, in_trunc_tol)
            validateattributes(in_trunc_tol, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename);
            obj.trunc_tol = in_trunc_tol;
        end

        function set.spsd_correction(obj, in_spsd_correction)
            validateattributes(in_spsd_correction, {'logical'}, {}, mfilename);
            obj.spsd_correction = in_spsd_correction;
        end

        function set.qrmethod(obj, in_qrmethod)
            validateattributes(in_qrmethod, {'QRmethod_t'}, {}, mfilename);
            obj.qrmethod = in_qrmethod;
        end

        function set.decomposition_type(obj, in_decomposition_type)
            validateattributes(in_decomposition_type, {'Decomposition_t'}, {}, mfilename);
            obj.decomposition_type = in_decomposition_type;
        end

        function set.verbose(obj, in_verbose)
            validateattributes(in_verbose, {'Verbose_t'}, {}, mfilename);
            obj.verbose = in_verbose;
        end
    end
end
