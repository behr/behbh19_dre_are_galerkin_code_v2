classdef QuadRule_GAUSS_LEGENDRE < QuadRule
    %QUADRULE_GAUSS_LEGENDRE     Implementation of Gauss-Legendre quadrature
    % methods using the abstract QuadRule class.
    %
    %   See also QUADRULE and QUADRULE_CLENSHAW_CURTIS.
    %
    %   Author: Maximilian Behr

    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(Hidden = true)
        s;
    end

    methods(Access = public)
        function obj = QuadRule_GAUSS_LEGENDRE(s)
            %QUADRULE_GAUSS_LEGENDRE  Constructor of class.
            %
            %   GL = QUADRULE_GAUSS_LEGENDRE(S)
            %   creates an instance of the class.
            %   GL represents a Gauss-Legendre method with
            %   S stages. It has order 2*S+1.
            %   S is a positive real integer double.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(s, {'double'}, {'real', '>', 0, 'scalar', 'integer'}, mfilename, inputname(1));
            obj.s = s;
        end

        function c = char(obj)
            c = sprintf('GAUSS_LEGENDRE_%d', obj.s);
        end

        function s = string(obj)
            s = char(obj);
        end

        function p = order(obj)
            p = 2 * obj.s + 1;
        end

        function q = use_start_point(obj) %#ok<*MANU>
            q = false;
        end

        function q = use_end_point(obj) %#ok<*MANU>
            q = false;
        end

        function equidistant = is_equidistant(obj)
            equidistant = false;
        end

        function [nodes, weights] = nodes_weights(obj, interval)
            %NODES_WEIGHTS  Nodes and the weights of the quadrature rule.
            %   [NODES, WEIGHTS] = quad.NODES_WEIGHTS() returns
            %   the nodes NODES and the WEIGHTS W of the quadrature rule
            %   for the interval [0,1].
            %
            %   [NODES, WEIGHTS] = quad.NODES_WEIGHTS(INTERVAL) returns
            %   the nodes NODES and the weights WEIGHTS of the
            %   quadrature rule for the interval INTERVAL.
            %
            %   Author: Maximilian Behr

            % check input args
            if nargin == 2
                validateattributes(interval, {'double'}, {'real', 'nonsparse', 'finite', 'nonnan'}, mfilename, inputname(2));
                assert(length(interval) == 2, 'invalid interval is given');
                assert(interval(1) < interval(2), 'invalid interval is given');
            end

            % Gauss-Legendre get number of nodes from order
            p = obj.order();
            p = round((p - 1)/2);
            if nargin < 2
                [nodes, weights] = QuadRule_GAUSS_LEGENDRE.legendre_points(p, [0, 1]);
            else
                [nodes, weights] = QuadRule_GAUSS_LEGENDRE.legendre_points(p, interval);
            end

            nodes = reshape(nodes, 1, []);
            weights = reshape(weights, 1, []);

            % assertion: sorted nodes and same number of nodes as weights
            assert(issorted(nodes), 'nodes have to be sorted.');
            assert(isrow(nodes), 'nodes have to be a row vector.');
            assert(isrow(weights), 'weights have to be a row vector.');
            assert(length(nodes) == length(weights), 'Number of nodes must be equal to number of weights.');
        end
    end

    methods(Access = private, Static)
        function [nodes, weights] = legendre_points(p, inter)
            %LEGENDRE_POINTS      Computes the nodes and weights for Gauss-Legendre
            %   quadrature.
            %
            %   [NODES, WEIGHTS] = LEGENDRE_POINTS(P, INTER) compute NODES and WEIGHTS
            %   for Gauss-Legendre quadrature on the interval [INTER(1), INTER(2)].
            %   INTER must be a row-vector with 2 columns.
            %
            %   [NODES, WEIGHTS] = LEGENDRE_POINTS(S) compute NODES and WEIGHTS
            %   for Gauss-Legendre quadratur on the interval [0, 1].
            %
            %   The Goloub-Welsch algorithm is used.
            %
            %   Author: Maximilian Behr

            %% check inputs
            validateattributes(p, {'numeric'}, {'real', 'integer', '>', 0, 'finite', 'nonnan', 'scalar'}, mfilename, inputname(1));
            interval = [-1, 1];
            if nargin >= 2
                validateattributes(inter, {'numeric'}, {'real', 'finite', 'nonnan', 'vector', 'nrows', 1, 'ncols', 2}, mfilename, inputname(1));
                assert(inter(1) < inter(2), 'no proper interval given');
                interval = inter;
            end

            %% Goloub-Welsch
            beta = .5 ./ sqrt(1-(2 * (1:p - 1)).^(-2));
            [V, D] = eig(diag(beta, 1)+diag(beta, -1));
            [nodes, idx] = sort(diag(D));
            weights = 2 * V(1, idx).^2;

            %% Quadrature nodes are symmetric
            idx = 1:floor(p/2);
            nodes = nodes(idx);
            weights = weights(idx);
            if mod(p, 2)
                nodes = [nodes; 0; -nodes(end:-1:1)];
                weights = [weights, 2 - sum(2*weights), weights(end:-1:1)];
            else
                nodes = [nodes; -nodes(end:-1:1)];
                weights = [weights, weights(end:-1:1)];
            end

            %% transform to interval
            diffinterval = diff(interval);
            nodes = (nodes + 1) / 2 * diffinterval + interval(1);
            weights = diffinterval * weights / 2;
        end
    end
end
