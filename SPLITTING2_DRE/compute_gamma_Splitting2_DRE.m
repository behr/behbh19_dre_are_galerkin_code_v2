%COMPUTE_GAMMA_SPLITTING2_DRE   Compute the coefficients gamma
%   for the symmetric Splitting Schemes.
%
%   References:
%       Mariano De Leo and Diego Rial and Constanza Sanchez de la Vega,
%       High order methods for irreversible equations,
%       2013, eqn 1.8
%
%   See also SPLITTING2_DRE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% setup
max_stages = 10; % stages

%% compute coefficients
for stages = 1:max_stages

    % setup matrix A
    A = sym(zeros(stages, stages));
    A(1, :) = 1;
    for k = 2:stages
        A(k, :) = sym((1:stages)).^(-2 * (k - 1));
    end

    % set vector b
    b = [sym(1/2); sym(zeros(stages-1, 1))];

    % solve and get gamma
    gamma = A \ b;

    fprintf('SYMMETRIC%d ORDER %d with %d stages:\n', 2*stages, 2*stages, stages);
    %fprintf('%f ', double(gamma));
    disp(gamma');
    fprintf('\n');
end
