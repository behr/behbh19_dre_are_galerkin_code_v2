%TRY_SPLITTING2_DRE    Playaround script for Splitting2_DRE solvers.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% problem data
instance = 'rail_371';
data = load(instance);
A = data.A;
B = data.B;
C = data.C;
E = data.E;
n = size(A, 1);
Z0 = zeros(n, 1);
T = 2^(-8);
h_ref = 2^(-14);
log_mod = 2^2;

%% picture of plot and diary
mypic = sprintf('%s_%s_h_ref%.2e_T_%.2e.fig', mfilename, instance, h_ref, T);
mydiary = sprintf('%s_%s_h_ref%.2e_T_%.2e.log', mfilename, instance, h_ref, T);
diary(mydiary)

%% create fine solver for reference solution
split_opt = Splitting2_DRE_options(Splitting2_t.SPLITTING_SYMMETRIC8);
split_opt.decomposition_type = Decomposition_t.DECOMPOSITION_SPARSE_DIRECT;
split_sol = Splitting2_DRE(A, E, B, C, Z0, split_opt, h_ref, T);
fprintf('Prepare solver for reference solution\n');
split_sol.prepare();

%% solve ARE to track convergence to stationary point
radi_opt = RADI_options();
radi_opt.rel2res_tol = 1e-19;
radi_opt.abs2res_tol = 1e-19;
radi_opt.maxiter = 400;
radi_opt.verbose = true;
[Z, stat] = RADI(A, E, B, C, radi_opt);
nrm2C = norm(C)^2;
abs2res = res2_ARE(A, E, B, C, Z);
rel2res = abs2res / nrm2C;
nrm2Xinf = norm(Z)^2;
fprintf('abs./rel. 2-norm residual = %.2e/%.2e, ||Xinf|| = %.2e\n', abs2res, rel2res, nrm2Xinf);

%% integrate and compare
results = [];
i = 0;
temp = tic();
while split_sol.t < split_sol.T

    % integrate with fine solver
    t = tic();
    split_sol.time_step();
    t = toc(t);
    temp2 = toc(temp);

    if mod(i, log_mod) == 0
        fprintf('walltime: time step = %.2e\n', t);

        % get solutions and compare
        [Lref, Dref] = split_sol.get_solution();

        % compute distance to stationary point
        t = tic();
        nrm2_Xt = two_norm_sym_handle(@(x) Lref*(Dref * (Lref' * x)), n);
        diff_Xinf = two_norm_sym_handle(@(x) Z*(Z' * x)-Lref*(Dref * (Lref' * x)), n);
        abs2res_Xt = res2_ARE(A, E, B, C, Lref, Dref);
        rel2res_Xt = abs2res_Xt / nrm2C;
        t = toc(t);
        fprintf('walltime: norm computation = %.2e\n', t);

        colsL = size(Lref, 2);
        results(end+1, :) = [split_sol.t, nrm2_Xt, diff_Xinf, abs2res_Xt, rel2res_Xt, colsL]; %#ok<*SAGROW>

        % print results
        est_rem_time = abs((temp2/split_sol.t)*split_sol.T - temp2);
        fprintf('%s - %s: T = %.2e, href = %.2e, t = %.2e, %.2f %%, est. rem. time = %.2e\n', ...
            datestr(now), instance, T, h_ref, split_sol.t, split_sol.t/T*100, est_rem_time);
        fprintf('\tnorm Xt = %.2e, diff Xinf = %.2e, colsL = %d\n', nrm2_Xt, diff_Xinf, colsL);
        fprintf('\tabs./rel. 2-norm residual Xt = %e/%e\n', abs2res_Xt, rel2res_Xt);
    end
    i = i + 1;
end

%% plot results
m = 2;
n = 3;

% titles and plot commands
titles = { ...
    '||Xref||', ...
    '||Xt - Xref||', ...
    '||R(Xt)||_2', ...
    '||R(Xt)||_2 / ||C||_2^2', ...
    'cols L'; ...
    };

% plot commands
plot_commands = {; ...
    @() semilogy(results(:, 1), results(:, 2)), ...
    @() semilogy(results(:, 1), results(:, 3)), ...
    @() semilogy(results(:, 1), results(:, 4)), ...
    @() semilogy(results(:, 1), results(:, 5)), ...
    @() plot(results(:, 1), results(:, 6)); ...
    };

for i = 1:numel(titles)
    subplot(m, n, i);
    plot_command = plot_commands{i};
    plot_command();
    title(titles{i});
end

hgsave(gcf, mypic);
diary('off');