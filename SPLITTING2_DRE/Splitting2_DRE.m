classdef Splitting2_DRE < Solver_DRE
    %SPLITTING2_DRE     Implements the splitting scheme for the autonomus
    %   differential Riccati equation.
    %
    %   The autonomous differential Riccati equation is given by:
    %
    %       E'\dot{X}E = A' X E + E' X A - E' X B B' X E + C' C,    (DRE)
    %             X(0) = Z0 Z0'.
    %
    %   SPLITTING2_DRE properties:
    %       A       - set private, double, n-x-n sparse or dense matrix A
    %       E       - set private, double, n-x-n sparse or dense matrix E or empty for identity
    %       B       - set private, double, n-x-b dense matrix B
    %       C       - set private, double, c-x-n dense matrix C
    %       Z0      - set private, double, n-x-z dense matrix Z or empty for zero
    %       Lk      - set private, double, n-x-lk dense matrix current low-rank approximation
    %       Dk      - set private, double, lk-x-n dense matrix current low-rank approximation
    %       irk     - IRKstep, implicit Runge-Kutta method
    %       IQHZ    - double, n-x-iqz dense low-rank approximation of integral term.
    %
    %   The matrices A and E must be either both dense or sparse. The mixed case is not supported.
    %
    %   SPLITTING2_DRE methods:
    %       SPLITTING2_DRE  - public, Constructor.
    %       m_prepare       - private, Prepare the solver.
    %       m_time_step     - private, Perform one step in time.
    %       m_get_solution  - private, Return the approximation.
    %
    %   See also SPLITTING2_DRE_OPTIONS.
    %
    %   This is a partly reimplmentation of Tony Stillfjords splitting
    %   DREsplit toolbox:
    %
    %       http://www.tonystillfjord.net/DREsplit.zip
    %
    %   For references see:
    %   [1] Stillfjord, Tony,
    %       Adaptive high-order splitting schemes for large-scale differential Riccati equations,
    %       Numerical Algorithms, 2018, https://doi.org/10.1007/s11075-017-0416-8
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % double, n-x-n sparse or dense matrix A
        A
        % double, n-x-n sparse or dense matrix E or empty for identity
        E
        % double, n-x-b dense matrix B
        B
        % double, c-x-n dense matrix C
        C
        % double, n-x-z dense matrix Z or empty for zero
        Z0
        % double, n-x-lk dense matrix current low-rank approximation
        Lk
        % double, lk-x-lk dense matrix current low-rank approximation
        Dk
    end

    properties(Access = private)
        % IRKstep, implicit Runge-Kutta method
        irk
        % map, key is h double and value os double dense matrix, n-x-iqz dense low-rank approximation of integral term
        IQZH
    end

    methods(Access = public)
        function obj = Splitting2_DRE(A, E, B, C, Z0, opt, h, T)
            %SPLITTING_DRE2  Constructor of the splitting scheme solver.
            %
            %   SPLIT = SPLITTING_DRE2(A, E, B, C, Z0, OPT, H, T)
            %   creates an instance of the class. A, E are double sparse
            %   matrices. If E is empty the identity is used.
            %   B, C and Z0 are double dense matrices. If Z0 is empty
            %   zero initial conditions are assumed. OPT must be an
            %   instance of SPLITTING2_DRE_OPTIONS class and holds the
            %   options for the solver. The matrices A and E must
            %   be either both dense or sparse. The mixed case is not supported.
            %
            %   See also SPLITTING2_DRE_OPTIONS.
            %
            %   Author: Maximilian Behr

            %% check input parameters
            validateattributes(A, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
            %assert(issparse(A), '%s must be sparse', inputname(1));
            issparseA = issparse(A);
            n = size(A, 1);
            if ~isempty(E)
                validateattributes(E, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan', 'size', [n, n]}, mfilename, inputname(2));
                if issparseA
                    assert(issparse(E), 'A is sparse, therefore %s must be also sparse.', inputname(2));
                end
            end
            validateattributes(B, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [n, NaN]}, mfilename, inputname(3));
            validateattributes(C, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [NaN, n]}, mfilename, inputname(4));
            if ~isempty(Z0)
                validateattributes(Z0, {'double'}, {'nonsparse', 'real', 'nonsparse', 'finite', 'nonnan', '2d', 'size', [n, NaN]}, mfilename, inputname(5));
            end
            validateattributes(opt, {'Splitting2_DRE_options'}, {}, mfilename, inputname(6));

            %% set input args to fields
            obj.A = A;
            if isempty(E)
                if issparseA
                    obj.E = speye(size(obj.A));
                else
                    obj.E = eye(size(obj.A));
                end
            else
                obj.E = E;
            end

            obj.B = full(B);
            obj.C = full(C);

            if isempty(Z0)
                obj.Z0 = zeros(n, 1);
            else
                obj.Z0 = Z0;
            end
            obj.Lk = obj.Z0;
            z0 = size(obj.Z0, 2);
            obj.Dk = eye(z0, z0);

            obj.options = opt;
            obj.options.validate();

            %% setup implicit Runge-Kutta method
            obj.irk = IRKstep(obj.A', obj.E', obj.options.irkmethod, obj.options.decomposition_type, obj.options.verbose);

            %% setup container for integral term
            % IQZH(h) contains a low-rank approximation of \int_0^{h} e^(sA')C'Ce^(sA) ds
            obj.IQZH = containers.Map('KeyType', 'double', 'ValueType', 'any');

            %% set integration parameters
            obj.t = 0;
            obj.h = h;
            obj.T = T;
            obj.validate_step_size();
        end
    end

    methods(Access = protected)
        function m_prepare(obj)
            %M_PREPARE  Prepre the solver, compute LU factorizations and
            %   integral terms.
            %
            %   M_PREPARE() compute LU factorizations and integral terms.
            %
            %   Author: Maximilian Behr

            %% add step sizes to cache and compute integral terms
            if (obj.options.splitting == Splitting2_t.SPLITTING_LIE)
                obj.irk.add_step_size_cache(obj.h);
                obj.approximate_iqh(obj.h, 1, obj.C');
            elseif (obj.options.splitting == Splitting2_t.SPLITTING_STRANG)
                obj.irk.add_step_size_cache(obj.h);
                obj.approximate_iqh(obj.h, 1, obj.C');
            elseif (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC2) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC4) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC6) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC8) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC10) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC12) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC14) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC16) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC18) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC20)

                % get number of stages
                len_gam = length(obj.options.splitting.gamma());
                hs = obj.h * (1 ./ (1:len_gam));

                % add step sizes h, h/2, h/3, ... h/stages to cache
                obj.irk.add_step_size_cache(hs);
                obj.approximate_iqh(obj.h, len_gam, obj.C');
            else
                error('Splitting Scheme %s not implemented.', obj.options.splitting);
            end
        end

        function m_time_step(obj)
            %M_TIME_STEP  Perform a time step with step size h with splitting scheme.
            %
            %   SPLIT.M_TIME_STEP() performes one time step with step size h
            %   with splitting scheme.
            %
            %   See also SOLVER_DRE and SPLITTING2_DRE.
            %
            %   Author: Maximilian Behr

            % current iterates
            L = obj.Lk;
            D = obj.Dk;

            if obj.options.splitting == Splitting2_t.SPLITTING_LIE
                % Operator TF(h)TG(h)

                % approximate exphL
                exphL = obj.irk.step_cache(obj.h, L);

                % apply TF(h)TG(h)
                [L, D] = obj.apply_TF_TG(1, obj.h, L, exphL, D);

            elseif obj.options.splitting == Splitting2_t.SPLITTING_STRANG
                % Operator TG(h/2)TF(h)TG(h/2)

                % approximate exphL
                exphL = obj.irk.step_cache(obj.h, L);

                % apply TG(h/2)TF(h)TG(h/2)
                D = obj.apply_TG(obj.h/2, L, D);
                [L, D] = obj.apply_TF(obj.h, exphL, D);
                D = obj.apply_TG(obj.h/2, L, D);

            elseif (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC2) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC4) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC6) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC8) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC10) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC12) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC14) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC16) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC18) || ...
                    (obj.options.splitting == Splitting2_t.SPLITTING_SYMMETRIC20)
                % Operator  gamma(1) ( TG(h) TF(h)    +  TF(h) TG(h)) +
                %           gamma(2) ((TG(h) TF(h))^2 + (TF(h) TG(h))^2) +
                %                   ...
                %           gamma(s) ((TG(h) TF(h))^s + (TF(h) TG(h))^s) +

                gam = obj.options.splitting.gamma();
                len_gam = length(gam);
                gamL = cell(1, len_gam);
                gamD = cell(1, len_gam);
                for k = 1:len_gam
                    % approximate expm(h/k A')L
                    exphL = obj.irk.step_cache(obj.h/k, L);

                    % apply gamma(k)*((TF(h/k) TG(h/k))^k + (TG(h/k) TF(h/k))^k) and truncate
                    [L1, D1] = obj.apply_TG_TF(k, obj.h/k, exphL, D);
                    [L2, D2] = obj.apply_TF_TG(k, obj.h/k, L, exphL, D);
                    [gamL{k}, gamD{k}] = obj.truncate_LDLT([L1, L2], blkdiag(gam(k)*D1, gam(k)*D2));
                end

                %% sum and truncate
                L = gamL{1};
                D = gamD{1};
                for k = 2:len_gam
                    [L, D] = obj.truncate_LDLT([L, gamL{k}], blkdiag(D, gamD{k}));
                end

            else
                error('Splitting Scheme %s not implemented.', obj.options.splitting);
            end

            %% correct definitness, i.e. remove negative values in D
            if obj.options.spsd_correction
                % debugging purposes only:
                % Check that D is diagonal and L is orthogonal
                % assert(isdiag(D), 'D assumed to be diagonal');
                % disp(norm(L'*L - eye(size(D))));

                if obj.options.splitting == Splitting2_t.SPLITTING_STRANG
                    % STRANG is a special case
                    % D is not diagonal but should be symmetric, L should be orthogonal
                    % compare apply_TG, apply_TF and truncate_LDLT
                    [V, D] = eig(D, 'vector');
                    idx = D > 0;
                    removed_vals = D(~idx);
                    D = diag(D(idx));
                    L = L * V(:, idx);
                else
                    % D must be diagonal and L orthogonal
                    % remove negative values in D and truncate even more
                    idx = diag(D) > 0;
                    removed_vals = D(~idx, ~idx);
                    D = D(idx, idx);
                    L = L(:, idx);
                end
                % verbose information
                if obj.options.verbose >= Verbose_t.HIGH
                    fprintf('Removed %d negative values from D\n', length(removed_vals));
                    disp(removed_vals);
                end
            end

            %% set new LDLT factorization
            obj.Lk = L;
            obj.Dk = D;

            %% update time
            obj.t = obj.t + obj.h;

            %% print status
            obj.print_status_verbose();
        end

        function [L, D] = m_get_solution(obj)
            %M_GET_SOLUTION  Return the approximation at the current time
            %   as LDL' factorization
            %
            %   M_GET_SOLUTION() returns the approximation at the current
            %   time as LDL' factorization.
            %
            %   Author: Maximilian Behr
            L = obj.Lk;
            D = obj.Dk;
        end
    end

    methods(Access = private)
        function print_status_verbose(obj)
            %PRINT_STATUS  Status information about the Splitting2_DRE solver instance.
            %
            %   SPLIT.PRINT_STATUS() status information abouth the
            %   Splitting2_DRE solver instance.
            %
            %   See also Splitting2_DRE.
            %
            %   Author: Maximilian Behr
            if obj.options.verbose >= Verbose_t.MEDIUM
                obj.print_status();
                fprintf('%s, %s, IRK = %s, QuadRule = %s\n', class(obj), char(obj.options.splitting), char(obj.options.irkmethod), char(obj.options.quadrule));
            end
            if obj.options.verbose >= Verbose_t.HIGH
                fprintf('size(Lk)   = %d-x-%d\n', size(obj.Lk));
                fprintf('size(IQZH) = %d-x-%d\n', size(obj.IQZH));
            end
        end

        function Dnew = apply_TG(obj, h, L, D)
            %APPLY_TG  Apply the operator TG(h).
            %
            %   DNEW = SPLIT.APPLY_TG(H, L, D) applies the operator
            %   TG(H) to the real low-rank factorization L D L'.
            %   The new low-rank factorization is L DNEW L'.
            %   computes low-rank approximations for the integral term
            %
            %   See also SPLITTING2_DRE/APPLY_TF.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));

            %% apply operator TG(h)
            LTB = L' * obj.B;
            Dnew = (eye(size(D)) + h * D * (LTB * LTB')) \ D;
            Dnew = 1 / 2 * (Dnew + Dnew');
        end

        function [Lnew, Dnew] = apply_TF(obj, h, exphL, D)
            %APPLY_TF  Apply the operator TF(h).
            %
            %   [LNEW, DNEW] = SPLIT.APPLY_TF(H, EXPHL, D) applies the
            %   operator TF(H) to the real low-rank factorization L D L'.
            %   EXPHL must be an approximation of expm(H*A')L.
            %   The new low-rank factorization is LNEW DNEW LNEW'.
            %
            %   See also SPLITTING2_DRE/APPLY_TG.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));

            %% apply operator TF(H)
            Lnew = [exphL, obj.IQZH(h)];
            iqzh = size(obj.IQZH(h), 2);
            Dnew = blkdiag(D, eye(iqzh, iqzh));
            [Lnew, Dnew] = obj.truncate_LDLT(Lnew, Dnew);
        end

        function [Lnew, Dnew] = apply_TF_TG(obj, k, h, L, exphL, D)
            %APPLY_TF_TG  Apply the operator TF(h)TG(h) k-times.
            %
            %   [LNEW, DNEW] = SPLIT.APPLY_TF_TG(K, H, EXPHL, D) applies the
            %   operator TF(H)TG(H) k-times to the real low-rank
            %   factorization L D L'.
            %   EXPHL must be an approximation of expm(H*A')L.
            %   The new low-rank factorization is LNEW DNEW LNEW'.
            %
            %   See also SPLITTING2_DRE/APPLY_TG_TF.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(k, {'double'}, {'real', 'integer', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(3));

            %% apply operator TF(h)TG(h)
            Dnew = obj.apply_TG(h, L, D);
            [Lnew, Dnew] = obj.apply_TF(h, exphL, Dnew);

            %% apply operator (TF(h)TG(h))^(k-1)
            for i = 1:(k - 1)
                Dnew = obj.apply_TG(h, Lnew, Dnew);
                exphLnew = obj.irk.step_cache(h, Lnew);
                [Lnew, Dnew] = obj.apply_TF(h, exphLnew, Dnew);
            end
        end

        function [Lnew, Dnew] = apply_TG_TF(obj, k, h, exphL, D)
            %APPLY_TG_TF  Apply the operator TG(h)TF(h) k-times.
            %
            %   [LNEW, DNEW] = SPLIT.APPLY_TG_TF(K, H, EXPHL, D) applies the
            %   operator TG(H)TF(H) k-times to the real low-rank
            %   factorization L D L'.
            %   EXPHL must be an approximation of expm(H*A')L.
            %   The new low-rank factorization is LNEW DNEW LNEW'.
            %
            %   See also SPLITTING2_DRE/APPLY_TF_TG.
            %
            %   Author: Maximilian Behr

            %% check input args
            validateattributes(k, {'double'}, {'real', 'integer', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(3));

            %% apply operator TG(h)TF(h)
            [Lnew, Dnew] = obj.apply_TF(h, exphL, D);
            Dnew = obj.apply_TG(h, Lnew, Dnew);

            %% apply operator (TG(h)TF(h))^(k-1)
            for i = 1:(k - 1)
                exphLnew = obj.irk.step_cache(h, Lnew);
                [Lnew, Dnew] = obj.apply_TF(h, exphLnew, Dnew);
                Dnew = obj.apply_TG(h, Lnew, Dnew);
            end
        end


        function approximate_iqh(obj, h, divs, Z0)
            %APPROXIMATE_IQH  Low-rank approximation of the integral term
            %   by means of quadrature rule and implicit Runge-Kutta method.
            %
            %   SPLIT.APPROXIMATE_IQH(H, DIVS, Z0)
            %   computes low-rank approximations for the integral term
            %
            %       \int_0^{H/1} e^{sA'}Z0 Z0' e^{sA} ds
            %       \int_0^{H/2} e^{sA'}Z0 Z0' e^{sA} ds
            %                   ...
            %       \int_0^{H/DIVS} e^{sA'}Z0 Z0' e^{sA} ds
            %
            %   DIVS is a positive double scalar integer.
            %   H is a positive double scalar.
            %   Z0 is a dense double low-rank matrix.
            %   On return IQZH is a map with double keys
            %
            %       H/1, H/2, ... H/DIVS.
            %
            %   and the corresponding values are the low-rank approximations
            %   of the integral term. This means
            %
            %   IQZH(H/1) is low-rank approximation of \int_0^{H/1} e^{sA'}Z0 Z0' e^{sA} ds,
            %   IQZH(H/2) is low-rank approximation of \int_0^{H/2} e^{sA'}Z0 Z0' e^{sA} ds,
            %                       ...
            %   IQZH(H/DIVS) is low-rank approximation of \int_0^{H/DIVS} e^{sA'}Z0 Z0' e^{sA} ds.
            %
            %   IQZH is the property of this class.
            %   On entry it is assumed that IQZH is a map with keys double and values any.
            %
            %   See also SPLITTING2_DRE and IRKstep.
            %
            %   Author: Maximilian Behr

            %% check some input arguments
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));
            n = size(obj.A, 1);
            validateattributes(divs, {'double'}, {'real', 'scalar', '>=', 1, 'integer', 'finite', 'nonnan'}, mfilename, inputname(3));
            validateattributes(Z0, {'double'}, {'real', '2d', 'finite', 'nonnan', 'size', [n, NaN]}, mfilename, inputname(4));

            %% approximate \int_0^{H/LCMDIVS} e^{sA'}Z0 Z0' e^{sA} ds using quadrature rule
            for div = 1:divs
                % get quadrature nodes and weights
                [nodes, weights] = nodes_weights(obj.options.quadrule, [0, h / div]);

                % allocate memory for low-rank factor
                z0 = size(Z0, 2);
                IQZH_div = zeros(n, z0*length(nodes));

                % apply quadrature rule
                MinvTZ0 = obj.E' \ Z0;
                if use_start_point(obj.options.quadrule)
                    IQZH_div(:, 1:z0) = sqrt(weights(1)) * MinvTZ0;
                else
                    IQZH_div(:, 1:z0) = sqrt(weights(1)) * obj.irk.step(nodes(1), MinvTZ0);
                end

                for i = 2:length(nodes)
                    dt = nodes(i) - nodes(i-1);
                    IQZH_div(:, 1+(i - 1)*z0:i*z0) = sqrt(weights(i)) / sqrt(weights(i-1)) * obj.irk.step(dt, IQZH_div(:, 1+(i - 2)*z0:(i - 1)*z0));
                end

                % truncate and store the results in the container
                obj.IQZH(h/div) = obj.truncate_ZZT(IQZH_div);
            end
        end

        function [Znew, colsnew, colsold] = truncate_ZZT(obj, Zold)
            %TRUNCATE_ZZT    Truncate a real low-rank factor Z using a compact svd.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(Zold, {'double'}, {'real', 'nonnan', 'finite', '2d'}, mfilename, inputname(2));

            %% truncate
            colsold = size(Zold, 2);
            [Znew, Snew, ~] = svd(Zold, 0);
            tol = obj.options.trunc_tol * max(abs(diag(Snew)));
            idx = diag(Snew) >= tol;
            Snew = abs(Snew(idx, idx));
            Znew = Znew(:, idx) * Snew;
            colsnew = size(Znew, 2);
        end

        function [Lnew, Dnew, colsnew, colsold] = truncate_LDLT(obj, Lold, Dold)
            %TRUNCATE_LDLT     Truncate a real low-rank LDLT representation.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(Lold, {'double'}, {'real', 'nonnan', 'finite', '2d'}, mfilename, inputname(2));
            colsold = size(Lold, 2);
            validateattributes(Dold, {'double'}, {'real', 'nonnan', 'finite', '2d', 'square', 'size', [colsold, colsold]}, mfilename, inputname(3));

            %% truncate
            % compute the QR decomposition
            if obj.options.qrmethod == QRmethod_t.QR
                [Lnew, R] = qr(Lold, 0);
            elseif obj.options.qrmethod == QRmethod_t.QRF
                [Qi, Tau, R] = DGEQRF(Lold);
            elseif obj.options.qrmethod == QRmethod_t.QRT
                [Qi, T, R] = DGEQRT(Lold);
            else
                error('QRmethod %s not implemented.', obj.options.qrmethod);
            end

            % truncation process
            RDoldRT = R * Dold * R';
            RDoldRT = 1 / 2 * (RDoldRT + RDoldRT');
            [V, Dnew] = eig(RDoldRT, 'vector');
            tol = obj.options.trunc_tol * sqrt(max(abs(Dnew)));
            idx = abs(Dnew) >= tol;
            V = V(:, idx);
            Dnew = diag(Dnew(idx));

            % compute the new low-rank factor L
            if obj.options.qrmethod == QRmethod_t.QR
                Lnew = Lnew * V;
            elseif obj.options.qrmethod == QRmethod_t.QRF
                Lnew = DORMQR(Qi, Tau, V);
            elseif obj.options.qrmethod == QRmethod_t.QRT
                Lnew = DGEMQRT(Qi, T, V);
            else
                error('QRmethod %s not implemented.', obj.options.qrmethod);
            end

            % set the remaing information about the truncation process
            colsnew = size(Lnew, 1);
        end
    end
end
