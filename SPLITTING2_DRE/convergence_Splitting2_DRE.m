%CONVERGENCE_SPLITTING2_DRE
%   Script tries to experimentally find the order of convergence of Splitting2_DRE solvers.
%
%
%   See also SPLITTING2_DRE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% problem data
instance = 'rail_371';
data = load(instance);
A = data.A;
B = data.B;
C = data.C;
E = data.E;
Z0 = ones(size(A, 1), 1);
T = 2^2;
h_ref = 2^(-5);
hs = 2.^(-1:-1:-5);
print_mod = 10; % interval for printing

%% check convergence for different splitting scheme solvers
if isCIrun()
    split_solvers = { ...
        struct('split', Splitting2_t.SPLITTING_LIE, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '*'), ...
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '+'), ...
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '>'), ...
        };
else
    split_solvers = { ...
        struct('split', Splitting2_t.SPLITTING_LIE, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '*'), ...
        struct('split', Splitting2_t.SPLITTING_LIE, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 'o'), ...
        struct('split', Splitting2_t.SPLITTING_LIE, 'irk', IRKmethod_GAUSS_LEGENDRE(3), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 'h'), ...
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '+'), ...
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_RADAUIIA(2), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 'x'), ...
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 's'), ...
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_RADAUIIA(3), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 'd'), ...
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_GAUSS_LEGENDRE(3), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '^'), ...
        struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_RADAUIIA(4), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 'v'), ...
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '>'), ...
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_RADAUIIA(2), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '<'), ...
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 'p'), ...
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_RADAUIIA(3), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 'h'), ...
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_GAUSS_LEGENDRE(3), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', '*'), ...
        struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_RADAUIIA(4), 'quad', QuadRule_GAUSS_LEGENDRE(10), 'marker', 'o'), ...
        };
end


%% create fine solver for reference solution
split_ref = Splitting2_t.SPLITTING_SYMMETRIC8;
irk_ref = IRKmethod_RADAUIIA(5);
quad_ref = QuadRule_GAUSS_LEGENDRE(30);
split_ref_opt = Splitting2_DRE_options(split_ref, irk_ref, quad_ref);
split_ref_sol = Splitting2_DRE(A, E, B, C, Z0, split_ref_opt, h_ref, T);
split_ref_sol.prepare();

%% solve with fine solver to get reference solution at final time T
fprintf('%s: Create reference solution with fine splitting scheme solver.\n', datestr(now));
print_mod_counter = 0;
while split_ref_sol.t < split_ref_sol.T
    % step in time
    split_ref_sol.time_step();

    % check if status should be printed
    percent = split_ref_sol.t / T * 100;
    if percent >= print_mod_counter
        [L, D] = split_ref_sol.get_solution();
        print_status(split_ref_sol);
        print_mod_counter = print_mod_counter + print_mod;
    end
end
[Lref, Dref] = split_ref_sol.get_solution();
fprintf('\n');

%% iterate over different solvers and step sizes
% create cell array to log data
logcell = cell(length(split_solvers), 3);
logcell_i = 1;
for split_solver_i = 1:length(split_solvers)
    relnrm2_errs = [];
    for h = hs

        %% create splitting scheme solver
        split_solver = split_solvers{split_solver_i};
        split = split_solver.split;
        irk = split_solver.irk;
        quad = split_solver.quad;
        split_opt = Splitting2_DRE_options(split, irk, quad);
        split_sol = Splitting2_DRE(A, E, B, C, Z0, split_opt, h, T);
        split_sol.prepare();

        %% time step and stor error
        fprintf('instance = %s, T = %.8f, h = %.8f, splitting = %s, irk = %s, quad = %s\n', instance, T, h, split, irk, quad);
        split_sol.time_step_until(split_sol.T);

        %% compute error at final time
        [L, D] = split_sol.get_solution();
        absnrm2_err = two_norm_sym_handle(@(x) Lref*(Dref * (Lref' * x))-L*(D * (L' * x)), size(L, 1));
        nrm2_ref = two_norm_sym_handle(@(x) Lref*(Dref * (Lref' * x)), size(L, 1));
        relnrm2_err = absnrm2_err / nrm2_ref;

        %% log relnrm2 errors at final time
        relnrm2_errs = [relnrm2_errs, relnrm2_err]; %#ok<*AGROW>
    end

    %% log relnrm2erros for this solvers
    % get idenitifier as string and store step size and relative 2nrm error
    split_identifier = sprintf('%s-%s-%s', split, irk, quad);
    logcell{logcell_i, 1} = split_identifier;
    logcell{logcell_i, 2} = relnrm2_errs;
    logcell{logcell_i, 3} = split_solver.marker;
    logcell_i = logcell_i + 1;
end

%% compute eoc and print
for i = 1:size(logcell, 1)
    relerrs = logcell{i, 2};
    l = length(relerrs);
    eoc_min = min(abs(log(relerrs(1:(l - 1))./relerrs(2:l))./log(hs(1:(l - 1))./hs(2:l))));
    eoc_max = max(abs(log(relerrs(1:(l - 1))./relerrs(2:l))./log(hs(1:(l - 1))./hs(2:l))));
    eoc_mean = mean(abs(log(relerrs(1:(l - 1))./relerrs(2:l))./log(hs(1:(l - 1))./hs(2:l))));
    mean_relerr = mean(relerrs);
    fprintf('Solver = %-60s EOC min/max/mean = %.2e / %.2e / %.2e, mean rel. 2-norm error = %.2e\n', logcell{i, 1}, eoc_min, eoc_max, eoc_mean, mean_relerr);
end

%% plot results and save figure
% on CI we do not print
if ~isCIrun()
    fig = figure();
    set(fig, 'position', [0, 0, 2048, 1024]);
    for i = 1:size(logcell, 1)
        loglog(hs, logcell{i, 2}, ['--', logcell{i, 3}])
        hold on
        grid on
    end
    legend(logcell(:, 1), 'location', 'northeastoutside', 'Interpreter', 'none')

    order_hs = hs;
    loglog(order_hs, order_hs, '--k', 'DisplayName', 'h')
    loglog(order_hs, order_hs.^2, '--k', 'DisplayName', 'h^2')
    loglog(order_hs, order_hs.^3, '--k', 'DisplayName', 'h^3')
    loglog(order_hs, order_hs.^4, '--k', 'DisplayName', 'h^4')
    loglog(order_hs, order_hs.^5, '--k', 'DisplayName', 'h^5')
    set(gcf, 'position', [0, 0, 1024, 1024])
    title(sprintf('Order of Solvers for %s', instance), 'Interpreter', 'none')
    hold off

    mybasename = sprintf('%s_%s', mfilename, instance);
    [mydir, ~, ~] = fileparts(mfilename('fullpath'));
    mypic = fullfile(mydir, mybasename);
    saveas(gcf, mypic, 'epsc');
    saveas(gcf, mypic, 'jpg');
end
