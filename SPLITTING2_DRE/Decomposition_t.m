%DECOMPOSITION_T   Enumeration for the decomposition objects for IRKstep.
%
%   Currently supported decomposition objects are
%       - DECOMPOSITION_MATLAB          builtin MATLAB decomposition object
%       - DECOMPOSITION_BANDED          decomposition_banded object
%       - DECOMPOSITION_SPARSE_DIRECT   decomposition_sparse_direct object
%
%   See also DECOMPOSITION, DECOMPOSITION_BANDED, DECOMPOSITION_SPARSE_DIRECT,
%   IRKSTEP and SPLITTING2_DRE_OPTIONS.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

classdef (Sealed = true) Decomposition_t< int8
    enumeration
        % builtin MATLAB decomposition object
        DECOMPOSITION_MATLAB(0),
        % decomposition_banded object
        DECOMPOSITION_BANDED(1),
        % decompoistion_sparse_direct object
        DECOMPOSITION_SPARSE_DIRECT(2),
    end
end
