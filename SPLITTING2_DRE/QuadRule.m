classdef (Abstract) QuadRule
    %QUADRULE     Class for quadrature rules.
    %
    %   Gauss-Legendre quadrature rules are available with s nodes, i.e.
    %   the s roots of the Legendre Polynomial of degree s+1.
    %   The order of a Gauss-Legendre quadrature rule is p=2*s+1.
    %   See [1] Ch. 5.3.3. Formula 5.3.17.
    %   Closed Newton-Cotes formulas are also implemented.
    %   A quadrature rule has order p if the error fullfils
    %
    %          b
    %          /
    %       || | f(x) dx - quadrature rule(f) || < C * (b-a)^p,
    %          /
    %          a
    %
    %   for smooth enough functions. Please note that the maximal degree of a
    %   polynomial which is exact integrated by this formula is different.
    %
    %   See:
    %       https://de.wikipedia.org/wiki/Newton-Cotes-Formeln
    %       https://en.wikipedia.org/wiki/Newton%E2%80%93Cotes_formulas
    %       https://en.wikipedia.org/wiki/Gaussian_quadrature#Error_estimates
    %       [1] DAHLQUIST, Germund; BJORCK, Ake. Numerical Methods in Scientific Computing: Volume 1. Siam, 2008.
    %
    %   QUADRULE methods:
    %       char            - Character array representation of the quadrature rule.
    %       string          - String array representation of the quadrature rule.
    %       order           - Return the global order of the method.
    %       use_start_point - Return true if quadrature rule has the start point of interval as node.
    %       use_end_point   - Return true if quadrature rule has the end point of interval as node.
    %       is_equidistant  - Return true if quadrature rule is equidistant.
    %       nodes_weights   - Return the nodes and weights for a given interval.
    %
    %   See also QUADRULE_GAUSS_LEGENDRE and QUADRULE_CLENSHAW_CURTIS.
    %
    %   Author: Maximilian Behr

    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    methods(Abstract)
        %CHAR  Character array representation of the quadrature rule.
        %   Return a character array representation of the
        %   quadrature rule.
        %
        %   C = CHAR(QUAD) returns a character array representation of
        %   the quadrature rule QUAD.
        %
        %   Author: Maximilian Behr
        c = char(obj)

        %STRING  String array representation of the quadrature rule.
        %   Return a string array representation of the
        %   quadrature rule.
        %
        %   C = STRING(QUAD) returns a string array representation of
        %   the quadrature rule QUAD.
        %
        %   Author: Maximilian Behr
        c = string(obj)

        %ORDER  Order of the quadrature rule.
        %   The order is the number P such that the error of the
        %   quadrature rule applied to the integral from a to b
        %   is smaller equals C*(B-A)^P, where C is a constant
        %   indepent of A and B.
        %
        %   P = QUAD.ORDER() returns the order of the
        %   quadrature rule QUAD.
        %
        %   Author: Maximilian Behr
        p = order(obj)

        %USE_START_POINT  True if quadrature rule uses start point of interval.
        %   Q = QUAD.START_POINT() returns true if the quadrature
        %   rule QUAD uses start point of interval as node, otherwise false.
        %
        %   See also QuadRule_t.use_end_point.
        %
        %   Author: Maximilian Behr
        q = use_start_point(obj)

        %USE_END_POINT  True if quadrature rule uses end point of interval.
        %   Q = QUAD.USE_END_POINT() returns true if the quadrature
        %   rule QUAD uses end point of interval as node, otherwise false.
        %
        %   See also QuadRule_t.use_start_point.
        %
        %   Author: Maximilian Behr
        q = use_end_point(obj)

        %IS_EQUIDISTANT  True if quadrature rule uses equidistant nodes.
        %   IS_EQUIDISTANT returns true if the quadrature rule
        %   uses equidistant nodes only. Otherwise false is returned.
        %
        %   EQUIDISTANT = QUAD.IS_EQUIDISTANT() returns true if
        %   the quadrature rule QUAD uses equidistant nodes only
        %   otherwise false.
        %
        %   Author: Maximilian Behr
        equidistant = is_equidistant(obj)

        %NODES_WEIGHTS  Nodes and the weights of the quadrature rule.
        %   [NODES, WEIGHTS] = quad.NODES_WEIGHTS() returns
        %   the nodes NODES and the WEIGHTS W of the quadrature rule
        %   for the interval [0,1].
        %
        %   [NODES, WEIGHTS] = quad.NODES_WEIGHTS(INTERVAL) returns
        %   the nodes NODES and the weights WEIGHTS of the
        %   quadrature rule for the interval INTERVAL.
        %
        %   Author: Maximilian Behr
        [nodes, weights] = nodes_weights(obj, interval)
    end
end
