%QRMETHOD_T   Enumeration for the QR method used in Splitting2_DRE.
%
%   Currently supported QR methods are
%       - QR    builtin MATLAB qr function
%       - QRF   QRTOOLS, Householder QR
%       - QRT   QRTOOLS, Blocked Householder QR
%
%   See also QR, DGEQRF, DORMQR, DGEQRT, DGEMQRT and SPLITTING2_DRE_OPTIONS.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

classdef (Sealed = true) QRmethod_t < int8
    enumeration
        % builtin MATLAB qr function
        QR(0),
        % QRTOOLS, Householder QR: DGEQRF, DORMGQR, DORGQR
        QRF(1),
        % QRTOOLS, Blocked Householder QR: DGEQRT, DGEMQRT
        QRT(2),
    end
end
