classdef test_QuadRule < matlab.unittest.TestCase
    %TEST_QUADRULE   Test for QuadRule class.
    %
    %   TEST_QuadRule solves a quadrature problem using different
    %   quadrature rules.
    %
    %   See also QUADRULE, QUADRULE_GAUSS_LEGENDRE and QUADRULE_CLENSHAW_CURTIS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        problem = { ... ,
            struct('func', @(x) sin(x), 'inter', [0, pi], 'solution', 2, 'abs_tol', 1e-10, 'rel_tol', 1e-10, 'max_nodes', 100) ...
            struct('func', @(x) cos(sin(x)), 'inter', [0, 2 * pi], 'solution', 4.80787886126882599654, 'abs_tol', 1e-14, 'rel_tol', 1e-14, 'max_nodes', 100) ...
            struct('func', @(x) exp(-sqrt(x)).*cos(sin(x)), 'inter', [1, 8 * pi], 'solution', 1.02612916255451133051, 'abs_tol', 1e-10, 'rel_tol', 1e-10, 'max_nodes', 100) ...
            };
        quad_rule = {@(s) QuadRule_GAUSS_LEGENDRE(s), @(s) QuadRule_CLENSHAW_CURTIS(s)};
    end

    properties
        verbose = ~isCIrun();
    end

    methods(Test)
        function test(obj, problem, quad_rule) %#ok<*INUSL>

            %% load problem
            if obj.verbose
                fprintf('\n');
            end
            func = problem.func;
            solution = problem.solution;
            inter = problem.inter;
            abs_tol = problem.abs_tol;
            rel_tol = problem.rel_tol;
            max_nodes = problem.max_nodes;

            if obj.verbose
                f(1) = fprintf('quad_rule = %s, func = %s, a = %.2e, b = %.2e\n', func2str(quad_rule), func2str(func), inter(1), inter(2));
            end

            %% apply quadrature rule for different number of nodes
            tol_reached = false;
            for nodes = 2:max_nodes
                % compute integral by quadrature rule
                q_rule = quad_rule(nodes);
                [q_nodes, q_weights] = q_rule.nodes_weights(inter);
                int_func = sum(q_weights.*func(q_nodes));
                abs_err = abs(int_func-solution);
                rel_err = abs_err / abs(solution);

                % print abs. and rel error
                if obj.verbose
                    f(2) = fprintf('nodes = %2d, abs./rel. error = %.2e/%.2e, abs./rel. tol = %.2e/%.2e\n', nodes, abs_err, rel_err, abs_tol, rel_tol);
                end

                % break if first time tolerance is reached
                if abs_err < abs_tol && rel_err < rel_tol
                    if obj.verbose
                        fprintf('abs./rel. tolerances reached\n');
                    end
                    tol_reached = true;
                    break;
                end
            end

            %% test if tol was reached
            obj.fatalAssertTrue(tol_reached);
            if obj.verbose
                fprintf('%s\n', repmat('-', 1, max(f) - 1));
            end
        end
    end
end
