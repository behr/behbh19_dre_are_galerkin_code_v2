classdef test_Splitting2_DRE < matlab.unittest.TestCase
    %TEST_SPLITTING_DRE2   Test for Splitting2_DRE solver.
    %
    %   TEST_SPLITTING2_DRE compares the Splitting2_DRE solver with
    %   MDM_FAC_SYM_DRE solver. The rail example and several
    %   examples from the slicot benchmark collection are used.
    %
    %   See also SPLITTING2_DRE and MDM_FAC_SYM_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        problem = { ...
            struct('instance', 'rail_371', 'T', 2^-1, 'h', 2^-4, 'rel_tol', 1e-3, 'print_mod', 50) ...
            struct('instance', 'beam', 'T', 2^-2, 'h', 2^-6, 'rel_tol', 5e-1, 'print_mod', 50) ...
            };

        split_solver = { ...
            struct('split', Splitting2_t.SPLITTING_LIE, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10)) ...
            struct('split', Splitting2_t.SPLITTING_STRANG, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10)) ...
            struct('split', Splitting2_t.SPLITTING_SYMMETRIC2, 'irk', IRKmethod_GAUSS_LEGENDRE(1), 'quad', QuadRule_GAUSS_LEGENDRE(10)) ...
            struct('split', Splitting2_t.SPLITTING_SYMMETRIC4, 'irk', IRKmethod_GAUSS_LEGENDRE(2), 'quad', QuadRule_GAUSS_LEGENDRE(10)) ...
            struct('split', Splitting2_t.SPLITTING_SYMMETRIC6, 'irk', IRKmethod_GAUSS_LEGENDRE(3), 'quad', QuadRule_GAUSS_LEGENDRE(10)) ...
            struct('split', Splitting2_t.SPLITTING_SYMMETRIC8, 'irk', IRKmethod_GAUSS_LEGENDRE(4), 'quad', QuadRule_GAUSS_LEGENDRE(10)) ...
            struct('split', Splitting2_t.SPLITTING_SYMMETRIC10, 'irk', IRKmethod_GAUSS_LEGENDRE(5), 'quad', QuadRule_GAUSS_LEGENDRE(10)) ...
            };

        spsd_corr = {false, true};
        qr_method =  {QRmethod_t.QR, QRmethod_t.QRF, QRmethod_t.QRT};
        decomposition_type = {Decomposition_t.DECOMPOSITION_MATLAB, Decomposition_t.DECOMPOSITION_BANDED, Decomposition_t.DECOMPOSITION_SPARSE_DIRECT};
        dense = {false, true};

    end

    properties
        verbose = ~isCIrun();
    end

    methods(Test)

        function test(obj, problem, split_solver, spsd_corr, qr_method, decomposition_type, dense) %#ok<*INUSL>

            %% load data
            if obj.verbose
                fprintf('\n');
            end
            instance = problem.instance;
            T = problem.T;
            h = problem.h;
            rel_tol = problem.rel_tol;
            data = load(instance);
            A = data.A;
            B = full(data.B);
            C = full(data.C);
            n = size(A, 1);
            Z0 = ones(n, 1);

            if isfield(data, 'E')
                E = data.E;
            else
                E = speye(n, n);
            end

            if dense
               A = full(A);
               E = full(E);
            else
               A = sparse(A);
               E = sparse(E);
            end


            %% create splitting_solver
            split = split_solver.split;
            irk = split_solver.irk;
            quad = split_solver.quad;
            trunc_tol = sqrt(eps);
            split_opt = Splitting2_DRE_options(split, irk, quad, trunc_tol, spsd_corr, qr_method, decomposition_type);
            split_sol = Splitting2_DRE(A, E, B, C, Z0, split_opt, h, T);
            split_sol.prepare();

            %% create MDM_FAC_SYM_DRE
            mdm_opt = MDM_DRE_options(true);
            mdm_sol = MDM_FAC_SYM_DRE(full(A), full(E), full(B), full(C), full(Z0), mdm_opt, h, T);
            mdm_sol.prepare();

            %% solve and compare solution
            if obj.verbose
                f(1) = fprintf('instance = %s, h = %.2e, T = %.2e\n', instance, h, T);
                f(2) = fprintf('splitting = %s, irk = %s, quad = %s, spsd_corr = %d\n', split, irk, quad, spsd_corr);
                f(3) = fprintf('qr_method = %s, decomposition = %s, dense = %s\n', qr_method, decomposition_type, string(dense));
            end
            print_mod = problem.print_mod;
            print_mod_counter = 0;
            while mdm_sol.t < T

                % make time step with splitting
                split_sol.time_step();

                % make time step with mdm
                mdm_sol.time_step();

                % get solution
                [L, D] = split_sol.get_solution();
                Xt = mdm_sol.get_solution();

                % compare
                nrm2X_mdm = two_norm_sym_handle(@(x) Xt*x, n);
                nrm2X_split = two_norm_sym_handle(@(x) (L * (D * (L' * x))), n);
                abs_nrm2err = two_norm_sym_handle(@(x) Xt*x-(L * (D * (L' * x))), n);
                rel_nrm2err = abs_nrm2err / nrm2X_mdm;

                % print intermediate results
                percent = mdm_sol.t / T * 100;
                if percent >= print_mod_counter && obj.verbose
                    f(4) = fprintf('t = %.4e, %6.2f%%, size(L) = %3d-x-%3d, abs./rel. 2-norm error = %.2e/%.2e, 2-norm MDM/Split = %.2e/%.2e\n', ...
                        mdm_sol.t, mdm_sol.t/T*100, size(L), abs_nrm2err, rel_nrm2err, nrm2X_mdm, nrm2X_split);
                    print_mod_counter = print_mod_counter + print_mod;
                end

                % check tolerance for relative error
                obj.fatalAssertLessThan(rel_nrm2err, rel_tol);

            end
            if obj.verbose
                length_print = length(char(split)) + length(char(irk)) + length(char(quad));
                f(5) = fprintf('Time %s-%s-%s = %.2e, avg. time per step = %.2e\n', ...
                    char(split), char(irk), char(quad), split_sol.wtime_time_step, split_sol.wtime_time_step/T*h);
                f(6) = fprintf('Time %-*s = %.2e, avg. time per step = %.2e\n', ...
                    length_print+2, upper(class(mdm_sol)), mdm_sol.wtime_time_step, mdm_sol.wtime_time_step/T*h);
                fprintf('%s\n', repmat('-', 1, max(f) - 1));
            end
        end
    end
end
