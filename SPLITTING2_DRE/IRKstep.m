classdef IRKstep < custom_handle
    %IRKstep class performes one step with an implicit Runge-Kutta method.
    %
    %   IRKstep approximation e^(hE\A)x0 which is the solution
    %   of E\dot{x}(t) = A x(t), x(0)=x0 at t = h by doing one step of an
    %   implicit Runge-Kutta method.
    %   The following methods are available:
    %
    %       * GAUSS_LEGENDRE(S),  for 1 <= S <= 15
    %       * RADAU_IIA_2(S), for 2<= S <= 15
    %
    %   Please note that GAUSS_LEGENDRE_S has S stages and the maximal
    %   order 2*S. The method RADAU_IIA_S has S stages and the order is 2*S-1.
    %   IRKstep tries to reuse existing sparse LU decompositions. It is possible
    %   to add step sizes to an instance of IRKstep. IRKstep will then compute
    %   the necessary sparse LU decompositions and cache them.
    %   The cached sparse LU decomposition will be reused to gain more performance.
    %   You can add step sizes to the cache using the method IRKstep/add_step_size_cache.
    %   The Decomposition_t.DECOMPOSITION_BANDED type usually needs much more memory
    %   then the Decomposition-t.DECOMPOSITION_MATLAB type.
    %
    %   IRKstep properties:
    %       A                       - set private, real, n-x-n sparse or dense matrix A
    %       E                       - set private, real, n-x-n sparse or dense matrix E or empty for identity
    %       n                       - set private, double, dimension of the system
    %       irkmethod               - set private, IRKmethod, desired implicit Runge-Kutta method
    %       decomposition_type      - set private, Decomposition_t, desired decomposition type
    %       order                   - set private, double, order of the implicit Runge-Kutta method
    %       stages                  - set private, double, number of stages of the implicit Runge-Kutta method
    %       symmetric               - set private, logical, true if the implicit Runge-Kutta method is symmetric
    %       fsal                    - set private, logical, first same as last, true if implicit Runge-Kutta has this property.
    %       irkcache_decompositions - set private, cache for sparse LU decompositions
    %       irk_tableau_V           - set private, spectral decomp of IRK Tableau matrix A=V*D*invV
    %       irk_tableau_invD        - set private, spectral decomp of IRK Tableau matrix A=V*D*invV
    %       irk_tableau_invV        - set private, spectral decomp of IRK Tableau matrix A=V*D*invV
    %       irk_tableau_d           - set private, IRK Tableau data for d' = b'*inv(A)
    %
    %   The matrices A and E must be either both dense or sparse. The mixed case is not supported.
    %
    %   IRKstep methods:
    %       IRKstep                     - public, Constructor.
    %       step                        - public, Perform one step, if available a cached sparse LU decomposition is used.
    %       step_cache                  - public, Perform one step using a cached sparse LU decomposition.
    %       get_step_size_cache         - public, Return step sizes, for which a cached sparse LU decomposition exists.
    %       step_size_in_cache          - public, Return true if the step size is in the cache other false.
    %       add_step_size_cache         - public, Add a step size and its corresponding sparse LU decomposition to the cache.
    %       del_step_size_cache         - public, Remove a step size from cache and its corresponding sparse LU decomposition.
    %       compute_decompositions      - private, Compute the necessary sparse LU decompositions to perform one step with step size h.
    %       step_with_decompositions    - private, Perform one step using given sparse LU decompositions.
    %
    %   See also IRKMETHOD, IRKMETHOD_GAUSS_LEGENDRE and IRKMETHOD_RADAUIIA.
    %
    %   Implementation Details:
    %
    %   The implicit Runge Kutta method with s stages and step size h applied to
    %
    %       \dot{x}(t) = f(t,x(t)), x(t_0)=x_0
    %
    %   is given by the system
    %
    %       g_i = x_0 + h \sum_{j=1}^{s} a_{i,j} f(t_0 + c_j h, g_j)
    %       x_1 = x_0 + h \sum_{j=1}^{s} b_j     f(t_0 + c_j h, g_j).
    %
    %   The parameters are given by the Butcher tableau:
    %
    %       c | a
    %       --+-----
    %         | b^T
    %
    %   We follow Hairer; Wanner, Solving Ordinary Differential Equations II,
    %   Ch. IV.8, Implementation of Implicit Runge-Kutta Methods.
    %   By substituting z_i = g_i - x_0 we obtain
    %
    %       z_i =       h \sum_{j=1}^{s} a_{i,j} f(t_0 + c_j h, z_j + x0)
    %       x_1 = x_0 + h \sum_{j=1}^{s} b_j     f(t_0 + c_j h, g_j)
    %
    %   We define d^T = b^T a^{-1} and obtain
    %
    %       z_i =       h \sum_{j=1}^{s} a_{i,j} f(t_0 + c_j h, z_j + x0)
    %       x_1 = x_0 +   \sum_{j=1}^{s} d_j z_j
    %
    %   It remains to solve for z_i. In our case we have
    %
    %       f(t,x) = E^{-1} A^{-1} x
    %
    %   and the system for z_i is given by
    %
    %                     | h E^{-1} A x_0 |                                    | z_1 |
    %                     |        .       |                                    |  .  |
    %       (a \kron I_n) |        .       | = ( I_{sn} - h (a \kron E^{-1} A)) |  .  |
    %                     |        .       |                                    |  .  |
    %                     | h E^{-1} A x_0 |                                    | z_s |
    %
    %   We multiply the system with I_s \kron E and a^{-1} \kron I_n from the right and obtain
    %
    %       | h A x_0 |                                       | z_1 |
    %       |    .    |                                       |  .  |
    %       |    .    | = ( a^{-1} \kron E - h (I_s \kron A)) |  .  |
    %       |    .    |                                       |  .  |
    %       | h A x_0 |                                       | z_s |
    %
    %   Since A and E are sparse and real the sytem matrix has a complicated
    %   sparsity pattern and usually sparse direct methods perform bad.
    %   We assume that the matrix a from the butcher can be diagonalized
    %   as a = V D V^{-1}. After some transformations we obtain
    %
    %                          | h A x_0 |
    %                          |    .    |
    %       (V^{-1} \kron I_n) |    .    | =
    %                          |    .    |
    %                          | h A x_0 |
    %
    %                                                             | z_1 |
    %                                                             |  .  |
    %       ( D^{-1} \kron E - h (I_s \kron A))(V^{-1} \kron I_n) |  .  |
    %                                                             |  .  |
    %                                                             | z_s |
    %
    %   The system matrix is now block diagonal and by taking awareness
    %   on the occurence of real and pairs of complex conjugated eigenvalues
    %   as well as eigenvectors the system can be solved more efficiently.
    %
    %   The disadvantage is that the matrix V gets more and more ill-conditioned.
    %   Anyway up to roughly 8 stages the condition number is below 1e+4.
    %
    %   The requiered lu decompositions are cached and time stepping can be done
    %   by reusing the step function.
    %
    %   The needed Tableau data was generated using the irk_tableau.m script
    %   and the ButcherTableau class in the HELPERS directory.
    %
    %   Exemplary we explain the implementation for the step functions
    %   in case of GAUSS_LEGENDRE_4:
    %
    %   For performing one step with GAUSS_LEGENDRE_4, we have to solve
    %   the system
    %
    %                     | h*A*x0 |   | (invD(1)*E - h*A) tz1 |
    %   (V^{-1}\kron I_n) | h*A*x0 | = | (invD(2)*E - h*A) tz2 |.
    %                     | h*A*x0 |   | (invD(3)*E - h*A) tz3 |
    %                     | h*A*x0 |   | (invD(3)*E - h*A) tz4 |
    %
    %   As h*A*x0 is real and due to the special structure of invV
    %   the right hand side is given by
    %
    %                     | h*A*x0 |   | rhs1       |
    %   (V^{-1}\kron I_n) | h*A*x0 | = | conj(rhh1) |.
    %                     | h*A*x0 |   | rhs2       |
    %                     | h*A*x0 |   | conj(rh2)  |
    %
    %   where
    %
    %   rhs1 = sum(invV(1,:))*h*A*x0,
    %   rhs2 = sum(invV(3,:))*h*A*x0
    %
    %   We obtain the linear equation systems
    %
    %   (invD(1)*E - h*A) tz1 = rhs1,
    %   (invD(2)*E - h*A) tz2 = conj(rhs1),
    %   (invD(3)*E - h*A) tz3 = rhs2,
    %   (invD(4)*E - h*A) tz3 = conj(rhs2).
    %
    %   As invD(1) / invD(2) and invD(3) / invD(4) are complex conjuageted
    %   and h, A, E are real we have
    %
    %   tz1 = conj(tz2)
    %   tz3 = conj(tz4)
    %
    %   Now we can do the back transformation to obtain z_1 and z_2
    %
    %   | z1 |                 | tz1 |
    %   | z2 | = (V \kron I_n) | tz2 |
    %   | z3 |                 | tz3 |
    %
    %   and perform the time stepping
    %
    %   x_1 = x_0 + d(1)*z_1 + d(2)*z_2 + d(3)*z_3
    %
    %   For the implementation of the Radau IIA it is exploited that
    %   the method has the FSAL property, which means
    %   that the last line of the coefficient matrix A coincides with
    %   the vector b. This means that the last entry of d is exactly 1
    %   and the other ones are 0.
    %
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % set private, double, n-x-n sparse or dense matrix A
        A
        % set private, double, n-x-n sparse or dense matrix E or empty for identity
        E
        % set private, double, dimension of the system
        n
        % set private, IRKmethod, desired implicit Runge-Kutta method
        irkmethod
        % set private, Decomposition_t, desired decomposition type
        decomposition_type
        % set private, real, order of the implicit Runge-Kutta method
        order
        % set private, real, number of stages of the implicit Runge-Kutta method
        stages
        % set private, logical, true if the implicit Runge-Kutta method is symmetric
        symmetric
        % set private, logical, first same as last, true if implicit Runge-Kutta has this property
        fsal
        % set private, cache for sparse LU decompositions
        irkcache_decompositions
        % set private, spectral decomp of IRK Tableau matrix A=V*D*invV
        irk_tableau_V
        % set private, spectral decomp of IRK Tableau matrix A=V*D*invV
        irk_tableau_invD
        % set private, spectral decomp of IRK Tableau matrix A=V*D*invV
        irk_tableau_invV
        % set private, IRK Tableau data for d' = b'*inv(A)
        irk_tableau_d
        % Verbose_t instance, control the verbosity of the solver
        verbose
    end

    methods(Access = public)

        function obj = IRKstep(A, E, irkmethod, decomposition_type, verbose)
            %IRKstep  Constructor of class.
            %
            %   IRK = IRKSTEP(A, E, IRKMETHOD, DECOMPOSITION_TYPE VERBOSE)
            %   creates an instance of the class. A and E are double sparse
            %   or dense n-x-n matrices and IRKMETHOD_T must be an instance
            %   IRKmethod_t. DECOMPOSITION_TYPE must be an instance of
            %   Decomposition_t. The matrices A and E must be either both
            %   dense or sparse. The mixed case is not supported.
            %
            %   IRK = IRKSTEP(A, E, IRKMETHOD, DECOMPOSITION_TYPE)
            %   verbose mode is set to NONE.
            %
            %   IRK = IRKSTEP(A, E, IRKMETHOD) decomposition_type is set
            %   to DECOMPOSITION_MATLAB and verbose mode is set to NONE.
            %
            %   See also IRKMETHOD_T, DECOMPOSITION_T, BUTCHERTABLEAU and BUTCHERTABLEAU_PRINT.
            %
            %   Author: Maximilian Behr

            %% check parameters
            validateattributes(A, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
            issparseA = issparse(A);
            if ~isempty(E)
                validateattributes(E, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan', 'nrows', size(A, 1)}, mfilename, inputname(2));
                if issparseA
                    assert(issparse(E), 'A is sparse, therefore %s must be also sparse.', inputname(2));
                end
            end
            validateattributes(irkmethod, {'IRKmethod'}, {}, mfilename, inputname(3));

            obj.A = A;
            if isempty(E)
                if issparseA
                    obj.E = speye(size(obj.A));
                else
                    obj.E = eye(size(obj.A));
                end
            else
                obj.E = E;
            end
            obj.n = size(obj.A, 1);
            obj.irkmethod = irkmethod;
            obj.decomposition_type = Decomposition_t.DECOMPOSITION_MATLAB;
            obj.verbose = Verbose_t.NONE;

            if nargin >= 4
                validateattributes(decomposition_type, {'Decomposition_t'}, {}, mfilename, inputname(4));
                obj.decomposition_type = decomposition_type;
            end

            if nargin >= 5
                validateattributes(verbose, {'Verbose_t'}, {}, mfilename, inputname(5));
                obj.verbose = verbose;
            end

            %% set the needed tableau data to cache
            [~, ~, ~, d, V, invV, ~, invD] = get_tableau(obj.irkmethod);
            obj.irk_tableau_V = V;
            obj.irk_tableau_invV = invV;
            obj.irk_tableau_d = d;
            obj.irk_tableau_invD = invD;

            %% setup parameters
            obj.stages = stages(obj.irkmethod);
            obj.order = order(obj.irkmethod);
            obj.symmetric = is_symmetric(obj.irkmethod);
            obj.fsal = is_fsal(obj.irkmethod);

            %% setup a map for the cache of the sparse lu decomposition
            % the key is the step size, the value is a cell array of decomposition objects
            obj.irkcache_decompositions = containers.Map('KeyType', 'double', 'ValueType', 'any');

            %% print
            if obj.verbose >= Verbose_t.HIGH
                obj.print_status_verbose();
            end

        end

        function xh = step(obj, h, x0)
            %STEP  Perform one step with step size h. If available
            %   a sparse LU decomposition is used. Otherwise
            %   the sparse LU decomposition is computed from scratch.
            %
            %   XH = IRK.STEP(H, X0) perform one step with step size
            %   H. If availbale a sparse LU decomposition will be used.
            %   Otherwise the sparse LU decomposition will be computed
            %   from scratch. The newly computed sparse LU decomposition
            %   is in this case not added to the cache.
            %
            %   See also IRKSTEP/STEP_NO_CACHE, IRKSTEP/STEP_CACHE and IRKSTEP/ADD_STEP_SIZE_CACHE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(h, {'double'}, {'real', 'scalar', '>=', 0, 'finite', 'nonnan'}, mfilename, inputname(2));
            validateattributes(x0, {'double'}, {'real', 'nonsparse', 'finite', 'nonnan', 'nrows', obj.n}, mfilename, inputname(3));

            %% step size 0 immediate return
            if h == 0
                xh = x0;
                return;
            end

            %% check if suitable decomposition is available, otherwise compute from scratch
            if obj.irkcache_decompositions.isKey(h)
                decomp = obj.irkcache_decompositions(h);
            else
                decomp = obj.compute_decompositions(h);
            end

            %% perform step
            xh = obj.step_with_decompositions(h, x0, decomp);

            %% print
            obj.print_status_verbose();
        end

        function xh = step_cache(obj, h, x0)
            %STEP_CACHE  Perform one step with step size h using a cached
            %   sparse LU decomposition.
            %
            %   XH = IRK.STEP_CACHE(H, X0) perform one step with step size
            %   h by reusing the corresponding cached sparse LU decomposition.
            %   X0 must be real dense and of size n-x-p. n is the size
            %   of the matrix A. If the corresponding LU decomposition
            %   for the step size is not in the cache an error is thrown.
            %   X0
            %
            %   See also IRKSTEP/STEP_NO_CACHE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));
            validateattributes(x0, {'double'}, {'real', 'nonsparse', 'finite', 'nonnan', 'nrows', obj.n}, mfilename, inputname(3));

            %% check if suitable decomposition is available
            if ~obj.irkcache_decompositions.isKey(h)
                error('No suitable LU decomposition in the cache for step size h = %.2e\n', h);
            end

            %% perform step by reusing the sparse lu decomposition from cache
            decomp = obj.irkcache_decompositions(h);
            xh = obj.step_with_decompositions(h, x0, decomp);

            %% print
            obj.print_status_verbose();
        end

        function step_sizes = get_step_size_cache(obj)
            %GET_STEP_SIZE_CACHE  Return all step sizes for which a cached sparse
            %   LU decomposition is available.
            %
            %   STEPS = IRK.GET_STEP_SIZE_CACHE() return all step sizes
            %   for which a cached sparse LU decomposition is available.
            %
            %   See also IRKSTEP/STEP_CACHE, IRKSTEP/ADD_STEP_SIZE_CACHE and IRKSTEP/DEL_STEP_SIZE_CACHE.
            %
            %   Author: Maximilian Behr
            step_sizes = sort(cell2mat(obj.irkcache_decompositions.keys));
        end

        function q = step_size_in_cache(obj, h)
            %STEP_SIZE_IN_CACHE  Return true if the step size is in the cache
            %   other false.
            %
            %   Q = IRK.STEP_SIZE_IN_CACHE() Return true if the step size
            %   is in the cache other false.
            %
            %   See also IRKSTEP/STEP_CACHE, IRKSTEP/ADD_STEP_SIZE_CACHE and IRKSTEP/DEL_STEP_SIZE_CACHE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));
            q = obj.irkcache_decompositions.isKey(h);
        end

        function add_step_size_cache(obj, hs)
            %ADD_STEP_SIZE_CACHE  Add a step sizes to the cache.
            %
            %   IRK.ADD_STEP_SIZE_CACHE(HS) compute the LU decompositions
            %   to perform a step with step size h. The LU decompositions
            %   are also added to the cache. If the necessary LU decompositions
            %   are already available in the cache an warning is printed.
            %   HS is a vector an contains the step sizes to be added.
            %
            %   See also IRKSTEP/STEP_CACHE, IRKSTEP/GET_STEP_SIZE_CACHE and IRKSTEP/DEL_STEP_SIZE_CACHE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(hs, {'double'}, {'real', 'vector', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));

            %% compute the LU decompositions if necessary and add to cache.
            for h = hs
                if obj.irkcache_decompositions.isKey(h)
                    warning('LU decomposition for step size h = %.2e is already in the cache.\n', h);
                else
                    decomp = obj.compute_decompositions(h);
                    obj.irkcache_decompositions(h) = decomp;
                end
            end

            %% print
            obj.print_status_verbose();
        end

        function del_step_size_cache(obj, hs)
            %DEL_STEP_SIZE_CACHE  Delete a step size from the cache.
            %
            %   IRK.DEL_STEP_SIZE_CACHE(HS) deletes the cached LU decompositions
            %   corresponding to the step size given in vector HS. If the corresponding
            %   LU decompositions are not in the cached a warning is printed.
            %
            %   See also IRKSTEP/STEP_CACHE, IRKSTEP/GET_STEP_SIZE_CACHE and IRKSTEP/ADD_STEP_SIZE_CACHE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(hs, {'double'}, {'real', 'vector', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));

            %% check if corresponding LU decompositions are in the cache and delete, otherwise print a warning
            for h = hs
                if obj.irkcache_decompositions.isKey(h)
                    obj.irkcache_decompositions.remove(h);
                else
                    warning('Sparse LU decomposition for step size h = %.2e is not in cache.\n', h);
                end
            end

            %% print
            obj.print_status_verbose();
        end
    end

    methods(Access = private)

        function print_status_verbose(obj)
            %PRINT_STATUS  Status information about the IRKstep instance.
            %
            %   IRK.PRINT_STATUS() status information abouth the IRKstep
            %   instance.
            %
            %   See also IRKSTEP.
            %
            %   Author: Maximilian Behr
            if obj.verbose >= Verbose_t.MEDIUM
                fprintf('%s, %s, stages = %d, fsal = %d, n = %d, number of cached step sizes = %d\n', ...
                    class(obj), char(obj.irkmethod), obj.stages, obj.fsal, obj.n, length(obj.irkcache_decompositions.keys()));
            end
            if obj.verbose >= Verbose_t.HIGH
                fprintf('%s, cache step sizes: ', class(obj));
                fprintf('%f ', obj.get_step_size_cache());
                fprintf('\n');
                decomps = obj.irkcache_decompositions.values();
                no_decomps = 0;
                if ~isempty(decomps)
                    no_decomps = length(decomps) * length(decomps{1});
                end
                fprintf('%s, cache number of decompositions = %d\n', class(obj), no_decomps);
            end
        end

        function decomp = compute_decompositions(obj, h)
            %COMPUTE_DECOMPOSITIONS  Compute the necessary sparse
            %   LU decompositions to perform a step with step size h.
            %
            %   DECOMP = IRK.COMPUTE_DECOMPOSITIONS(H) computes
            %   the necessary LU decompositions to perform one step
            %   with step size H. The computed sparse LU decompositions
            %   are returned ass a cell array.
            %
            %   See also IRKSTEP/STEP_CACHE, IRKSTEP/STEP_NO_CACHE and IRKSTEP/ADD_STEP_SIZE_CACHE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));

            %% first perform some consistency checks for Runge-Kutta Tableau invD
            s = obj.stages;

            % number of stages must match length of D
            assert(s == length(obj.irk_tableau_invD), 'Number of stages does not fit to invD.');

            % check if complex eigenvalues values occur in conjugated pairs
            i = 1;
            while i < s
                if imag(obj.irk_tableau_invD(i)) ~= 0 && i < s
                    cpx1 = obj.irk_tableau_invD(i);
                    cpx2 = obj.irk_tableau_invD(i+1);
                    if abs(cpx1-conj(cpx2)) > 0
                        disp(cpx1)
                        disp(cpx2)
                        disp(obj.irk_tableau_invD)
                        error('Complex values must occur in conjugated pairs.');
                    end
                    i = i + 1;
                end
                i = i + 1;
            end

            % if number of stages is even only complex values must occur
            % if number of stages is odd exactly one real eigenvalue must occur at first position
            if mod(s, 2) == 0
                assert(all(imag(obj.irk_tableau_invD) ~= 0), 'For even number of stages only complex eigenvalues occur.');
            else
                assert(imag(obj.irk_tableau_invD(1)) == 0, 'First value must be real.');
                assert(all(imag(obj.irk_tableau_invD(2:end)) ~= 0), 'All values must be complex, except the first.');
            end

            % if the method has the fsal property than all entries of d
            % must be zero except the last one, which must be 1
            if obj.fsal
                for i = 1:(s - 1)
                    if obj.irk_tableau_d(i) ~= 0
                        error('The fsal property is not fullfilled.');
                    end
                end
                if obj.irk_tableau_d(end) ~= 1
                    error('The fsal property is not fullfilled.');
                end
            end

            %% compute the lu decompositions
            if mod(s, 2) == 0
                decomp = cell(s/2, 1);
                % only lu decompositions for invD(1), invD(3), invD(5), ...
                % because invD(1)/invD(2), invD(3)/invD(4), invD(5)/invD(6) are complex conjugated pairs
                for i = 1:s / 2
                    if obj.decomposition_type == Decomposition_t.DECOMPOSITION_MATLAB
                        decomp{i} = decomposition(obj.irk_tableau_invD(2*i-1)*obj.E-h*obj.A);
                    elseif obj.decomposition_type == Decomposition_t.DECOMPOSITION_BANDED
                        decomp{i} = decomposition_banded(obj.irk_tableau_invD(2*i-1)*obj.E-h*obj.A);
                    elseif obj.decomposition_type == Decomposition_t.DECOMPOSITION_SPARSE_DIRECT
                        decomp{i} = decomposition_sparse_direct(obj.irk_tableau_invD(2*i-1)*obj.E-h*obj.A);
                    else
                        error('Decomposition_t is not implemented');
                    end
                end
            else
                decomp = cell(1+(s - 1)/2, 1);
                % first lu is real because invD(1) is real
                if obj.decomposition_type == Decomposition_t.DECOMPOSITION_MATLAB
                    decomp{1} = decomposition(real(obj.irk_tableau_invD(1))*obj.E-h*obj.A);
                elseif obj.decomposition_type == Decomposition_t.DECOMPOSITION_BANDED
                    decomp{1} = decomposition_banded(real(obj.irk_tableau_invD(1))*obj.E-h*obj.A);
                elseif obj.decomposition_type == Decomposition_t.DECOMPOSITION_SPARSE_DIRECT
                    decomp{1} = decomposition_sparse_direct(real(obj.irk_tableau_invD(1))*obj.E-h*obj.A);
                else
                    error('Decomposition_t is not implemented');
                end

                % only lu decompositions for  invD(2), invD(4), invD(6), ...
                % because of complex conjugated pairs
                for i = 1:(s - 1) / 2
                    if obj.decomposition_type == Decomposition_t.DECOMPOSITION_MATLAB
                        decomp{i+1} = decomposition(obj.irk_tableau_invD(2*i)*obj.E-h*obj.A);
                    elseif obj.decomposition_type == Decomposition_t.DECOMPOSITION_BANDED
                        decomp{i+1} = decomposition_banded(obj.irk_tableau_invD(2*i)*obj.E-h*obj.A);
                    elseif obj.decomposition_type == Decomposition_t.DECOMPOSITION_SPARSE_DIRECT
                        decomp{i+1} = decomposition_sparse_direct(obj.irk_tableau_invD(2*i)*obj.E-h*obj.A);
                    else
                        error('Decomposition_t is not implemented');
                    end

                end
            end
        end

        function xh = step_with_decompositions(obj, h, x0, decomp)
            %STEP_WITH_DECOMPOSITIONS  Perform one step with step
            %   size h using the given sparse LU decompositions stored
            %   in a cell array.
            %
            %   XH = IRK.STEP_WITH_DECOMPOSITIONS(X0, H, DECOMP) performs
            %   one step with step size H by using the sparse LU
            %   decompositions in the cell array DECOMP. The sparse
            %   LU decompositions must fit to the step size H.
            %
            %   See also IRKSTEP/STEP, IRKSTEP/STEP_CACHE and IRKSTEP/STEP_NO_CACHE.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(h, {'double'}, {'real', 'scalar', '>', 0, 'finite', 'nonnan'}, mfilename, inputname(2));
            validateattributes(x0, {'double'}, {'real', 'nonsparse', 'finite', 'nonnan', 'nrows', obj.n}, mfilename, inputname(3));
            assert(isa(decomp, 'cell'), '%s has wrong type', inputname(3));
            assert(all(cellfun(@(x) isa(x, 'decomposition') || isa(x, 'decomposition_banded') || isa(x, 'decomposition_sparse_direct'), decomp)), ...
                'All elements of %s must be decomposition, decomposition_banded or decomposition_sparse_direct objects', inputname(4));
            % check the number of decomposition objects
            s = obj.stages;
            if mod(s, 2) == 0
                assert(length(decomp) == s/2, '%s wrong number of sparse LU decomposition objects. Given = %d, Needed = %d', inputname(4), length(decomp), s/2);
            else
                assert(length(decomp) == (s - 1)/2+1, '%s wrong number of sparse LU decomposition objects. Given = %d, Needed = %d', inputname(4), length(decomp), (s - 1)/2+1);
            end

            %% perform one step with implicit Runge-Kutta method
            s = obj.stages;
            hAx0 = h * (obj.A * x0);

            %% even or odd number of stages
            if mod(s, 2) == 0
                % compute the transformed right hand sides, solve the system for tz
                % get the second solution as complex conjugated ones
                tz = cell(s, 1);
                for i = 1:(s / 2)
                    rhs = sum(obj.irk_tableau_invV(2*i-1, :)) * hAx0;
                    tz{2*i-1} = decomp{i} \ rhs;
                    tz{2*i} = conj(tz{2*i-1});
                end
            else
                % first treat the real eigenvalue
                tz = cell(s, 1);
                rhs = real(sum(obj.irk_tableau_invV(1, :))) * hAx0;
                tz{1} = decomp{1} \ rhs;

                % now the complex conjugated pairs
                for i = 1:(s - 1) / 2
                    rhs = sum(obj.irk_tableau_invV(2*i, :)) * hAx0;
                    tz{2*i} = decomp{i+1} \ rhs;
                    tz{2*i+1} = conj(tz{2*i});
                end
            end

            %% IRK method has the property first same as last or not
            if ~obj.fsal
                % transform tz to z, z must be real
                z = cell(s, 1);
                for i = 1:s
                    z{i} = zeros(size(x0));
                    for j = 1:s
                        z{i} = z{i} + obj.irk_tableau_V(i, j) * tz{j};
                    end
                    z{i} = real(z{i});
                end

                % advance with time step
                xh = x0;
                for i = 1:s
                    xh = xh + obj.irk_tableau_d(i) * z{i};
                end
            else
                % the method has the fsal property
                % therefore d(1:end-1) is exactly zero
                % and d(end) is exactly 1

                % we only have to compute the last zs
                zs = zeros(size(x0));
                for j = 1:s
                    zs = zs + obj.irk_tableau_V(s, j) * tz{j};
                end
                % zs must be real
                zs = real(zs);

                % advance with time step
                xh = x0 + zs;

            end
        end
    end
end

% The methods below are not used anymore.
% The function step_with_decompositions handles all cases in one function.
% This is the old implementation of step_with_decompositions for each case.
% It is left that other users may get an idea what is going on.
%
% %% GAUSS_LEGENDRE_2
% function xh = step_GL2(obj, x0)
% % Do one step with GAUSS_LEGENDRE_2, i.e.
%
% hAx0 = obj.h*(obj.A*x0);
% rhs = sum(obj.irkcache_invV(1,:))*hAx0;
%
% % solve (invD(1)*E - h*A) tz1 = rhs
% tz1 = obj.irkcache_GL2_decomp \ rhs;
% tz2 = conj(tz1);
%
% % compute z1 and z2 they must be real
% z1 = real(obj.irkcache_V(1,1)*tz1 + obj.irkcache_V(1,2)*tz2);
% z2 = real(obj.irkcache_V(2,1)*tz1 + obj.irkcache_V(2,2)*tz2);
%
% % advance with time step
% xh = x0 + obj.irkcache_d(1)*z1 + obj.irkcache_d(2)*z2;
% end
%
% %% GAUSS_LEGENDRE_3
% function xh = step_GL3(obj, x0)
%
% hAx0 = obj.h*(obj.A*x0);
% rhs1 = real(sum(obj.irkcache_invV(1,:)))*hAx0;
% rhs2 = sum(obj.irkcache_invV(2,:))*hAx0;
%
% % (invD(1)* E - h*A) tz1 = rhs1
% tz1 = obj.irkcache_GL3_decomp1 \ rhs1;
%
% % (invD(2)* E - h*A) tz2 = rhs2
% tz2 = obj.irkcache_GL3_decomp2 \ rhs2;
% tz3 = conj(tz2);
%
% % compute z1, z2 and z3 they must be real
% z1 = real(obj.irkcache_V(1,1)*tz1 + obj.irkcache_V(1,2)*tz2 + obj.irkcache_V(1,3)*tz3);
% z2 = real(obj.irkcache_V(2,1)*tz1 + obj.irkcache_V(2,2)*tz2 + obj.irkcache_V(2,3)*tz3);
% z3 = real(obj.irkcache_V(3,1)*tz1 + obj.irkcache_V(3,2)*tz2 + obj.irkcache_V(3,3)*tz3);
%
% % advance with time step
% xh = x0 + obj.irkcache_d(1)*z1 + obj.irkcache_d(2)*z2 + obj.irkcache_d(3)*z3;
% end
%
% function xh = step_GL4(obj, x0)
%
% hAx0 = obj.h*(obj.A*x0);
% rhs1 = sum(obj.irkcache_invV(1,:))*hAx0;
% rhs2 = sum(obj.irkcache_invV(3,:))*hAx0;
%
% % (invD(1)* E - h*A) tz1 = rhs1,
% tz1 = obj.irkcache_GL4_decomp1 \ rhs1;
% tz2 = conj(tz1);
%
% % (invD(3)* E - h*A) tz3 = rhs2
% tz3 = obj.irkcache_GL4_decomp2 \ rhs2;
% tz4 = conj(tz3);
%
% % compute z1, z2 and z3 they must be real
% z1 = real(obj.irkcache_V(1,1)*tz1 + obj.irkcache_V(1,2)*tz2 + obj.irkcache_V(1,3)*tz3 + obj.irkcache_V(1,4)*tz4);
% z2 = real(obj.irkcache_V(2,1)*tz1 + obj.irkcache_V(2,2)*tz2 + obj.irkcache_V(2,3)*tz3 + obj.irkcache_V(2,4)*tz4);
% z3 = real(obj.irkcache_V(3,1)*tz1 + obj.irkcache_V(3,2)*tz2 + obj.irkcache_V(3,3)*tz3 + obj.irkcache_V(3,4)*tz4);
% z4 = real(obj.irkcache_V(4,1)*tz1 + obj.irkcache_V(4,2)*tz2 + obj.irkcache_V(4,3)*tz3 + obj.irkcache_V(4,4)*tz4);
%
% % advance with time step
% xh = x0 + obj.irkcache_d(1)*z1 + obj.irkcache_d(2)*z2 + obj.irkcache_d(3)*z3 + obj.irkcache_d(4)*z4;
% end
%
