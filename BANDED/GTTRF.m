function [LU, IPIV] = GTTRF(T)
%GTTRF    Interface to {d,z}gttrf LAPACK routines.
%
%  # Purpose:
%
%  Interface to LAPACK {d,z}gttrf routines for computing an LU factorization
%  of a tridiagonal n-by-n matrix A.
%
%  # Description:
%
%  GTTRF computes a LU factorization of the tridiagonal matrix A.
%  A must be a double or double complex n-by-n matrix.
%  The order n of the matrix A must be greater equals 2.
%
%    [LU, IPIV] = GTTRF(T).
%
%  The subdiagonal, diagonal and superdiagonal are stored in the columns
%  of the n-by-3 matrix T.
%
%  # Calling Sequence:
%
%    [LU, IPIV] = GTTRF(T).
%
%  See also TO_TRIDIAG, GTSV, GTTRS AND TEST_BANDED.
%
%  Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2020
narginchk(1, 1);
nargoutchk(2, 2);
validateattributes(T, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'ncols', 3}, mfilename, inputname(1), 1);
[LU, IPIV] = mextools_call('GTTRF', T);
end
