function AB = to_banded(A, KL, KU)
%TO_BANDED    Convert bands of A to LAPACK banded storage format.
%
%   AB = TO_BANDED(A, KL, KU) extract the KL subdiagonals and
%   KU superdiagonals of the matrix A and store them into the rows
%   of AB. The matrix A must be square and of order n.
%   KL and KU must be nonegative scalars and strictly smaller than n.
%   The matrix AB is compatible with the LAPACK banded routines.
%   Compare also the LAPACK banded storage format
%   https://www.netlib.org/lapack/lug/node124.html.
%
%   Example:
%
%   >> A
%   A =
%       1     5     9    13
%       2     6    10    14
%       3     7    11    15
%       4     8    12    16
%
%   >> AB = to_banded(A, 0, 1)
%   AB =
%       0     5    10    15
%       1     6    11    16
%
%   >> AB = to_banded(A, 1, 2)
%   AB =
%       0     0     9    14
%       0     5    10    15
%       1     6    11    16
%       2     7    12     0
%
%   See also: TEST_BANDED.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

%% check input arguments
validateattributes(A, {'double'}, {'square', 'nonnan', 'finite', '2d'}, mfilename, inputname(1));
n = size(A, 1);
validateattributes(KL, {'double'}, {'real', 'integer', '>=', 0, '<', n, 'scalar'}, mfilename, inputname(2));
validateattributes(KU, {'double'}, {'real', 'integer', '>=', 0, '<', n, 'scalar'}, mfilename, inputname(3));

%% transform a to LAPACK banded storage format
AB = transpose(fliplr(spdiags(A, -KL:KU)));

end
