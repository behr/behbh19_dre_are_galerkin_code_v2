function T = to_tridiag(A)
%TO_TRIDIAG    Extract subdiagonal, diagonal and superdiagonal of A.
%
%   T = TO_TRIDIAG(A) extracted the subdiagonal, diagonal and
%   superdiagonal of A and stores them into the columns of T.
%   At return T is a dense n-by-3 matrix.
%   A must be square and the order must be greater equals 2.
%
%   See also: TEST_BANDED.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

%% check input arguments
validateattributes(A, {'double'}, {'square', 'nonempty', 'nonnan', 'finite', '2d'}, mfilename, inputname(1));
n = size(A, 1);

%% transform a to LAPACK banded storage format
if n == 1
  T = [0, A, 0];
else
  T = [[diag(A, -1); 0], diag(A), [diag(A, 1); 0]];
end
end
