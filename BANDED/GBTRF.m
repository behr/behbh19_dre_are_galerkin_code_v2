function [LU, IPIV] = GBTRF(AB, KL, KU)
%GBTRF    Interface to {d,z}gbtrf LAPACK routines.
%
%  # Purpose:
%
%  Interface to LAPACK {d,z}gbtrf routines for computing an LU factorization
%  of a banded matrix A.
%
%  # Description:
%
%  GBTRF computes a LU factorization of the banded matrix A.
%  A must be a double or double complex n-by-n matrix.
%
%    [LU, IPIV] = GBTRF(AB, KL, KU).
%
%  KL and KU is the number of sub and superdiagonals of A.
%  The matrix A must be given in LAPACK band storage format.
%  That is the bands of A are stored in (KL + 1 + KU)-by-n matrix AB.
%
%  # Calling Sequence:
%
%    [LU, IPIV] = GBTRF(AB, KL, KU).
%
%  See also TO_BANDED, GBSV, GBTRS AND TEST_BANDED.
%
%  Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2020
narginchk(3, 3);
nargoutchk(2, 2);
validateattributes(AB, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
no_bands = size(AB, 1);
validateattributes(KL, {'numeric'}, {'real', 'scalar', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0}, mfilename, inputname(2), 2);
validateattributes(KU, {'numeric'}, {'real', 'scalar', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0}, mfilename, inputname(3), 3);
assert((KL + KU + 1) == no_bands, '%s and %s does not fit to the number of bands of %s', inputname(2), inputname(3), inputname(1));
[LU, IPIV] = mextools_call('GBTRF', AB, KL, KU);
end
