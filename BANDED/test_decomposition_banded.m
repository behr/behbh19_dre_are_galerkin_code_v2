classdef test_decomposition_banded < matlab.unittest.TestCase
    %TEST_DECOMPOSITION_BANDED   Test for decomposition_banded solver.
    %
    %   TEST_DECOMPOSITION_BANDED tests the functionality of the
    %   decomposition_banded class.
    %
    %   TEST_DECOMPOSITION_BANDED properties:
    %       instance    - problem instance
    %       nrhs        - number of right hand side
    %       cpx_rhs     - complex or real right hand side
    %       dense_rhs   - dense or sparse right hand side
    %
    %   TEST_DECOMPOSITION_BANDED methods:
    %       test_banded    - Test for decomposition_banded.
    %
    %   See also DECOMPOSITION_BANDED.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        instance = {'rail_1357', ...
            'conv_diff_1_6400', 'conv_diff_2_6400', 'conv_diff_3_6400', 'conv_diff_4_6400', ...
            'fss_K200M5Q2_400', ...
            'beam', 'build', 'CDplayer', ...
            'filter2D', ...
            'T2DAH', 'T2DAL', ...
            };
        nrhs = {1, 5};
        cpx_rhs = {false, true};
        dense_rhs = {false, true};
        modifier = {@(x) x, @(x) -x, ...
            @(x) sparse(diag(diag(x,-1),-1) + diag(diag(x,0),0) + diag(diag(x,1),1))
            };
    end

    properties
        abstol = 1e-6;
        reltol = 1e-8;
        verbose = ~isCIrun();
        randseed = 1;
        alphas = [0, 1, 1 + 1i, -1 + 1i, -1, -1 - 1i, -1i, -1 + 1i];
        betas = [1, 1 + 1i, -1 + 1i, -1, -1 - 1i, -1i, -1 + 1i];
    end

    methods(Test)
        function test_banded(obj, instance, nrhs, cpx_rhs, dense_rhs, modifier)

            %% load instance
            data = load(instance);
            A = data.A;
            n = size(A, 1);

            if isfield(data, 'E')
                E = data.E;
            else
                E = speye(size(A));
            end

            %% skip some tests on ciRun
            if any(strcmp({'flow_v0', 'flow_v05', 'chip_v0', 'chip_v01', 'T2DAH', 'T2DAL', 'beam', 'build', 'CDplayer'}, instance)) && isCIrun
                return;
            end

            %% set random seed
            rng(obj.randseed);
            b = rand(n, nrhs);
            if cpx_rhs
                b = b + 1i * rand(n, nrhs);
            end
            normb = norm(b);

            if ~dense_rhs
                b = sparse(b);
            end

            %% create a solver, solve system and compute residuals
            for alpha = obj.alphas
                for beta = obj.betas
                    ApE = alpha * A + beta * E;
                    ApE = modifier(ApE);
                    da = decomposition_banded(ApE);
                    x = da \ b;

                    %% check residual
                    abs2res = norm(full(ApE*x-b));
                    rel2res = abs2res / normb;
                    is_abstol = abs2res < obj.abstol;
                    is_reltol = rel2res < obj.reltol;
                    failed = ~(is_abstol && is_reltol);

                    %% compute comparison solution via backslash
                    x_backslash = ApE \ b;
                    abs2res_backslash = norm(full(ApE*x_backslash-b));
                    rel2res_backslash = abs2res_backslash / normb;

                    if failed || obj.verbose
                        fprintf('\n');
                        if failed
                            fprintf('FAILED:\n');
                        end
                        f(1) = fprintf('%s, nrhs = %d, cpx_rhs = %d, dense_rhs = %d, alpha = %+.2e + %+.2e*i, beta = %+.2e + %+.2e*i \n', ...
                            instance, nrhs, cpx_rhs, dense_rhs, real(alpha), imag(alpha), real(beta), imag(beta));
                        f(2) = fprintf('is_abstol = %d\n', is_abstol);
                        f(3) = fprintf('is_reltol = %d\n', is_reltol);
                        f(4) = fprintf('modifier  = %s\n', func2str(modifier));
                        f(5) = fprintf('BANDED:       abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res, rel2res);
                        f(6) = fprintf('BACKSLASH:    abs./rel. 2-norm residual = %.2e/%.2e\n', abs2res_backslash, rel2res_backslash);
                        f(7) = fprintf('              abs./rel. 2-norm tol      = %.2e/%.2e\n', obj.abstol, obj.reltol);
                        f(8) = fprintf('choosen type = %s\n', da.type_);
                        f(9) = fprintf('kl = %d, ku = %d\n', da.kl, da.ku);
                        fprintf('%s\n', repmat('-', 1, max(f)-1));
                    end

                    %% check
                    obj.fatalAssertLessThan(abs2res, obj.abstol);
                    obj.fatalAssertLessThan(rel2res, obj.reltol);

                end
            end
        end
    end
end
