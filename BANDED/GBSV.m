function X = GBSV(AB, KL, KU, B)
%GBSV    Interface to {d,z}gbsv LAPACK routines.
%
%  # Purpose:

%  Interface to LAPACK {d,z}gbsv routines for solving banded
%  linear systems A X = B.
%
%  # Description:
%
%  GBSV solves a linear equation A X = B, where A is banded double or
%  double complex, square matrix n-by-n matrix.
%
%    X = GBSV(AB, KL, KU, B).
%
%  KL and KU is the number of subdiagonals and superdiagonals of A.
%  The matrix A must be given in LAPACK band storage format.
%  That is the bands of A are stored in the (KL + 1 + KU)-by-n matrix AB.
%
%  # Calling Sequence:
%
%    X = GBSV(AB, KL, KU, B).
%
%  See also TO_BANDED, GBTRF, GBTRS AND TEST_BANDED.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(4, 4);
nargoutchk(1, 1);
validateattributes(AB, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
[no_bands, n] = size(AB);
validateattributes(KL, {'numeric'}, {'real', 'scalar', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0}, mfilename, inputname(2), 2);
validateattributes(KU, {'numeric'}, {'real', 'scalar', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0}, mfilename, inputname(3), 3);
assert((KL + KU + 1) == no_bands, '%s and %s does not fit to the number of bands of %s', inputname(2), inputname(3), inputname(1));
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(4), 4);
X = mextools_call('GBSV', AB, KL, KU, B);
end
