function X = PBTRS(U, B)
%PBTRS    Interface to {d,z}pbtrs LAPACK routines.
%
%  # Purpose:
%
%  Interface to LAPACK {d,z}pbtrs routine for solving
%  the linear system A X = B for a symmetric positive definite
%  banded matrix A.
%  The Cholesky factorization U of A must be given as input.
%  The Cholesky factorization of A must be computed by
%
%    U = PBTRF(AB).
%
%  # Calling Sequence:
%
%    X = PBTRS(U, B).
%
%  See also TO_BANDED, PBSV, PBTRF and TEST_BANDED.
%
%  Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2020

narginchk(2, 2);
nargoutchk(1, 1);
validateattributes(U, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
n = size(U, 2);
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(2), 2);
X = mextools_call('PBTRS', U, B);
end
