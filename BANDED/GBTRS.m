function X = GBTRS(TRANS, LU, IPIV, KL, KU, B)
%GBTRS    Interface to {d,z}gbtrs LAPACK routines.
%
%  # Purpose:
%
%  Interface to LAPACK {d,z}gbtrs routine for solving a
%  linear system A X = B for a banded matrix A.
%
%  # Description:
%
%  GBTRS solves the linear system A X = B for a banded matrix A.
%  The LU factorization of A and the pivoting vecotor IPIV must be
%  given as input arguments. The LU factorization and the pivoting
%  vector can be computed by
%
%    [LU, IPIV] = GBTRF(AB, KL, KU).
%
%  A is a double or double complex n-by-n matrix banded matrix.
%  KL and KU is the number of subdiagonals and superdiagonals of A.
%  B must be double or double complex and dense.
%  TRANS must be a character.
%
%    TRANS:
%      'N' solve A X   = B
%      'T' solve A.' X = B
%      'C' solve A' X  = B
%
%  # Calling Sequence:
%
%    X = GBTRS(TRANS, LU, IPIV, KL, KU, B).
%
%   See also TO_BANDED, GBSV, GBTRF AND TEST_BANDED.
%
%  Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2020

narginchk(6, 6);
nargoutchk(1, 1);
validateattributes(TRANS, {'char'}, {'scalar'}, mfilename, inputname(1), 1);
assert((TRANS == 'N') || (TRANS == 'T') || (TRANS == 'C'), '%s must be ''N'', ''T'' or ''C''.', inputname(1), 1);
validateattributes(LU, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(2), 2);
[ldLU, n] = size(LU);
validateattributes(IPIV, {'int64'}, {'vector', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(3), 3);
validateattributes(KL, {'numeric'}, {'real', 'scalar', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0}, mfilename, inputname(4), 4);
validateattributes(KU, {'numeric'}, {'real', 'scalar', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', 0}, mfilename, inputname(5), 5);
assert((2 * KL + KU + 1) == ldLU, '%s and %s does not fit to the number of rows of %s', inputname(4), inputname(5), inputname(2));
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(6), 6);
X = mextools_call('GBTRS', TRANS, LU, IPIV, KL, KU, B);
end
