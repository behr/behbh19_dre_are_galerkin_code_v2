classdef decomposition_banded < custom_handle
    %DECOMPOSITION_BANDED   Implements a direct solver for linear systems based
    %   on the banded solvers GBTRF, GBTRS, PBTRF and PBTRS. A bandwidth
    %   reducing permutation is computed using symrcm. If the matrix
    %   is symmetric or hermitian the solver tries to perform a
    %   cholesky based banded decomposition. On failure
    %   the solver switches to the general banded solver GBTRF and GBTRS.
    %
    %   DECOMPOSITION_BANDED properties:
    %       LU      - set private, double, (number of bands)-x-n matrix the banded LU or Cholesky factorization
    %       ipiv    - set private, int64, the permutation vector
    %       kl      - set private, double, the number of bands below main diagonal
    %       ku      - set private, double, the number of bands above main diagonal
    %       perm    - set private, double, the bandwidth reducing permutation
    %       invperm - set private, double, the inverse of perm
    %       scale   - set private, double, -1/+1 indicates if the matrix was scaled
    %       type_   - set private, char, choosen type for factorization
    %
    %   DECOMPOSITION_BANDED methods:
    %       decomposition_banded    - Constructor.
    %       mldivide                - The backslash operator.
    %
    %   See also GBTRF, GBTRS, PBTRF, PBTRS and TEST_DECOMPOSITION_BANDED.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(SetAccess = private)
        % private, double, (number of bands)-x-n matrix the banded LU or Cholesky factorization
        LU = []; % LU adasd
        % private, int64, the permutation vector
        ipiv = [];
        % private, double, the number of bands below main diagonal
        kl = 0;
        % private, double, the number of bands above main diagonal
        ku = 0;
        % private, double, the bandwidth reducing permutation
        perm = [];
        % private, double, the inverse of perm
        invperm = [];
        % private, double, -1/+1 indicates if the matrix was scaled
        scale = 1;
        % private, char, choosen type for factorization
        type_ = 'auto';
        % private, char, size of matrix
        n = 0;
    end

    methods

        function obj = decomposition_banded(A, type)
            %DECOMPOSITION_BANDED  Matrix decomposition using banded solvers.
            %
            %   DA = DECOMPOSITION_BANDED(A) computes a bandwidth reducing
            %   permutation for A and a factorization using a banded factorization
            %   routine. If the matrix is symmetric or hermitian
            %   the solver tries to perform a cholesky factorization
            %   for A and -A. On failure the solver switches to
            %   to a general banded solver.
            %
            %   DA = DECOMPOSITION_BANDED(A, TYPE) specifies the TYPE of
            %   factorization to use. Possible values of TYPE depend
            %   on the matrix A:
            %
            %       'auto'  - default, type depends on input matrix
            %       'lu'    - LU factorization GBTRF
            %       'chol'  - cholesky factoriuation PBTRF
            %       'mchol' - cholesky factoriuation of -A PBTRF
            %
            %   See also TEST_DECOMPOSITION_BANDED.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(A, {'double'}, {'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
            obj.n = size(A, 1);
            assert(obj.n >= 1, 'The order of the matrix must be at least 1');

            %% check the type
            if nargin >= 2
                obj.type_ = lower(type);
                validateattributes(obj.type_, {'char'}, {'scalartext', '2d', 'finite', 'nonnan', 'nonempty'}, mfilename, inputname(1));
                assert(any(strcmp({'auto', 'lu', 'chol', 'mchol'}, obj.type_)), 'type must be ''auto'', ''lu'', ''chol'' or ''mchol''.');
            end

            %% compute bandwidth reducing permutation
            obj.perm = symrcm(A);
            obj.invperm = zeros(1, obj.n);
            obj.invperm(obj.perm) = 1:obj.n;
            A_perm = A(obj.perm, obj.perm);
            [obj.kl, obj.ku] = bandwidth(A_perm);

            %% transform to banded matrix
            A_perm_banded = to_banded(A_perm, obj.kl, obj.ku);

            %% treat manually specified factorization type
            if strcmp(obj.type_, 'lu')
                if (obj.kl <= 1 && obj.ku <= 1)
                    A_perm_tridiag = full(to_tridiag(A_perm));
                    [obj.LU, obj.ipiv] = GTTRF(A_perm_tridiag);
                    return;
                else
                    [obj.LU, obj.ipiv] = GBTRF(A_perm_banded, obj.kl, obj.ku);
                    return;
                end
            elseif strcmp(obj.type_, 'chol')
                assert(ishermitian(A), 'matrix %s has to be symmetric or hermitian', inputname(1));
                diagA = diag(A);
                assert(isreal(diagA), 'diagonal of %s has to be real', inputname(1));
                assert(all(diagA > 0), 'all diagonal entries of %s has to be positive', inputname(1));
                try
                    obj.LU = PBTRF(A_perm_banded(1:(1 + obj.ku), :));
                catch ME
                    rethrow(ME);
                end
                return;
            elseif strcmp(obj.type_, 'mchol')
                assert(ishermitian(A), 'matrix %s has to be symmetric or hermitian', inputname(1));
                diagA = diag(A);
                assert(isreal(diagA), 'diagonal of %s has to be real', inputname(1));
                assert(all(diagA < 0), 'all diagonal entries of %s has to be positive', inputname(1));
                try
                    obj.LU = PBTRF(-A_perm_banded(1:(1 + obj.ku), :));
                    obj.scale = -1;
                catch ME
                    rethrow(ME);
                end
                return;
            elseif strcmp(obj.type_, 'auto')

                %% check if it is candidate for cholesky
                if ishermitian(A)
                    diagA = full(diag(A));
                    if isreal(diagA)
                        if all(diagA > 0)
                            try
                                obj.LU = PBTRF(A_perm_banded(1:(1 + obj.ku), :));
                                obj.type_ = 'chol';
                                return;
                            catch
                            end
                        elseif all(diagA < 0)
                            try
                                obj.LU = PBTRF(-A_perm_banded(1:(1 + obj.ku), :));
                                obj.type_ = 'mchol';
                                obj.scale = -1;
                                return;
                            catch
                            end
                        end
                    end
                end

                %% perform LU factorization
                if (obj.kl <= 1 && obj.ku <= 1)
                    A_perm_tridiag = full(to_tridiag(A_perm));
                    [obj.LU, obj.ipiv] = GTTRF(A_perm_tridiag);
                    obj.type_ = 'lu';
                    return;
                else
                    [obj.LU, obj.ipiv] = GBTRF(A_perm_banded, obj.kl, obj.ku);
                    obj.type_ = 'lu';
                    return;
                end
            else
                error('not implemented, internal error.');
            end
        end

        function x = mldivide(da, b)
            %\  Matrix left divide for decomposition_banded using.
            %
            %   X = DA \ B, where DA is a decomposition_banded object,
            %   solve the linear system A*X = B,
            %   where DA = DECOMPOSITION_BANDED(A).
            %
            %   See also DECOMPOSITION_BANDED and TEST_DECOMPOSITION_BANDED.
            %
            %   Author: Maximilian Behr
            validateattributes(da, {'decomposition_banded'}, {}, mfilename, inputname(1));
            validateattributes(b, {'double'}, {'2d', 'nonempty', 'finite', 'nonnan', 'nrows', da.n}, mfilename, inputname(2));
            if issparse(b)
                b = full(b);
            end

            if ~isempty(da.perm)
                b = b(da.perm, :);
            end

            if isempty(da.ipiv)
                x = PBTRS(da.LU, b);
            else
                if (da.kl <= 1 && da.ku <= 1)
                    x = GTTRS('N', da.LU, da.ipiv, b);
                else
                    x = GBTRS('N', da.LU, da.ipiv, da.kl, da.ku, b);
                end
            end
            x = x(da.invperm, :);

            if da.scale == -1
                x = -x;
            end
        end
    end
end
