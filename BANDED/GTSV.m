function X = GTSV(T, B)
%GTSV    Interface to {d,z}gtsv LAPACK routines.
%
%  # Purpose:
%
%  Interface to LAPACK {d,z}gtsv routines for solving tridiagonal
%  linear systems A X = B.
%
%  # Description:
%
%  GTSV solves a linear equation A X = B, where A is tridiagonal double or
%  double complex, square n-by-n matrix.
%  The order n of the matrix A must be greater equals 2.
%
%    X = GTSV(T, B).
%
%  The subdiagonal, diagonal and superdiagonal are stored in the columns
%  of the n-by-3 matrix T.
%
%  # Calling Sequence:
%
%    X = GTSV(T, B).
%
%  See also TO_TRIDIAG, GTTRF, GTTRS AND TEST_BANDED.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(2, 2);
nargoutchk(1, 1);
validateattributes(T, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'ncols', 3}, mfilename, inputname(1), 1);
n = size(T, 1);
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(2), 2);
X = mextools_call('GTSV', T, B);
end
