function X = PBSV(AB, B, OVERWRITEA)
%PBSV    Interface to {d,z}pbsv LAPACK routines.
%
%  # Purpose:

%  Interface to LAPACK {d,z}pbsv routine for solving A X = B for a
%  symmetric positive definite banded matrix A.
%
%  # Description:
%
%  PBSV solves a linear equation A X = B, where A is banded double or
%  double complex, square, symmetric/hermitian positive definite matrix.
%
%    X = PBSV(AB, B, OVERWRITEA).
%
%  The upper triangular part of the matrix A must be given in
%  LAPACK band storage format.
%  That is the diagonals and the superdiagonals are stored in
%  the rows of the matrix AB.
%  If OVERWRITEA is nonzero AB is overwritten at exit by its Cholesky
%  factor U. That is U' U = A. U is in banded storage. default is 0.
%
%  # Calling Sequence:
%
%    X = PBSV(AB, B).
%    X = PBSV(AB, B, OVERWRITEA).
%
%  See also TO_BANDED, PBTRF, PBTRS and TEST_BANDED.
%
%  Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2020

narginchk(2, 3);
nargoutchk(1, 1);
validateattributes(AB, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
n = size(AB, 2);
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(2), 2);

if nargin == 2
    X = mextools_call('PBSV', AB, B);
else
    validateattributes(OVERWRITEA, {'numeric'}, {'real', 'scalar', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(3), 3);
    X = mextools_call('PBSV', AB, B, OVERWRITEA);
end
end
