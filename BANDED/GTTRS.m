function X = GTTRS(TRANS, LU, IPIV, B)
%GTTRS    Interface to {d,z}gttrs LAPACK routines.
%
%  # Purpose:
%
%  Interface to LAPACK {d,z}gttrs routine for solving a
%  linear system A X = B for a tridiagonal matrix A.
%
%  # Description:
%
%  GTTRS solves the linear system A X = B for a tridiagonal matrix A.
%  The LU factorization of A and the pivoting vecotor IPIV must be
%  given as input arguments. The order n of the matrix A must be
%  greater equals 2. The LU factorization and the pivoting
%  vector can be computed by
%
%    [LU, IPIV] = GTTRF(T).
%
%  The subdiagonal, diagonal and superdiagonal are stored in the columns
%  of the n-by-3 matrix T.

%  TRANS must be a character.
%
%    TRANS:
%      'N' solve A X   = B
%      'T' solve A.' X = B
%      'C' solve A' X  = B
%
%  # Calling Sequence:
%
%    X = GTTRS(TRANS, LU, IPIV, B).
%
%  See also TO_TRIDIAG, GTSV, GTTRF AND TEST_BANDED.
%
%  Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2020

narginchk(4, 4);
nargoutchk(1, 1);
validateattributes(TRANS, {'char'}, {'scalar'}, mfilename, inputname(1), 1);
assert((TRANS == 'N') || (TRANS == 'T') || (TRANS == 'C'), '%s must be ''N'', ''T'' or ''C''.', inputname(1), 1);
validateattributes(LU, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'ncols', 4}, mfilename, inputname(2), 2);
n = size(LU, 1);
validateattributes(IPIV, {'int64'}, {'vector', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(3), 3);
validateattributes(B, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', n}, mfilename, inputname(4), 4);
X = mextools_call('GTTRS', TRANS, LU, IPIV, B);
end
