classdef test_BANDED < matlab.unittest.TestCase
    %TEST_BANDED   Tests the functionality of the banded solvers.
    %
    %   TEST_BANDED tests the functionality of the banded solvers
    %   GBSV, PBSV, GBTRF, GBTRS, PBTRF and PBTRS.
    %
    %   TEST_BANDED properties:
    %       meth        - banded solver
    %       n           - order of matrix A
    %       nrhs        - number of right hand sides  / columns B
    %       cpxA        - logical true for complex A
    %       cpxB        - logical true for complex B
    %       kl          - the number of subdiagonals of A
    %       ku          - the number of superdiagonals of A
    %       overwriteA  - overwrite A or not
    %       transA      - solve with (non)transpose or conjugate
    %
    %   TEST_BANDED methods:
    %       test_BANDED_solve - Test for all interfaced LAPACK banded routines.
    %
    %   See also GBSV, PBSV, GBTRF, GBTRS, PBTRF, PBTRS and TO_BANDED.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        meth = {'GB', 'PB', 'GT'};
        n = {1, 5, 14};
        nrhs = {1, 6};
        cpxA = {false, true};
        cpxB = {false, true};
        kl = {0, 1, 3, 14};
        ku = {0, 1, 3, 14};
        overwriteA = {0, 1};
        trans = {'N', 'T', 'C'};
    end

    properties
        abstol = 1e-8;
        reltol = 1e-9;
        verbose = ~isCIrun();
        randseed = 1;
    end

    methods(Test)
        function test_BANDED_solve(obj, meth, n, nrhs, cpxA, cpxB, kl, ku, overwriteA, trans)

            %% set random seed
            rng(obj.randseed);

            %% check the meth def
            if ~any(strcmp(meth, {'GB', 'PB', 'GT'}))
                error('Unknown banded solver %s', meth);
            end

            %% KL and KU must be smaller than n
            if kl >= n || ku >= n
                return;
            end

            %% we can skip trans for PB (symmetric positive definite banded)
            if any(strcmpi(meth, {'PB'})) && (trans ~= 'N' || overwriteA ||(kl ~=ku))
                return;
            end

            %% we can skip trans for GT (tridiagonal)
            if any(strcmpi(meth, {'GT'})) && (kl > 1 || ku > 1)
                return;
            end

            %% generate random matrix
            if cpxA
                A = tril(triu(rand(n, n) + 1i*rand(n, n), -kl), ku);
            else
                A = tril(triu(rand(n, n), -kl), ku);
            end

            %% PBSV needs banded symmetric/hermitian positiv definite
            if any(strcmpi(meth, {'PB'}))
                A = A + A';
                e = abs(min(real(eig(A)))) + 1;
                A = A + e * eye(size(A));
            end

            %% generate right hand side
            B = rand(n, nrhs);
            if cpxB
                B = B + 1i * rand(n, nrhs);
            end

            %% transform A to banded and solve
            if strcmpi(meth, 'PB')
                AB = to_banded(A, 0, ku);
                X = PBSV(AB, B, overwriteA);
                AB = to_banded(A, 0, ku);
                U = PBTRF(AB);
                X2 = PBTRS(U, B);
            elseif strcmpi(meth, 'GB')
                AB = to_banded(A, kl, ku);
                X = GBSV(AB, kl, ku, B);
                [LU, IPIV] = GBTRF(AB, kl, ku);
                X2 = GBTRS(trans, LU, IPIV, kl, ku, B);
            elseif strcmp(meth, 'GT')
                TA = to_tridiag(A);
                X = GTSV(TA, B);
                [LU, IPIV] = GTTRF(TA);
                X2 = GTTRS(trans, LU, IPIV, B);
            end

            %% residual for X
            abs1res_X = norm(A*X - B, 1);
            rel1res_X = abs1res_X / norm(B, 1);
            is_abs1res_X = abs1res_X < obj.abstol;
            is_rel1res_X = rel1res_X < obj.reltol;

            %% residual for X2 handle the trans parameter
            if trans == 'N'
                modifier = @(x) x;
            elseif trans == 'T'
                modifier = @(x) transpose(x);
            elseif trans == 'C'
                modifier = @(x) ctranspose(x);
            else
                error('invalid value for trans');
            end

            abs1res_X2 = norm(modifier(A)*X2-B, 1);
            rel1res_X2 = abs1res_X2 / norm(B, 1);
            is_abs1res_X2 = abs1res_X2 < obj.abstol;
            is_rel1res_X2 = rel1res_X2 < obj.reltol;

            %% evaluate the checks and print message on error
            failed = ~(is_abs1res_X && is_rel1res_X && is_abs1res_X2 && is_rel1res_X2);

            if failed || obj.verbose
                fprintf('\n');
                if failed, fprintf('FAILED:\n');
                    fprintf('cond A = %.2e\n', cond(A));
                    disp(A)
                    disp(TA)
                end
                f(1) = fprintf('%s, n = %d, nrhs = %d, cpxA = %d, cpxB = %d, kl = %d, ku = %d, overwriteA = %d, trans = %s\n', ...
                    meth, n, nrhs, cpxA, cpxB, kl, ku, overwriteA, trans);
                f(2) = fprintf('abs./rel. 2-norm residual X   = %.2e/%.2e\n', abs1res_X, rel1res_X);
                f(3) = fprintf('abs./rel. 2-norm residual X2  = %.2e/%.2e\n', abs1res_X2, rel1res_X2);
                f(4) = fprintf('abs./rel. 2-norm residual tol = %.2e/%.2e\n', obj.abstol, obj.reltol);
                fprintf('%s\n', repmat('-', 1, max(f)-1));
            end

            %% check
            obj.fatalAssertTrue(is_abs1res_X);
            obj.fatalAssertTrue(is_rel1res_X);
            obj.fatalAssertTrue(is_abs1res_X2);
            obj.fatalAssertTrue(is_rel1res_X2);
        end
    end
end
