function U = PBTRF(AB)
%PBTRF    Interface to {d,z}pbtrf LAPACK routines.
%
%  # Purpose:
%
%  Interface to LAPACK {d,z}pbtrf routines for Cholesky factorization of
%  symmetric positive definite banded matrix A.
%
%  # Description:
%
%  PBTRF computes a Cholesky factorization of banded, double or
%  double complex, square, symmetric/hermitian positive definite matrix A.
%
%    U = PBTRF(AB).
%
%  The upper triangular part of the matrix A must be given in
%  LAPACK band storage format.
%  That is the diagonals and the superdiagonals are stored in
%  the rows of the matrix AB. U is banded and U'*U is A.
%
%  # Calling Sequence:
%
%    U = PBTRF(AB).
%
%  See also TO_BANDED, PBSV, PBTRS and TEST_BANDED.
%
%  Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2020

narginchk(1, 1);
nargoutchk(1, 1);
validateattributes(AB, {'double'}, {'2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
U = mextools_call('PBTRF', AB);
end
