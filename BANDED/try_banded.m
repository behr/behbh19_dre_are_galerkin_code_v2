%TRY_BANDED    Playaround script for banded solvers.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all;
clc

%% data
instance = 'cookie';
data = load(instance);
A = data.A;
E = speye(size(A));
B = data.B;
C = data.C;
A = (5 + 2i) * E - 2^(-2) * A;
n = size(A, 1);
RHS = rand(n, 100);

%% decomposition and solve
dA = decomposition(A);

f = @() dA \ RHS;
t = timeit(f);

%% reordering
p = symrcm(A);

%% compare decomposition solve and banded LU
% compute decomposition
dA = decomposition(A);

% compute banded LU
Aperm = A(p, p);
[kl, ku] = bandwidth(Aperm);
AB = to_banded(A, kl, ku);
[LU, IPIV] = GBTRF(AB, kl, ku);
%AB = to_banded(Aperm,0,ku);
%U = PBTRF(-AB);

for nrhs = 1:10:100
    RHS = rand(n, nrhs);
    f = @() dA \ RHS;
    t_decomp = timeit(f);
    f = @() GBTRS('N', LU, IPIV, kl, ku, RHS);
    t_gbtrs = timeit(f, 1);
    %f = @() PBTRS(U, B);
    %t_pbtrs = timeit(f, 1);
    t_pbtrs = inf;
    fprintf('decomposition vs. banded LU vs. banded CHOL: RHS = %d, time = %.2e / %.2e / %.2e, speedup = %.2e / %.2e\n', nrhs, t_decomp, t_gbtrs, t_pbtrs, t_decomp/t_gbtrs, t_decomp/t_pbtrs);
end

