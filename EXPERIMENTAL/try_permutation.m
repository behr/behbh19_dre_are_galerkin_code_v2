%TRY_PERMUTATION    Playaround script for sparse matrix permutations.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%
%% clear
clearvars, close all;
%clc

%% load matrix
instance = 'rail_20209';
%instance = 'rail_5177';
%instance = 'rail_1357';
%instance = 'rail_371';
%instance = 'flow_v05';
%instance = 'chip_v0';
%instance = 'filter2D';
%instance = 'conv_diff_3_10000';

instances = { ...
    'rail_371', ...
    'rail_1357', ...
    'rail_5177', ...
    'rail_20209', ...
    'rail_79841', ....
    'chip_v0', 'chip_v1', ...
    'flow_v05', ...
    'GasSensor', ...
    'filter2D', ...
    'T2DAH', ...
    'T2DAL', ...
    'conv_diff_3_10000'
    };

for i_instance = 1:length(instances)
    instance = instances{i_instance};
    data = load(instance);
    A_orig = data.A;
    if isfield(data,'E')
        E_orig = data.E;
    else
        E_orig = speye(size(A_orig));
    end
    B_orig = data.B;
    C_orig = data.C;
    RHS = C_orig';

    %% iterate over all reordering methods
    spparms('default');
    pre_reordering_methods = {'AMD', 'SYMAMD', 'COLAMD', 'DISSECT'};

    for cpx = [0, 1, 2, 3]
        if cpx == 0
            A = A_orig - E_orig;
        elseif cpx == 1
            A = A_orig + E_orig;
        elseif cpx == 2
            A = A_orig - (1 + 1i)*E_orig;
        else
            A = A_orig - (-1 + 1i)*E_orig;
        end

        %% get time with backslash for reference
        f = @() A \ RHS;
        t_ref_backslash = timeit(f);
        fprintf('REF TIME BACKSLASH = %.4f\n', t_ref_backslash);

        for i_reordering = 1:length(pre_reordering_methods)

            n = length(pre_reordering_methods);
            meth = pre_reordering_methods{i_reordering};

            switch meth
                case 'AMD'
                    p = amd(A_orig|E_orig);
                    A_perm = A(p,p);
                case 'SYMAMD'
                    p = symamd(A_orig|E_orig);
                    A_perm = A(p,p);
                case 'COLAMD'
                    p = colamd(A_orig'|E_orig');
                    invp(p) = 1:length(p); %#ok<*SAGROW>
                    A_perm = A(p,:);
                case 'SYMRCM'
                    p = symrcm(A_orig|E_orig);
                    A_perm = A(p,p);
                case 'DISSECT'
                    p = dissect(A_orig|E_orig, 'NumIterations', 10);
                    A_perm = A(p,p);
                otherwise
                    error('not implemented');
            end

            %% compute LU, spyplots and compute bandwidth and nonzeros
            %[L_perm,U_perm, p, q] = lu(A_perm);
            %subplot(n, 3, 3*(i_reordering-1) + 1)
            %spy(A_perm), title(meth)
            %subplot(n, 3, 3*(i_reordering-1) + 2)
            %spy(L_perm), title('L')
            %subplot(n,3, 3*(i_reordering-1) + 3)
            %spy(U_perm), title('U')

            %[kl,ku] = bandwidth(A_perm);
            %fprintf('%-10s Bandwidth = %8d, KL/KU = %8d/%8d, NNZ(L) = %8d, NNZ(U) = %8d, wtime V4 / UMF / V4 + REORDERING / UMF + REORDERING = %.4f / %.4f / %.4f / %.4f\n', ...
            %    meth, kl+ku+1, kl, ku, nnz(L_perm), nnz(U_perm), wtimes);

            %% backslash with perm
            umfpack_default = spparms('umfpack');
            autommd_default = spparms('autommd');
            autoamd_default = spparms('autoamd');

            wtime = zeros(4, 1);
            spparms('umfpack', 0);
            spparms('autommd', 0);
            spparms('autoamd', 0);
            f = @() A_perm' \ RHS;
            wtimes(1) = timeit(f);

            spparms('umfpack', 1);
            spparms('autommd', 0);
            spparms('autoamd', 0);
            f = @() A_perm' \ RHS;
            wtimes(2) = timeit(f);

            spparms('umfpack', 0);
            spparms('autommd', 1);
            spparms('autoamd', 1);
            f = @() A_perm' \ RHS;
            wtimes(3) = timeit(f);

            spparms('umfpack', 1);
            spparms('autommd', 1);
            spparms('autoamd', 1);
            f = @() A_perm' \ RHS;
            wtimes(4) = timeit(f);

            spparms('umfpack', umfpack_default);
            spparms('autommd', autommd_default);
            spparms('autoamd', autoamd_default);

            printed = fprintf('CPX = %d, PREREORDERING = %-10s wtime V4 / UMF / V4 + REORDERING / UMF + REORDERING = %2.4f / %2.4f / %2.4f / %2.4f\n', cpx, meth, wtimes);

        end
        fprintf('%s\n',repmat('-',1,printed));
    end
end