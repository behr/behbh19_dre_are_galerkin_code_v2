classdef ShiftedSolver < handle
    %SHIFTEDSOLVER  Implements a solver for shifted sparse linear system
    %   with a cached fill-in reducing prereordering.
    %
    %   WARNING: THIS IS A QUITE HACKY PIECE OF CODE. SEVERAL ASSUMPTIONS
    %   ON THE LOW LEVEL STRUCTURE OF THE MATLAB INTERNAL DECOMPOSITION
    %   OBJCET HASE BEEN MADE. THE CODE IS PROBABLY NOT COMPATIBLE
    %   WITH OTHER MATLAB VERSIONS AND CAN BE SEEN AS HIGHLY
    %   EXPERIMENTALLY AND IS QUITE LIKELY TO BREAK IF THE INTERNAL
    %   STRUCTURE OF THE DECOMPOSITION IS CHANGED OR SOME ATTRIBUTES
    %   BECOME NON PUBLIC ANYMORE.
    %
    %   The ShiftedSolver class aims to solve sequences of sparse linear
    %   shifted systems (A + shift(i) E) x = b for sparse square matrices
    %   A, E and shift parameters shift(i) more efficiently.
    %   The strategy is to compute a fill-in reducing permutation
    %   for the matrices A + shift E ones and reuse the permutation
    %   for the other systems (A+ shift(i) E) x = b.
    %   The cached permutation is applied to the shifted system
    %   before the MATLAB internal sparse direct solver is applied.
    %   The available permutations are defined in Permutation_t enumeration
    %   class. If the Permutation_t instance NONE is used, no permutation
    %   is cached and the MATLAB internal sparse direct solver is used
    %   to solve the system (A + shift(i) E) x = b.
    %
    %   If the Permutation_t instance is NONE, then
    %   no fill-in reducing permutation is cached.
    %
    %   If the Permutation_t instance is AUTO, the fill-in reducing
    %   permutation is extracted from the underyling decomposition object.
    %   AUTO is a bit experimental.
    %
    %   SHIFTEDSOLVER methods:
    %       SHIFTEDSOLVER   - Constructor.
    %       solve           - solve the system (A + shift(i) E) x = b.
    %
    %   SHIFTEDSOLVER properties:
    %       A_perm          - get/set private, sparse n-x-n permuted matrix A
    %       E_perm          - get/set private, sparse n-x-n permuted matrix E
    %       perm_t          - get/set private, Permutation_t desired reordering
    %       perm_row        - get/set private, row permutation vector
    %       perm_col        - get/set private, col permutation vector
    %       perm_row_inv    - get/set private, inverse row permutation vector
    %       perm_col_inv    - get/set private, inverse col permutation vector
    %
    %   See also PERMUTATION_T, RADI, RADI_OPTIONS, TEST_RADI.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(Access = private)
        % get/set private, sparse n-x-n permuted matrix A
        A_perm
        % get/set private, sparse n-x-n permuted matrix E
        E_perm
        % get/set private, Permutation_t desired reordering
        perm_t
        % get/set private, row permutation vector
        perm_row = [];
        % get/set private, col permutation vector
        perm_col = [];
        % get/set private, inverse row permutation vector
        perm_row_inv = [];
        % get/set private, inverse col permutation vector
        perm_col_inv = [];
        % get/set private, logical true if solve was not called
        first_call = true;
    end

    properties (Access = private, Constant = true, Hidden = true, Transient = true)
        % activate/decative internal debug mode, mainly useful for AUTO
        DEBUGMODE = false;
    end

    methods(Access = public)
        function obj = ShiftedSolver(A, E, perm_t)
            %SHIFTEDSOLVER  Constructor of class.
            %
            %   SHIFTEDSOL = SHIFTEDSOLVER(A, E, PERM_T) creates an
            %   instance of the class. A and E must be sparse n-x-n matrices
            %   and PERM_T must be an instance of Permutation_t.
            %   If E is empty the identity matrix is used.
            %
            %   See also PERMUTATION_T, RADI, TEST_RADI.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(A, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan'}, mfilename, inputname(1));
            assert(issparse(A), '%s must be sparse', inputname(1));
            n = size(A, 1);
            isemptyE = isempty(E);
            if ~isemptyE
                validateattributes(E, {'double'}, {'real', 'square', '2d', 'finite', 'nonnan', 'size', [n, n]}, mfilename, inputname(2));
                assert(issparse(E), '%s must be sparse', inputname(2))
            else
                E = speye(size(A));
            end
            validateattributes(perm_t, {'Permutation_t'}, {}, mfilename, inputname(3));
            obj.perm_t = perm_t;

            %% compute the permutation
            switch perm_t
                case {Permutation_t.NONE, Permutation_t.AUTO}
                case Permutation_t.AMD
                    ApE = A - E;
                    obj.perm_col = amd(ApE);
                    obj.perm_row = obj.perm_col;
                case Permutation_t.COLAMD
                    ApE = A - E;
                    obj.perm_col = colamd(ApE);
                case Permutation_t.SYMAMD
                    ApE = A - E;
                    obj.perm_col = symamd(ApE);
                    obj.perm_row = obj.perm_col;
                case Permutation_t.DISSECT
                    ApE = A - E;
                    obj.perm_col = dissect(ApE);
                    obj.perm_row = obj.perm_col;
                case Permutation_t.SYMRCM
                    ApE = A - E;
                    obj.perm_row = symrcm(ApE);
                    obj.perm_col = obj.perm_row;
                case Permutation_t.COLPERM
                    ApE = A - E;
                    obj.perm_col = colperm(ApE);
                    if issymmetric(A) && issymmetric(E)
                        obj.perm_row = obj.perm_col;
                    end
                otherwise
                    error('not implemented');
            end

            %% compute inverse permutations
            obj.perm_row_inv(obj.perm_row) = 1:length(obj.perm_row);
            obj.perm_col_inv(obj.perm_col) = 1:length(obj.perm_col);

            %% permute A and E
            obj.A_perm = A;
            obj.E_perm = E;
            if ~isempty(obj.perm_col)
                obj.A_perm = obj.A_perm(:,obj.perm_col);
                obj.E_perm = obj.E_perm(:,obj.perm_col);
            end

            if ~isempty(obj.perm_row)
                obj.A_perm = obj.A_perm(obj.perm_row,:);
                obj.E_perm = obj.E_perm(obj.perm_row,:);
            end
        end


        function x = solve(obj, shift, b)
            %SOLVE  Solve the linear system (A + shift*E)x = b.
            %
            %   X = SHIFTEDSOL.SOLVE(SHIFT, B) solve the shifted
            %   system (A + SHIFT*E)X = B. SHIFT must be a scalar.
            %
            %   See also PERMUTATION_T, RADI, TEST_RADI.
            %
            %   Author: Maximilian Behr

            %% check input arguments
            validateattributes(shift, {'double'}, {'scalar', 'finite', 'nonnan'}, mfilename, inputname(2));
            n = size(obj.A_perm,1);
            validateattributes(b, {'double'}, {'2d', 'size', [n, NaN] ,'finite', 'nonnan'}, mfilename, inputname(3));

            %% get permutation from
            if (obj.perm_t == Permutation_t.AUTO) && obj.first_call

                % set first call to false
                obj.first_call = false;

                % set spparms to tight to obtain a better fill-in reducing reordering
                default_spparms = spparms();
                spparms('tight');

                % compute decomposition and solve
                dApE = decomposition(obj.A_perm + shift*obj.E_perm);
                x = dApE \ b;

                % reset spparms
                spparms(default_spparms);

                % extract the permutation
                underlying = dApE.Underlying;
                if isa(underlying, 'matlab.internal.decomposition.SparseLU')
                    % FORMULA: A = S * Pleft' * L * U *Pright''
                    fprintf('Detected Sparse LU\n');
                    obj.perm_row = underlying.umfpack_.factors.p;
                    obj.perm_row_inv(obj.perm_row) = 1:length(obj.perm_row);
                    obj.perm_col = underlying.umfpack_.factors.q;
                    obj.perm_col_inv(obj.perm_col) = 1:length(obj.perm_col);

                    % DEBUGMODE
                    if obj.DEBUGMODE
                        S = underlying.factors.S;
                        Pleft = underlying.factors.Pleft;
                        L = underlying.factors.L;
                        U = underlying.factors.U;
                        Pright = underlying.factors.Pright;
                        Atemp = (obj.A_perm + shift*obj.E_perm);
                        formula_err = norm(Atemp - S*Pleft'*L*U*Pright','fro');
                        Atemp = S\(obj.A_perm + shift*obj.E_perm);
                        perm_err = norm(Atemp(obj.perm_row,obj.perm_col) - L*U,'fro');
                        fprintf('DEBUGMODE: formula error      = %.2e\n', formula_err);
                        fprintf('DEBUGMODE: permutation error  = %.2e\n', perm_err);
                        assert(formula_err < 1e-5);
                        assert(perm_err < 1e-5);
                    end
                elseif isa(underlying, 'matlab.internal.decomposition.SparseCholesky')
                    fprintf('Detected Sparse Cholesky\n');
                    % FORMULA: A = P L L' P'
                    obj.perm_col_inv = underlying.factors.P*([1:n]'); %#ok<*NBRAK>
                    obj.perm_row_inv = obj.perm_col_inv;
                    obj.perm_col(obj.perm_col_inv) = 1:length(obj.perm_col_inv);
                    obj.perm_row = obj.perm_col;

                    % DEBUGMODE
                    if obj.DEBUGMODE
                        P = underlying.factors.P;
                        L = underlying.factors.L;
                        Atemp = obj.A_perm + shift*obj.E_perm;
                        scale = dApE.ScaleFactor;
                        formula_err = norm(scale*Atemp - P*(L*L')*P','fro');
                        perm_err = norm(scale*Atemp(obj.perm_row,obj.perm_col) - L*L','fro');
                        fprintf('DEBUGMODE: formula error      = %.2e\n', formula_err);
                        fprintf('DEBUGMODE: permutation error  = %.2e\n', perm_err);
                        assert(formula_err < 1e-5);
                        assert(perm_err < 1e-5);
                    end
                elseif isa(underlying, 'matlab.internal.decomposition.SparseLDL')
                    % FORMULA: A = S \ P * L * D * L' * P' / S
                    fprintf('Detected Sparse LDL\n');
                    obj.perm_col_inv = underlying.factors.P*([1:n]');
                    obj.perm_row_inv = obj.perm_col_inv;
                    obj.perm_col(obj.perm_col_inv) = 1:length(obj.perm_col_inv);
                    obj.perm_row = obj.perm_col;

                    % DEBUGMODE
                    if obj.DEBUGMODE
                        S = underlying.factors.S;
                        P = underlying.factors.P;
                        L = underlying.factors.L;
                        D = underlying.factors.D;
                        Atemp = obj.A_perm + shift*obj.E_perm;
                        formula_err = norm(Atemp - S\P*L*D*L'*P'/S,'fro');
                        Atemp = S*(obj.A_perm + shift*obj.E_perm)*S;
                        perm_err = norm(Atemp(obj.perm_row,obj.perm_col) - L*D*L','fro');
                        fprintf('DEBUGMODE: formula error      = %.2e\n', formula_err);
                        fprintf('DEBUGMODE: permutation error  = %.2e\n', perm_err);
                        assert(formula_err < 1e-5);
                        assert(perm_err < 1e-5);
                    end
                elseif isa(underlying, 'matlab.internal.decomposition.Banded')
                    fprintf('Banded solver was choosen by decomposition. No permutation extracted.\n');
                elseif isa(underlying, 'matlab.internal.decomposition.Diagonal')
                    fprintf('Diagonal solver was choosen by decomposition. No permutation extracted.\n');
                elseif strcmpi(dApE.Type, 'permutedTriangular')
                    fprintf('permutedTriangular solver was choosen by decomposition. No permutation extracted.\n');
                elseif isa(underlying, 'matlab.internal.decomposition.SparseTriangular')
                    % FORMULA: A = Pleft * T * Pright'
                    fprintf('Detected Sparse Triangular\n');
                    obj.perm_col = underlying.factors.Pright*([1:n]');
                    obj.perm_col_inv(obj.perm_col) = 1:length(obj.perm_col);
                    obj.perm_row_inv = underlying.factors.Pleft*([1:n]');
                    obj.perm_row(obj.perm_row_inv) = 1:length(obj.perm_row_inv);

                    % DEBUG CODE
                    if obj.DEBUGMODE
                        T = underlying.factors.T;
                        Pleft = underlying.factors.Pleft;
                        Pright = underlying.factors.Pright;
                        Atemp = obj.A_perm + shift*obj.E_perm;
                        formula_err = norm(Atemp - Pleft * T * Pright','fro');
                        perm_err = norm(Atemp(obj.perm_row,obj.perm_col) - Pleft * T * Pright','fro');
                        fprintf('DEBUGMODE: formula error      = %.2e\n', formula_err);
                        fprintf('DEBUGMODE: permutation error  = %.2e\n', perm_err);
                        assert(formula_err < 1e-5);
                        assert(perm_err < 1e-5);
                    end
                else
                    warning('Cannot extract permutation from underlying decomposition object');
                end

                % permute the matrices
                if ~isempty(obj.perm_col)
                    obj.A_perm = obj.A_perm(:,obj.perm_col);
                    obj.E_perm = obj.E_perm(:,obj.perm_col);
                end

                if ~isempty(obj.perm_row)
                    obj.A_perm = obj.A_perm(obj.perm_row,:);
                    obj.E_perm = obj.E_perm(obj.perm_row,:);
                end

                return;
            end

            %% permute and solve
            if isempty(obj.perm_row)
                x_perm = (obj.A_perm + shift*obj.E_perm)\b;
            else
                x_perm = (obj.A_perm + shift*obj.E_perm)\b(obj.perm_row,:);
            end

            if isempty(obj.perm_col)
                x = x_perm;
            else
                x = x_perm(obj.perm_col_inv,:);
            end

            %% set first_call to false
            obj.first_call = false;
        end
    end
end