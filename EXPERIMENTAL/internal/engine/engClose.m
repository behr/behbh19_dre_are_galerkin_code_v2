function engClose(engine)
%ENGCLOSE    Interface to engClose routine of MATLAB Engine API for C.
%
%  Read also: https://de.mathworks.com/help/matlab/apiref/engclose.html
%
%  # Purpose:
%
%  TBD.
%
%  # Calling Sequence:
%
%    ENGCLOSE(ENGINE)
%
%  Internal function.
%
%  See also ENGOPEN.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(1, 1);
nargoutchk(0, 0);
validateattributes(engine, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1), 1);
mextools_call('engClose', engine);