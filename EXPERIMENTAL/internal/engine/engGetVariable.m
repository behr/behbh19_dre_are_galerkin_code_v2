function var = engGetVariable(engine, varname)
%ENGGETVARIABLE    Interface to engGetVariable routine of MATLAB Engine API for C.
%
%  Read also: https://de.mathworks.com/help/matlab/apiref/enggetvariable.html
%
%  # Purpose:
%
%  TBD.
%
%  # Calling Sequence:
%
%    VAR = ENGGETVARIABLE(ENGINE, VARNAME)
%
%  Internal function.
%
%  See also ENGCLOSE, ENGOPEN AND ENGEVALSTRINGPARALLEL.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(2, 2);
nargoutchk(0, 1);
validateattributes(engine, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1), 1);
validateattributes(varname, {'char'}, {'scalartext', 'nonempty'}, mfilename, inputname(2), 2);
var = mextools_call('engGetVariable', engine, varname);
