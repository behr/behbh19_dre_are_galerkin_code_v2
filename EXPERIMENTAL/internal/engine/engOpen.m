function engine = engOpen(cmd)
%ENGOPEN    Interface to engOpen routine of MATLAB Engine API for C.
%
%  Read also: https://de.mathworks.com/help/matlab/apiref/engopen.html
%
%  # Purpose:
%
%  TBD.
%
%  # Calling Sequence:
%
%    ENGINE = ENGOPEN(CMD)
%
%  Internal function.
%
%  See also ENGCLOSE.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(1, 1);
nargoutchk(1, 1);
validateattributes(cmd, {'char'}, {'scalartext', 'nonempty'}, mfilename, inputname(1), 1);
engine = mextools_call('engOpen', cmd);