function engPutVariable(engine, varname, variable)
%ENGPUTVARIABLE    Interface to engPutVariable routine of MATLAB Engine API for C.
%
%  Read also: https://de.mathworks.com/help/matlab/apiref/engputvariable.html
%
%  # Purpose:
%
%  TBD.
%
%  # Calling Sequence:
%
%    ENGPUTVARIABLE(ENGINE, VARNAME, VARIABLE)
%
%  Internal function.
%
%  See also ENGCLOSE, ENGOPEN AND ENGGETVARIABLE.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(3, 3);
nargoutchk(0, 0);
validateattributes(engine, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1), 1);
validateattributes(varname, {'char'}, {'scalartext', 'nonempty'}, mfilename, inputname(2), 2);
mextools_call('engPutVariable', engine, varname, variable);