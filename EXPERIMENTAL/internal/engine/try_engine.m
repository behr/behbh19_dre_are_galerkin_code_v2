% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020

%% clear all
clear all, close all

%% engines
user = 'behr';
slaves = {'node067', 'node070', 'node071'};
login_command = @(node) sprintf('ssh %s@%s matlab', user, node);

%% try to open engines
engines = cell(1, numel(slaves));

for i = 1:numel(engines)
    slave = slaves{i};
    fprintf('Try to start engine %s\n', slave);
    engines{i} = engOpen(login_command(slave));
end


%% compute something on the slaves slaves
results = cell(1, numel(engines));
for i = 1:numel(engines)
    fprintf('Try to get hostname on engine %u\n', engines{i});
    engEvalString(engines{i}, 'rng(1); result=cond(rand(1000));');
    results{i} = engGetVariable(engines{i}, 'result');
end

for i = 1:numel(engines)
   fprintf('engine/results: %u/%s/%s/%e\n', engines{i}, lower(dec2hex(engines{i})), hostnames{i}, results{i});
end

%% try to close the engines
for i = 1:numel(engines)
   fprintf('Try to close the engines %u\n', engines{i});
   engClose(engines{i});
end

