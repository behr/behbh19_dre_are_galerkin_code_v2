function engEvalStringParallel(engines, cmds)
%ENGEVALSTRINGPARALLEL    Interface to engEvalString routine of MATLAB Engine API for C.
%
%
%  Read also: https://de.mathworks.com/help/matlab/apiref/engevalstring.html
%
%  # Purpose:
%
%  TBD.
%
%  # Calling Sequence:
%
%    ENGEVALSTRINGPARALLEL(ENGINES, CMDS)
%
%    ENGINES is a cell array of containing pointers to engines.
%    CMDS is a cell array of nonempty strings representing the command
%    which should be executed on the engine. This means
%    the command CMDS{i} is executed on the engine ENGINES{i}.
%    All engines must be different no duplicates are allowed.
%    CMDS and ENGINES must be of the same shape.
%
%  Internal function.
%
%  See also ENGCLOSE, ENGOPEN AND ENGEVALSTRING.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(2, 2);
nargoutchk(0, 0);
validateattributes(engines, {'cell'}, {'nonempty'}, mfilename, inputname(1), 1);
shape = size(engines);
validateattributes(cmds, {'cell'}, {'nonempty', 'size', shape}, mfilename, inputname(2), 2);
assert(numel(engines) == numel(cmds), '%s and %s have different numbers of elements.', inputname(1), inputname(2));
for i = numel(engines)
    validateattributes(engines{i}, {'uint64'}, {'scalar', 'real', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>', 0}, mfilename, inputname(1), 1);
    validateattributes(cmds{i}, {'char'}, {'scalartext', 'nonempty'}, mfilename, inputname(2), 2);
end
assert(numel(engines) == numel(unique(cell2mat(engines))), '%s contains duplicated engines', inputname(1));
mextools_call('engEvalStringParallel', engines, cmds);