%PERMUTATION_T   Enumeration for fill-in reducing permutation methods.
%
%   Currently supported fill-in reducing permutation methods
%       - NONE          no permutation is reused.
%       - AUTO          permutation is reused from decomposition object
%       - AMD           approximate minimum degree permutation
%       - COLAMD        column approximate minimum degree permutation.
%       - SYMAMD        symmetric approximate minimum degree permutation.
%       - DISSECT       nested dissection permutation.
%       - SYMRCM        symmetric reverse Cuthill-McKee reordering.
%       - COLPERM       column based nonzero count
%
%   If AUTO is chosen then the permutation is tried to be extracted
%   from the decomposition object.
%
%   AMD:
%       * row and column permutation, symmetric and nonsymmetric matrices
%
%   COLAMD:
%       * column permutation, non-symmetric matrices
%
%   SYMAMD:
%       * row and column permutation, symmetric matrices
%
%   DISSECT:
%       * row and column permutation, symmetric and nonsymmetric matrices
%
%   SYMRCM:
%       * row and column permutation, symmetric and nonsymmetric matrices
%
%   COLPERM:
%       * row and column permutation if the matrix is symmetric
%       * column permutation if the matrix is nonsymmetric
%
%   See also AMD, COLAMD, SYMAMD, DISSECT, SYMRCM, COLPERM and SHIFTEDSOLVER.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

classdef (Sealed = true) Permutation_t < int8
    enumeration
        % No permutation is used.
        NONE(0),
        % Permutation is reused from decomposition object
        AUTO(1),
        % Approximate minimum degree permutation.
        AMD(2),
        % Column approximate minimum degree permutation.
        COLAMD(3),
        % Symmetric approximate minimum degree permutation.
        SYMAMD(4),
        % Nested dissection permutation.
        DISSECT(5),
        % Symmetric reverse Cuthill-McKee reordering.
        SYMRCM(6),
        % Column based nonzero count
        COLPERM(7)
    end
end
