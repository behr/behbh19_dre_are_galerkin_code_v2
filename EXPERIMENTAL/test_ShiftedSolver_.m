classdef test_ShiftedSolver < matlab.unittest.TestCase
    %TEST_SHIFTEDSOLVER   Test for ShiftedSolver class.
    %
    %   TEST_SHIFTEDSOLVER tests the functionality of the ShiftedSolver
    %   class.
    %
    %   TEST_SHIFTEDSOLVER properties:
    %       instance    - problem instance
    %       shift       - shift parameter
    %       modifier    - modifes the matrices
    %       randperm    - apply a random column permutation to the modified matrix
    %       nrhs        - number of right hand side
    %       perm_t      - Permutation_t instance permutation type
    %
    %   The modifier modifies the matrix to enforce some structural
    %   properties triangular, diagonal,
    %   tridiagonal, positive / negative diagonal. This increases
    %   the coverage rate for detected decompositions in AUTO mode.
    %   If permcols is true a random column permutation is
    %   applied to the modified matrix to detect the case of
    %   a morally permutated triangular matrix of the internal
    %   decomposition object in AUTO mode.
    %
    %   If something seems to be weird turn the DEBUGMODE in ShiftedSolver
    %   on.
    %
    %   TEST_SHIFTEDSOLVER methods:
    %       test_ShiftedSol    - Test for ShiftedSolver.
    %
    %   See also SHIFTEDSOLVER, PERMUTATION_T, RADI and RADI_OPTIONS.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        instance = { 'rail_371', 'conv_diff_1_100', 'fss_K10M2Q2_20', 'build'};
        nrhs = {3};
        modifier = { ...
            @(x) x, ...
            @(x) -x, ...
            @(x) sparse(diag(diag(x,-1),-1) + diag(diag(x,0),0) + diag(diag(x,1),1)),...
            @(x) (x -diag(diag(x)) + sparse(diag(ones(size(x,1),1)))), ...
            @(x) (x -diag(diag(x)) - sparse(diag(ones(size(x,1),1)))), ...
            @(x) triu(x), ...
            @(x) tril(x), ...
            @(x) diag(diag(x))
            };
        permcols = {false, true};
        use_umfpack = {false, true};
        perm_t = { ...
            Permutation_t.NONE, ...
            Permutation_t.AUTO, ...
            Permutation_t.AMD, ...
            Permutation_t.COLAMD, ...
            Permutation_t.SYMAMD, ...
            Permutation_t.DISSECT, ...
            Permutation_t.SYMRCM, ...
            Permutation_t.COLPERM
            };
    end

    properties
        abstol = 1e-8;
        reltol = 1e-10;
        verbose = ~isCIrun();
        randseed = 1;
        shifts = [1, 1 + 1i, -1 + 1i, -1, -1 - 1i, -1i, -1 + 1i];
    end

    methods(Test)
        function test_ShiftedSol(obj, instance, nrhs, modifier, permcols, use_umfpack, perm_t)

            %% load instance
            data = load(instance);
            A = data.A;
            n = size(A, 1);

            if isfield(data, 'E')
                E = data.E;
            else
                E = speye(size(A));
            end

            A = modifier(A);
            E = modifier(E);

            if permcols
                p = randperm(size(A,1));
                A = A(:,p);
                E = E(:,p);
            end

            % set umfpack on off
            default_spparms = spparms();
            if use_umfpack
                spparms('umfpack',1);
            else
                spparms('umfpack',0);
            end

            %% set random seed
            rng(obj.randseed);
            b = rand(n, nrhs);
            nrmb = norm(b);

            %% solve with shiftedsolver
            shiftedsol = ShiftedSolver(A, E, perm_t);
            for shift = obj.shifts
                x = shiftedsol.solve(shift, b);

                %% compute residuals
                abs2res = norm((A + shift * E)*x-b);
                rel2res = abs2res / nrmb;

                %% print
                is_abstol = abs2res < obj.abstol;
                is_reltol = rel2res < obj.reltol;
                failed = ~(is_abstol && is_reltol);
                if failed || obj.verbose
                    fprintf('\n');
                    if failed
                        fprintf('FAILED:\n');
                    end
                    fprintf('%s, %s, %s, permcols = %d, use_umfpack = %d, shift = %.2e + %.2e*i, nrhs = %d\n', ...
                        instance, char(perm_t), func2str(modifier), permcols, use_umfpack, real(shift), imag(shift), nrhs);
                    fprintf('is_abstol = %d\n', is_abstol);
                    fprintf('is_reltol = %d\n', is_reltol);
                    fprintf('abs./rel. 2-norm res = %.2e / %.2e\n', abs2res, rel2res);
                    fprintf('abs./rel. 2-norm tol = %.2e / %.2e\n', obj.abstol, obj.reltol);
                end

                %% check
                obj.fatalAssertLessThan(abs2res, obj.abstol);
                obj.fatalAssertLessThan(rel2res, obj.reltol);

                %% reset the spparms settings
                spparms(default_spparms);
            end
        end
    end
end
