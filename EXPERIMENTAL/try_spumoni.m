%TRY_SPUMONI    Playaround script with spumoni.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%
%% clear
clearvars, close all;
clc

%% load matrix
%instance = 'rail_79841';
%instance = 'rail_5177';
instance = 'rail_1357';
%instance = 'rail_371';
%instance = 'flow_v05';
%instance = 'chip_v0';
%instance = 'filter2D';
%instance = 'conv_diff_3_10000';
data = load(instance);
A_orig = data.A;
if isfield(data,'E')
    E_orig = data.E;
else
    E_orig = speye(size(A_orig));
end
B_orig = data.B;
C_orig = data.C;
RHS = C_orig';


%% solve and log
spparms('default');
spparms('umfpack',1);
spparms('spumoni',2);
spparms('tight');
tic
X = (A_orig+ (-1+2i)*E_orig)\RHS; %#ok<*NASGU>
toc

tic
dA = decomposition(A_orig+ (-1+2i)*E_orig);
X = dA\RHS;
toc

tic
X = dA\RHS;
toc
