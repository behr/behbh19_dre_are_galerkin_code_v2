Matrices from Rail Example

@MastersThesis{Saa03,
author = {J. Saak},
title = {Effiziente numerische {L}"osung eines {O}ptimalsteuerungsproblems f"ur die {A}bk"uhlung von Stahlprofilen},
school = {Fachbereich 3/Mathematik und Informatik,
Universit"at Bremen},
year = {2003},
type = {Diplomarbeit},
address = {D-28334 Bremen},
month = sep }

fdm_2d_matrix.m and fdm_2d_vector.m are parts from LYAPACK
@MISC{lyapack,
  author =       {T. Penzl},
  title =        {LYAPACK - A MATLAB Toolbox for Large Lyapunov and Riccati Equations, Model Reduction Problems, and Linear–Quadratic Optimal Control Problems},
  howpublished = {netlib},
  note =         {Version 1.0},
  url =          {http://www.netlib.org/lyapack},
  year =         {1999}
}

Examples in slicot_benchmark are taken from:
http://slicot.org/20-site/126-benchmark-examples-for-model-reduction

fss.m models the Flexible Space Structures Model from MORwiki

@MISC{morwiki-flexspacstruc,
  author =       {{The MORwiki Community}},
  title =        {Flexible Space Structures},
  howpublished = {{MORwiki} -- Model Order Reduction Wiki},
  url =          {http://modelreduction.org/index.php/Flexible_Space_Structures},
  year =         2018
}
