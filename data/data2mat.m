%DATA2MAT   Read matrices from mtx file in data directory
%   and write to mat binary files.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear all
clearvars, clc, close all

%% verbose
verbose = ~isCIrun();

%% read rail, chip, flow, filter and thruster matrices
basenames = {'rail_371', 'rail_1357', 'rail_5177', 'rail_20209', 'rail_79841', 'chip_v0', 'chip_v01', 'filter2D', 'flow_v0', 'flow_v05', 'T2DAH', 'T2DAL', 'GasSensor'};
for i_b = 1:length(basenames)
    basename = basenames{i_b};

    mat = @(name) sprintf('%s.%s', basename, name);
    A = mmread(which(mat('A')));
    E = mmread(which(mat('E')));
    B = mmread(which(mat('B')));

    if strcmp(basename, 'spiral_inductor_peec')
        B = full(B);
        C = full(B)';
    else
        C = mmread(which(mat('C')));
        B = full(B);
        C = full(C);
    end

    % check if matrix is numerically symmetric and symmetrize if necessary
    if ~ishermitian(A)
        rel_sym_error = norm(A-A', 1) / norm(A, 1);
        if rel_sym_error < eps
            fprintf('%s: A is numerical hermitian: ||A - A''||_1 / ||A||_1 = %.2e.\n', basename, rel_sym_error);
            A = 1 / 2 * (A + A');
        end
    end

    if ~ishermitian(E)
        rel_sym_error = norm(E-E', 1) / norm(E, 1);
        if rel_sym_error < eps
            fprintf('%s: E is numerical hermitian: ||A - A''||_1 / ||A||_1 = %.2e.\n', basename, rel_sym_error);
            E = 1 / 2 * (E + E');
        end
    end

    % create filename for mat file
    [datadir, dataname, datapext] = fileparts(which(mat('A')));
    datamatname = fullfile(datadir, sprintf('%s.mat', basename));
    save(datamatname, 'A', 'E', 'B', 'C');
end

%% COOKIE EXAMPLE
data = load('ABCE');
mu = sqrt(10)*[0.2; 0.4; 0.6; 0.8];
A = data.A0 + mu(1)*data.A1 + mu(2) *data.A2 + mu(3) * data.A3 +mu(4)*data.A4;
E = data.E;
B = full(data.B);
C = full(data.C);
[datadir, ~, ~] = fileparts(which('ABCE.mat'));
datamatname = fullfile(datadir, 'cookie.mat');
save(datamatname, 'A', 'E', 'B', 'C');

%% convection diffusion problem 1-4 with differenz sizes
nodes_list = 5:5:100;
problems = [1, 2, 3, 4];
[datadir, ~, ~] = fileparts(mfilename('fullpath'));
datadir = fullfile(datadir, 'conv_diff');
mkdir(datadir);
for problem = problems
    for nodes = nodes_list
        [A, B, C] = conv_diff(problem, nodes);
        B = full(B);
        C = full(C);
        if verbose
            fprintf('%s - generated model: conv_diff, problem=%d, size=%d\n', datestr(now), problem, size(A, 1));
        end
        datamatname = fullfile(datadir, sprintf('conv_diff_%d_%d.mat', problem, size(A, 1)));
        save(datamatname, 'A', 'B', 'C');
    end
end

%% flexible space structures model for different model parameters
% model parameters
KS = [10, 50, 100, 200, 500, 1000, 2500, 5000, 7500, 10000];
MS = [2, 5];
QS = [2, 5];
[datadir, ~, ~] = fileparts(mfilename('fullpath'));
datadir = fullfile(datadir, 'fss');
mkdir(datadir);
for K = KS
    for M = MS
        for Q = QS
            [A, B, C] = fss(K, M, Q);
            B = full(B);
            C = full(C);
            if verbose
                fprintf('%s - generated model: fss, K=%d, M=%d, Q=%d, size=%d\n', datestr(now), K, M, Q, size(A, 1));
            end
            datamatname = fullfile(datadir, sprintf('fss_K%dM%dQ%d_%d.mat', K, M, Q, size(A, 1)));
            save(datamatname, 'A', 'B', 'C');
        end
    end
end
