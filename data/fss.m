function [A, B, C] = fss(K, M, Q)
%FSS   Generates matrices for a flexible space structures problem.
%
%   [A, B, C] = FSS(K, M, Q) generates system matrices for the
%   flexible space structures problem from morwiki.
%   K is the number of modes, M is the number of actuators and
%   Q the number of sensors.
%
%   References:
%   https://morwiki.mpi-magdeburg.mpg.de/morwiki/index.php/Flexible_Space_Structures
%
%   See also DATA2MAT.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%

%% check input parameters
validateattributes(K, {'double'}, {'real', 'scalar', 'finite', 'nonnan', 'positive', 'nonsparse'}, mfilename, inputname(1));
validateattributes(M, {'double'}, {'real', 'scalar', 'finite', 'nonnan', 'positive', 'nonsparse'}, mfilename, inputname(2));
validateattributes(Q, {'double'}, {'real', 'scalar', 'finite', 'nonnan', 'positive', 'nonsparse'}, mfilename, inputname(3));

%% sample damping ration and sample natural frequencies
rng(1009);
xi = rand(1, K) * 0.001;
omega = rand(1, K) * 100.0;

%% generate matrices
A_k = cellfun(@(p) sparse([-2.0 * p(1) * p(2), -p(2); p(2), 0]), num2cell([xi; omega], 1), 'UniformOutput', 0);
A = blkdiag(A_k{:});
B = kron(rand(K, M), [1; 0]);
C = 10.0 * rand(Q, 2*K);
end
