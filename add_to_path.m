%ADD_TO_PATH    Script adds all necessary directories to path.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

s = pwd;
addpath(s);
addpath(genpath(sprintf('%s/ABSTRACT', s)));
addpath(genpath(sprintf('%s/ARE', s)));
addpath(genpath(sprintf('%s/ARE_GALERKIN_DRE', s)));
addpath(genpath(sprintf('%s/BANDED', s)));
addpath(genpath(sprintf('%s/COMPARE_REFERENCE_DRE', s)));
addpath(genpath(sprintf('%s/DATALOGGER', s)));
addpath(genpath(sprintf('%s/EXAMPLES/', s)));
addpath(genpath(sprintf('%s/HELPERS', s)));
addpath(sprintf('%s/MEXTOOLS', s));
addpath(genpath(sprintf('%s/MODIFIED_DAVISON_MAKI_DRE', s)));
addpath(sprintf('%s/QRTOOLS', s));
addpath(genpath(sprintf('%s/REFERENCE_SOLUTION_DRE', s)));
addpath(genpath(sprintf('%s/SOLUTIONFORMULA_DRE', s)));
addpath(sprintf('%s/SPARSE_DIRECT', s));
addpath(sprintf('%s/SPARSE_DIRECT/internal/CHOLMOD', s));
addpath(sprintf('%s/SPARSE_DIRECT/internal/MA57', s));
addpath(sprintf('%s/SPARSE_DIRECT/internal/UMFPACK', s));
addpath(sprintf('%s/SPLITTING2_DRE', s));
addpath(genpath(sprintf('%s/data', s)));
addpath(genpath(sprintf('%s/data_reference_solution', s)));
addpath(genpath(sprintf('%s/mmread', s)));


mext = mexext();

%% check if mextools_call was compiled
MEX_FILES = {'mextools_call'};
for i_MEX_FILE = 1:numel(MEX_FILES)
    MEX_FILE = sprintf('%s.%s', MEX_FILES{i_MEX_FILE}, mext);
    if ~(exist(MEX_FILE, 'file') == 3)
        warning('mextools_call seems not to be compiled, cannot find MEX-file %s.', MEX_FILE);
    end
end

%% check if MDM_SYM_DRE_STEP was compiled
MEX_FILES = {'MDM_SYM_DRE_STEP'};
for i_MEX_FILE = 1:numel(MEX_FILES)
    MEX_FILE = sprintf('%s.%s', MEX_FILES{i_MEX_FILE}, mext);
    if ~(exist(MEX_FILE, 'file') == 3)
        warning('Cannot find MEX-file %s.', MEX_FILE);
    end
end

%% check if MDM_SYM_DRE_STEP MEX parameter file exist
PARAMETERFILE = 'parameters_MDM_SYM_DRE_STEP.mtx';
if ~(exist(PARAMETERFILE, 'file') == 2)
    warning('Cannot find file %s with optimal parameter setting for MDM_SYM_DRE_STEP MEX interface.', PARAMETERFILE);
end

%% clear
clear s MEX_FILES MEX_FILE mext i_MEX_FILE PARAMETERFILE
