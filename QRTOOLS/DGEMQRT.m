function QC = DGEMQRT(Qi, T, C)
%DGEMQRT    Interface to LAPACK dgemqrt routine.
%
%   QC = DGEMQRT(QI, T, C) for documentation see DGEQMQRT.c
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(3, 3);
nargoutchk(1, 1);
validateattributes(Qi, {'double'}, {'real', '2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
[m, minmn] = size(Qi);
validateattributes(T, {'double'}, {'real', '2d', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'ncols', minmn}, mfilename, inputname(2), 2);
validateattributes(C, {'double'}, {'real', '2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(3), 3);
assert(size(C, 1) <= m, '%s has wrong number of rows', inputname(3));
QC = mextools_call('DGEMQRT', Qi, T, C);
end
