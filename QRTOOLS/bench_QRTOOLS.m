%BENCH_QRTOOLS
%   Benchmark script for QRTOOLS routines.
%
%   See also BENCH_QRTOOLS.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, close all; %clc

%% data for benchmark run
% size of matrix
if isCIrun()
    ms = 256:512:1792;
    ns = 64:32:128;
    runs = 5;
else
    ms = 256:512:8192;
    ns = 64:32:256;
    runs = 5;
end
verbose = ~isCIrun();

%% warm up the machine using dgemm
if verbose
    fprintf('%s\n', datestr(now));
    fprintf('# warmup the machine using dgemm and copy operations #\n');
end

if ~isCIrun()
    b = rand(2000, 2000);
    for i = 1:50, a = b;
        c = a;
        a = a + b * c;
    end
end

%% compare and QR and measue time
% print headers
methods = {'QR', 'DGEQRF', 'DGEQRT'};
if verbose
    fprintf('%35s%14s%10s\n', methods{:});
end
for m = ms
    for n = ns
        % create some data for QR
        A = rand(m, n);
        c = ceil(n/3) + 1;
        C = rand(n, c);

        methods_wtime(numel(methods)) = 0;
        % standard QR and application
        wtime(runs) = 0; %#ok<*SAGROW>
        for i = 1:runs
            t = tic;
            [Q, R] = qr(A, 0);
            QC = Q * C;
            t = toc(t);
            wtime(i) = t;
        end
        methods_wtime(1) = median(wtime);

        % DGEQRF and application
        wtime(runs) = 0;
        for i = 1:runs
            t = tic;
            [Qi, Tau, R] = DGEQRF(A);
            QC = DORMQR(Qi, Tau, C);
            t = toc(t);
            wtime(i) = t;
        end
        methods_wtime(2) = median(wtime);

        % DGEQRT and application
        wtime(runs) = 0;
        for i = 1:runs
            t = tic;
            [Qi, T, R] = DGEQRT(A);
            QC = DGEMQRT(Qi, T, C);
            t = toc(t);
            wtime(i) = t;
        end
        methods_wtime(3) = median(wtime);

        %% compute the fastes
        [~, idx_sort] = sort(methods_wtime);

        % print wtime info
        if verbose
            fprintf('m = %5d, n = %5d, c = %5d, %f, %f, %f fastest to slowest = %s/%s/%s, speedup to QR = %f\n', ...
                m, n, c, methods_wtime(:), methods{idx_sort}, methods_wtime(1)/methods_wtime(idx_sort(1)));
        end
    end
end
