function Q = DORGQR(Qi, Tau, l)
%DORGQR    Interface to LAPACK dorgqr routine.
%
%   Q = DORGQR(QI, TAU, L) for documentation see DORGQR.c
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
narginchk(3, 3);
nargoutchk(1, 1);
validateattributes(Qi, {'double'}, {'real', '2d', 'nonsparse', 'nonempty', 'finite', 'nonnan'}, mfilename, inputname(1), 1);
[m, minmn] = size(Qi);
validateattributes(Tau, {'double'}, {'real', 'vector', 'nonsparse', 'nonempty', 'finite', 'nonnan', 'nrows', minmn, 'ncols', 1}, mfilename, inputname(2), 2);
validateattributes(l, {'numeric'}, {'real', 'scalar', 'integer', 'nonsparse', 'nonempty', 'finite', 'nonnan', '>=', minmn, '<=', m}, mfilename, inputname(3), 3);
Q = mextools_call('DORGQR', Qi, Tau, l);
end
