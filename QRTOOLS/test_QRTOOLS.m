classdef test_QRTOOLS < matlab.unittest.TestCase
    %TEST_QRTOOLS   Test for QR functions defined in QRTOOLS directory.
    %
    %   TEST_QRTOOLS tests the functionality of the QR factorization,
    %   application and generation routines.
    %
    %   TEST_QRTOOLS properties:
    %       meth    - method for factorization
    %       m       - Number of rows of matrix A
    %       n       - Number of columns of matrix A
    %       c1      - Number of rows of matrix C
    %       c2      - Number of columns of matrix C
    %
    %   TEST_QRTOOLS methods:
    %       test_QR    - Test for DGEQRF, DORGQR, DORMQR, DGEQRT and
    %       DGEMQRT.
    %
    %   See also DGEQRF, DORGQR, DORMQR, DGEQRT and DGEMQRT.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        meth = {'QRF', 'QRT'};
        m = {1, 3, 12, 67, 117, 254, 387};
        n = {1, 3, 12, 67, 117, 254, 387};
        c1 = {1, 3, 12, 67, 117, 254, 387};
        c2 = {1, 3, 12, 67, 117, 254, 387};
    end

    properties
        abstol = 1e-8;
        verbose = ~isCIrun();
        randseed = 1;
    end

    methods(Test)
        function test_QR(obj, meth, m, n, c1, c2)

            %% check the meth def
            if ~(strcmp(meth, 'QRF') || strcmp(meth, 'QRT'))
                error('Unknown QR method %s', meth);
            end

            %% set random seed
            rng(obj.randseed);

            %% dimension of C must fit
            c1 = min(c1, m);

            %% generate random matrix
            A = rand(m, n);
            C = rand(c1, c2);
            minmn = min(m, n);

            %% factorization DGEQRF and DGEQRT
            if strcmp(meth, 'QRF')
                [Qi, T, R] = DGEQRF(A);
            elseif strcmp(meth, 'QRT')
                [Qi, T, R] = DGEQRT(A);
            end

            %% application DORMQR, DGEMQRT
            if strcmp(meth, 'QRF')
                QC = DORMQR(Qi, T, C);
            elseif strcmp(meth, 'QRT')
                QC = DGEMQRT(Qi, T, C);
            end

            %% generation DORGQR, DGEMQRT
            if strcmp(meth, 'QRF')
                Qfull = DORGQR(Qi, T, m);
                Qred = DORGQR(Qi, T, minmn);
            elseif strcmp(meth, 'QRT')
                % generate by application
                Qfull = DGEMQRT(Qi, T, eye(m, m));
                Qred = DGEMQRT(Qi, T, eye(m, minmn));
            end

            %% generate full version of R = [R; zeros()]
            Rfull = R;
            if m > n
                Rfull = [R; zeros(m-n, n)];
            end

            %% test upper triangular
            is_triuR = istriu(R);

            %% test lower triangular
            is_trilQi = istril(Qi);

            %% check the application routine
            abserr_QC = norm(QC-Qfull(:, 1:c1)*C, 1);
            is_QC = abserr_QC < obj.abstol;

            %% test orthogonality
            abserr_orthQfull = norm(Qfull'*Qfull-eye(m, m), 1);
            abserr_orthQred = norm(Qred'*Qred-eye(minmn, minmn), 1);
            is_orthQfull = abserr_orthQfull < obj.abstol;
            is_orthQred = abserr_orthQred < obj.abstol;

            %% test proper factorization A = Q * R
            abserr_AequalsQfullRfull = norm(A-Qfull*Rfull, 1);
            abserr_AequalsQredR = norm(A-Qred*R, 1);
            is_AequalsQfullRfull = abserr_AequalsQfullRfull < obj.abstol;
            is_AequalsQredR = abserr_AequalsQredR < obj.abstol;

            % evaluate the checks and print message on error
            failed = ~(is_triuR && is_trilQi && is_QC && is_orthQfull && is_orthQred && is_AequalsQfullRfull && is_AequalsQredR);

            if failed || obj.verbose
                fprintf('\n');
                if failed, fprintf('FAILED:\n');
                end
                f(1) = fprintf('%s, m = %4d, n = %4d, c1 = %4d, c2 = %4d, abstol = %.2e\n', meth, m, n, c1, c2, obj.abstol);
                f(2) = fprintf('R is upper triangular         = %s\n', string(is_triuR));
                f(3) = fprintf('Qi is lower triangular        = %s\n', string(is_trilQi));
                f(4) = fprintf('Qfull(:,1:c1)*C is approx. QC = %s, abs. 2-norm err = %.2e\n', string(is_QC), abserr_QC);
                f(5) = fprintf('Qfull is orthogonal / unitary = %s, abs. 2-norm err = %.2e\n', string(is_orthQfull), abserr_orthQfull);
                f(6) = fprintf('Qred is orthogonal / unitary  = %s, abs. 2-norm err = %.2e\n', string(is_orthQred), abserr_orthQred);
                f(7) = fprintf('A is approx. Qfull * Rfull    = %s, abs. 2-norm err = %.2e\n', string(is_AequalsQfullRfull), abserr_AequalsQfullRfull);
                f(8) = fprintf('A is approx. Qred * R         = %s, abs. 2-norm err = %.2e\n', string(is_AequalsQredR), abserr_AequalsQredR);
                fprintf('%s\n', repmat('-', 1, max(f)-1));
            end

            % check
            obj.fatalAssertTrue(is_triuR);
            obj.fatalAssertTrue(is_trilQi);
            obj.fatalAssertTrue(is_QC);
            obj.fatalAssertTrue(is_orthQfull);
            obj.fatalAssertTrue(is_orthQred);
            obj.fatalAssertTrue(is_AequalsQfullRfull);
            obj.fatalAssertTrue(is_AequalsQfullRfull);
            obj.fatalAssertTrue(is_AequalsQredR);
        end
    end
end
