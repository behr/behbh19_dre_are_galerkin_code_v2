%MEXTOOLS_CALL  Gate to all mex interfaced routines internal use only.
%
%   [...] = MEXTOOLS_CALL(FUNC,...) calls the function FUNC. FUNC must be
%   the function name as string.
%
%   MEXTOOLS_CALL() prints all callable functions.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
help mextools_call
error('mextools_call mexFunction not found.');
