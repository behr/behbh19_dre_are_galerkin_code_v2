//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *  # Calling Sequence:
 *
 *    ENGINE = ENGOPEN(HOST)
 *    ENGCLOSE(HOST)
 *    ENGEVALSTRING(ENGINE, COMMAND)
 *    VAR = ENGGETVARIABLE(ENGINE, VARNAME)
 *    ENGPUTVARIABLE(ENGINE, VAR)
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* ENGINE_INTERFACE */
#include "engine_interface.h"

/* OPENMP */
#include <omp.h>

/*-----------------------------------------------------------------------------
 *  Function mex_engOpen
 *-----------------------------------------------------------------------------*/
#define MEXNAME "engOpen"

mextools_int mex_engOpen(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;
  void **engine_out = NULL;

  /*------------------------------------------------------------------------
   *  get command
   *-----------------------------------------------------------------------*/
  const char *cmd = (char *)mxArrayToString(prhs[0]);
  info = !cmd;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mxArrayToString",
                    "mxArrayToString returned a null pointer %ld", info);

  /*------------------------------------------------------------------------
   *  call engOpen
   *-----------------------------------------------------------------------*/
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME ": Try to open engine with command \"%s\".\n", cmd);
#endif
  Engine *engine = engOpen(cmd);
  info = !engine;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    MEXNAME " returned a null pointer %ld", info);
  //info = engOutputBuffer(engine, NULL, 0);
  //COND_WARNING_GOTO(info, FREE, MEXNAME ":engOutputBuffer",
  //                  "engOutputBufferr eturned a nonzero info value %ld", info);

#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME
                ": Opened engine \"%p\" with command \"%s\" successful.\n",
                (void *)engine, cmd);
#endif

  /*------------------------------------------------------------------------
   *  set output
   *-----------------------------------------------------------------------*/
  plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  engine_out = (void **)mxGetUint64s(plhs[0]);
  *engine_out = (void *)engine;

  /*------------------------------------------------------------------------
   *  return info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}

/*-----------------------------------------------------------------------------
 *  Function mex_engClose
 *-----------------------------------------------------------------------------*/
#undef MEXNAME
#define MEXNAME "engClose"

mextools_int mex_engClose(int nlhs, mxArray *plhs[], int nrhs,
                          const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(plhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;
  Engine *engine_in = NULL;

  /*------------------------------------------------------------------------
   *  get engine and close
   *-----------------------------------------------------------------------*/
  engine_in = (Engine *)(*mxGetUint64s(prhs[0]));

  /*------------------------------------------------------------------------
   *  call engClose
   *-----------------------------------------------------------------------*/
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME ": Try to close the engine \"%p\"\n",
                (void *)engine_in);
#endif
  info = engClose(engine_in);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    MEXNAME " returned a null pointer %ld", info);
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME ": Closed the engine \"%p\" successful.\n",
                (void *)engine_in);
#endif

  /*------------------------------------------------------------------------
   *  return info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}

/*-----------------------------------------------------------------------------
 *  Function mex_engEvalString
 *-----------------------------------------------------------------------------*/
#undef MEXNAME
#define MEXNAME "engEvalString"

mextools_int mex_engEvalString(int nlhs, mxArray *plhs[], int nrhs,
                               const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(plhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;
  Engine *engine_in = NULL;

  /*------------------------------------------------------------------------
   *  get engine and string to evaluate
   *-----------------------------------------------------------------------*/
  engine_in = (Engine *)(*mxGetUint64s(prhs[0]));
  const char *cmd = (char *)mxArrayToString(prhs[1]);
  info = !cmd;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mxArrayToString",
                    "mxArrayToString returned a null pointer %ld", info);

  /*------------------------------------------------------------------------
   *  call
   *-----------------------------------------------------------------------*/
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME ": Evaluate \"%s\" on the engine \"%p\".\n", cmd,
                (void *)engine_in);
#endif
  info = engEvalString(engine_in, cmd);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    "engine returned a nonzero info value %ld", info);
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME ": Evaluated \"%s\" on the engine \"%p\" successful.\n",
                cmd, (void *)engine_in);
#endif

  /*------------------------------------------------------------------------
   *  return info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}

/*-----------------------------------------------------------------------------
 *  Function mex_engEvalStringParallel
 *-----------------------------------------------------------------------------*/
#undef MEXNAME
#define MEXNAME "engEvalStringParallel"

mextools_int mex_engEvalStringParallel(int nlhs, mxArray *plhs[], int nrhs,
                                       const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(plhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0, num_elements = 0;
  Engine **engines = NULL;
  char **cmds = NULL;
  mextools_int *infos = NULL;

  /*------------------------------------------------------------------------
   *  get engines and commands
   *-----------------------------------------------------------------------*/
  num_elements = (mextools_int)mxGetNumberOfElements(prhs[0]);
  info = mextools_malloc((void **)&engines, sizeof(Engine *) * num_elements);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  info = mextools_malloc((void **)&cmds, sizeof(char *) * num_elements);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  for (mextools_int i = 0; i < num_elements; ++i) {
    // get engine
    mxArray *engine = mxGetCell(prhs[0], i);
    info = !engine;
    COND_WARNING_GOTO(info, FREE, MEXNAME ":mxGetCell",
                      "mxGetCell returned a null pointer %ld", info);
    engines[i] = (Engine *)(*mxGetUint64s(engine));

    // get command
    mxArray *cmd = mxGetCell(prhs[1], i);
    info = !cmd;
    COND_WARNING_GOTO(info, FREE, MEXNAME ":mxGetCell",
                      "mxGetCell returned a null pointer %ld", info);
    cmds[i] = (char *)mxArrayToString(cmd);
  }

  /*------------------------------------------------------------------------
   *  call on the engines the commands in parallel
   *-----------------------------------------------------------------------*/
  info = mextools_malloc((void **)&infos, sizeof(mextools_int) * num_elements);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

#pragma omp parallel for num_threads(num_elements)
  for (mextools_int i = 0; i < num_elements; ++i) {
    // get engine
#ifdef DEBUG
    PRINT_MESSAGE(MEXNAME ": Evaluate \"%s\" on the engine \"%p\".\n", cmds[i],
                  (void *)engines[i]);
#endif
    infos[i] = engEvalString(engines[i], cmds[i]);
  }

  /*------------------------------------------------------------------------
   *  check the info value
   *-----------------------------------------------------------------------*/
  for (mextools_int i = 0; i < num_elements; ++i) {
    info = infos[i];
    COND_WARNING_GOTO(
        info, FREE, MEXNAME ":engEvalString",
        "engEvalString on engine \"%p\" with command \"%s\" returned info %ld",
        engines[i], cmds[i], info);
#ifdef DEBUG
    PRINT_MESSAGE(MEXNAME
                  ": Evaluated \"%s\" on the engine \"%p\" successful.\n",
                  cmds[i], (void *)engines[i]);
#endif
  }

  /*------------------------------------------------------------------------
   *  return info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}

/*-----------------------------------------------------------------------------
 *  Function mex_engGetVariable
 *-----------------------------------------------------------------------------*/
#undef MEXNAME
#define MEXNAME "engGetVariable"

mextools_int mex_engGetVariable(int nlhs, mxArray *plhs[], int nrhs,
                                const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;
  Engine *engine_in = NULL;

  /*------------------------------------------------------------------------
   *  get engine and variable
   *-----------------------------------------------------------------------*/
  engine_in = (Engine *)(*mxGetUint64s(prhs[0]));
  const char *varname = (char *)mxArrayToString(prhs[1]);
  info = !varname;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mxArrayToString",
                    "mxArrayToString returned a null pointer %ld", info);

  /*------------------------------------------------------------------------
   *  call engGetVariable
   *-----------------------------------------------------------------------*/
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME ": Try to get variable \"%s\" from engine \"%p\".\n",
                varname, (void *)engine_in);
#endif
  mxArray *variable = engGetVariable(engine_in, varname);
  info = !variable;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    "engine returned a null pointer %ld", info);
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME
                ": Got variable \"%s\" from engine \"%p\" successful.\n",
                varname, (void *)engine_in);
#endif

  /*------------------------------------------------------------------------
   *  set to output
   *-----------------------------------------------------------------------*/
  plhs[0] = variable;

  /*------------------------------------------------------------------------
   *  return info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}

/*-----------------------------------------------------------------------------
 *  Function mex_engPutVariable
 *-----------------------------------------------------------------------------*/
#undef MEXNAME
#define MEXNAME "engPutVariable"

mextools_int mex_engPutVariable(int nlhs, mxArray *plhs[], int nrhs,
                                const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(plhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;
  Engine *engine_in = NULL;

  /*------------------------------------------------------------------------
   *  get engine and variable
   *-----------------------------------------------------------------------*/
  engine_in = (Engine *)(*mxGetUint64s(prhs[0]));
  const char *varname = (char *)mxArrayToString(prhs[1]);
  info = !varname;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mxArrayToString",
                    "mxArrayToString returned a null pointer %ld", info);

  /*------------------------------------------------------------------------
   *  call engPutVariable
   *-----------------------------------------------------------------------*/
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME ": Try to put variable \"%s\" on engine \"%p\".\n",
                varname, (void *)engine_in);
#endif
  info = engPutVariable(engine_in, varname, prhs[2]);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    "engine returned a nonzero info value %ld", info);
#ifdef DEBUG
  PRINT_MESSAGE(MEXNAME ": Put variable \"%s\" on engine \"%p\" successful.\n",
                varname, (void *)engine_in);
#endif

  /*------------------------------------------------------------------------
   *  return info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}
