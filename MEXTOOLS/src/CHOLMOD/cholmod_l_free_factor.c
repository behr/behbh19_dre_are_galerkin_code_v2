//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * cholmod_l_free_factor    Interface to CHOLMOD cholmod_l_free_factor
 * routine,
 *
 *  # Purpose:
 *
 *  Interface to CHOLMOD cholmod_l_free_factor routine.
 *
 *  # Description:
 *
 *  Free internal memory of cholmod_factor object.
 *
 *  # Calling Sequence:
 *
 *    CHOLMOD_L_FREE_FACTOR(FACTOR)
 *
 *  See also CHOLMOD_L_ANALYZE, CHOLMOD_L_PRINT_FACTOR.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* CHOLMOD */
#include "cholmod.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "cholmod_l_free_factor"

/* mex_cholmod_l_free_factor */
mextools_int mex_cholmod_l_free_factor(int nlhs, mxArray *plhs[], int nrhs,
                                       const mxArray *prhs[]) {
  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(plhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;

  /*------------------------------------------------------------------------
   *  cholmod_common
   *-----------------------------------------------------------------------*/
  cholmod_common cc;
  cholmod_l_start(&cc);
  CHOLMOD_COMMON_DEFAULTS(cc);

  /*------------------------------------------------------------------------
   *  call print function
   *-----------------------------------------------------------------------*/
  cholmod_factor *fac = (cholmod_factor *)(*mxGetUint64s(prhs[0]));
#ifdef DEBUG
  PRINT_MESSAGE("CHOLMOD_FACTOR = %p\n", (void *)fac);
#endif
  info = cholmod_l_free_factor(&fac, &cc) < 0;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME, "CHOLMOD returned %ld",
                    info);

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  cholmod_l_finish(&cc);
  return info;
}
