//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * cholmod_l_print_factor    Interface to CHOLMOD cholmod_l_print_factor
 * routine,
 *
 *  # Purpose:
 *
 *  Interface to CHOLMOD cholmod_l_print_factor routine.
 *
 *  # Description:
 *
 *  Print information about cholmod_factor object.
 *
 *  # Calling Sequence:
 *
 *    CHOLMOD_L_PRINT_FACTOR(FAC)
 *    CHOLMOD_L_PRINT_FACTOR(FAC, PRINT_LEVEL)
 *
 *  See also CHOLMOD_L_ANALYZE, CHOLMOD_L_FACTORIZE AND CHOLMOD_L_FREE_FACTOR.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* CHOLMOD */
#include "cholmod.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "cholmod_l_print_factor"

/* mex_cholmod_l_print_factor */
mextools_int mex_cholmod_l_print_factor(int nlhs, mxArray *plhs[], int nrhs,
                                        const mxArray *prhs[]) {
  UNUSED_VARIABLE(plhs);
  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  not available on Windows
   *-----------------------------------------------------------------------*/
#if defined(_WIN32) || defined(_WIN64)
  UNUSED_VARIABLE(prhs);
  PRINT_MESSAGE("cholmod_l_print_factor is not available on Windows.\n");
  return 0;
#else

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0, print_level = 3;

  /*------------------------------------------------------------------------
   *  cholmod_common
   *-----------------------------------------------------------------------*/
  cholmod_common cc;
  cholmod_l_start(&cc);
  CHOLMOD_COMMON_DEFAULTS(cc);

  /*------------------------------------------------------------------------
   *  call print function
   *-----------------------------------------------------------------------*/
  cholmod_factor *fac = (cholmod_factor *)(*mxGetUint64s(prhs[0]));
  if (nrhs >= 2) {
    print_level += MAX(mxGetScalar(prhs[1]), 0);
  }
  cc.print = print_level;
#ifdef DEBUG
  PRINT_MESSAGE("CHOLMOD_FACTOR = %p\n", (void *)fac);
#endif
  info = cholmod_l_print_factor(fac, "", &cc) < 0;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME, "CHOLMOD returned %ld",
                    info);

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  cholmod_l_finish(&cc);
  return info;
#endif
}
