//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * cholmod_l_analyze    Interface to CHOLMOD cholmod_l_analyze routine.
 *
 *  # Purpose:

 *  Interface to CHOLMOD cholmod_l_analyze routines.
 *
 *  # Description:
 *
 *  CHOLMOD_L_ANALYZE performs a symbolic analysis for a sparse
 *  double or complex real square matrix A. A may be also sparse logical.
 *
 *  # Calling Sequence:
 *
 *    SYMBOLIC = CHOLMOD_L_ANALYZE(A)
 *    SYMBOLIC = CHOLMOD_L_ANALYZE(A, NMETHODS)
 *
 *  See also CHOLMOD_L_PRINT_FACTOR, CHOLMOD_L_FREE_FACTOR.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* CHOLMOD */
#include "cholmod.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "cholmod_l_analyze"

/* mex_cholmod_l_analyze */
mextools_int mex_cholmod_l_analyze(int nlhs, mxArray *plhs[], int nrhs,
                                   const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, info = 0;
  mextools_int *Ap = NULL, *Ai = NULL;
  void **symbolic_out = NULL;

  /*------------------------------------------------------------------------
   *  cholmod_common
   *-----------------------------------------------------------------------*/
  cholmod_common cc;
  cholmod_l_start(&cc);
  CHOLMOD_COMMON_DEFAULTS(cc);
  cc.nmethods = (nrhs >= 2) ? MAX(mxGetScalar(prhs[1]), 0) : 0;

  /*------------------------------------------------------------------------
   *  get cholmod_sparse from A
   *-----------------------------------------------------------------------*/
  n = mxGetN(prhs[0]);
  Ap = (mextools_int *)mxGetJc(prhs[0]);
  Ai = (mextools_int *)mxGetIr(prhs[0]);

  cholmod_sparse cholA;
  cholA.nrow = n;
  cholA.ncol = n;
  cholA.nzmax = Ap[n];
  cholA.p = (void *)Ap;
  cholA.i = (void *)Ai;
  cholA.nz = NULL;
  if (mxIsComplex(prhs[0])) {
    cholA.x = (void *)mxGetComplexDoubles(prhs[0]);
    cholA.xtype = CHOLMOD_COMPLEX;
  } else if (mxIsLogical(prhs[0])) {
    cholA.x = NULL;
    cholA.xtype = CHOLMOD_PATTERN;
  } else {
    cholA.x = (void *)mxGetDoubles(prhs[0]);
    cholA.xtype = CHOLMOD_REAL;
  }
  cholA.z = NULL;
  cholA.stype = 1; // use upper triangular part
  cholA.itype = CHOLMOD_LONG;
  cholA.dtype = CHOLMOD_DOUBLE;
  cholA.sorted = 1;
  cholA.packed = 1;

  /*------------------------------------------------------------------------
   *  cholmod_analyze
   *-----------------------------------------------------------------------*/
  cholmod_factor *symbolic = cholmod_l_analyze(&cholA, &cc);
  info = !symbolic;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    "cholmod returned a null pointer %ld", info);

  /*------------------------------------------------------------------------
   *  set output
   *-----------------------------------------------------------------------*/
  plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  symbolic_out = (void **)mxGetUint64s(plhs[0]);
  *symbolic_out = (void *)symbolic;
#ifdef DEBUG
  PRINT_MESSAGE("SYMBOLIC = %p\n", (void *)symbolic);
#endif

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  cholmod_l_finish(&cc);
  return info;
}
