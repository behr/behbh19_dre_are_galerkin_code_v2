//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * cholmod_l_solve    Interface to CHOLMOD cholmod_l_solve routine.
 *
 *  # Purpose:

 *  Interface to CHOLMOD cholmod_l_solve routine.
 *
 *  # Description:
 *
 *  CHOLMOD_L_SOLVE performs solving using factorization using
 *  the numerical factorization
 *
 *  # Calling Sequence:
 *
 *    X = CHOLMOD_L_SOLVE(NUMERIC, B)
 *    X = CHOLMOD_L_SOLVE_OMP(NUMERIC, B)
 *    X = CHOLMOD_L_SOLVE_OMP(NUMERIC, B, DIVS)
 *
 *  See also CHOLMOD_L_FACTORIZE, CHOLMOD_L_ANAYLZE.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* CHOLMOD */
#include "cholmod.h"

/* OPENMP */
#ifdef _OPENMP
#include <omp.h>
#endif

/* NAME OF THE MEX FUNCTION */
#ifdef _OPENMP
#define MEXNAME "cholmod_l_solve_omp"
#define MEXFUNCNAME mex_cholmod_l_solve_omp
#else
#define MEXNAME "cholmod_l_solve"
#define MEXFUNCNAME mex_cholmod_l_solve
#endif

/* mex_cholmod_l_solve_{,omp} */
mextools_int MEXFUNCNAME(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, info = 0;
  mextools_int rowsB = 0, colsB = 0, complexB = 0;
  cholmod_dense *X = NULL;
  const char *ARG_NAMES[] = {"NUMERIC", "B", "DIVS"};

  /*------------------------------------------------------------------------
   *  cholmod_common
   *-----------------------------------------------------------------------*/
  cholmod_common cc;
  cholmod_l_start(&cc);
  cc.print_function = mexPrintf;

  /*------------------------------------------------------------------------
   *  variable for openmp variant
   *-----------------------------------------------------------------------*/
#ifdef _OPENMP
  const mextools_int num_cores = omp_get_num_procs();
  mextools_int no_col_blocks = 0;
  mextools_int cols_last_block = 0;

  cholmod_common *ccs = NULL;
  cholmod_dense *Bcols = NULL;
  cholmod_dense **Xcols = NULL;
#endif

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // get numeric factorization
  cholmod_factor *numeric = (cholmod_factor *)(*mxGetUint64s(prhs[0]));
  n = numeric->n;

  // check the size of B
  rowsB = mxGetM(prhs[1]);
  colsB = mxGetN(prhs[1]);
  complexB = mxIsComplex(prhs[1]);
  MEX_CHECK_ARG(n != rowsB, MEXNAME ":arguments",
                "Argument %d: '%s' has wrong number of rows (%d): "
                "expected %d.",
                2, ARG_NAMES[1], rowsB, n);

#ifdef _OPENMP
  /*------------------------------------------------------------------------
   *  check the divs scalar
   *-----------------------------------------------------------------------*/
  mextools_int divs = 6;
  if (nrhs >= 3) {
    divs = MAX(mxGetScalar(prhs[2]), 4);
  }

  /*------------------------------------------------------------------------
   *  number of column blocks, we use pairs of columns
   *-----------------------------------------------------------------------*/
  no_col_blocks = colsB / divs + ((colsB % divs) > 0);
  cols_last_block = ((colsB % divs) > 0) ? (colsB % divs) : divs;

  /*------------------------------------------------------------------------
   *  allocate memory for cholmod_common structures
   *-----------------------------------------------------------------------*/
  info = mextools_malloc((void **)&ccs, sizeof(cholmod_common) * no_col_blocks);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  for (mextools_int i = 0; i < no_col_blocks; ++i) {
    cholmod_l_start(&(ccs[i]));
  }

  /*------------------------------------------------------------------------
   *  allocate memory for columns of B
   *-----------------------------------------------------------------------*/
  info =
      mextools_malloc((void **)&Bcols, sizeof(cholmod_dense) * no_col_blocks);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);
  for (mextools_int i = 0; i < no_col_blocks; ++i) {
    mextools_int cols = (i + 1 >= no_col_blocks) ? cols_last_block : divs;
    Bcols[i].nrow = rowsB;
    Bcols[i].ncol = cols;
    Bcols[i].nzmax = rowsB * cols;
    Bcols[i].d = rowsB;
    if (complexB) {
      double complex *vals = (double complex *)mxGetComplexDoubles(prhs[1]);
      Bcols[i].x = (void *)(vals + divs * i * rowsB);
      Bcols[i].xtype = CHOLMOD_COMPLEX;
    } else {
      double *vals = mxGetDoubles(prhs[1]);
      Bcols[i].x = (void *)(vals + divs * i * rowsB);
      Bcols[i].xtype = CHOLMOD_REAL;
    }
    Bcols[i].z = NULL;
    Bcols[i].dtype = CHOLMOD_DOUBLE;
  }

  /*------------------------------------------------------------------------
   *  allocate memory for columns of X
   *-----------------------------------------------------------------------*/
  info =
      mextools_malloc((void **)&Xcols, sizeof(cholmod_dense *) * no_col_blocks);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  call cholmod_solve in parallel
   *-----------------------------------------------------------------------*/
#pragma omp parallel for num_threads(num_cores)
  for (mextools_int i = 0; i < no_col_blocks; ++i) {
    Xcols[i] = cholmod_l_solve(CHOLMOD_A, numeric, &(Bcols[i]), &(ccs[i]));
  }

  /*------------------------------------------------------------------------
   *  check the results
   *-----------------------------------------------------------------------*/
  for (mextools_int i = 0; i < no_col_blocks; ++i) {
    // check for nullpointer
    info = !Xcols[i];
    COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                      "CHOLMOD returned a null pointer, info = %ld", info);

    // for real/complex B all Xcols must be real/complex
    if (complexB) {
      info = (Xcols[i]->xtype != CHOLMOD_COMPLEX);
    } else {
      info = (Xcols[i]->xtype != CHOLMOD_REAL);
    }
    COND_WARNING_GOTO(
        info, FREE, MEXNAME ":" MEXNAME,
        "CHOLMOD returned a solution of unexpected xtype, info = %ld", info);
  }

  /*------------------------------------------------------------------------
   *  create output X
   *-----------------------------------------------------------------------*/
  if (complexB) {
    // plhs[0] = mxCreateDoubleMatrix(rowsB, colsB, mxCOMPLEX);
    plhs[0] =
        mxCreateUninitNumericMatrix(rowsB, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
    double complex *outX = (double complex *)mxGetComplexDoubles(plhs[0]);
    for (mextools_int i = 0; i < no_col_blocks; ++i) {
      mextools_int Xnrow = Xcols[i]->nrow;
      mextools_int Xncol = Xcols[i]->ncol;
      mextools_int Xld = Xcols[i]->d;
      double complex *Xval = (double complex *)Xcols[i]->x;
      zlacpy("A", &Xnrow, &Xncol, Xval, &Xld, outX, &rowsB);
      outX += rowsB * Xncol;
    }
  } else {
    // plhs[0] = mxCreateDoubleMatrix(rowsB, colsB, mxREAL);
    plhs[0] = mxCreateUninitNumericMatrix(rowsB, colsB, mxDOUBLE_CLASS, mxREAL);
    double *outX = mxGetDoubles(plhs[0]);
    for (mextools_int i = 0; i < no_col_blocks; ++i) {
      mextools_int Xnrow = Xcols[i]->nrow;
      mextools_int Xncol = Xcols[i]->ncol;
      mextools_int Xld = Xcols[i]->d;
      double *Xval = (double *)Xcols[i]->x;
      dlacpy("A", &Xnrow, &Xncol, Xval, &Xld, outX, &rowsB);
      outX += rowsB * Xncol;
    }
  }

#else
  /*------------------------------------------------------------------------
   *  get cholmod_dense from B
   *-----------------------------------------------------------------------*/
  cholmod_dense cholB;
  cholB.nrow = rowsB;
  cholB.ncol = colsB;
  cholB.nzmax = rowsB * colsB;
  cholB.d = rowsB;
  if (complexB) {
    cholB.x = (void *)mxGetComplexDoubles(prhs[1]);
    cholB.xtype = CHOLMOD_COMPLEX;
  } else {
    cholB.x = (void *)mxGetDoubles(prhs[1]);
    cholB.xtype = CHOLMOD_REAL;
  }
  cholB.z = NULL;
  cholB.dtype = CHOLMOD_DOUBLE;

  /*------------------------------------------------------------------------
   *  call cholmod_solve
   *-----------------------------------------------------------------------*/
  X = cholmod_l_solve(CHOLMOD_A, numeric, &cholB, &cc);
  info = !X;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    "CHOLMOD returned a null pointer, info = %ld", info);

  /*------------------------------------------------------------------------
   *  create output from X
   *-----------------------------------------------------------------------*/
  mextools_int Xnrow = X->nrow, Xncol = X->ncol, Xld = X->d;
  mextools_int complexX = X->xtype == CHOLMOD_COMPLEX;
  if (complexX) {
    // plhs[0] = mxCreateDoubleMatrix(Xnrow, Xncol, mxCOMPLEX);
    plhs[0] =
        mxCreateUninitNumericMatrix(Xnrow, Xncol, mxDOUBLE_CLASS, mxCOMPLEX);
    double complex *outX = (double complex *)mxGetComplexDoubles(plhs[0]);
    zlacpy("A", &Xnrow, &Xncol, (double complex *)X->x, &Xld, outX, &Xnrow);
  } else {
    // plhs[0] = mxCreateDoubleMatrix(Xnrow, Xncol, mxREAL);
    plhs[0] = mxCreateUninitNumericMatrix(Xnrow, Xncol, mxDOUBLE_CLASS, mxREAL);
    double *outX = mxGetDoubles(plhs[0]);
    dlacpy("A", &Xnrow, &Xncol, (double *)X->x, &Xld, outX, &Xnrow);
  }
#endif

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  if (X) {
    cholmod_l_free_dense(&X, &cc);
  }

#ifdef _OPENMP
  if (ccs) {
    for (mextools_int i = 0; i < no_col_blocks; ++i) {
      cholmod_l_finish(&(ccs[i]));
    }
    free(ccs);
    ccs = NULL;
  }

  if (Bcols) {
    free(Bcols);
    Bcols = NULL;
  }

  if (Xcols) {
    for (mextools_int i = 0; i < no_col_blocks; ++i) {
      cholmod_l_free_dense(&(Xcols[i]), &cc);
    }
    free(Xcols);
    Xcols = NULL;
  }

#endif

  cholmod_l_finish(&cc);
  return info;
}
