//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * cholmod_l_factorize    Interface to CHOLMOD cholmod_l_factorize routine.
 *
 *  # Purpose:
 *
 *  Interface to CHOLMOD cholmod_l_factorize routine.
 *
 *  # Description:
 *
 *  CHOLMOD_L_FACTORIZE performs numeric factorization using
 *  the symbolic factorization.
 *
 *  # Calling Sequence:
 *
 *    NUMERIC = CHOLMOD_L_FACTORIZE(A, SYMBOLIC)
 *
 *  See also CHOLMOD_L_PRINT_FACTOR, CHOLMOD_L_FREE_FACTOR.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* CHOLMOD */
#include "cholmod.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "cholmod_l_factorize"

/* mex_cholmod_l_factorize */
mextools_int mex_cholmod_l_factorize(int nlhs, mxArray *plhs[], int nrhs,
                                     const mxArray *prhs[]) {
  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, info = 0;
  mextools_int *Ap = NULL, *Ai = NULL;
  void **numeric_out = NULL;

  /*------------------------------------------------------------------------
   *  cholmod_common
   *-----------------------------------------------------------------------*/
  cholmod_common cc;
  cholmod_l_start(&cc);
  CHOLMOD_COMMON_DEFAULTS(cc);

  /*------------------------------------------------------------------------
   *  get input
   *-----------------------------------------------------------------------*/
  n = mxGetN(prhs[0]);

  /*------------------------------------------------------------------------
   *  get cholmod_sparse from A
   *-----------------------------------------------------------------------*/
  Ap = (mextools_int *)mxGetJc(prhs[0]);
  Ai = (mextools_int *)mxGetIr(prhs[0]);

  cholmod_sparse cholA;
  cholA.nrow = n;
  cholA.ncol = n;
  cholA.nzmax = Ap[n];
  cholA.p = (void *)Ap;
  cholA.i = (void *)Ai;
  cholA.nz = NULL;
  if (mxIsComplex(prhs[0])) {
    cholA.x = (void *)mxGetComplexDoubles(prhs[0]);
    cholA.xtype = CHOLMOD_COMPLEX;
  } else {
    cholA.x = (void *)mxGetDoubles(prhs[0]);
    cholA.xtype = CHOLMOD_REAL;
  }
  cholA.z = NULL;
  cholA.stype = 1; // use upper triangular part
  cholA.itype = CHOLMOD_LONG;
  cholA.dtype = CHOLMOD_DOUBLE;
  cholA.sorted = 1;
  cholA.packed = 1;

  /*------------------------------------------------------------------------
   *  get copy of cholmod_factor
   *-----------------------------------------------------------------------*/
  cholmod_factor *symbolic = (cholmod_factor *)(*mxGetUint64s(prhs[1]));
  cholmod_factor *numeric = cholmod_l_copy_factor(symbolic, &cc);
  info = !numeric;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    "cholmod returned a null pointer, info = %ld", info);

  /*------------------------------------------------------------------------
   *  call cholmod_factorize
   *-----------------------------------------------------------------------*/
  // quick return if matrix is not positive definite
  cc.quick_return_if_not_posdef = 1;
  info = cholmod_l_factorize(&cholA, numeric, &cc) < 0;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME, "CHOLMOD returned %ld",
                    info);

  /*------------------------------------------------------------------------
   *  check if matrix is positive definite
   *-----------------------------------------------------------------------*/
  if (cc.status != CHOLMOD_OK) {
    // free numeric and error
    info = cholmod_l_free_factor(&numeric, &cc);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                      "CHOLMOD matrix is not positive definite, info = %ld",
                      info);
  }

  /*------------------------------------------------------------------------
   *  set output
   *-----------------------------------------------------------------------*/
  plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  numeric_out = (void **)mxGetUint64s(plhs[0]);
  *numeric_out = (void *)numeric;
#ifdef DEBUG
  PRINT_MESSAGE("NUMERIC = %p\n", (void *)numeric);
#endif

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  cholmod_l_finish(&cc);
  return info;
}
