//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * umfpack_symbolic    Interface to UMFPACK umfpack_{d,z}l_symbolic routines.
 *
 *  # Purpose:

 *  Interface to UMFPACK umfpack_{d,z}l_symbolic routines.
 *
 *  # Description:
 *
 *  UMFPACK_{D,Z}L_SYMBOLIC performs a symbolic analysis for a sparse
 *  double real, double complex or logical square matrix A.
 *
 *  # Calling Sequence:
 *
 *    SYMBOLIC = UMFPACK_DL_SYMBOLIC(A).
 *    SYMBOLIC = UMFPACK_ZL_SYMBOLIC(A).
 *
 *  See also UMFPACK_DL_SYMBOLIC_FREE, UMFPACK_ZL_SYMBOLIC_FREE.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* UMFPACK */
#include "umfpack.h"

/* NAME OF THE MEX FUNCTION */
#if UMFPACK_COMPLEX
#define MEXNAME "umfpack_zl_symbolic"
#define MEXFUNCNAME mex_umfpack_zl_symbolic
#else
#define MEXNAME "umfpack_dl_symbolic"
#define MEXFUNCNAME mex_umfpack_dl_symbolic
#endif

/* mex_umfpack_{d,z}l_symbolic */
mextools_int MEXFUNCNAME(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, info = 0, islogicalA = 0;
  mextools_int *Ap = NULL, *Ai = NULL;
  void *Symbolic = NULL;

  /*------------------------------------------------------------------------
   *  symbolic factorization
   *-----------------------------------------------------------------------*/
  islogicalA = mxIsLogical(prhs[0]);
  n = mxGetM(prhs[0]);
  Ap = (mextools_int *)mxGetJc(prhs[0]);
  Ai = (mextools_int *)mxGetIr(prhs[0]);

  if (UMFPACK_COMPLEX) {
    info = umfpack_zl_symbolic(
        n, n, Ap, Ai,
        islogicalA ? NULL : (double *)mxGetComplexDoubles(prhs[0]), NULL,
        &Symbolic, NULL, NULL);
  } else {
    info = umfpack_dl_symbolic(n, n, Ap, Ai,
                               islogicalA ? NULL : mxGetDoubles(prhs[0]),
                               &Symbolic, NULL, NULL);
  }
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME, "UMFPACK returned %ld->%s",
                    info, umfpack_error(info));

  /*------------------------------------------------------------------------
   *  set output
   *-----------------------------------------------------------------------*/
  plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  void **Symbolic_out = (void **)mxGetUint64s(plhs[0]);
  *Symbolic_out = Symbolic;
#ifdef DEBUG
  PRINT_MESSAGE("SYMBOLIC = %p\n", (void *)Symbolic);
#endif

  /*------------------------------------------------------------------------
   *  free allocated memory and return info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}
