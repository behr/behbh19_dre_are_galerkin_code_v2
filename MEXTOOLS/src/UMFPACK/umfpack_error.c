//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/* MEXTOOLS HEADER */
#include "mextools.h"

/* UMFPACK */
#include "umfpack.h"

const char *umfpack_error(mextools_int info) {
  switch (info) {
  case UMFPACK_OK:
    return "UMFPACK_OK";
  case UMFPACK_WARNING_singular_matrix:
    return "UMFPACK_WARNING_singular_matrix";
  case UMFPACK_WARNING_determinant_underflow:
    return "UMFPACK_WARNING_determinant_underflow";
  case UMFPACK_WARNING_determinant_overflow:
    return "UMFPACK_WARNING_determinant_overflow";
  case UMFPACK_ERROR_out_of_memory:
    return "UMFPACK_ERROR_out_of_memory";
  case UMFPACK_ERROR_invalid_Numeric_object:
    return "UMFPACK_ERROR_invalid_Numeric_object";
  case UMFPACK_ERROR_invalid_Symbolic_object:
    return "UMFPACK_ERROR_invalid_Symbolic_object";
  case UMFPACK_ERROR_argument_missing:
    return "UMFPACK_ERROR_argument_missing";
  case UMFPACK_ERROR_n_nonpositive:
    return "UMFPACK_ERROR_n_nonpositive";
  case UMFPACK_ERROR_invalid_matrix:
    return "UMFPACK_ERROR_invalid_system";
  case UMFPACK_ERROR_different_pattern:
    return "UMFPACK_ERROR_different_pattern";
  case UMFPACK_ERROR_invalid_system:
    return "UMFPACK_ERROR_invalid_system";
  case UMFPACK_ERROR_invalid_permutation:
    return "UMFPACK_ERROR_invalid_permutation";
  case UMFPACK_ERROR_internal_error:
    return "UMFPACK_ERROR_internal_error";
  case UMFPACK_ERROR_file_IO:
    return "UMFPACK_ERROR_file_IO";
  case UMFPACK_ERROR_ordering_failed:
    return "UMFPACK_ERROR_ordering_failed";
  default:
    return "UMFPACK_ERROR_unknown";
  }
}
