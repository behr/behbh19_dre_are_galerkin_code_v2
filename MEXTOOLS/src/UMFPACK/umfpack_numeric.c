//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * umfpack_numeric    Interface to UMFPACK umfpack_{d,z}l_numeric routines.
 *
 *  # Purpose:

 *  Interface to UMFPACK umfpack_{d,z}l_numeric routines.
 *
 *  # Description:
 *
 *  UMFPACK_{D,Z}L_NUMERIC performs a numeric factorization for a sparse
 *  double or complex real square matrix A.
 *
 *  # Calling Sequence:
 *
 *    NUMERIC = UMFPACK_DL_NUMERIC(A, SYMBOLIC)
 *    NUMERIC = UMFPACK_ZL_NUMERIC(A, SYMBOLIC)
 *
 *  See also UMFPACK_DL_SYMBOLIC, UMFPACK_ZL_SYMBOLIC.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* UMFPACK */
#include "umfpack.h"

/* NAME OF THE MEX FUNCTION */
#if UMFPACK_COMPLEX
#define MEXNAME "umfpack_zl_numeric"
#define MEXFUNCNAME mex_umfpack_zl_numeric
#else
#define MEXNAME "umfpack_dl_numeric"
#define MEXFUNCNAME mex_umfpack_dl_numeric
#endif

/* mex_umfpack_{d,z}l_numeric */
mextools_int MEXFUNCNAME(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;
  mextools_int *Ap = NULL, *Ai = NULL;
  void *Symbolic = NULL, *Numeric;

  /*------------------------------------------------------------------------
   *  create output
   *-----------------------------------------------------------------------*/
  plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
  void **Numeric_out = (void **)mxGetUint64s(plhs[0]);
  *Numeric_out = 0;

  /*------------------------------------------------------------------------
   *  numeric factorization
   *-----------------------------------------------------------------------*/
  Ap = (mextools_int *)mxGetJc(prhs[0]);
  Ai = (mextools_int *)mxGetIr(prhs[0]);

  Symbolic = (void *)(*mxGetUint64s(prhs[1]));
#ifdef DEBUG
  PRINT_MESSAGE("SYMBOLIC = %p\n", (void *)Symbolic);
#endif

  if (UMFPACK_COMPLEX) {
    info = umfpack_zl_numeric(Ap, Ai, (double *)mxGetComplexDoubles(prhs[0]),
                              NULL, Symbolic, &Numeric, NULL, NULL);
  } else {
    info = umfpack_dl_numeric(Ap, Ai, (double *)mxGetDoubles(prhs[0]), Symbolic,
                              &Numeric, NULL, NULL);
  }
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME, "UMFPACK returned %ld->%s",
                    info, umfpack_error(info));

  /*------------------------------------------------------------------------
   *  set output
   *-----------------------------------------------------------------------*/
  *Numeric_out = Numeric;
#ifdef DEBUG
  PRINT_MESSAGE("NUMERIC = %p\n", (void *)Numeric);
#endif

  /*------------------------------------------------------------------------
   *  free allocated memory and return info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}
