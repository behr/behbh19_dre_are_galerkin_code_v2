//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * umfpack_free   Interface to UMFPACK umfpack_{d,z}l_free_{symbolic,numeric}
 *  routines.
 *
 *  # Purpose:

 *  Interface to UMFPACK umfpack_{d,z}l_free_{numeric,symbolic} routines.
 *
 *  # Description:
 *
 *  Frees UMFPACK allocated memory.
 *
 *  # Calling Sequence:
 *
 *    UMFPACK_DL_FREE_SYMBOLIC(SYMBOLIC)
 *    UMFPACK_ZL_FREE_SYMBOLIC(SYMBOLIC)
 *    UMFPACK_DL_FREE_NUMERIC(NUMERIC)
 *    UMFPACK_ZL_FREE_NUMERIC(NUMERIC)
 *
 *  See also UMFPACK_DL_SYMBOLIC, UMFPACK_ZL_SYMBOLIC, UMFPACK_DL_NUMERIC and
 *  UMFPACK_ZL_SYMBOLIC.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* UMFPACK */
#include "umfpack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME TOSTRING(UMFPACK_FUNC)

/* mex_umfpack_{d,z}l_free_{numeric,symbolic} */
mextools_int CONCAT(mex_, UMFPACK_FUNC)(int nlhs, mxArray *plhs[], int nrhs,
                                        const mxArray *prhs[]) {
  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(plhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  free the object
   *-----------------------------------------------------------------------*/
  void *ptr = (void *)(*mxGetUint64s(prhs[0]));
#ifdef DEBUG
  PRINT_MESSAGE("PTR = %p\n", (void *)ptr);
#endif
  UMFPACK_FUNC(&ptr);
  return 0;
}
