//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * umfpack_numeric    Interface to UMFPACK
 * umfpack_{d,z}l_report_{symbolic, numeric} routines.
 *
 *  # Purpose:
 *
 *  Interface to UMFPACK umfpack_{d,z}l_report_{symbolic, numeric} routines.
 *
 *  # Description:
 *
 *  Print information about UMFPACK opaque objects Numeric and Symbolic.
 *
 *  # Calling Sequence:
 *
 *    UMFPACK_DL_REPORT_SYMBOLIC(SYMBOLIC)
 *    UMFPACK_ZL_REPORT_SYMBOLIC(SYMBOLIC)
 *    UMFPACK_DL_REPORT_NUMERIC(NUMERIC)
 *    UMFPACK_ZL_REPORT_NUMERIC(NUMERIC)
 *    UMFPACK_DL_REPORT_SYMBOLIC(SYMBOLIC, PRINT_LEVEL)
 *    UMFPACK_ZL_REPORT_SYMBOLIC(SYMBOLIC, PRINT_LEVEL)
 *    UMFPACK_DL_REPORT_NUMERIC(NUMERIC, PRINT_LEVEL)
 *    UMFPACK_ZL_REPORT_NUMERIC(NUMERIC, PRINT_LEVEL)
 *
 *  See also UMFPACK_DL_SYMBOLIC, UMFPACK_ZL_SYMBOLIC, UMFPACK_DL_NUMERIC and
 *  UMFPACK_ZL_SYMBOLIC.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* UMFPACK */
#include "umfpack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME TOSTRING(UMFPACK_FUNC)

/* mex_umfpack_{d,z}l_report_{symbolic, numeric} */
mextools_int CONCAT(mex_, UMFPACK_FUNC)(int nlhs, mxArray *plhs[], int nrhs,
                                        const mxArray *prhs[]) {
  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(plhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;
  void *ptr = NULL;
  double Control[UMFPACK_CONTROL];

  /*------------------------------------------------------------------------
   *  Control array
   *-----------------------------------------------------------------------*/
  Control[UMFPACK_PRL] = 3;
  if (nrhs >= 2) {
    Control[UMFPACK_PRL] += MIN(MAX(mxGetScalar(prhs[1]), 0), 6);
  }

  /*------------------------------------------------------------------------
   *  call report function
   *-----------------------------------------------------------------------*/
  ptr = (void *)(*mxGetUint64s(prhs[0]));
#ifdef DEBUG
  PRINT_MESSAGE("UMFPACK_OBJECT = %p\n", (void *)ptr);
#endif

  info = UMFPACK_FUNC(ptr, Control);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME, "UMFPACK returned %ld->%s",
                    info, umfpack_error(info));

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}
