//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * umfpack_solve    Interface to UMFPACK umfpack_{d,z}l_solve routines.
 *
 *  # Purpose:

 *  Interface to UMFPACK umfpack_{d,z}l_solve routines.
 *
 *  # Description:
 *
 *  UMFPACK_{D,Z}L_SOLVE performs solves the linear system A*X = B
 *  using double or complex real square matrix A. B must be a double
 *  or complex double dense matrix.
 *
 *  # Calling Sequence:
 *
 *    X = UMFPACK_DL_SOLVE(A, NUMERIC, B)
 *    X = UMFPACK_ZL_SOLVE(A, NUMERIC, B)
 *    X = UMFPACK_DL_SOLVE_OMP(A, NUMERIC, B)
 *    X = UMFPACK_ZL_SOLVE_OMP(A, NUMERIC, B)
 *
 *
 *  See also UMFPACK_DL_SYMBOLIC_FREE, UMFPACK_ZL_SYMBOLIC_FREE.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* UMFPACK */
#include "umfpack.h"

/* OPENMP */
#ifdef _OPENMP
#include <omp.h>
#endif

/* NAME OF THE MEX FUNCTION */
#if UMFPACK_COMPLEX
#ifdef _OPENMP
#define MEXNAME "umfpack_zl_solve_omp"
#define MEXFUNCNAME mex_umfpack_zl_solve_omp
#else
#define MEXNAME "umfpack_zl_solve"
#define MEXFUNCNAME mex_umfpack_zl_solve
#endif
#else
#ifdef _OPENMP
#define MEXNAME "umfpack_dl_solve_omp"
#define MEXFUNCNAME mex_umfpack_dl_solve_omp
#else
#define MEXNAME "umfpack_dl_solve"
#define MEXFUNCNAME mex_umfpack_dl_solve
#endif
#endif

/* mex_umfpack_{d,z}l_solve{,_omp} */
mextools_int MEXFUNCNAME(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, info = 0;
  mextools_int complexB = 0, colsB = 0, n_infos = 0;
  const mextools_int ione = 1, itwo = 2;
  mextools_int *Ap = NULL, *Ai = NULL, *infos = NULL;
  double *Ax = NULL, *Xx = NULL, *Bx = NULL, *B_real_imag = NULL;
  double *X_real_imag = NULL;
  void *Numeric = NULL;

#ifdef _OPENMP
  const mextools_int num_cores = omp_get_num_procs();
#endif

  /*------------------------------------------------------------------------
   *  get sparse A
   *-----------------------------------------------------------------------*/
  n = mxGetM(prhs[0]);
  Ap = (mextools_int *)mxGetJc(prhs[0]);
  Ai = (mextools_int *)mxGetIr(prhs[0]);
  if (UMFPACK_COMPLEX) {
    Ax = (double *)mxGetComplexDoubles(prhs[0]);
  } else {
    Ax = (double *)mxGetDoubles(prhs[0]);
  }

  /*------------------------------------------------------------------------
   *  get numeric factorization
   *-----------------------------------------------------------------------*/
  Numeric = (void *)(*mxGetUint64s(prhs[1]));
  info = !Numeric;
  COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME,
                    "NULL pointer dereference %ld", info);

  /*------------------------------------------------------------------------
   *  get B
   *-----------------------------------------------------------------------*/
  colsB = mxGetN(prhs[2]);
  complexB = mxIsComplex(prhs[2]);
  if (complexB) {
    Bx = (double *)mxGetComplexDoubles(prhs[2]);
  } else {
    Bx = (double *)mxGetDoubles(prhs[2]);
  }

  /*------------------------------------------------------------------------
   *  prepare output matrices X as solution of A*X = B
   *-----------------------------------------------------------------------*/
  if (UMFPACK_COMPLEX || complexB) {
    // plhs[0] = mxCreateDoubleMatrix(n, colsB, mxCOMPLEX);
    plhs[0] = mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
    Xx = (double *)mxGetComplexDoubles(plhs[0]);
  } else {
    // plhs[0] = mxCreateDoubleMatrix(n, colsB, mxREAL);
    plhs[0] = mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxREAL);
    Xx = (double *)mxGetDoubles(plhs[0]);
  }

  /*------------------------------------------------------------------------
   *  prepare array for infos values
   *-----------------------------------------------------------------------*/
  n_infos = (!UMFPACK_COMPLEX && complexB) ? 2 * colsB : colsB;
  info = mextools_malloc((void **)&infos, sizeof(mextools_int) * n_infos);
  memset(infos, 0, sizeof(mextools_int) * n_infos);

  /*------------------------------------------------------------------------
   *  solve the system
   *-----------------------------------------------------------------------*/
  if (UMFPACK_COMPLEX) {
    if (complexB) {
      // solve
#ifdef _OPENMP
#pragma omp parallel for num_threads(num_cores)
#endif
      for (mextools_int i = 0; i < colsB; ++i) {
        infos[i] =
            umfpack_zl_solve(UMFPACK_A, Ap, Ai, Ax, NULL, Xx + i * 2 * n, NULL,
                             Bx + i * 2 * n, NULL, Numeric, NULL, NULL);
      }
    } else {
      // allocate memory for real part of B
      info = mextools_malloc((void **)&B_real_imag,
                             sizeof(double) * 2 * n * colsB);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                        "mextools_malloc returned %ld", info);

      // set B_real_imag to 0
      mextools_int temp = 2 * colsB;
      const double dzero = 0;
      dlaset("A", &n, &temp, &dzero, &dzero, B_real_imag, &n);

      // copy B to realpart of B_real_imag
      temp = colsB * n;
      dcopy(&temp, Bx, &ione, B_real_imag, &itwo);

      // solve
#ifdef _OPENMP
#pragma omp parallel for num_threads(num_cores)
#endif
      for (mextools_int i = 0; i < colsB; ++i) {
        infos[i] = umfpack_zl_solve(UMFPACK_A, Ap, Ai, Ax, NULL, Xx + i * 2 * n,
                                    NULL, B_real_imag + i * 2 * n, NULL,
                                    Numeric, NULL, NULL);
      }
    }
  } else {
    if (complexB) {
      // allocate memory for real and imaginary part of B
      info = mextools_malloc((void **)&B_real_imag,
                             sizeof(double) * 2 * colsB * n);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                        "mextools_malloc returned %ld", info);

      info = mextools_malloc((void **)&X_real_imag,
                             sizeof(double) * 2 * colsB * n);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                        "mextools_malloc returned %ld", info);

      // copy real and imaginary part to B_real_imag
      mextools_int temp = n * colsB;
      dcopy(&temp, Bx, &itwo, B_real_imag, &ione);
      dcopy(&temp, Bx + 1, &itwo, B_real_imag + temp, &ione);

      // solve
#ifdef _OPENMP
#pragma omp parallel for num_threads(num_cores)
#endif
      for (mextools_int i = 0; i < 2 * colsB; ++i) {
        // solve
        infos[i] = umfpack_dl_solve(UMFPACK_A, Ap, Ai, Ax, X_real_imag + i * n,
                                    B_real_imag + i * n, Numeric, NULL, NULL);
      }

      // copy real and imaginary part to X
      temp = n * colsB;
      dcopy(&temp, X_real_imag, &ione, Xx, &itwo);
      dcopy(&temp, X_real_imag + temp, &ione, Xx + 1, &itwo);
    } else {
      // solve
#ifdef _OPENMP
#pragma omp parallel for num_threads(num_cores)
#endif
      for (mextools_int i = 0; i < colsB; ++i) {
        infos[i] = umfpack_dl_solve(UMFPACK_A, Ap, Ai, Ax, Xx + i * n,
                                    Bx + i * n, Numeric, NULL, NULL);
      }
    }
  }

  /*------------------------------------------------------------------------
   *  check info values
   *-----------------------------------------------------------------------*/
  for (mextools_int i = 0; i < n_infos; ++i) {
    info = infos[i];
    if (info) {
      COND_WARNING_GOTO(info, FREE, MEXNAME ":" MEXNAME, "UMFPACK returned %ld",
                        info);
    }
  }

/*------------------------------------------------------------------------
 *  free allocated memory and check for nonzero info
 *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(B_real_imag);
  FREE_POINTER(X_real_imag);
  FREE_POINTER(infos);
  return info;
}
