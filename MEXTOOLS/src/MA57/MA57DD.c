//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *MA57DD    Interface to MA57DD routine of MA57.
 *
 *  # Purpose:
 *
 *  Solve the linear system A X = B using the numeric
 *  factorization FACT and IFACT of A obtained from MA57BD.
 *  VALS, IRN and JCN is the coordinate storage
 *  of the upper trianguluar part of A.
 *  A must be sparse, real symmetric for MA57DD.
 *  B is the right hand side and must be double real or complex dense.
 *
 *  # Calling Sequence:
 *
 *    X = MA57DD(VALS, IRN, JCN, FACT, IFACT, B)
 *
 *  Internal function.
 *
 *  See also MA57AD, MA57BD AND TEST_MA57.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* MA57 */
#include "ma57.h"

/* OPENMP */
#ifdef _OPENMP
#include <omp.h>
#endif

/* NAME OF THE MEX FUNCTION */
#ifdef _OPENMP
#define MEXNAME "MA57DD_omp"
#define MEXFUNCNAME mex_MA57DD_omp
#else
#define MEXNAME "MA57DD"
#define MEXFUNCNAME mex_MA57DD
#endif

/* mex_M{A57DD_{,_omp} */
mextools_int MEXFUNCNAME(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " needs interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  const mextools_int JOB = 0;
  mextools_int N = 0, NE = 0, info = 0, NRHS = 0, colsB = 0, realB = 0;
  mextools_int *IRN = NULL, *JCN = NULL, LFACT = 0, *IFACT = NULL, LIFACT = 0;
  mextools_int *IWORK = NULL;
  mextools_int *ICNTL = NULL, *INFO = NULL;
  double *CNTL = NULL, *RINFO = NULL;
  const mextools_int ione = 1, itwo = 2;
  double *A = NULL, *FACT = NULL, *B = NULL, *X = NULL, *RESID = NULL;
  double *WORK = NULL;
  double *B_real_imag = NULL, *X_real_imag = NULL;

#ifdef _OPENMP
  const mextools_int num_cores = omp_get_num_procs();
#endif

  /*------------------------------------------------------------------------
   *  prepare the call
   *-----------------------------------------------------------------------*/
  // NE
  NE = MAX(mxGetM(prhs[0]), mxGetN(prhs[0]));

  // A
  A = mxGetDoubles(prhs[0]);

  // get IRN and ICN
  IRN = (mextools_int *)mxGetInt64s(prhs[1]);
  JCN = (mextools_int *)mxGetInt64s(prhs[2]);

  // FACT AND LFACT
  FACT = mxGetDoubles(prhs[3]);
  LFACT = MAX(mxGetM(prhs[3]), mxGetN(prhs[3]));

  // IFACT AND LIFACT
  IFACT = (mextools_int *)mxGetInt64s(prhs[4]);
  LIFACT = MAX(mxGetM(prhs[4]), mxGetN(prhs[4]));

  // N, colsB, B and X
  N = mxGetM(prhs[5]);
  colsB = mxGetN(prhs[5]);
  realB = !mxIsComplex(prhs[5]);
  if (realB) {
    B = mxGetDoubles(prhs[5]);
    NRHS = colsB;
    plhs[0] = mxCreateUninitNumericMatrix(N, colsB, mxDOUBLE_CLASS, mxREAL);
    X = mxGetDoubles(plhs[0]);
  } else {
    B = (double *)mxGetComplexDoubles(prhs[5]);
    NRHS = 2 * colsB;
    plhs[0] = mxCreateUninitNumericMatrix(N, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
    X = (double *)mxGetComplexDoubles(plhs[0]);
  }

  // allocate RESID
  info = mextools_malloc((void **)&RESID, sizeof(double) * N * NRHS);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // allocate WORK
  info =
      mextools_malloc((void **)&WORK, sizeof(double) * MA57DD_WORK(N) * NRHS);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // allocate IWORK
  info = mextools_malloc((void **)&IWORK,
                         sizeof(mextools_int) * MA57AD_IWORK(N) * NRHS);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // allocate ICNTL
  info = mextools_malloc((void **)&ICNTL,
                         sizeof(mextools_int) * MA57_ICNTL * NRHS);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // allocate CNTL
  info = mextools_malloc((void **)&CNTL, sizeof(double) * MA57_CNTL * NRHS);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // set CNTL and ICNTL
  for (mextools_int i = 0; i < NRHS; ++i) {
    ma57id(CNTL + i * MA57_CNTL, ICNTL + i * MA57_ICNTL);
    ICNTL[MA57_ICNTL_ORDERING + i * MA57_ICNTL] = MA57_ICNTL_ORDERING_MC47;
  }

  // allocate INFO
  info =
      mextools_malloc((void **)&INFO, sizeof(mextools_int) * MA57_INFO * NRHS);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // allocate RINFO
  info = mextools_malloc((void **)&RINFO, sizeof(double) * MA57_RINFO * NRHS);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  prepare the case of complex B for MA57DD
   *-----------------------------------------------------------------------*/
  if (!realB) {
    info = mextools_malloc((void **)&B_real_imag, sizeof(double) * N * NRHS);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                      "mextools_malloc returned %ld", info);

    // copy real and imaginary part of B to B_real_imag
    mextools_int temp = N * colsB;
    dcopy(&temp, B, &itwo, B_real_imag, &ione);
    dcopy(&temp, B + 1, &itwo, B_real_imag + temp, &ione);

    info = mextools_malloc((void **)&X_real_imag, sizeof(double) * N * NRHS);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                      "mextools_malloc returned %ld", info);
  }

  /*------------------------------------------------------------------------
   *  call MA57DD
   *-----------------------------------------------------------------------*/
  if (!realB) {
#ifdef _OPENMP
#pragma omp parallel for num_threads(num_cores)
#endif
    for (mextools_int i = 0; i < NRHS; ++i) {
      ma57dd(&JOB, &N, &NE, A, IRN, JCN, FACT, &LFACT, IFACT, &LIFACT,
             B_real_imag + i * N, X_real_imag + i * N, RESID + i * N,
             WORK + i * MA57DD_WORK(N), IWORK + i * N, ICNTL + i * MA57_ICNTL,
             CNTL + i * MA57_CNTL, INFO + i * MA57_INFO,
             RINFO + i * MA57_RINFO);
    }

    // copy real and imaginary part to X
    mextools_int temp = N * colsB;
    dcopy(&temp, X_real_imag, &ione, (double *)X, &itwo);
    dcopy(&temp, X_real_imag + temp, &ione, (double *)X + 1, &itwo);
  } else {
#ifdef _OPENMP
#pragma omp parallel for num_threads(num_cores)
#endif
    for (mextools_int i = 0; i < NRHS; ++i) {
      ma57dd(&JOB, &N, &NE, A, IRN, JCN, FACT, &LFACT, IFACT, &LIFACT,
             B + i * N, X + i * N, RESID + i * N, WORK + i * MA57DD_WORK(N),
             IWORK + i * N, ICNTL + i * MA57_ICNTL, CNTL + i * MA57_CNTL,
             INFO + i * MA57_INFO, RINFO + i * MA57_RINFO);
    }
  }

  /*------------------------------------------------------------------------
   *  check the info values
   *-----------------------------------------------------------------------*/
  for (mextools_int i = 0; i < NRHS; ++i) {
    info = INFO[0 + i * MA57_INFO];
    if (info) {
      COND_WARNING_GOTO(info, FREE, MEXNAME ":ma57dd",
                        "ma57dd returned %ld when solving for column %ld", info,
                        i);
    }
  }

/*------------------------------------------------------------------------
 *  free allocated memory and check for nonzero info
 *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(RESID);
  FREE_POINTER(IWORK);
  FREE_POINTER(ICNTL);
  FREE_POINTER(CNTL);
  FREE_POINTER(INFO);
  FREE_POINTER(RINFO);
  FREE_POINTER(WORK);
  FREE_POINTER(B_real_imag);
  FREE_POINTER(X_real_imag);
  return info;
}
