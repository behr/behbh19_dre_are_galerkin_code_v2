//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *MA57AD    Interface to MA57AD routine of MA57.
 *
 *  # Purpose:
 *
 *  Perform symbolic analysis using the sparsity pattern
 *  given in coordinate storage format. N is the order of the matrix.
 *  IRN and JCN are row and column indices of the nonzero entries of
 *  the upper triangular part.
 *  IRN and JCN must hold the indices of the nonzero
 *  entries in the upper triangular.
 *  IRN and JCN must be of class int64.
 *  MA57AD is for sparse double real symmetric matrices.
 *
 *  # Calling Sequence:
 *
 *    [KEEP, LFACT, LIFACT] = MA57AD(N, IRN, JCN)
 *
 *  Internal function.
 *
 *  See also MA57BD, MA57DD, AND TEST_MA57.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* MA57 */
#include "ma57.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "MA57AD"

/* mex_MA57AD */
mextools_int mex_MA57AD(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " needs interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int N = 0, NE = 0, LKEEP = 0, info = 0;
  mextools_int *IRN = NULL, *JCN = NULL, *KEEP = NULL, *IWORK = NULL;
  mextools_int *LFACT = NULL, *LIFACT = NULL;
  mextools_int ICNTL[MA57_ICNTL], INFO[MA57_INFO];
  double CNTL[MA57_CNTL], RINFO[MA57_RINFO];

  /*------------------------------------------------------------------------
   *  prepare the call
   *-----------------------------------------------------------------------*/
  // get N, NE, IRN and JCN
  N = mxGetScalar(prhs[0]);
  NE = MAX(mxGetM(prhs[1]), mxGetN(prhs[1]));
  IRN = (mextools_int *)mxGetInt64s(prhs[1]);
  JCN = (mextools_int *)mxGetInt64s(prhs[2]);

  // LKEEP and KEEP
  LKEEP = MA57_LKEEP(N, NE);
  plhs[0] = mxCreateNumericMatrix(LKEEP, 1, mxINT64_CLASS, mxREAL);
  KEEP = (mextools_int *)mxGetInt64s(plhs[0]);

  // allocate memroy for IWORK
  info =
      mextools_malloc((void **)&IWORK, sizeof(mextools_int) * MA57AD_IWORK(N));
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // ICNTL
  ma57id(CNTL, ICNTL);
  ICNTL[MA57_ICNTL_ORDERING] = MA57_ICNTL_ORDERING_MC47;

  /*------------------------------------------------------------------------
   *  prepare the output LFACT and LIFACT
   *-----------------------------------------------------------------------*/
  plhs[1] = mxCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);
  plhs[2] = mxCreateNumericMatrix(1, 1, mxINT64_CLASS, mxREAL);

  /*------------------------------------------------------------------------
   *  call MA57AD
   *-----------------------------------------------------------------------*/
  ma57ad(&N, &NE, IRN, JCN, &LKEEP, KEEP, IWORK, ICNTL, INFO, RINFO);
  info = INFO[0];
  COND_WARNING_GOTO(info, FREE, MEXNAME ":ma57ad", "ma57ad returned %ld", info);

  /*------------------------------------------------------------------------
   *  set LFACT and LIFACT
   *-----------------------------------------------------------------------*/
  LFACT = (mextools_int *)mxGetInt64s(plhs[1]);
  *LFACT = MA57_LFACT_SCALE * INFO[MA57_INFO_LFACT];

  LIFACT = (mextools_int *)mxGetInt64s(plhs[2]);
  *LIFACT = MA57_LIFACT_SCALE * INFO[MA57_INFO_LIFACT];

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(IWORK);
  return info;
}
