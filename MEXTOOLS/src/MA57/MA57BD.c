//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *MA57BD    Interface to MA57BD routine of MA57.
 *
 *  # Purpose:
 *
 *  Perform numerical factorization using information
 *  from the symbolic analysis. N is the order of the
 *  symmetric matrix and VALS contains the values of the
 *  upper triangular of the symmetric/hermitian matrix.
 *  KEEP, LFACT and LIFACT are returned by MA57AD and must be
 *  unchanged.
 *
 *  # Calling Sequence:
 *
 *    [FACT, IFACT] = MA57BD(N, VALS, KEEP, LFACT, LIFACT)
 *
 *  Internal function.
 *
 *  See also MA57AD, MA57DD, AND TEST_MA57.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* MA57 */
#include "ma57.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "MA57BD"

/* mex_MA57AD */
mextools_int mex_MA57BD(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " needs interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int N = 0, NE = 0, info = 0;
  mextools_int LFACT = 0, LIFACT = 0, LKEEP = 0;
  mextools_int *KEEP = NULL, *IFACT = NULL, *IWORK = NULL;
  mextools_int ICNTL[MA57_ICNTL], INFO[MA57_INFO];
  double CNTL[MA57_CNTL], RINFO[MA57_RINFO];
  double *A = NULL, *FACT = NULL;

  /*------------------------------------------------------------------------
   *  prepare the call
   *-----------------------------------------------------------------------*/
  // N AND NE
  N = mxGetScalar(prhs[0]);
  NE = MAX(mxGetM(prhs[1]), mxGetN(prhs[1]));

  // VALS
  A = mxGetDoubles(prhs[1]);

  // FACT AND LFACT
  LFACT = *mxGetInt64s(prhs[3]);
  plhs[0] = mxCreateNumericMatrix(LFACT, 1, mxDOUBLE_CLASS, mxREAL);
  FACT = mxGetDoubles(plhs[0]);

  // IFACT AND LIFACT
  LIFACT = *mxGetInt64s(prhs[4]);
  plhs[1] = mxCreateNumericMatrix(LIFACT, 1, mxINT64_CLASS, mxREAL);
  IFACT = (mextools_int *)mxGetInt64s(plhs[1]);

  // LKEEP and KEEP
  LKEEP = MAX(mxGetM(prhs[2]), mxGetN(prhs[2]));
  KEEP = (mextools_int *)mxGetInt64s(prhs[2]);

  // allocate memory for IWORK
  info =
      mextools_malloc((void **)&IWORK, sizeof(mextools_int) * MA57BD_IWORK(N));
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);
  // ICNTL
  ma57id(CNTL, ICNTL);
  ICNTL[MA57_ICNTL_ORDERING] = MA57_ICNTL_ORDERING_MC47;

  /*------------------------------------------------------------------------
   *  call MA57BD
   *-----------------------------------------------------------------------*/
  ma57bd(&N, &NE, A, FACT, &LFACT, IFACT, &LIFACT, &LKEEP, KEEP, IWORK, ICNTL,
         CNTL, INFO, RINFO);
  info = INFO[0];
  COND_WARNING_GOTO(info, FREE, MEXNAME ":ma57bd", "ma57bd returned %ld", info);

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(IWORK);
  return info;
}
