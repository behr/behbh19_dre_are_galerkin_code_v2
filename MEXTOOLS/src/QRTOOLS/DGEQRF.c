//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *  DGEQRF
 *
 *  # Purpose:
 *
 *  Interface to LAPACK dgeqrf routine for QR factorization of a double dense
 *  m-by-n matrix A.
 *
 *  # Description:
 *
 *  DGEQRF computes a QR factorization of a real m-by-n matrix A.
 *  The matrix A must be double, dense and of size m-by-n.
 *
 *  # Calling Sequences:
 *
 *      [Qi, TAU, R] = DGEQRF(A)
 *
 *  # Description
 *  On exit the matrix Qi is double, dense and of size  m-by-min(m, n).
 *  The matrix Q is represented as a product of elementary reflectors
 *
 *      Q = H(1) H(2) ... H(k), where k = min(m, n).
 *
 *  Each H(i) has the form
 *
 *      H(i) = I - Tau(i) v_i v_i'
 *
 *  where Tau(i) is a real scalar. The double, dense vector Tau is on
 *  exit of size min(m, n)-by-1 and contains the scalars for the
 *  Householder matrices H(i). The vector v_i have the property
 *
 *      v_i(1:i-1) = 0, v_i(i) = 1.
 *
 *  The nonzero elements of v_i are stored in the i-the column of Qi,
 *  that is
 *
 *      Qi(i:end,i) = v_i(i:end).
 *
 *  The elements above the main diagonal of Qi are zero on exit.
 *  The Qi represents together with Tau the matrix Q implicitly,
 *  therefore it is called Qi.
 *
 *  R is on exit of double, dense size min(m, n)-by-n and represents the upper
 *  triangular part of the QR factorization.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* QRTOOLS HEADER */
#include "qrtools.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "DGEQRF"

/* mex_DGEQRF */
mextools_int mex_DGEQRF(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]) {
  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int m = 0, n = 0, minmn = 0, lwork = 0, info = 0;
  double dwork = 0, done = 1.0, dzero = 0.0;
  double *Qi = NULL, *Tau = NULL, *R = NULL, *WORK = NULL;

  /*------------------------------------------------------------------------
   *  get dimensions
   *-----------------------------------------------------------------------*/
  m = mxGetM(prhs[0]);
  n = mxGetN(prhs[0]);
  minmn = MIN(m, n);

  /*------------------------------------------------------------------------
   *  prepare the outpout
   *-----------------------------------------------------------------------*/
  // plhs[0] = mxCreateDoubleMatrix(m, minmn, mxREAL);
  plhs[0] = mxCreateUninitNumericMatrix(m, minmn, mxDOUBLE_CLASS, mxREAL);
  info = mextools_malloc((void **)(&Qi), sizeof(double) * m * n);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // memcpy(Qi, MXGETPR(prhs[0]), sizeof(double) * m * n);
  dlacpy("A", &m, &n, MXGETPR(prhs[0]), &m, Qi, &m);

  // plhs[1] = mxCreateDoubleMatrix(minmn, 1, mxREAL);
  plhs[1] = mxCreateUninitNumericMatrix(minmn, 1, mxDOUBLE_CLASS, mxREAL);
  Tau = MXGETPR(plhs[1]);

  plhs[2] = mxCreateDoubleMatrix(minmn, n, mxREAL);
  R = MXGETPR(plhs[2]);
  // seems to be already initialized
  // memset(R, 0, sizeof(double) * minmn * n);

  /*------------------------------------------------------------------------
   *  workspace query
   *-----------------------------------------------------------------------*/
  lwork = -1;
  dgeqrf(&m, &n, Qi, &m, Tau, &dwork, &lwork, &info);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":dgeqrf", "dgeqrf returned %ld", info);

  /*------------------------------------------------------------------------
   * allocate WORK
   *-----------------------------------------------------------------------*/
  lwork = round(dwork);
  info = mextools_malloc((void **)(&WORK), sizeof(double) * lwork);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  DGEQRF
   *-----------------------------------------------------------------------*/
  dgeqrf(&m, &n, Qi, &m, Tau, WORK, &lwork, &info);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":dgeqrf", "dgeqrf returned %ld", info);

  /*------------------------------------------------------------------------
   *  Extract R from Qi
   *-----------------------------------------------------------------------*/
  dlacpy("U", &minmn, &n, Qi, &m, R, &minmn);

  /*------------------------------------------------------------------------
   *  copy the first min(m, n) columns of Qi to output plhs[0]
   *-----------------------------------------------------------------------*/
  // memcpy(MXGETPR(plhs[0]), Qi, sizeof(double) * m * minmn);
  dlacpy("A", &m, &minmn, Qi, &m, MXGETPR(plhs[0]), &m);

  /*------------------------------------------------------------------------
   *  reset the diagonal and upper diagonal part
   *-----------------------------------------------------------------------*/
  dlaset("U", &m, &minmn, &dzero, &done, MXGETPR(plhs[0]), &m);

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(Qi);
  FREE_POINTER(WORK);

  return info;
}
