//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *  DORMQR
 *
 *  # Purpose:
 *
 *  Interface to LAPACK dormqr routine for multiplication with the
 *  orthogonal matrix Q from the QR decomposition.
 *
 *  # Description:
 *
 *  DORMQR multiplies a double dense k-by-l matrix C with the orthogonal
 *  m-by-m matrix Q. This means the operation
 *
 *      Q(:, 1:k) * C
 *
 *  is performed. The matrix Q is given in implicit form by the Housholder
 *  vectors and their scalar factors. This means we assume that
 *  these quantities are generated using DGEQRF:
 *
 *      [QI, TAU, R] = DGEQRF(A)
 *
 *  A is a double, dense m-by-n matrix and the columns of
 *  QI contain the Householder vectors and vector TAU the scalars.
 *  The number of rows k of C must clearly fulfill the condition: 0 < k <= m.
 *
 *  # Calling Sequence:
 *
 *      QC = DORMQR(QI, TAU, C);
 *
 *  On exit QC = Q(:, 1:k) * C, where the multiplication is preformed
 *  using the Householder vector QI in Q and the scalars in Tau.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* QRTOOLS HEADER */
#include "qrtools.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "DORMQR"

/* mex_DORMQR */
mextools_int mex_DORMQR(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int m = 0, minmn = 0, k = 0, colsC = 0, lwork = 0, info = 0;
  double dwork;
  double *Qi = NULL, *Tau = NULL, *QC = NULL, *WORK = NULL;

  /*------------------------------------------------------------------------
   *  get dimensions
   *-----------------------------------------------------------------------*/
  m = mxGetM(prhs[0]);
  minmn = mxGetN(prhs[0]);
  k = mxGetM(prhs[2]);
  colsC = mxGetN(prhs[2]);

  /*------------------------------------------------------------------------
   *  get the matrix Qi
   *-----------------------------------------------------------------------*/
  Qi = MXGETPR(prhs[0]);

  /*------------------------------------------------------------------------
   *  get elementary reflectors Tau
   *-----------------------------------------------------------------------*/
  Tau = MXGETPR(prhs[1]);

  /*------------------------------------------------------------------------
   *  prepare output matrices QC = [C; zeros(m-rowsC,colsC)]
   *-----------------------------------------------------------------------*/
  plhs[0] = mxCreateDoubleMatrix(m, colsC, mxREAL);
  QC = MXGETPR(plhs[0]);
  // seems to be already initialized
  // memset(QC, 0, sizeof(double) * m * colsC);
  dlacpy("A", &k, &colsC, MXGETPR(prhs[2]), &k, QC, &m);

  /*------------------------------------------------------------------------
   *  workspace query
   *-----------------------------------------------------------------------*/
  k = MIN(k, minmn); // number of elementary reflextors to use
  lwork = -1;
  dormqr("L", "N", &m, &colsC, &k, Qi, &m, Tau, QC, &m, &dwork, &lwork, &info);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":dormqr", "dormqr returned %ld", info);

  /*------------------------------------------------------------------------
   * allocate WORK
   *-----------------------------------------------------------------------*/
  lwork = dwork;
  info = mextools_malloc((void **)(&WORK), sizeof(double) * lwork);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  DORMQR
   *-----------------------------------------------------------------------*/
  dormqr("L", "N", &m, &colsC, &k, Qi, &m, Tau, QC, &m, WORK, &lwork, &info);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":dormqr", "dormqr returned %ld", info);

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(WORK);

  return info;
}
