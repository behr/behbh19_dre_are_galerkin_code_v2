//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *  DGEQRT
 *
 *  # Purpose:
 *
 *  Interface to LAPACK dgeqrt routine for QR factorization of a double
 *  dense m-by-n matrix A.
 *
 *  # Calling Sequences:
 *
 *      [QI, T, R] = DGEQRT(A)
 *
 *  # Description
 *  On exit the matrix Qi is double, dense and of size m-by-min(m, n).
 *  The matrix Q is represented as a product of elementary reflectors
 *
 *      Q = H(1) H(2) ... H(k), where k = min(m, n).
 *
 *  The elementary reflector is given by
 *
 *      H(i) = I - Tau(i) v_i v_i'
 *
 *  where Tau(i) is a real scalar and v_i is the Householder vector.
 *  The product of the elementary Reflectors is stored in
 *  compact WY format, that is
 *
 *      Q = I - V T V'
 *
 *  The matrix V contains the Housholder vectors and is
 *  stored below the main diagonal of Qi on exit. The main
 *  diagonal of QI is set to 1 and the upper part of QI is
 *  set to 0 on exit. QI is a double dense m-b-min(m, n)
 *
 *  T is on exit a double dense NB-by-min(m,n) matrix,
 *  where NB is the Block size, which is internally used by
 *  DGEQRT. The Block size is returned as an double real scalar.
 *  The matrix R double, dense and of size min(m, n)-by-m
 *  and represents the upper triangular part of the QR factorization.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* QRTOOLS HEADER */
#include "qrtools.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "DGEQRT"

/* mex_DGEQRT */
mextools_int mex_DGEQRT(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int m = 0, n = 0, minmn = 0, nb = 0, lwork, info = 0;
  double dzero = 0.0, done = 1.0;
  double *Qi = NULL, *T = NULL, *R = NULL, *WORK = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  m = mxGetM(prhs[0]);
  n = mxGetN(prhs[0]);
  minmn = MIN(m, n);

  /*------------------------------------------------------------------------
   *  get blocksizes
   *-----------------------------------------------------------------------*/
  qrt_blocksize(minmn, &nb);

  /*------------------------------------------------------------------------
   *  prepare output matrices
   *-----------------------------------------------------------------------*/
  // plhs[0] = mxCreateDoubleMatrix(m, minmn, mxREAL);
  plhs[0] = mxCreateUninitNumericMatrix(m, minmn, mxDOUBLE_CLASS, mxREAL);
  info = mextools_malloc((void **)(&Qi), sizeof(double) * m * n);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  // memcpy(Qi, MXGETPR(prhs[0]), sizeof(double) * m * n);
  dlacpy("A", &m, &n, MXGETPR(prhs[0]), &m, Qi, &m);

  plhs[1] = mxCreateDoubleMatrix(nb, minmn, mxREAL);
  T = MXGETPR(plhs[1]);

  plhs[2] = mxCreateDoubleMatrix(minmn, n, mxREAL);
  R = MXGETPR(plhs[2]);

  plhs[3] = mxCreateDoubleScalar(nb);

  /*------------------------------------------------------------------------
   * allocate WORK
   *-----------------------------------------------------------------------*/
  lwork = nb * n;
  info = mextools_malloc((void **)(&WORK), sizeof(double) * lwork);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  DGEQRT
   *-----------------------------------------------------------------------*/
  dgeqrt(&m, &n, &nb, Qi, &m, T, &nb, WORK, &info);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":dgeqrt", "dgeqrt returned %ld", info);

  /*------------------------------------------------------------------------
   *  Extract R from Qi
   *-----------------------------------------------------------------------*/
  dlacpy("U", &minmn, &n, Qi, &m, R, &minmn);

  /*------------------------------------------------------------------------
   *  copy the first min(m, n) columns of Qi to output plhs[0]
   *-----------------------------------------------------------------------*/
  // memcpy(MXGETPR(plhs[0]), Qi, sizeof(double) * m * minmn);
  dlacpy("A", &m, &minmn, Qi, &m, MXGETPR(plhs[0]), &m);

  /*------------------------------------------------------------------------
   *  reset the diagonal and upper diagonal part
   *-----------------------------------------------------------------------*/
  dlaset("U", &m, &minmn, &dzero, &done, MXGETPR(plhs[0]), &m);

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(Qi);
  FREE_POINTER(WORK);

  return info;
}
