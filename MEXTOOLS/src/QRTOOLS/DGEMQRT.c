//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *  DGEMQRT
 *
 *  # Purpose:
 *
 *  Interface to LAPACK dgemqrt routine for multiplication with the
 *  orthogonal matrix Q from the QR decomposition.
 *
 *   # Description:
 *
 *  DGEMQRT multiplies a double dense k-by-l matrix C with the orthogonal
 *  m-by-m matrix Q. This means the operation
 *
 *      Q(:, 1:k) * C
 *
 *  is performed. The matrix Q is given as a product of
 *  Householder vectors
 *
 *      Q = H(1) H(2) ... H(k), where k = min(m, n).
 *
 *  The product of the Householder vectors is stored
 *  in compact WY format, that is:
 *
 *      Q = I - V T V'
 *
 *  We assume that this format was generated by the DGEQRT routine:
 *
 *      [Qi, T, R] = DGEQRT(A).
 *
 *  A is a double, dense m-by-n matrix and the columns of
 *  Qi contain the Householder vectors.
 *
 *  The number of rows k of C must clearly fulfill the condition: 0 < k <= m.
 *
 *  # Calling Sequence:
 *
 *      QC = DGEMQRT(Qi, T, C)
 *
 *  On exit QC = Q(:, 1:k) * C, where the multiplication is preformed
 *  using the Householder vectors in Qi and the matrix T.
 *
 *  DGEMQRT multiplies a double dense matrix C by the orthogonal matrix Q
 *  implicitly given by the compact WY representation as returned
 *  by DGEQRT,
 *
 *  On exit QC = Q(:, 1:k) * C, where the multiplication is preformed
 *  using the Householder vector Qi and T.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* QRTOOLS HEADER */
#include "qrtools.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "DGEMQRT"

/* mex_DGEMQRT */
mextools_int mex_DGEMQRT(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]) {
  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int m = 0, minmn = 0, k = 0, nb = 0, lwork = 0, info = 0;
  mextools_int rowsC = 0, colsC = 0;
  double *Qi = NULL, *T = NULL, *QC = NULL, *WORK = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  m = mxGetM(prhs[0]);
  minmn = mxGetN(prhs[0]);
  qrt_blocksize(minmn, &nb);

  if ((nb != ((mextools_int)mxGetM(prhs[1])))) {
    mexErrMsgIdAndTxt(MEXNAME ":arguments", ATFILE ": T is of wrong size");
  }

  rowsC = mxGetM(prhs[2]);
  colsC = mxGetN(prhs[2]);

  /*------------------------------------------------------------------------
   *  get the matrix Qi and T
   *-----------------------------------------------------------------------*/
  Qi = MXGETPR(prhs[0]);
  T = MXGETPR(prhs[1]);

  /*------------------------------------------------------------------------
   *  prepare output matrices QC = [C; zeros(m-rowsC,colsC)]
   *-----------------------------------------------------------------------*/
  plhs[0] = mxCreateDoubleMatrix(m, colsC, mxREAL);
  QC = MXGETPR(plhs[0]);
  dlacpy("A", &rowsC, &colsC, MXGETPR(prhs[2]), &rowsC, QC, &m);

  /*------------------------------------------------------------------------
   * allocate WORK
   *-----------------------------------------------------------------------*/
  lwork = colsC * nb;
  info = mextools_malloc((void **)(&WORK), sizeof(double) * lwork);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  DGEMQRT part
   *-----------------------------------------------------------------------*/
  // Conditions on the choice of K:
  //
  //  * MIN(M, N) >= K        (1) (Condition from DGEQRT(A) where A is M-by-N)
  //  * M >= K                (2) (Condition from DGEMQRT)
  //  * K >= NB               (3) (Condition from DGEMQRT)
  //  * NB = MIN(M, N, 32)    (3) (Defined in qrt_blocksize)
  //
  // We have to apply at least H(1) H(2) ... H(MIN(M, N)) Householder
  // matrices to QC.  Due to the zero block in QC, it is enough
  // to apply MIN(ROWSC, MIN(M, N)).
  //
  //  * K >= MIN(ROWSC, M, N)
  //  Therefore we obtain:
  //
  //  * MIN(M, N) >= K >= MIN(M, N, 32)
  //  *              K >= MIN(ROWSC, M, N) = MIN(ROWSC, MIN(M, N))
  //
  //  We define set K = MAX(NB, MIN(ROWSC, MIN(M,N)), then everything should be
  //  fine
  //
  k = MAX(nb, MIN(rowsC, minmn));
  dgemqrt("L", "N", &m, &colsC, &k, &nb, Qi, &m, T, &nb, QC, &m, WORK, &info);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":dgemqrt", "dgemqrt returned %ld",
                    info);

/*------------------------------------------------------------------------
 *  free allocated memory and check for nonzero info
 *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(WORK);

  return info;
}
