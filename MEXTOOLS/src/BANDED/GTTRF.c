//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *GTTRF    Interface to {d,z}gttrf LAPACK routines.
 *
 *  # Purpose:
 *
 *  Interface to LAPACK {d,z}gttrf routines for computing an LU factorization
 *  of a tridiagonal n-by-n matrix A.
 *
 *  # Description:
 *
 *  GTTRF computes a LU factorization of the tridiagonal matrix A.
 *  A must be a double or double complex n-by-n matrix.
 *  The order n of the matrix A must be greater equals 2.
 *
 *    [LU, IPIV] = GTTRF(T).
 *
 *  The subdiagonal, diagonal and superdiagonal are stored in the columns
 *  of the n-by-3 matrix T.
 *
 *  # Calling Sequence:
 *
 *    [LU, IPIV] = GTTRF(T).
 *
 *  See also TO_TRIDIAG, GTSV, GTTRS AND TEST_BANDED.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "GTTRF"

/* mex_GTTRF */
mextools_int mex_GTTRF(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " needs interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, info = 0;
  mextools_int realA = 0;
  mextools_int *ipiv = NULL;
  const mextools_int ithree = 3;
  double *T = NULL, *LU = NULL;
  double complex *T_cpx = NULL, *LU_cpx = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // get order of matrix
  n = mxGetM(prhs[0]);
  realA = !mxIsComplex(prhs[0]);

  /*------------------------------------------------------------------------
   *  prepare output for LU and IPIV
   *-----------------------------------------------------------------------*/
  if (realA) {
    T = mxGetDoubles(prhs[0]);
    plhs[0] = mxCreateDoubleMatrix(n, 4, mxREAL);
    LU = mxGetDoubles(plhs[0]);
    dlacpy("A", &n, &ithree, T, &n, LU, &n);
  } else {
    T_cpx = (double complex *)mxGetComplexDoubles(prhs[0]);
    plhs[0] = mxCreateDoubleMatrix(n, 4, mxCOMPLEX);
    LU_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
    zlacpy("A", &n, &ithree, T_cpx, &n, LU_cpx, &n);
  }

  plhs[1] = mxCreateUninitNumericMatrix(n, 1, mxINT64_CLASS, mxREAL);
  ipiv = (mextools_int *)mxGetInt64s(plhs[1]);

  /*------------------------------------------------------------------------
   *  call {d,z}gttrf
   *-----------------------------------------------------------------------*/
  if (realA) {
    dgttrf(&n, LU, LU + n, LU + 2 * n, LU + 3 * n, ipiv, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":dgttrf", "dgttrf returned %ld",
                      info);
  } else {
    zgttrf(&n, LU_cpx, LU_cpx + n, LU_cpx + 2 * n, LU_cpx + 3 * n, ipiv, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":zgttrf", "zgttrf returned %ld",
                      info);
  }

  /*------------------------------------------------------------------------
   *  check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}
