//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *PBTRS    Interface to {d,z}pbtrs LAPACK routines.
 *
 *  # Purpose:
 *
 *  Interface to LAPACK {d,z}pbtrs routine for solving
 *  the linear system A X = B for a symmetric positive definite
 *  banded matrix A.
 *  The Cholesky factorization U of A must be given as input.
 *  The Cholesky factorization of A must be computed by
 *
 *    U = PBTRF(AB).
 *
 *  # Calling Sequence:
 *
 *    X = PBTRS(U, B).
 *
 *  See also TO_BANDED, PBSV, PBTRF and TEST_BANDED.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "PBTRS"

/* mex_PBTRS */
mextools_int mex_PBTRS(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, kd = 0, info = 0;
  mextools_int no_bands = 0, colsB = 0;
  mextools_int realU = 0, realB = 0;
  const mextools_int ione = 1, itwo = 2;
  double *U = NULL, *X = NULL, *B = NULL, *B_real_imag = NULL;
  double complex *U_cpx = NULL, *X_cpx = NULL, *B_cpx = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // size of banded A
  no_bands = mxGetM(prhs[0]);
  n = mxGetN(prhs[0]);
  kd = no_bands - 1;

  // check the size of B
  colsB = mxGetN(prhs[1]);

  /*------------------------------------------------------------------------
   *  determine if A and B are real or complex and get the pointer
   *-----------------------------------------------------------------------*/
  realU = !mxIsComplex(prhs[0]);
  realB = !mxIsComplex(prhs[1]);

  if (realU) {
    U = mxGetDoubles(prhs[0]);
  } else {
    U_cpx = (double complex *)mxGetComplexDoubles(prhs[0]);
  }

  if (realB) {
    B = mxGetDoubles(prhs[1]);
  } else {
    B_cpx = (double complex *)mxGetComplexDoubles(prhs[1]);
  }

  /*------------------------------------------------------------------------
   *  prepare output matrices X as solution of A*X = B and solve
   *-----------------------------------------------------------------------*/
  if (realU) {
    if (realB) {
      // A and B are real
      plhs[0] = mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxREAL);
      X = mxGetDoubles(plhs[0]);
      dlacpy("A", &n, &colsB, B, &n, X, &n);
      dpbtrs("U", &n, &kd, &colsB, U, &no_bands, X, &n, &info);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dpbtrs", "dpbtrs returned %ld",
                        info);
    } else {
      // A is real and B is complex
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      info = mextools_malloc((void **)&B_real_imag,
                             sizeof(double) * 2 * n * colsB);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                        "mextools_malloc returned %ld", info);

      // copy real/imaginary part to first/second n *colsB columns of
      // B_real_imag
      const mextools_int n2 = n * colsB;
      dcopy(&n2, (double *)B_cpx, &itwo, B_real_imag, &ione);
      dcopy(&n2, (double *)B_cpx + 1, &itwo, B_real_imag + n2, &ione);
      colsB *= 2;
      dpbtrs("U", &n, &kd, &colsB, U, &no_bands, B_real_imag, &n, &info);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dpbtrs", "dpbtrs returned %ld",
                        info);

      // copy result to X_cpx
      dcopy(&n2, B_real_imag, &ione, (double *)X_cpx, &itwo);
      dcopy(&n2, B_real_imag + n2, &ione, (double *)X_cpx + 1, &itwo);
    }
  } else {
    if (realB) {
      plhs[0] = mxCreateDoubleMatrix(n, colsB, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      mextools_int n2 = n * colsB;
      // copy B to realpart of X_cpx
      // X_cpx seems to be already initial by 0
      dcopy(&n2, B, &ione, (double *)X_cpx, &itwo);
    } else {
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      // copy B to X_cpx
      zlacpy("A", &n, &colsB, B_cpx, &n, X_cpx, &n);
    }
    zpbtrs("U", &n, &kd, &colsB, U_cpx, &no_bands, X_cpx, &n, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":zpbtrs", "zpbtrs returned %ld",
                      info);
  }

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(B_real_imag);

  return info;
}
