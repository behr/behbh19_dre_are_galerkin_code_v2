//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *GBTRS    Interface to {d,z}gbtrs LAPACK routines.
 *
 *  # Purpose:
 *
 *  Interface to LAPACK {d,z}gbtrs routine for solving a
 *  linear system A X = B for a banded matrix A.
 *
 *  # Description:
 *
 *  GBTRS solves the linear system A X = B for a banded matrix A.
 *  The LU factorization of A and the pivoting vecotor IPIV must be
 *  given as input arguments. The LU factorization and the pivoting
 *  vector can be computed by
 *
 *    [LU, IPIV] = GBTRF(AB, KL, KU).
 *
 *  A is a double or double complex n-by-n matrix banded matrix.
 *  KL and KU is the number of subdiagonals and superdiagonals of A.
 *  B must be double or double complex and dense.
 *  TRANS must be a character.
 *
 *    TRANS:
 *      'N' solve A X   = B
 *      'T' solve A.' X = B
 *      'C' solve A' X  = B
 *
 *  # Calling Sequence:
 *
 *    X = GBTRS(TRANS, LU, IPIV, KL, KU, B).
 *
 *   See also TO_BANDED, GBSV, GBTRF AND TEST_BANDED.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "GBTRS"

/* mex_GBTRS */
mextools_int mex_GBTRS(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, kl = 0, ku = 0, info = 0;
  mextools_int ldLU = 0, colsB = 0;
  mextools_int realLU = 0, realB = 0;
  const mextools_int ione = 1, itwo = 2;
  mextools_int *ipiv = NULL;
  double *LU = NULL, *X = NULL, *B = NULL, *B_real_imag = NULL;
  double complex *LU_cpx = NULL, *X_cpx = NULL, *B_cpx = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // get TRANS
  const char *TRANS = (char *)mxGetChars(prhs[0]);
  // size of banded A
  ldLU = mxGetM(prhs[1]);
  n = mxGetN(prhs[1]);

  // get IPIV
  ipiv = (mextools_int *) mxGetInt64s(prhs[2]);

  // get KL and KU
  kl = mxGetScalar(prhs[3]);
  ku = mxGetScalar(prhs[4]);

  // check the size of B
  colsB = mxGetN(prhs[5]);

  /*------------------------------------------------------------------------
   *  determine if LU and B are real or complex and get the pointer
   *-----------------------------------------------------------------------*/
  realLU = !mxIsComplex(prhs[1]);
  realB = !mxIsComplex(prhs[5]);

  if (realLU) {
    LU = mxGetDoubles(prhs[1]);
  } else {
    LU_cpx = (double complex *)mxGetComplexDoubles(prhs[1]);
  }

  if (realB) {
    B = mxGetDoubles(prhs[5]);
  } else {
    B_cpx = (double complex *)mxGetComplexDoubles(prhs[5]);
  }

  /*------------------------------------------------------------------------
   *  prepare output matrices X as solution of A*X = B and solve
   *-----------------------------------------------------------------------*/
  if (realLU) {
    if (realB) {
      // A and B are real
      plhs[0] = mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxREAL);
      X = mxGetDoubles(plhs[0]);
      dlacpy("A", &n, &colsB, B, &n, X, &n);
      dgbtrs(TRANS, &n, &kl, &ku, &colsB, LU, &ldLU, ipiv, X, &n, &info);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dgbtrs", "dgbtrs returned %ld",
                        info);
    } else {
      // A is real and B is complex
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      info = mextools_malloc((void **)&B_real_imag,
                             sizeof(double) * 2 * n * colsB);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                        "mextools_malloc returned %ld", info);

      // copy real/imaginary part to first/second n *colsB columns of
      // B_real_imag
      const mextools_int n2 = n * colsB;
      dcopy(&n2, (double *)B_cpx, &itwo, B_real_imag, &ione);
      dcopy(&n2, (double *)B_cpx + 1, &itwo, B_real_imag + n2, &ione);
      colsB *= 2;
      dgbtrs(TRANS, &n, &kl, &ku, &colsB, LU, &ldLU, ipiv, B_real_imag, &n,
             &info);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dgbtrs", "dgbtrs returned %ld",
                        info);

      // copy result to X_cpx
      dcopy(&n2, B_real_imag, &ione, (double *)X_cpx, &itwo);
      dcopy(&n2, B_real_imag + n2, &ione, (double *)X_cpx + 1, &itwo);
    }
  } else {
    if (realB) {
      plhs[0] = mxCreateDoubleMatrix(n, colsB, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      mextools_int n2 = n * colsB;
      // copy B to realpart of X_cpx
      // X_cpx is already initial by 0
      dcopy(&n2, B, &ione, (double *)X_cpx, &itwo);
    } else {
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);

      // copy B to X_cpx
      zlacpy("A", &n, &colsB, B_cpx, &n, X_cpx, &n);
    }
    zgbtrs(TRANS, &n, &kl, &ku, &colsB, LU_cpx, &ldLU, ipiv, X_cpx, &n, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":zgbtrs", "zgbtrs returned %ld",
                      info);
  }

  /*------------------------------------------------------------------------
   *  check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:

  return info;
}
