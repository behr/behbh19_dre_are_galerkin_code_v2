//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *GBSV    Interface to {d,z}gbsv LAPACK routines.
 *
 *  # Purpose:

 *  Interface to LAPACK {d,z}gbsv routines for solving banded
 *  linear systems A X = B.
 *
 *  # Description:
 *
 *  GBSV solves a linear equation A X = B, where A is banded double or
 *  double complex, square matrix n-by-n matrix.
 *
 *    X = GBSV(AB, KL, KU, B).
 *
 *  KL and KU is the number of subdiagonals and superdiagonals of A.
 *  The matrix A must be given in LAPACK band storage format.
 *  That is the bands of A are stored in the (KL + 1 + KU)-by-n matrix AB.
 *
 *  # Calling Sequence:
 *
 *    X = GBSV(AB, KL, KU, B).
 *
 *  See also TO_BANDED, GBTRF, GBTRS AND TEST_BANDED.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "GBSV"

/* mex_GBSV */
mextools_int mex_GBSV(int nlhs, mxArray *plhs[], int nrhs,
                      const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " needs interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, kl = 0, ku = 0, info = 0;
  mextools_int no_bands = 0, ldLUAB = 0, colsB = 0;
  mextools_int realA = 0, realB = 0;
  const mextools_int ione = 1, itwo = 2;
  mextools_int *ipiv = NULL;
  double *A = NULL, *LUAB = NULL, *X = NULL, *B = NULL, *B_real_imag = NULL;
  double complex *A_cpx = NULL, *LUAB_cpx = NULL, *X_cpx = NULL, *B_cpx = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // size of banded A
  no_bands = mxGetM(prhs[0]);
  n = mxGetN(prhs[0]);

  // get KL and KU
  kl = mxGetScalar(prhs[1]);
  ku = mxGetScalar(prhs[2]);

  // check the size of B
  colsB = mxGetN(prhs[3]);

  /*------------------------------------------------------------------------
   *  determine if A and B are real or complex and get the pointer
   *-----------------------------------------------------------------------*/
  realA = !mxIsComplex(prhs[0]);
  realB = !mxIsComplex(prhs[3]);

  if (realA) {
    A = mxGetDoubles(prhs[0]);
  } else {
    A_cpx = (double complex *)mxGetComplexDoubles(prhs[0]);
  }

  if (realB) {
    B = mxGetDoubles(prhs[3]);
  } else {
    B_cpx = (double complex *)mxGetComplexDoubles(prhs[3]);
  }

  /*------------------------------------------------------------------------
   *  allocate memory for ipiv
   *-----------------------------------------------------------------------*/
  info = mextools_malloc((void **)&ipiv, sizeof(mextools_int) * n);
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  allocate memory for LUAB the LU factorization of A
   *-----------------------------------------------------------------------*/
  ldLUAB = 2 * kl + ku + 1;
  if (realA) {
    info = mextools_malloc((void **)&LUAB, sizeof(double) * ldLUAB * n);
  } else {
    info = mextools_malloc((void **)&LUAB_cpx,
                           sizeof(double complex) * ldLUAB * n);
  }
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  copy bands to LUAB
   *-----------------------------------------------------------------------*/
  if (realA) {
    dlacpy("A", &no_bands, &n, A, &no_bands, LUAB + kl, &ldLUAB);
  } else {
    zlacpy("A", &no_bands, &n, A_cpx, &no_bands, LUAB_cpx + kl, &ldLUAB);
  }

  /*------------------------------------------------------------------------
   *  prepare output matrices X as solution of A*X = B and solve
   *-----------------------------------------------------------------------*/
  if (realA) {
    if (realB) {
      // A and B are real
      plhs[0] = mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxREAL);
      X = mxGetDoubles(plhs[0]);
      dlacpy("A", &n, &colsB, B, &n, X, &n);
      dgbsv(&n, &kl, &ku, &colsB, LUAB, &ldLUAB, ipiv, X, &n, &info);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dgbsv", "dgbsv returned %ld",
                        info);
    } else {
      // A is real and B is complex
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      info = mextools_malloc((void **)&B_real_imag,
                             sizeof(double) * 2 * n * colsB);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                        "mextools_malloc returned %ld", info);

      // copy real/imaginary part to first/second n *colsB columns of
      // B_real_imag
      const mextools_int n2 = n * colsB;
      dcopy(&n2, (double *)B_cpx, &itwo, B_real_imag, &ione);
      dcopy(&n2, (double *)B_cpx + 1, &itwo, B_real_imag + n2, &ione);
      colsB *= 2;
      dgbsv(&n, &kl, &ku, &colsB, LUAB, &ldLUAB, ipiv, B_real_imag, &n, &info);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dgbsv", "dgbsv returned %ld",
                        info);

      // copy result to X_cpx
      dcopy(&n2, B_real_imag, &ione, (double *)X_cpx, &itwo);
      dcopy(&n2, B_real_imag + n2, &ione, (double *)X_cpx + 1, &itwo);
    }
  } else {
    if (realB) {
      plhs[0] = mxCreateDoubleMatrix(n, colsB, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      mextools_int n2 = n * colsB;
      // copy B to realpart of X_cpx
      // X_cpx is already initial by 0
      dcopy(&n2, B, &ione, (double *)X_cpx, &itwo);
    } else {
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      // copy B to X_cpx
      zlacpy("A", &n, &colsB, B_cpx, &n, X_cpx, &n);
    }
    zgbsv(&n, &kl, &ku, &colsB, LUAB_cpx, &ldLUAB, ipiv, X_cpx, &n, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":zgbsv", "zgbsv returned %ld", info);
  }

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(B_real_imag);
  FREE_POINTER(LUAB);
  FREE_POINTER(LUAB_cpx);
  FREE_POINTER(ipiv);

  return info;
}
