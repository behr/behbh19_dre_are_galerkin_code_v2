//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *PBSV    Interface to {d,z}pbsv LAPACK routines.
 *
 *  # Purpose:

 *  Interface to LAPACK {d,z}pbsv routine for solving A X = B for a
 *  symmetric positive definite banded matrix A.
 *
 *  # Description:
 *
 *  PBSV solves a linear equation A X = B, where A is banded double or
 *  double complex, square, symmetric/hermitian positive definite matrix.
 *
 *    X = PBSV(AB, B, OVERWRITEA).
 *
 *  The upper triangular part of the matrix A must be given in
 *  LAPACK band storage format.
 *  That is the diagonals and the superdiagonals are stored in
 *  the rows of the matrix AB.
 *  If OVERWRITEA is nonzero AB is overwritten at exit by its Cholesky
 *  factor U. That is U' U = A. U is in banded storage. default is 0.
 *
 *  # Calling Sequence:
 *
 *    X = PBSV(AB, B).
 *    X = PBSV(AB, B, OVERWRITEA).
 *
 *  See also TO_BANDED, PBTRF, PBTRS and TEST_BANDED.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "PBSV"

/* mex_PBSV */
mextools_int mex_PBSV(int nlhs, mxArray *plhs[], int nrhs,
                      const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, kd = 0, info = 0;
  mextools_int no_bands = 0, colsB = 0;
  mextools_int realA = 0, realB = 0;
  mextools_int overwriteA = 0;
  const mextools_int ione = 1, itwo = 2;
  double *A = NULL, *LUAB = NULL, *X = NULL, *B = NULL, *B_real_imag = NULL;
  double complex *A_cpx = NULL, *LUAB_cpx = NULL, *X_cpx = NULL, *B_cpx = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // get the option overwriteA
  overwriteA = nrhs >= 3 ? mxGetScalar(prhs[0]) : 0;

  // size of banded A
  no_bands = mxGetM(prhs[0]);
  n = mxGetN(prhs[0]);
  kd = no_bands - 1;

  // check the size of B
  colsB = mxGetN(prhs[1]);

  /*------------------------------------------------------------------------
   *  determine if A and B are real or complex and get the pointer
   *-----------------------------------------------------------------------*/
  realA = !mxIsComplex(prhs[0]);
  realB = !mxIsComplex(prhs[1]);

  if (realA) {
    A = mxGetDoubles(prhs[0]);
  } else {
    A_cpx = (double complex *)mxGetComplexDoubles(prhs[0]);
  }

  if (realB) {
    B = mxGetDoubles(prhs[1]);
  } else {
    B_cpx = (double complex *)mxGetComplexDoubles(prhs[1]);
  }

  /*------------------------------------------------------------------------
   *  allocate memory for LUAB the Cholesky factorization of A
   *-----------------------------------------------------------------------*/
  if (!overwriteA) {
    if (realA) {
      info = mextools_malloc((void **)&LUAB, sizeof(double) * no_bands * n);
    } else {
      info = mextools_malloc((void **)&LUAB_cpx,
                             sizeof(double complex) * no_bands * n);
    }
    COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                      "mextools_malloc returned %ld", info);
  }

  /*------------------------------------------------------------------------
   *  copy bands to LUAB
   *-----------------------------------------------------------------------*/
  if (!overwriteA) {
    if (realA) {
      dlacpy("A", &no_bands, &n, A, &no_bands, LUAB, &no_bands);
    } else {
      zlacpy("A", &no_bands, &n, A_cpx, &no_bands, LUAB_cpx, &no_bands);
    }
  }

  /*------------------------------------------------------------------------
   *  prepare output matrices X as solution of A*X = B and solve
   *-----------------------------------------------------------------------*/
  if (realA) {
    if (realB) {
      // A and B are real
      plhs[0] = mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxREAL);
      X = mxGetDoubles(plhs[0]);
      dlacpy("A", &n, &colsB, B, &n, X, &n);
      if (overwriteA) {
        dpbsv("U", &n, &kd, &colsB, A, &no_bands, X, &n, &info);
      } else {
        dpbsv("U", &n, &kd, &colsB, LUAB, &no_bands, X, &n, &info);
      }
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dpbsv", "dpbsv returned %ld",
                        info);
    } else {
      // A is real and B is complex
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      info = mextools_malloc((void **)&B_real_imag,
                             sizeof(double) * 2 * n * colsB);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                        "mextools_malloc returned %ld", info);

      // copy real/imaginary part to first/second n *colsB columns of
      // B_real_imag
      const mextools_int n2 = n * colsB;
      dcopy(&n2, (double *)B_cpx, &itwo, B_real_imag, &ione);
      dcopy(&n2, (double *)B_cpx + 1, &itwo, B_real_imag + n2, &ione);
      colsB *= 2;
      if (overwriteA) {
        dpbsv("U", &n, &kd, &colsB, A, &no_bands, B_real_imag, &n, &info);
      } else {
        dpbsv("U", &n, &kd, &colsB, LUAB, &no_bands, B_real_imag, &n, &info);
      }
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dpbsv", "dpbsv returned %ld",
                        info);

      // copy result to X_cpx
      dcopy(&n2, B_real_imag, &ione, (double *)X_cpx, &itwo);
      dcopy(&n2, B_real_imag + n2, &ione, (double *)X_cpx + 1, &itwo);
    }
  } else {
    if (realB) {
      plhs[0] = mxCreateDoubleMatrix(n, colsB, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      mextools_int n2 = n * colsB;
      // copy B to realpart of X_cpx
      // X_cpx seems to be already initial by 0
      dcopy(&n2, B, &ione, (double *)X_cpx, &itwo);
    } else {
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);

      // copy B to X_cpx
      zlacpy("A", &n, &colsB, B_cpx, &n, X_cpx, &n);
    }
    if (overwriteA) {
      zpbsv("U", &n, &kd, &colsB, A_cpx, &no_bands, X_cpx, &n, &info);
    } else {
      zpbsv("U", &n, &kd, &colsB, LUAB_cpx, &no_bands, X_cpx, &n, &info);
    }
    COND_WARNING_GOTO(info, FREE, MEXNAME ":zpbsv", "zpbsv returned %ld", info);
  }

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(B_real_imag);
  FREE_POINTER(LUAB);
  FREE_POINTER(LUAB_cpx);

  return info;
}
