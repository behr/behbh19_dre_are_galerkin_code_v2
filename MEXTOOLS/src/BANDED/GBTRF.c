//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *GBTRF    Interface to {d,z}gbtrf LAPACK routines.
 *
 *  # Purpose:
 *
 *  Interface to LAPACK {d,z}gbtrf routines for computing an LU factorization
 *  of a banded matrix A.
 *
 *  # Description:
 *
 *  GBTRF computes a LU factorization of the banded matrix A.
 *  A must be a double or double complex n-by-n matrix.
 *
 *    [LU, IPIV] = GBTRF(AB, KL, KU).
 *
 *  KL and KU is the number of sub and superdiagonals of A.
 *  The matrix A must be given in LAPACK band storage format.
 *  That is the bands of A are stored in (KL + 1 + KU)-by-n matrix AB.
 *
 *  # Calling Sequence:
 *
 *    [LU, IPIV] = GBTRF(AB, KL, KU).
 *
 *  See also TO_BANDED, GBSV, GBTRS AND TEST_BANDED.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "GBTRF"

/* mex_GBTRF */
mextools_int mex_GBTRF(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " needs interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, kl = 0, ku = 0, info = 0;
  mextools_int no_bands = 0, ldLU = 0;
  mextools_int realA = 0;
  mextools_int *ipiv = NULL;
  double *A = NULL, *LU = NULL;
  double complex *A_cpx = NULL, *LU_cpx = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // size of banded A
  no_bands = mxGetM(prhs[0]);
  n = mxGetN(prhs[0]);

  // get KL and KU
  kl = mxGetScalar(prhs[1]);
  ku = mxGetScalar(prhs[2]);

  /*------------------------------------------------------------------------
   *  prepare output for LU and IPIV
   *-----------------------------------------------------------------------*/
  ldLU = 2 * kl + ku + 1;
  realA = !mxIsComplex(prhs[0]);
  if (realA) {
    A = mxGetDoubles(prhs[0]);
    plhs[0] = mxCreateDoubleMatrix(ldLU, n, mxREAL);
    LU = mxGetDoubles(plhs[0]);
    dlacpy("A", &no_bands, &n, A, &no_bands, LU + kl, &ldLU);
  } else {
    A_cpx = (double complex *)mxGetComplexDoubles(prhs[0]);
    plhs[0] = mxCreateDoubleMatrix(ldLU, n, mxCOMPLEX);
    LU_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
    zlacpy("A", &no_bands, &n, A_cpx, &no_bands, LU_cpx + kl, &ldLU);
  }

  plhs[1] = mxCreateUninitNumericMatrix(n, 1, mxINT64_CLASS, mxREAL);
  ipiv = (mextools_int *)mxGetInt64s(plhs[1]);

  /*------------------------------------------------------------------------
   *  prepare output matrices X as solution of A*X = B and solve
   *-----------------------------------------------------------------------*/
  if (realA) {
    dgbtrf(&n, &n, &kl, &ku, LU, &ldLU, ipiv, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":dgbtrf", "dgbtrf returned %ld",
                      info);
  } else {
    zgbtrf(&n, &n, &kl, &ku, LU_cpx, &ldLU, ipiv, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":zgbtrf", "zgbtrf returned %ld",
                      info);
  }

  /*------------------------------------------------------------------------
   *  check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:

  return info;
}
