//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *GTSV    Interface to {d,z}gtsv LAPACK routines.
 *
 *  # Purpose:
 *
 *  Interface to LAPACK {d,z}gtsv routines for solving tridiagonal
 *  linear systems A X = B.
 *
 *  # Description:
 *
 *  GTSV solves a linear equation A X = B, where A is tridiagonal double or
 *  double complex, square n-by-n matrix.
 *  The order n of the matrix A must be greater equals 2.
 *
 *    X = GTSV(T, B).
 *
 *  The subdiagonal, diagonal and superdiagonal are stored in the columns
 *  of the n-by-3 matrix T.
 *
 *  # Calling Sequence:
 *
 *    X = GTSV(T, B).
 *
 *  See also TO_TRIDIAG, GTTRF, GTTRS AND TEST_BANDED.
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "GTSV"

/* mex_GTSV */
mextools_int mex_GTSV(int nlhs, mxArray *plhs[], int nrhs,
                      const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " needs interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, info = 0;
  mextools_int colsB = 0;
  mextools_int realT = 0, realB = 0;
  const mextools_int ione = 1, itwo = 2, ithree = 3;
  double *T = NULL, *T2 = NULL, *X = NULL, *B = NULL, *B_real_imag = NULL;
  double complex *T_cpx = NULL, *T2_cpx = NULL, *X_cpx = NULL, *B_cpx = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // order of matrix
  n = mxGetM(prhs[0]);

  // number of columns of B
  colsB = mxGetN(prhs[1]);

  /*------------------------------------------------------------------------
   *  determine if A and B are real or complex and get the pointer
   *-----------------------------------------------------------------------*/
  realT = !mxIsComplex(prhs[0]);
  realB = !mxIsComplex(prhs[1]);

  if (realT) {
    T = mxGetDoubles(prhs[0]);
  } else {
    T_cpx = (double complex *)mxGetComplexDoubles(prhs[0]);
  }

  if (realB) {
    B = mxGetDoubles(prhs[1]);
  } else {
    B_cpx = (double complex *)mxGetComplexDoubles(prhs[1]);
  }

  /*------------------------------------------------------------------------
   *  allocate memory for copy of diagonals
   *-----------------------------------------------------------------------*/
  if (realT) {
    info = mextools_malloc((void **)&T2, sizeof(double) * n * ithree);
  } else {
    info =
        mextools_malloc((void **)&T2_cpx, sizeof(double complex) * n * ithree);
  }
  COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   *  copy subdiagonal, superdiagonal and diagonal to T2
   *-----------------------------------------------------------------------*/
  if (realT) {
    dlacpy("A", &n, &ithree, T, &n, T2, &n);
  } else {
    zlacpy("A", &n, &ithree, T_cpx, &n, T2_cpx, &n);
  }

  /*------------------------------------------------------------------------
   *  prepare output matrices X as solution of A*X = B and solve
   *-----------------------------------------------------------------------*/
  if (realT) {
    if (realB) {
      // T and B are real
      plhs[0] = mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxREAL);
      X = mxGetDoubles(plhs[0]);
      dlacpy("A", &n, &colsB, B, &n, X, &n);
      dgtsv(&n, &colsB, T2, T2 + n, T2 + 2 * n, X, &n, &info);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dgtsv", "dgtsv returned %ld",
                        info);
    } else {
      // A is real and B is complex
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      info = mextools_malloc((void **)&B_real_imag,
                             sizeof(double) * 2 * n * colsB);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":mextools_malloc",
                        "mextools_malloc returned %ld", info);

      // copy real/imaginary part to first/second n *colsB columns of
      // B_real_imag
      const mextools_int n2 = n * colsB;
      dcopy(&n2, (double *)B_cpx, &itwo, B_real_imag, &ione);
      dcopy(&n2, (double *)B_cpx + 1, &itwo, B_real_imag + n2, &ione);
      colsB *= 2;
      dgtsv(&n, &colsB, T2, T2 + n, T2 + 2 * n, B_real_imag, &n, &info);
      COND_WARNING_GOTO(info, FREE, MEXNAME ":dgtsv", "dgtsv returned %ld",
                        info);

      // copy result to X_cpx
      dcopy(&n2, B_real_imag, &ione, (double *)X_cpx, &itwo);
      dcopy(&n2, B_real_imag + n2, &ione, (double *)X_cpx + 1, &itwo);
    }
  } else {
    if (realB) {
      plhs[0] = mxCreateDoubleMatrix(n, colsB, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      mextools_int n2 = n * colsB;
      // copy B to realpart of X_cpx
      // X_cpx is already initial by 0
      dcopy(&n2, B, &ione, (double *)X_cpx, &itwo);
    } else {
      plhs[0] =
          mxCreateUninitNumericMatrix(n, colsB, mxDOUBLE_CLASS, mxCOMPLEX);
      X_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
      // copy B to X_cpx
      zlacpy("A", &n, &colsB, B_cpx, &n, X_cpx, &n);
    }
    zgtsv(&n, &colsB, T2_cpx, T2_cpx + n, T2_cpx + 2 * n, X_cpx, &n, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":zgtsv", "zgtsv returned %ld", info);
  }

  /*------------------------------------------------------------------------
   *  free allocated memory and check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(B_real_imag);
  FREE_POINTER(T2);
  FREE_POINTER(T2_cpx);

  return info;
}
