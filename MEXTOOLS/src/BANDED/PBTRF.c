//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *PBTRF    Interface to {d,z}pbtrf LAPACK routines.
 *
 *  # Purpose:
 *
 *  Interface to LAPACK {d,z}pbtrf routines for Cholesky factorization of
 *  symmetric positive definite banded matrix A.
 *
 *  # Description:
 *
 *  PBTRF computes a Cholesky factorization of banded, double or
 *  double complex, square, symmetric/hermitian positive definite matrix A.
 *
 *    U = PBTRF(AB).
 *
 *  The upper triangular part of the matrix A must be given in
 *  LAPACK band storage format.
 *  That is the diagonals and the superdiagonals are stored in
 *  the rows of the matrix AB. U is banded and U'*U is A.
 *
 *  # Calling Sequence:
 *
 *    U = PBTRF(AB).
 *
 *  See also TO_BANDED, PBSV, PBTRS and TEST_BANDED.
 *
 *  Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "PBTRF"

/* mex_PBTRF */
mextools_int mex_PBTRF(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]) {

  UNUSED_VARIABLE(nlhs);
  UNUSED_VARIABLE(nrhs);

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning "PBTRF is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", "PBTRF need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, kd = 0, info = 0;
  mextools_int no_bands = 0, realA = 0;
  double *A = NULL, *U = NULL;
  double complex *A_cpx = NULL, *U_cpx = NULL;

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // size of banded A
  no_bands = mxGetM(prhs[0]);
  n = mxGetN(prhs[0]);
  kd = no_bands - 1;

  /*------------------------------------------------------------------------
   *  prepare output matrices U
   *-----------------------------------------------------------------------*/
  realA = !mxIsComplex(prhs[0]);
  if (realA) {
    A = mxGetDoubles(prhs[0]);
    // plhs[0] = mxCreateDoubleMatrix(no_bands, n, mxREAL);
    plhs[0] = mxCreateUninitNumericMatrix(no_bands, n, mxDOUBLE_CLASS, mxREAL);
    U = mxGetDoubles(plhs[0]);
    dlacpy("A", &no_bands, &n, A, &no_bands, U, &no_bands);
  } else {
    A_cpx = (double complex *)mxGetComplexDoubles(prhs[0]);
    // plhs[0] = mxCreateDoubleMatrix(no_bands, n, mxCOMPLEX);
    plhs[0] =
        mxCreateUninitNumericMatrix(no_bands, n, mxDOUBLE_CLASS, mxCOMPLEX);
    U_cpx = (double complex *)mxGetComplexDoubles(plhs[0]);
    zlacpy("A", &no_bands, &n, A_cpx, &no_bands, U_cpx, &no_bands);
  }

  /*------------------------------------------------------------------------
   *  compute cholesky factorization
   *-----------------------------------------------------------------------*/
  if (realA) {
    dpbtrf("U", &n, &kd, U, &no_bands, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":dpbtrf", "dpbtrf returned %ld",
                      info);
  } else {
    zpbtrf("U", &n, &kd, U_cpx, &no_bands, &info);
    COND_WARNING_GOTO(info, FREE, MEXNAME ":zpbtrf", "zpbtrf returned %ld",
                      info);
  }

  /*------------------------------------------------------------------------
   *  check for nonzero info
   *-----------------------------------------------------------------------*/
FREE:
  return info;
}
