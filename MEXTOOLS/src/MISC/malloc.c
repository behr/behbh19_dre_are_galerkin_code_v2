//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/* MEXTOOLS HEADER */
#include "mextools.h"

/**
 * @brief Allocates memory.
 * @param[in,out] memptr    pointer
 * @param[in] size          size of bytes
 * @return 0 on success, otherwise a nonzero value.
 *
 *  The function @ref mextools_malloc allocates @p size bytes
 *  and places the adress of the allocated memory
 *  to @p *memtpr.
 */
int mextools_malloc(void **memptr, size_t size) {
#if defined(__MINGW32__) || defined(__MINGW64__)
  *memptr = malloc(size);
  return *memptr == NULL;
#else
  return posix_memalign(memptr, ALIGNMENT, size);
#endif
}
