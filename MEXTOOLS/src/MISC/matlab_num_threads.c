//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/* MEXTOOLS HEADER */
#include "mextools.h"

/**
 * @brief Get the number of computational threads of MATLAB.
 * @return Number of computational threads used by MATLAB.
 *
 *  The function @ref mextools_matlab_get_num_threads calls the
 *  MATLAB function maxNumCompThreads and returns the current
 *  number of computational threds used by MATLAB.
 */
int mextools_matlab_get_num_threads(void) {
  mxArray *lhs = NULL;
  mexCallMATLAB(1, &lhs, 0, NULL, "maxNumCompThreads");
  int num_threads = mxGetScalar(lhs);
  mxDestroyArray(lhs);
  return num_threads;
}

/**
 * @brief Set the number of computational threads of MATLAB.
 * @param[in] num_threads   number of desired threads
 * @return nothing.
 *
 *  The function @ref mextools_matlab_set_num_threads calls the
 *  MATLAB function maxNumCompThreads and sets number of
 *  computational threads used by MATLAB.
 */
void mextools_matlab_set_num_threads(mextools_int num_threads) {
  mxArray *rhs = NULL;
  rhs = mxCreateDoubleScalar(MAX(1, num_threads));
  mexCallMATLAB(0, NULL, 1, &rhs, "maxNumCompThreads");
  mxDestroyArray(rhs);
}

/**
 * @brief Reset the number of computational threads of MATLAB to default.
 * @return nothing.
 *
 *  The function @ref mextools_matlab_set_default_num_threads calls the
 *  MATLAB function maxNumCompThreads and sets number of
 *  computational threads used by MATLAB to the default value.
 */
void mextools_matlab_set_default_num_threads(void) {
  mxArray *rhs = NULL;
  rhs = mxCreateString("automatic");
  mexCallMATLAB(0, NULL, 1, &rhs, "maxNumCompThreads");
  mxDestroyArray(rhs);
}
