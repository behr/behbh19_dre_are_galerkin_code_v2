// The MIT License (MIT)
//
// Copyright (c) 2016 Elmar Peise
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* MDM_SYM_DRE_STEP HEADER */
#include "mdm_sym_dre_step.h"

#define DREC_SPLIT(n) ((n >= 16) ? ((n + 8) / 16) * 8 : n / 2)

static void ReLAPACK_dgetrf_rec(const mextools_int *, const mextools_int *,
                                double *, const mextools_int *, mextools_int *,
                                mextools_int *, mextools_int);

/** DGETRF computes an LU factorization of a general M-by-N matrix A using
 * partial pivoting with row mextools_interchanges.
 *
 * This routine is functionally equivalent to LAPACK's dgetrf.
 * For details see
 * http://www.netlib.org/lapack/explore-html/d3/d6a/dgetrf_8f.html
 * */
void ReLAPACK_dgetrf(const mextools_int *m, const mextools_int *n, double *A,
                     const mextools_int *ldA, mextools_int *ipiv,
                     mextools_int *info) {
  ReLAPACK_dgetrfx(m, n, A, ldA, ipiv, info, 24);
}

void ReLAPACK_dgetrfx(const mextools_int *m, const mextools_int *n, double *A,
                      const mextools_int *ldA, mextools_int *ipiv,
                      mextools_int *info, mextools_int CROSSOVER) {

  // Check arguments
  *info = 0;
  if (*m < 0)
    *info = -1;
  else if (*n < 0)
    *info = -2;
  else if (*ldA < MAX(1, *m))
    *info = -4;
  if (*info) {
    return;
  }

  mextools_int sn = MIN(*m, *n);

  ReLAPACK_dgetrf_rec(m, &sn, A, ldA, ipiv, info, CROSSOVER);

  // Right remainder
  if (*m < *n) {
    // ants
    double ONE[] = {1.};
    mextools_int iONE[] = {1.};

    // Splitting
    mextools_int rn = *n - *m;

    // A_L A_R
    double *A_L = A;
    double *A_R = A + *ldA * *m;

    // A_R = apply(ipiv, A_R)
    dlaswp(&rn, A_R, ldA, iONE, m, ipiv, iONE);
    // A_R = A_S \ A_R
    dtrsm("L", "L", "N", "U", m, &rn, ONE, A_L, ldA, A_R, ldA);
  }
}

/** dgetrf's recursive compute kernel */
static void ReLAPACK_dgetrf_rec(const mextools_int *m, const mextools_int *n,
                                double *A, const mextools_int *ldA,
                                mextools_int *ipiv, mextools_int *info,
                                mextools_int CROSSOVER) {

  if (*n <= MAX(CROSSOVER, 1)) {
    // Unblocked
    dgetf2(m, n, A, ldA, ipiv, info);
    return;
  }

  // ants
  double ONE[] = {1.};
  double MONE[] = {-1.};
  mextools_int iONE[] = {1};

  // Splitting
  mextools_int n1 = DREC_SPLIT(*n);
  mextools_int n2 = *n - n1;
  mextools_int m2 = *m - n1;

  // A_L A_R
  double *A_L = A;
  double *A_R = A + *ldA * n1;

  // A_TL A_TR
  // A_BL A_BR
  double *A_TL = A;
  double *A_TR = A + *ldA * n1;
  double *A_BL = A + n1;
  double *A_BR = A + *ldA * n1 + n1;

  // ipiv_T
  // ipiv_B
  mextools_int *ipiv_T = ipiv;
  mextools_int *ipiv_B = ipiv + n1;

  // recursion(A_L, ipiv_T)
  ReLAPACK_dgetrf_rec(m, &n1, A_L, ldA, ipiv_T, info, CROSSOVER);
  // apply pivots to A_R
  dlaswp(&n2, A_R, ldA, iONE, &n1, ipiv_T, iONE);

  // A_TR = A_TL \ A_TR
  dtrsm("L", "L", "N", "U", &n1, &n2, ONE, A_TL, ldA, A_TR, ldA);
  // A_BR = A_BR - A_BL * A_TR
  dgemm("N", "N", &m2, &n2, &n1, MONE, A_BL, ldA, A_TR, ldA, ONE, A_BR, ldA);

  // recursion(A_BR, ipiv_B)
  ReLAPACK_dgetrf_rec(&m2, &n2, A_BR, ldA, ipiv_B, info, CROSSOVER);
  if (*info)
    *info += n1;
  // apply pivots to A_BL
  dlaswp(&n1, A_BL, ldA, iONE, &n2, ipiv_B, iONE);
  // shift pivots
  mextools_int i;
  for (i = 0; i < n2; i++)
    ipiv_B[i] += n1;
}
