//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/**
 * @file MDM_SYM_DRE_STEP_unblocked.c
 * @brief Perform a time step of Davison-Maki method for symmetric
 * differential Riccati equations using unblocked code.
 * @author Maximilian Behr
 */

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* MDM_SYM_DRE_STEP HEADER */
#include "mdm_sym_dre_step.h"

/**
 * @brief Perform a step of the modified Davison-Maki method unblocked variant.
 * @param[in] exp_hH11T     transpose (1,1) block of matrix exponential
 * @param[in] exp_hH12T     transpose (1,2) block of matrix exponential
 * @param[in] exp_hH21T     transpose (2,1) block of matrix exponential
 * @param[in] exp_hH22T     transpose (2,2) block of matrix exponential
 * @param[in,out] Xk        the current iterate
 * @param[in] n             order of the matrices
 * @param[in] trf           factorization method
 * @param[in] mm            multiplication method
 * @param[in] ldUV2P        leading dimension parameter
 * @return 0 on succes, otherwise a nonzero value.
 *
 * @ref MDM_SYM_DRE_STEP_unblocked performs one step of the
 * modified Davison-Maki method applied to symmetric DREs.
 * One time step of the modified Davison Maki method is given by
 *
 *      UT = exp_hH11T + Xk * exp_hH12T (i)
 *      VT = exp_hH21T + Xk * exp_hH22T (ii)
 *      Xk = UT \ VT                    (iii)
 *      Xk = 1 / 2 * (Xk + Xk')         (iv)
 *
 * Xk must be a symmetric n-by-n matrix. The
 * matrices @p exp_hH11T, @p exp_hH12T, @p exp_hH21T
 * and @p exp_hH22T must be also n-by-n matrices.
 * Morever we assume that all matrices are given
 * in column-major format and have the leading dimension
 * @p n. On exit the matrix @p Xk is overwritten
 * by the result of the time step.
 *
 */
int MDM_SYM_DRE_STEP_unblocked(const double *restrict exp_hH11T,
                               const double *restrict exp_hH12T,
                               const double *restrict exp_hH21T,
                               const double *restrict exp_hH22T,
                               double *restrict Xk, const mextools_int n,
                               const mextools_int trf, const mextools_int mm,
                               const mextools_int ldUV2P) {
  /*------------------------------------------------------------------------
   * local defines
   *-----------------------------------------------------------------------*/
#undef MDM_FUN
#define MDM_FUN "MDM_SYM_DRE_STEP_unblocked"

  /*------------------------------------------------------------------------
   * variables
   *-----------------------------------------------------------------------*/
  mextools_int info = 0;
  mextools_int *restrict ipiv = NULL;
  const double done = 1.0;
  double *restrict UT = NULL, *restrict VT = NULL;

  /*------------------------------------------------------------------------
   * If ldUV2P is smaller equals 0 set leading dimension to n.
   * if ldUV2P is positive set leading dimension to next
   * multiple of 2**ldUV2P.
   *-----------------------------------------------------------------------*/
  mextools_int _ldUV2P = n;
  if (ldUV2P > 0) {
    mextools_int temp = 1L << ldUV2P;
    mextools_int rem = n % temp;
    if (rem) {
      _ldUV2P = n + MAX(temp - rem, 0);
    }
  }
  const mextools_int ldUT = _ldUV2P;
  const mextools_int ldVT = _ldUV2P;

  /*------------------------------------------------------------------------
   * allocate memory for UT, VT and ipiv
   *-----------------------------------------------------------------------*/
  info = mextools_malloc((void **)&UT, sizeof(double) * ldUT * n);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  info = mextools_malloc((void **)&VT, sizeof(double) * ldVT * n);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  info = mextools_malloc((void **)&ipiv, sizeof(mextools_int) * n);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  /*------------------------------------------------------------------------
   * Step (i):  UT = exp_hH11T + Xk * exp_hH12T
   * Step (ii): VT = exp_hH21T + Xk * exp_hH22T
   *-----------------------------------------------------------------------*/
  // UT <- exp_hH11T, VT <- exp_hH21T
  dlacpy("A", &n, &n, exp_hH11T, &n, UT, &ldUT);
  dlacpy("A", &n, &n, exp_hH21T, &n, VT, &ldVT);

  // UT <- exp_hH11T + Xk * exp_hH12T, VT <- exp_hH21T + XK * exp_hH22T
  if (mm <= 1) {
    dsymm("L", "L", &n, &n, &done, Xk, &n, exp_hH12T, &n, &done, UT, &ldUT);
    dsymm("L", "L", &n, &n, &done, Xk, &n, exp_hH22T, &n, &done, VT, &ldVT);
  } else if (mm == 2) {
    dgemm("N", "N", &n, &n, &n, &done, Xk, &n, exp_hH12T, &n, &done, UT, &ldUT);
    dgemm("N", "N", &n, &n, &n, &done, Xk, &n, exp_hH22T, &n, &done, VT, &ldVT);
  } else  {
    // Xk is symmetric! It may give better memory access.
    dgemm("T", "N", &n, &n, &n, &done, Xk, &n, exp_hH12T, &n, &done, UT, &ldUT);
    dgemm("T", "N", &n, &n, &n, &done, Xk, &n, exp_hH22T, &n, &done, VT, &ldVT);
  }

  /*------------------------------------------------------------------------
   *  Step (iii): Solve the system: Xk = UT \ VT
   *-----------------------------------------------------------------------*/
  info = MDM_dgetrf(n, n, UT, ldUT, ipiv, trf);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":MDM_dgetrf", "dgetrf returned %ld",
                    info);

  dgetrs("N", &n, &n, UT, &ldUT, ipiv, VT, &ldVT, &info);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":mdm_dgetrs", "dgetrs returned %ld",
                    info);

  /*------------------------------------------------------------------------
   * Step (iv): Xk = 1 / 2 * (Xk + Xk')
   *-----------------------------------------------------------------------*/
  MDM_symmetrize(n, VT, ldVT);

  /*------------------------------------------------------------------------
   * copy VT to Xk
   *-----------------------------------------------------------------------*/
  dlacpy("A", &n, &n, VT, &ldVT, Xk, &n);

  /*------------------------------------------------------------------------
   *  free allocated memory
   *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(UT);
  FREE_POINTER(VT);
  FREE_POINTER(ipiv);
  return info;
}
