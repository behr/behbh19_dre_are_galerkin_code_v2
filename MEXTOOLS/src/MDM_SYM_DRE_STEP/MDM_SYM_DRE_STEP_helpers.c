//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/**
 * @file MDM_SYM_DRE_STEP_helpers.c
 * @brief Helper functions for modified Davison-Maki step functions.
 * @author Maximilian Behr
 */

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* MDM_SYM_DRE_STEP HEADER */
#include "mdm_sym_dre_step.h"

/**
 * @brief Transpose copy of a matrix
 * @param[in] m           number of rows of the matrix @p A
 * @param[in] n           number of columns of the matrix @p A
 * @param[in] A           matrix @p A in column major format
 * @param[in] ldA         leading dimension of @p A
 * @param[out] B          on exit the transpose of @p A
 * @param[in] ldB         leading dimension of @p B
 *
 *  @ref MDM_copy_trans copy the transpose
 *  @p A to @p B. @p A and @p B are in column major format.
 */
inline void MDM_copy_trans(const mextools_int m, const mextools_int n,
                           const double *const restrict A,
                           const mextools_int ldA, double *const restrict B,
                           const mextools_int ldB) {
#ifdef NO_DOMATCOPY
  for (mextools_int j = 0; j < n; ++j) {
    for (mextools_int i = 0; i < m; ++i) {
      B[j + i * ldB] = A[i + j * ldA];
    }
  }
  return;
#else
  const double done = 1;
  mkl_domatcopy("C", "T", &m, &n, &done, A, &ldA, B, &ldB);
  return;
#endif
}

/**
 * @brief Wrapper around LU factorization routines from LAPACK
 * @param[in] m           number of rows of the matrix @p A
 * @param[in] n           number of columns of the matrix @p A
 * @param[in, out] A      the matrix for factorization
 * @param[in] ldA         leading dimension of @p A
 * @param[in, out] ipiv   pivoting vector
 * @param[in] trf         the LU factorization variant
 * @return 0 on success, otherwise a nonzero value.
 *
 *  The function @ref MDM_dgetrf is a wrapper around
 *  the routines dgetrf and dgetrf2.
 */
inline int MDM_dgetrf(mextools_int m, mextools_int n, double *const restrict A,
                      mextools_int ldA, mextools_int *const restrict ipiv,
                      mextools_int trf) {
  mextools_int info = 0;
  if (trf <= 1) {
    dgetrf(&m, &n, A, &ldA, ipiv, &info);
  } else if (trf == 2) {
    dgetrf2(&m, &n, A, &ldA, ipiv, &info);
  } else if (trf == 3) {
    ReLAPACK_dgetrfx(&m, &n, A, &ldA, ipiv, &info, 24);
  } else if (trf == 4) {
    ReLAPACK_dgetrfx(&m, &n, A, &ldA, ipiv, &info, 48);
  } else if (trf == 5) {
    dgetf2(&m, &n, A, &ldA, ipiv, &info);
  } else if (trf == 6) {
    cr_dgetrf(&m, &n, A, &ldA, ipiv, &info);
  } else if (trf == 7) {
    ll_dgetrf(&m, &n, A, &ldA, ipiv, &info);
  } else {
    rec_dgetrf(&m, &n, A, &ldA, ipiv, &info);
  }
  return info;
}

/**
 * @brief Symmetrize a square matrix by copying the lower to the upper
 * tridiagonal part.
 * @param[in] n         The order of the matrix a. n>=0.
 * @param[in,out] a     a is a double precision array of dimension n (col
 * major!).
 * @param[in] lda       The leading dimension of the array a.
 *
 * @ref MDM_symmetrize performs symmetrizes a matrix @p a by copying the
 * lower to the upper block.
 *
 */
void MDM_symmetrize(mextools_int n, double *restrict a, mextools_int lda) {
  mextools_int ione = 1;
  for (mextools_int i = 0; i < n - 1; ++i) {
    mextools_int tmp = n - (i + 1);
    dcopy(&tmp, a + (i + 1) + i * lda, &ione, a + i + (i + 1) * lda, &lda);
  }
}

#if 0
// old implementation the above if faster
void mdm_symmetrize(mextools_int n, double *restrict a, mextools_int lda) {
  for (mextools_int j = 0; j < n; ++j) {
    for (mextools_int i = 0; i < j; ++i) {
      a[i + j * lda] = a[j + i * lda];
    }
  }
}
#endif
