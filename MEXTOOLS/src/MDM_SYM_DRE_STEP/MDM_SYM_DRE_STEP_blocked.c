//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/**
 * @file MDM_SYM_DRE_STEP_blocked.c
 * @brief Perform a time step of Davison-Maki method for symmetric
 * differential Riccati equations.
 * @author Maximilian Behr
 */

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* MDM_SYM_DRE_STEP HEADER */
#include "mdm_sym_dre_step.h"

/**
 * @brief Perform a step of the modified Davison-Maki method.
 * @param[in] exp_hH11T     transpose (1,1) block of matrix exponential
 * @param[in] exp_hH12T     transpose (1,2) block of matrix exponential
 * @param[in] exp_hH21T     transpose (2,1) block of matrix exponential
 * @param[in] exp_hH22T     transpose (2,2) block of matrix exponential
 * @param[in,out] Xk        the current iterate
 * @param[in] n             order of the matrices
 * @param[in] bs            blocksize
 * @param[in] trf           factorization method
 * @param[in] mm            matrix multiplication method
 * @param[in] LUcpy         copy diagonal blocks of LU decomposition
 * @param[in] Lsolve        variant for solving with L
 * @param[in] Usolve        variant for solving with U
 * @param[in] ldUV2P        leading dimension parameter
 *
 * @ref MDM_SYM_DRE_STEP_fun performs one step of the
 * modified Davison-Maki method applied to symmetric DREs.
 * One time step of the modified Davison Maki method is given by
 *
 *      UT = exp_hH11T + Xk * exp_hH12T (i)
 *      VT = exp_hH21T + Xk * exp_hH22T (ii)
 *      Xk = UT \ VT                    (iii)
 *      Xk = 1 / 2 * (Xk + Xk')         (iv)
 *
 * Xk must be a symmetric n-by-n matrix. The
 * matrices @p exp_hH11T, @p exp_hH12T, @p exp_hH21T
 * and @p exp_hH22T must be also n-by-n matrices.
 * Morever we assume that all matrices are given
 * in column-major format and have the leading dimension
 * @p n. On exit the matrix @p Xk is overwritten
 * by the result of the time step.
 *
 */
int MDM_SYM_DRE_STEP_blocked(const double *const restrict exp_hH11T,
                             const double *const restrict exp_hH12T,
                             const double *const restrict exp_hH21T,
                             const double *const restrict exp_hH22T,
                             double *restrict Xk, const mextools_int n,
                             const mextools_int bs, const mextools_int trf,
                             const mextools_int mm, const mextools_int LUcpy,
                             const mextools_int Lsolve,
                             const mextools_int Usolve,
                             const mextools_int ldUV2P) {
  /*------------------------------------------------------------------------
   * call unblocked code if blocksize is larger equals n
   *-----------------------------------------------------------------------*/
  if (bs <= 0 || bs >= n) {
    return MDM_SYM_DRE_STEP_unblocked(exp_hH11T, exp_hH12T, exp_hH21T,
                                      exp_hH22T, Xk, n, trf, mm, ldUV2P);
  }

  /*------------------------------------------------------------------------
   * local defines
   *-----------------------------------------------------------------------*/
#undef MDM_FUN
#define MDM_FUN "MDM_SYM_DRE_STEP_blocked"
#if 0
  PRINT_MESSAGE("bs      = %d\n", bs);
  PRINT_MESSAGE("trf     = %d\n", trf);
  PRINT_MESSAGE("mm      = %d\n", mm);
  PRINT_MESSAGE("LUcpy   = %d\n", LUcpy);
  PRINT_MESSAGE("Lsolve  = %d\n", Lsolve);
  PRINT_MESSAGE("Usolve  = %d\n", Usolve);
  PRINT_MESSAGE("ldUV2P  = %d\n", ldUV2P);
#endif

  /*------------------------------------------------------------------------
   * variables
   *-----------------------------------------------------------------------*/
  const mextools_int ione = 1;
  mextools_int info = 0;
  mextools_int *restrict ipiv = NULL, *restrict ipiv_perm = NULL;
  const double done = 1.0, dmone = -1.0;
  double *restrict UT = NULL, *restrict VT = NULL, *restrict LUmem = NULL;

  /*------------------------------------------------------------------------
   * number of blocks in one direction and size of last block
   *-----------------------------------------------------------------------*/
  const mextools_int no_blocks = n / bs + ((n % bs) > 0);
  const mextools_int bs_last = ((n % bs) > 0) ? (n % bs) : bs;

  /*------------------------------------------------------------------------
   * Set the leading dimension of UT and VT to be
   * a multiple of the blocksize, if ldUV2P is smaller equals -1.
   * If ldUV2P is equals 0 set leading dimension to n.
   * if ldUV2P is positive set leading dimension to next
   * multiple of 2**ldUV2P.
   *-----------------------------------------------------------------------*/
  mextools_int _ldUV2P = n;
  if (ldUV2P <= -1) {
    _ldUV2P = n + MAX(bs - bs_last, 0);
  } else if (ldUV2P == 0) {
    _ldUV2P = n;
  } else {
    mextools_int temp = 1L << ldUV2P;
    mextools_int rem = n % temp;
    if (rem) {
      _ldUV2P = n + MAX(temp - rem, 0);
    }
  }
  const mextools_int ldUT = _ldUV2P;
  const mextools_int ldVT = _ldUV2P;

  /*------------------------------------------------------------------------
   * allocate memory for UT, VT, ipiv and LUmem
   *-----------------------------------------------------------------------*/
  info = mextools_malloc((void **)&UT, sizeof(double) * ldUT * n);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  info = mextools_malloc((void **)&VT, sizeof(double) * ldVT * n);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  info = mextools_malloc((void **)&ipiv, sizeof(mextools_int) * n);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  info = mextools_malloc((void **)&ipiv_perm, sizeof(mextools_int) * n);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                    "mextools_malloc returned %ld", info);

  if (LUcpy >= 2) {
    info =
        mextools_malloc((void **)&LUmem, sizeof(double) * no_blocks * bs * n);
    COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                      "mextools_malloc returned %ld", info);
  } else if (LUcpy == 1) {
    info = mextools_malloc((void **)&LUmem, sizeof(double) * bs * n);
    COND_WARNING_GOTO(info, FREE, MDM_FUN ":mextools_malloc",
                      "mextools_malloc returned %ld", info);
  }

  /*------------------------------------------------------------------------
   * Step (i):  UT = exp_hH11T + Xk * exp_hH12T
   *-----------------------------------------------------------------------*/
  // UT <- exp_hH11T
  dlacpy("A", &n, &n, exp_hH11T, &n, UT, &ldUT);

  // UT <- exp_hH11T + Xk * exp_hH12T
  if (mm <= 1) {
    dsymm("L", "L", &n, &n, &done, Xk, &n, exp_hH12T, &n, &done, UT, &ldUT);
  } else if (mm == 2) {
    dgemm("N", "N", &n, &n, &n, &done, Xk, &n, exp_hH12T, &n, &done, UT, &ldUT);
  } else {
    // Xk is symmetric. It may give better memory access.
    dgemm("T", "N", &n, &n, &n, &done, Xk, &n, exp_hH12T, &n, &done, UT, &ldUT);
  }

  /*------------------------------------------------------------------------
   *  Prepare for Step (iii) Compute the LU factorization of UT
   *-----------------------------------------------------------------------*/
  info = MDM_dgetrf(n, n, UT, ldUT, ipiv, trf);
  COND_WARNING_GOTO(info, FREE, MDM_FUN ":MDM_dgetrf", "dgetrf returned %ld",
                    info);

  /*------------------------------------------------------------------------
   * Step (ii): VT = exp_hH21T + Xk * exp_hH22T
   *
   * The permutation ipiv has to be applied to VT.
   * VT is given by VT = exp_hH21T + XK * exp_hH22T.
   *
   * We could first compute VT = exp_hH21T + Xk * exp_hH22T
   * and then apply ipiv.
   *
   * We only need the block upper triangular part of ipiv * VT
   * for the next steps.
   * Therefore apply ipiv to exp_hH21T and Xk first and compute
   * then the block upper triangular part of
   * ipiv exp_hH21T + ipiv * Xk * exp_hH22T using dgemm.
   *-----------------------------------------------------------------------*/
  // VT <- exp_hH21T
  dlacpy("A", &n, &n, exp_hH21T, &n, VT, &ldVT);

  // apply ipiv to VT
  dlaswp(&n, VT, &ldVT, &ione, &n, ipiv, &ione);

  // transform ipiv to a permutation
  for (mextools_int i = 0; i < n; ++i) {
    ipiv_perm[i] = i + 1;
  }

  for (mextools_int i = 0; i < n; ++i) {
    mextools_int temp = ipiv_perm[ipiv[i] - 1];
    ipiv_perm[ipiv[i] - 1] = ipiv_perm[i];
    ipiv_perm[i] = temp;
  }

  // apply the permutation backward to the columns of Xk
  dlapmt(&ione, &n, &n, Xk, &n, ipiv_perm);

  // compute each block upper triangular part of ipiv * VT
  // ipiv * VT = ipiv * exp_hH21T + ipiv * Xk * exp_hH22T.
    for (mextools_int j = 0; j < no_blocks; ++j) {
      mextools_int m2 = MIN((j + 1) * bs, n);
      mextools_int bs2 = (j + 1 >= no_blocks) ? bs_last : bs;
      dgemm("T", "N", &m2, &bs2, &n, &done, Xk, &n,
            BLOCKIDX(exp_hH22T, 0, j, bs, n), &n, &done,
            BLOCKIDX(VT, 0, j, bs, ldVT), &ldVT);
    }

  /*------------------------------------------------------------------------
   * Step (iii): Xk = UT \ VT
   *-----------------------------------------------------------------------*/

// access block (i, j) in LUmem for LUcpy >= 3
#define BLOCKIDX_LU_MEM3(A, I, J, BS, N) (A) + (J) * (BS) + (I) * (BS) * (N)

  // access block (i, j) in LUmem for LUcpy == 2
#define BLOCKIDX_LU_MEM2(A, I, J, BS, N)                                       \
  (A) + (J) * (BS) * (BS) + (I) * (BS) * (N)

// access block (i, j) in LUmem for LUcpy == 1
#define BLOCKIDX_LU_MEM1(A, I, BS) (A) + (I) * (BS) * (BS)

  if (LUcpy >= 3) {
    for (mextools_int i = 0; i < no_blocks; ++i) {
      mextools_int bs2 = (i + 1 >= no_blocks) ? bs_last : bs;
      MDM_copy_trans(bs2, n, BLOCKIDX(UT, i, 0, bs, ldUT), ldUT,
                     BLOCKIDX_LU_MEM3(LUmem, i, 0, bs, n), n);
    }
  } else if (LUcpy == 2) {
    // copy row blocks of LU to the LUmem
    for (mextools_int i = 0; i < no_blocks; ++i) {
      mextools_int bs2 = (i + 1 >= no_blocks) ? bs_last : bs;
      dlacpy("A", &bs2, &n, BLOCKIDX(UT, i, 0, bs, ldUT), &ldUT,
             BLOCKIDX_LU_MEM2(LUmem, i, 0, bs, n), &bs);
    }
  } else if (LUcpy == 1) {
    // copy diagonal blocks of LU to LUmem
    for (mextools_int i = 0; i < no_blocks; ++i) {
      mextools_int bs2 = (i + 1 >= no_blocks) ? bs_last : bs;
      dlacpy("A", &bs2, &bs2, BLOCKIDX(UT, i, i, bs, ldUT), &ldUT,
             BLOCKIDX_LU_MEM1(LUmem, i, bs), &bs);
    }
  }

  /*------------------------------------------------------------------------
   * Step (iii): Xk = UT \ VT, solve with L
   *-----------------------------------------------------------------------*/
  // Lsolve >= 3:
  //  This variant solves in block column-wise fashion for the block upper
  //  diagonal part. The variable bs2 is the number of columns of the
  //  current column block. The variable n2 is the number of rows
  //  of the current column block. The variant solves for each
  //  column block from the left to the right.
  if (Lsolve >= 3) {
    for (mextools_int i = 0; i < no_blocks; ++i) {
      mextools_int bs2 = (i + 1 >= no_blocks) ? bs_last : bs;
      mextools_int n2 = MIN((i + 1) * bs, n);
      dtrsm("L", "L", "N", "U", &n2, &bs2, &done, UT, &ldUT,
            BLOCKIDX(VT, 0, i, bs, ldVT), &ldVT);
    }
  }  else if (Lsolve == 2) {
    //
    // This variant solves in block rows-wise fashion for the block upper
    // diagonal part. The variable bs2 is the number of rows of the
    // current row block. The variable n2 is the number of cols
    // of the current row block. The variant solves for each
    // row block from the upper to the lower.
    //
    for (mextools_int i = 0; i < no_blocks; ++i) {
      mextools_int bs2 = (i + 1 >= no_blocks) ? bs_last : bs;
      mextools_int n2 = n - i * bs;

      // update block row for next solve
      mextools_int k2 = (i <= 0) ? 0 : i * bs;
      if (LUcpy >= 3) {
        dgemm("T", "N", &bs2, &n2, &k2, &dmone,
              BLOCKIDX_LU_MEM3(LUmem, i, 0, bs, n), &n,
              BLOCKIDX(VT, 0, i, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, i, i, bs, ldVT), &ldVT);
      } else if (LUcpy == 2) {
        dgemm("N", "N", &bs2, &n2, &k2, &dmone,
              BLOCKIDX_LU_MEM2(LUmem, i, 0, bs, n), &bs,
              BLOCKIDX(VT, 0, i, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, i, i, bs, ldVT), &ldVT);
      } else {
        dgemm("N", "N", &bs2, &n2, &k2, &dmone, BLOCKIDX(UT, i, 0, bs, ldUT),
              &ldUT, BLOCKIDX(VT, 0, i, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, i, i, bs, ldVT), &ldVT);
      }

      // solve with diagonal block of L for the block row
      if (LUcpy >= 3) {
        dtrsm("L", "U", "T", "U", &bs2, &n2, &done,
              BLOCKIDX_LU_MEM3(LUmem, i, i, bs, n), &n,
              BLOCKIDX(VT, i, i, bs, ldVT), &ldVT);
      } else if (LUcpy == 2) {
        dtrsm("L", "L", "N", "U", &bs2, &n2, &done,
              BLOCKIDX_LU_MEM2(LUmem, i, i, bs, n), &bs,
              BLOCKIDX(VT, i, i, bs, ldVT), &ldVT);
      } else if (LUcpy == 1) {
        dtrsm("L", "L", "N", "U", &bs2, &n2, &done,
              BLOCKIDX_LU_MEM1(LUmem, i, bs), &bs, BLOCKIDX(VT, i, i, bs, ldVT),
              &ldVT);
      } else {
        dtrsm("L", "L", "N", "U", &bs2, &n2, &done,
              BLOCKIDX(UT, i, i, bs, ldUT), &ldUT, BLOCKIDX(VT, i, i, bs, ldVT),
              &ldVT);
      }
    }
  } else {
    //
    // Variant Lsolve == 1:
    // This variant solves for each block for the block upper
    // diagonal part. The variable bsrows / bscols is the number of
    // rows / cols of the current block (i, j).
    // The variant works in column-wise fashion.
    // This means we solve in the following order:
    //  * block (0, 0)
    //  * block (0, 1), (1, 1)
    //  * block (0, 2), (1, 2), (2, 2)
    //  * ...
    //  * block (0, no_blocks - 1), ..., (no_blocks - 1 ,no_blocks - 1)
    //
    // Variant Lsolve == 0:
    // This variant solves for each block for the block upper
    // diagonal part. The variable bsrows / bscols is the number of
    // rows / cols of the current block (i, j).
    // The variant works in row-wise fashion.
    // This means we solve in the following order:
    //  * block (0, 0), (0, 1), ..., (0, no_blocks - 1)
    //  * block (1, 1), (1, 2), ..., (1, no_blocks - 1)
    //  * ....
    //  * block (no_blocks - 1, no_blocks -1)
    //
    // Depending on Lsolve the following loops are performed
    // Lsolve == 1:
    //    for (mextools_int j = 0; j < no_blocks; ++j)
    //      for (mextools_int i = 0; i <= j; ++i)
    //
    // Lsolve == 0:
    //    for (mextools_int i = 0; i < no_blocks; ++i)
    //      for (mextools_int j = i; j < no_blocks; ++j)
    //

    for (mextools_int t1 = 0; t1 < no_blocks; ++t1) {
      mextools_int start = 0, stop = t1 + 1;
      if (Lsolve <= 0) {
        start = t1;
        stop = no_blocks;
      }
      for (mextools_int t2 = start; t2 < stop; ++t2) {
        mextools_int i = Lsolve ? t2 : t1;
        mextools_int j = Lsolve ? t1 : t2;
        mextools_int bsrows = (i + 1 >= no_blocks) ? bs_last : bs;
        mextools_int bscols = (j + 1 >= no_blocks) ? bs_last : bs;

        // update block (i, j) for solving
        // first iteration: i <= 0 <=> k2 = 0 => quick return
        mextools_int k2 = (i <= 0) ? 0 : i * bs;
        if (LUcpy >= 3) {
          dgemm("T", "N", &bsrows, &bscols, &k2, &dmone,
                BLOCKIDX_LU_MEM3(LUmem, i, 0, bs, n), &n,
                BLOCKIDX(VT, 0, j, bs, ldVT), &ldVT, &done,
                BLOCKIDX(VT, i, j, bs, ldVT), &ldVT);
        } else if (LUcpy == 2) {
          dgemm("N", "N", &bsrows, &bscols, &k2, &dmone,
                BLOCKIDX_LU_MEM2(LUmem, i, 0, bs, n), &bs,
                BLOCKIDX(VT, 0, j, bs, ldVT), &ldVT, &done,
                BLOCKIDX(VT, i, j, bs, ldVT), &ldVT);
        } else {
          dgemm("N", "N", &bsrows, &bscols, &k2, &dmone,
                BLOCKIDX(UT, i, 0, bs, ldUT), &ldUT,
                BLOCKIDX(VT, 0, j, bs, ldVT), &ldVT, &done,
                BLOCKIDX(VT, i, j, bs, ldVT), &ldVT);
        }

        // solve with diagonal block L(i,i) for the block (i, j)
        if (LUcpy >= 3) {
          dtrsm("L", "U", "T", "U", &bsrows, &bscols, &done,
                BLOCKIDX_LU_MEM3(LUmem, i, i, bs, n), &n,
                BLOCKIDX(VT, i, j, bs, ldVT), &ldVT);
        } else if (LUcpy == 2) {
          dtrsm("L", "L", "N", "U", &bsrows, &bscols, &done,
                BLOCKIDX_LU_MEM2(LUmem, i, i, bs, n), &bs,
                BLOCKIDX(VT, i, j, bs, ldVT), &ldVT);
        } else if (LUcpy == 1) {
          dtrsm("L", "L", "N", "U", &bsrows, &bscols, &done,
                BLOCKIDX_LU_MEM1(LUmem, i, bs), &bs,
                BLOCKIDX(VT, i, j, bs, ldVT), &ldVT);
        } else {
          dtrsm("L", "L", "N", "U", &bsrows, &bscols, &done,
                BLOCKIDX(UT, i, i, bs, ldUT), &ldUT,
                BLOCKIDX(VT, i, j, bs, ldVT), &ldVT);
        }
      }
    }
  }


  /*------------------------------------------------------------------------
   * Step (iii) and (iv): Xk = UT \ VT,  Xk = 1 / 2 * (Xk + Xk'),
   * solve with U
   *-----------------------------------------------------------------------*/
  if (Usolve >= 3) {
    //
    // This variant solves in block column-wise fashion for the block upper
    // diagonal part. The variable bs2 is the number of columns of the
    // current column block. The variable m2 is the number of rows
    // of the current column block. The variable m3 is the number
    // rows of the next column block to solve. The variant solves for each
    // column block from the right to the left.
    //
    for (mextools_int j = no_blocks; j >= 1; --j) {
      mextools_int bs2 = (j >= no_blocks) ? bs_last : bs;
      mextools_int m2 = MIN(j * bs, n);
      mextools_int m3 = (j - 1) * bs;
      mextools_int k3 = n - (j - 1) * bs;

      // solve with U for each column block
      dtrsm("L", "U", "N", "N", &m2, &bs2, &done, UT, &ldUT,
            BLOCKIDX(VT, 0, j - 1, bs, ldVT), &ldVT);

      // fix symmetry on the diagonal block
      MDM_symmetrize(bs2, BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), ldVT);

      // fix symmetry copy upper upper diagonal column block to lower
      MDM_copy_trans(m3, bs2, BLOCKIDX(VT, 0, j - 1, bs, ldVT), ldVT,
                     BLOCKIDX(VT, j - 1, 0, bs, ldVT), ldVT);

      // update block upper off diagonal triangular part for next solve U
      // last iteration: i == 1 <=> m3 == 0 => quick return
      dgemm("N", "N", &m3, &bs, &k3, &dmone, BLOCKIDX(UT, 0, j - 1, bs, ldUT),
            &ldUT, BLOCKIDX(VT, j - 1, j - 2, bs, ldVT), &ldVT, &done,
            BLOCKIDX(VT, 0, j - 2, bs, ldVT), &ldVT);
    }
  } else if (Usolve == 2) {
    //
    // This variant solves in block row-wise fashion for the block upper
    // diagonal part. The variable bs2 is the number of rows of the
    // current row block. The variable n2 is the number of columns
    // of the current row block without the diagonal block. The variant
    // solve for each row block from the lower to the upper.
    //
    for (mextools_int j = no_blocks; j >= 1; --j) {
      mextools_int bs2 = (j >= no_blocks) ? bs_last : bs;
      mextools_int n2 = MAX(n - j * bs, 0);

      // update all blocks in row block right from the diagonal block
      if (LUcpy >= 3) {
        dgemm("T", "N", &bs, &n2, &n2, &dmone,
              BLOCKIDX_LU_MEM3(LUmem, j - 1, j, bs, n), &n,
              BLOCKIDX(VT, j, j, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, j - 1, j, bs, ldVT), &ldVT);
      } else if (LUcpy == 2) {
        dgemm("N", "N", &bs, &n2, &n2, &dmone,
              BLOCKIDX_LU_MEM2(LUmem, j - 1, j, bs, n), &bs,
              BLOCKIDX(VT, j, j, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, j - 1, j, bs, ldVT), &ldVT);
      } else {
        dgemm("N", "N", &bs, &n2, &n2, &dmone, BLOCKIDX(UT, j - 1, j, bs, ldUT),
              &ldUT, BLOCKIDX(VT, j, j, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, j - 1, j, bs, ldVT), &ldVT);
      }

      // solve for all blocks in row block right from the diagonal block
      if (LUcpy >= 3) {
        dtrsm("L", "L", "T", "N", &bs, &n2, &done,
              BLOCKIDX_LU_MEM3(LUmem, j - 1, j - 1, bs, n), &n,
              BLOCKIDX(VT, j - 1, j, bs, ldVT), &ldVT);
      } else if (LUcpy == 2) {
        dtrsm("L", "U", "N", "N", &bs, &n2, &done,
              BLOCKIDX_LU_MEM2(LUmem, j - 1, j - 1, bs, n), &bs,
              BLOCKIDX(VT, j - 1, j, bs, ldVT), &ldVT);
      } else if (LUcpy == 1) {
        dtrsm("L", "U", "N", "N", &bs, &n2, &done,
              BLOCKIDX_LU_MEM1(LUmem, j - 1, bs), &bs,
              BLOCKIDX(VT, j - 1, j, bs, ldVT), &ldVT);
      } else {
        dtrsm("L", "U", "N", "N", &bs, &n2, &done,
              BLOCKIDX(UT, j - 1, j - 1, bs, ldUT), &ldUT,
              BLOCKIDX(VT, j - 1, j, bs, ldVT), &ldVT);
      }

      // fix symmetry copy all blocks in row block right
      // from the diagonal block to the corresponding lower part
      MDM_copy_trans(bs, n2, BLOCKIDX(VT, j - 1, j, bs, ldVT), ldVT,
                     BLOCKIDX(VT, j, j - 1, bs, ldVT), ldVT);

      // update for solving with diagonal block
      if (LUcpy >= 3) {
        dgemm("T", "N", &bs, &bs, &n2, &dmone,
              BLOCKIDX_LU_MEM3(LUmem, j - 1, j, bs, n), &n,
              BLOCKIDX(VT, j, j - 1, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), &ldVT);
      } else if (LUcpy == 2) {
        dgemm("N", "N", &bs, &bs, &n2, &dmone,
              BLOCKIDX_LU_MEM2(LUmem, j - 1, j, bs, n), &bs,
              BLOCKIDX(VT, j, j - 1, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), &ldVT);
      } else {
        dgemm("N", "N", &bs, &bs, &n2, &dmone, BLOCKIDX(UT, j - 1, j, bs, ldUT),
              &ldUT, BLOCKIDX(VT, j, j - 1, bs, ldVT), &ldVT, &done,
              BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), &ldVT);
      }

      // solve for diagonal block
      if (LUcpy >= 3) {
        dtrsm("L", "L", "T", "N", &bs2, &bs2, &done,
              BLOCKIDX_LU_MEM3(LUmem, j - 1, j - 1, bs, n), &n,
              BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), &ldVT);
      } else if (LUcpy == 2) {
        dtrsm("L", "U", "N", "N", &bs2, &bs2, &done,
              BLOCKIDX_LU_MEM2(LUmem, j - 1, j - 1, bs, n), &bs,
              BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), &ldVT);
      } else if (LUcpy == 1) {
        dtrsm("L", "U", "N", "N", &bs2, &bs2, &done,
              BLOCKIDX_LU_MEM1(LUmem, j - 1, bs), &bs,
              BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), &ldVT);
      } else {
        dtrsm("L", "U", "N", "N", &bs2, &bs2, &done,
              BLOCKIDX(UT, j - 1, j - 1, bs, ldUT), &ldUT,
              BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), &ldVT);
      }

      // fix symmetry on the diagonal block
      MDM_symmetrize(bs2, BLOCKIDX(VT, j - 1, j - 1, bs, ldVT), ldVT);
    }
  } else {
    //
    // Variant Usolve == 1:
    // This variant solves for each block for the block upper
    // diagonal part. The variable bsrows / bscols is the number of
    // rows / cols of the current block (i, j).
    // The variant works in column-wise fashion.
    // This means we solve in the following order:
    //  * block (no_blocks - 1, no_blocks - 1), ..., (no_blocks - 1, 0)
    //  * block (no_blocks - 2, no_blocks - 2), ..., (no_blocks - 2, 0)
    //  * ...
    //  * block (0, 0)
    //
    // Variant Usolve == 0:
    // This variant solves for each block for the block upper
    // diagonal part. The variable bsrows / bscols is the number of
    // rows / cols of the current block (i, j).
    // The variant works in row-wise fashion.
    // This means we solve in the following order:
    //  * block (no_blocks - 1, no_blocks - 1)
    //  * block (no_blocks - 2, no_blocks - 1), (no_blocks - 2, no_blocks - 2)
    //  * ...
    //  * block (0, no_blocks -1), (0, no_blocks - 2), ..., (0, 0)
    //
    // Depending on Usolve the following loops are performed
    // Usolve == 1:
    //    for (mextools_int j = no_blocks; j >= 1; --j)
    //  for (mextools_int i = j; i >= 1; --i)
    //
    // Usolve == 0:
    //    for (mextools_int i = no_blocks; i >= 1; --i)
    //  for (mextools_int j = no_blocks; j >= i; --j)
    //
    for (mextools_int t1 = no_blocks; t1 >= 1; --t1) {
      mextools_int start = t1, stop = 1;
      if (Usolve <= 0) {
        start = no_blocks;
        stop = t1;
      }
      for (mextools_int t2 = start; t2 >= stop; --t2) {
        mextools_int i = Usolve ? t2 : t1;
        mextools_int j = Usolve ? t1 : t2;
        mextools_int bsrows = (i >= no_blocks) ? bs_last : bs;
        mextools_int bscols = (j >= no_blocks) ? bs_last : bs;

        // update block (i, j) for solving
        // first iteration: j >= no_blocks <=> k2 = 0 => quick return
        mextools_int k2 = (i >= no_blocks) ? 0 : n - i * bs;
        if (LUcpy >= 3) {
          dgemm("T", "N", &bsrows, &bscols, &k2, &dmone,
                BLOCKIDX_LU_MEM3(LUmem, i - 1, i, bs, n), &n,
                BLOCKIDX(VT, i, j - 1, bs, ldVT), &ldVT, &done,
                BLOCKIDX(VT, i - 1, j - 1, bs, ldVT), &ldVT);
        } else if (LUcpy == 2) {
          dgemm("N", "N", &bsrows, &bscols, &k2, &dmone,
                BLOCKIDX_LU_MEM2(LUmem, i - 1, i, bs, n), &bs,
                BLOCKIDX(VT, i, j - 1, bs, ldVT), &ldVT, &done,
                BLOCKIDX(VT, i - 1, j - 1, bs, ldVT), &ldVT);
        } else {
          dgemm("N", "N", &bsrows, &bscols, &k2, &dmone,
                BLOCKIDX(UT, i - 1, i, bs, ldUT), &ldUT,
                BLOCKIDX(VT, i, j - 1, bs, ldVT), &ldVT, &done,
                BLOCKIDX(VT, i - 1, j - 1, bs, ldVT), &ldVT);
        }

        // solve with U for block (i, j)
        if (LUcpy >= 3) {
          dtrsm("L", "L", "T", "N", &bsrows, &bscols, &done,
                BLOCKIDX_LU_MEM3(LUmem, i - 1, i - 1, bs, n), &n,
                BLOCKIDX(VT, i - 1, j - 1, bs, ldVT), &ldVT);
        } else if (LUcpy == 2) {
          dtrsm("L", "U", "N", "N", &bsrows, &bscols, &done,
                BLOCKIDX_LU_MEM2(LUmem, i - 1, i - 1, bs, n), &bs,
                BLOCKIDX(VT, i - 1, j - 1, bs, ldVT), &ldVT);
        } else if (LUcpy == 1) {
          dtrsm("L", "U", "N", "N", &bsrows, &bscols, &done,
                BLOCKIDX_LU_MEM1(LUmem, i - 1, bs), &bs,
                BLOCKIDX(VT, i - 1, j - 1, bs, ldVT), &ldVT);
        } else {
          dtrsm("L", "U", "N", "N", &bsrows, &bscols, &done,
                BLOCKIDX(UT, i - 1, i - 1, bs, ldUT), &ldUT,
                BLOCKIDX(VT, i - 1, j - 1, bs, ldVT), &ldVT);
        }

        if (i == j) {
          // diagonal block => fix the symmetry in (i, i)
          MDM_symmetrize(bsrows, BLOCKIDX(VT, i - 1, j - 1, bs, ldVT), ldVT);
        } else {
          // off diagonal block => transpose copy  from block (i, j) to (j, i)
          MDM_copy_trans(bsrows, bscols, BLOCKIDX(VT, i - 1, j - 1, bs, ldVT),
                         ldVT, BLOCKIDX(VT, j - 1, i - 1, bs, ldVT), ldVT);
        }
      }
    }
  }

  /*------------------------------------------------------------------------
   *  copy VT to Xk
   *-----------------------------------------------------------------------*/
  dlacpy("A", &n, &n, VT, &ldVT, Xk, &n);

/*------------------------------------------------------------------------
 *  free allocated memory
 *-----------------------------------------------------------------------*/
FREE:
  FREE_POINTER(ipiv);
  FREE_POINTER(ipiv_perm);
  FREE_POINTER(UT);
  FREE_POINTER(VT);
  FREE_POINTER(LUmem);

  return info;
}
