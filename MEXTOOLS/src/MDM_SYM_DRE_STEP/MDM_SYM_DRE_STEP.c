//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 *  MDM_SYM_DRE_STEP
 *
 *  # Purpose:
 *
 *  A more efficient implementation of a time step for modified Davison-Maki
 *  method for symmetric real differential Riccti equations.
 *  The implementation is designed for matrices of medium size.
 *  For tiny matrics (n <= 36) the implementation may performed
 *  bad. For larger matrices (n >= 1024) the implementation
 *  is also not optimal.
 *
 *  # Description:
 *
 *  The real symmetric differential Riccati equation is given by
 *
 *      \dot{X} = A' X + X A - X S X + Q, X(0) = X0.
 *
 *  The matrix A, S, Q, X0 must be double, dense, square and of same size.
 *  Moreover it is assumed the S, Q and X0 are symmetric.
 *
 *  The modified Davison-Maki method for the real symmetric differential
 *  Riccati equation is given by the following iteration:
 *
 *  1. Approximate matrix exponential of the Hamiltonian for given step size h.
 *  Divide the matrix exponential into 4 n-x-n blocks and transpose them.
 *
 *      ( exp_hH11  exp_hH12 )         ( -h A  h S  )
 *      (                    )  = expm (            )
 *      ( exp_hH21  exp_hH22 )         (  h Q  h A' )
 *
 *      exp_hH11T = exp_hH11'
 *      exp_hH12T = exp_hH12'
 *      exp_hH21T = exp_hH21'
 *      exp_hH22T = exp_hH22'
 *
 *  2. Time Stepping until desired time:
 *
 *      Xk = X0
 *      t=0
 *
 *      while t + h < T
 *          UT = exp_hH11T + Xk * exp_hH12T     (i)
 *          VT = exp_hH21T + Xk * exp_hH22T     (ii)
 *          Xk = UT \ VT                        (iii)
 *          Xk = 1 / 2 * (Xk + Xk')             (iv)
 *          // update time
 *          t = t + h
 *      end
 *
 *  The time step operations (i)-(iv) are performed by MDM_SYM_DRE_STEP.
 *  MDM_SYM_DRE_STEP can be called from MATLAB via:
 *
 *  # Calling Sequences:
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk)
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk,
 *  ARG_CHECK)
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk,
 *  ARG_CHECK, BS)
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk,
 *  ARG_CHECK, BS, TRF)
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk,
 *  ARG_CHECK, BS, TRF, MM)
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk,
 *  ARG_CHECK, BS, TRF, MM, LUCPY)
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk,
 *  ARG_CHECK, BS, TRF, MM, LUCPY, LSOLVE)
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk,
 *  ARG_CHECK, BS, TRF, MM, LUCPY, LSOLVE, USOLVE)
 *
 *  Xk = MDM_SYM_DRE_STEP(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T, Xk,
 *  ARG_CHECK, BS, TRF, MM, LUCPY, LSOLVE, USOLVE, LDUV2P)
 *
 *  The matrices exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T and Xk
 *  must be double, dense and of size n-by-n.
 *  Xk must be symmetric.
 *  MDM_SYM_DRE_STEP performs only some input argument checks.
 *
 *  Deactivate input argument check ARG_CHECK:
 *  0: No input argument checks are performed.
 *  nonzero: Input argument checks are performed.
 *  default: 1.
 *
 *  Tuning Parameters:
 *    The blocksize BS.
 *    BS is the blocksize >= 0.
 *    If BS is 0 then an unblocked variant is used.
 *    64, 128, or 256 are good values depending on your machine.
 *    default:
 *      If n <= 128, then 0.
 *      If 128 < n <= 256; then 64.
 *      If 256 < n; then 128.
 *
 *    The LU factorization TRF.
 *    1: dgetrf
 *    2: dgetrf2
 *    3: ReLAPACK_dgetrf with crossover size 24
 *    4: ReLAPACK_dgetrf with crossover size 48
 *    5: dgetf2
 *    6: Crout LU (Variant from LAPACK)
 *    7: Left Looking LU (Variant from LAPACK)
 *    8: Toledos recursive LU (Variant from LAPACK)
 *    TRF must be either 1, 2, 3, 4, 5, 6, 7 or 8.
 *    8 is usually a bad choice.
 *    1 is the better choice for larger matrices.
 *    3 or 4 is usually the better choice for small matrices.
 *    5 is good for tiny matrices.
 *    default:
 *      If n <= 32, then 5.
 *      If 32  < n <= 128, then 7.
 *      If 128 < n <= 256, then 3.
 *      If 256 < n, then 1.
 *
 *    The matrix multiplication method MM.
 *    1: dsymm
 *    2: dgemm
 *    3: dgemm with transpose flag for the first matrix
 *    MM must be either 1, 2 or 3.
 *    default: 3.
 *
 *    The data locality option LUCPY.
 *    0: No additional copy is performed.
 *    1: The diagonal blocks of the LU factorization are copied
 *    2: The row blocks of the LU factorization are copied.
 *    3: The row blocks of the LU factorization are transposed copied.
 *    A good choice is to do no additional copies, therefore choose 0.
 *    default: 0
 *
 *    Variant for solving with L option LSOLVE.
 *    0: Solve with L block by block in row-wise fashion.
 *    1: Solve with L block by block in column-wise fashion.
 *    2: Solve with L for row blocks.
 *    3: Solve with L for column blocks.
 *    default:
 *      If n <= 128, then 0.
 *      If 128 < n <= 256, then 3.
 *      If 256 < n <= 512, then 2.
 *      If 512 < n, then 1.
 *
 *    Variant for solving with U option USOLVE.
 *    0: Solve with U block by block in row-wise fashion.
 *    1: Solve with U block by block in column-wise fashion.
 *    2: Solve with U for row blocks.
 *    3: Solve with U for column blocks.
 *    default:
 *      If n <= 128, then 0.
 *      If 128 < n <= 256, then 3.
 *      If 256 < n <= 512, then 2.
 *      If 512 < n, then 1.
 *
 *    Leading dimension for the U and V factorization LDUV2P.
 *    LDUV2P must be an scalar greater equals -1.
 *    -1: The leading dimension is chosen to be the next multiple of the
 *    blocksize.
 *    Otherwise the leading dimension of U and V is chosen
 *    to be the next multiple of 2^(LDUV2P).
 *    Values larger than 8 may reduce the performance drastically.
 *    Chose simply -1 this leads usually to a good performance.
 *    default: -1
 *
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BLAS, LAPACK HEADER */
#include "blas_lapack.h"

/* MDM_SYM_DRE_STEP HEADER */
#include "mdm_sym_dre_step.h"

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "MDM_SYM_DRE_STEP"

/* ENTRY POINT FOR MATLAB */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int n = 0, info = 0;
  mextools_int arg_check = 0, bs = 0, trf = 0, mm = 0, LUcpy = 0, Lsolve = 0,
               Usolve = 0, ldUV2P = 0;
  double *exp_hH11T = NULL, *exp_hH12T = NULL;
  double *exp_hH21T = NULL, *exp_hH22T = NULL;
  double *Xk = NULL, *Xkout = NULL;
  const char *ARG_NAMES[] = {"exp_hH11T", "exp_hH12T", "exp_hH21T", "exp_hH22T",
                             "Xk",        "ARG_CHECK", "BS",        "TRF",
                             "MM",        "LUCPY",     "LSOLVE",    "USOLVE",
                             "LDUV2P"};

  /*------------------------------------------------------------------------
   *  get ARG_CHECK argument
   *-----------------------------------------------------------------------*/
  arg_check = 1;
  if (nrhs >= 6) {
    MEX_CHECK_ARG(!mxIsScalar(prhs[5]), MEXNAME ":arguments",
                  "Argument %d: '%s' is not sscalar.", 6, ARG_NAMES[5]);
    arg_check = mxGetScalar(prhs[5]);
  }

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  if (arg_check) {
    // check number of input arguments
    MEX_CHECK_ARG(!(5 <= nrhs && nrhs <= 13), MEXNAME ":arguments",
                  "Number of input arguments must be greater equals 5 and "
                  "smaller equals 13: got %d.",
                  nrhs);

    // check the numer of output arguments
    MEX_CHECK_ARG(nlhs != 1, MEXNAME ":arguments",
                  "Expected exactly one output argument: got %d", nlhs);

    // check the argument types
    for (mextools_int i = 0; i < nrhs; ++i) {
      MEX_CHECK_ARG(
          !mxIsDouble(prhs[i]) || mxIsComplex(prhs[i]) || mxIsSparse(prhs[i]) ||
              mxIsEmpty(prhs[i]),
          MEXNAME ":arguments",
          "Argument %d: '%s' must be double, real, dense and nonempty.", i + 1,
          ARG_NAMES[i]);
    }

    // check the scalar arguments
    for (mextools_int i = 5; i < nrhs; ++i) {
      MEX_CHECK_ARG(!mxIsScalar(prhs[i]), MEXNAME ":arguments",
                    "Argument %d: '%s' is not scalar.", i + 1, ARG_NAMES[i]);
    }

    // check the size of matrices, all matrices must have same size
    n = mxGetM(prhs[0]);
    for (mextools_int i = 0; i < MIN(nrhs, 5); ++i) {
      mextools_int mi = mxGetM(prhs[i]);
      mextools_int ni = mxGetN(prhs[i]);
      MEX_CHECK_ARG(
          mi != n || ni != n, MEXNAME ":arguments",
          "Argument %d: '%s' has wrong number of rows / cols (%d/%d): "
          "expected (%d/%d).",
          i + 1, ARG_NAMES[i], mi, ni, n, n);
    }
  } else {
    n = mxGetM(prhs[0]);
  }

  /*------------------------------------------------------------------------
   *  default tuning parameter setting
   *-----------------------------------------------------------------------*/
  // blocksize BS
  if (n <= 128) {
    bs = 0;
  } else if (n <= 256) {
    bs = 64;
  } else {
    bs = 128;
  }

  // LU factorization TRF
  if (n <= 32) {
    trf = 5;
  } else if (n <= 128) {
    trf = 7;
  } else if (n <= 256) {
    trf = 3;
  } else {
    trf = 1;
  }

  // matrix multiplication
  mm = 3;

  // copy parts from LU decomposition
  LUcpy = 0;

  // variant for solving with L with and U
  if (n <= 128) {
    Lsolve = 0;
    Usolve = 0;
  } else if (n <= 256) {
    Lsolve = 3;
    Usolve = 3;
  } else if (n <= 512) {
    Lsolve = 2;
    Usolve = 2;
  } else {
    Lsolve = 1;
    Usolve = 1;
  }

  // increase of leading dimension
  ldUV2P = -1;

  /*------------------------------------------------------------------------
   *  overwrite default tuning paramters if given by input arguments
   *-----------------------------------------------------------------------*/
  {
    mextools_int *params[] = {&bs,     &trf,    &mm,    &LUcpy,
                              &Lsolve, &Usolve, &ldUV2P};
    for (int cnt = 0; cnt < (int)(sizeof(params) / sizeof(params[0])); ++cnt) {
      if (nrhs >= cnt + 7) {
        *params[cnt] = mxGetScalar(prhs[cnt + 6]);
      }
    }
  }
#if 0
  PRINT_MESSAGE("bs      = %d\n", bs);
  PRINT_MESSAGE("trf     = %d\n", trf);
  PRINT_MESSAGE("mm      = %d\n", mm);
  PRINT_MESSAGE("LUcpy   = %d\n", LUcpy);
  PRINT_MESSAGE("Lsolve  = %d\n", Lsolve);
  PRINT_MESSAGE("Usolve  = %d\n", Usolve);
  PRINT_MESSAGE("ldUV2P  = %d\n", ldUV2P);
#endif

  /*------------------------------------------------------------------------
   *  check if tuning paramters are valid
   *-----------------------------------------------------------------------*/
  if (arg_check) {
    int cnt = 6;
    MEX_CHECK_ARG(bs < -1, MEXNAME ":arguments",
                  "Argument %d: '%s' must be greater equals -1.", cnt + 1,
                  ARG_NAMES[cnt]);
    cnt++;

    MEX_CHECK_ARG(!(1 <= trf && trf <= 8), MEXNAME ":arguments",
                  "Argument %d: '%s' must be 1, 2, 3, 4, 5, 6, 7 or 8.",
                  cnt + 1, ARG_NAMES[cnt]);
    cnt++;

    MEX_CHECK_ARG(!(1 <= mm && mm <= 3), MEXNAME ":arguments",
                  "Argument %d: '%s' must be 1, 2 or 3.", cnt + 1,
                  ARG_NAMES[cnt]);
    cnt++;

    MEX_CHECK_ARG(!(0 <= LUcpy && LUcpy <= 3), MEXNAME ":arguments",
                  "Argument %d: '%s' must be 0, 1, 2 or 3.", cnt + 1,
                  ARG_NAMES[cnt]);
    cnt++;

    MEX_CHECK_ARG(!(0 <= Lsolve && Lsolve <= 3), MEXNAME ":arguments",
                  "Argument %d: '%s' must be 0, 1, 2 or 3.", cnt + 1,
                  ARG_NAMES[cnt]);
    cnt++;

    MEX_CHECK_ARG(!(0 <= Usolve && Usolve <= 3), MEXNAME ":arguments",
                  "Argument %d: '%s' must be 0, 1, 2 or 3.", cnt + 1,
                  ARG_NAMES[cnt]);
    cnt++;

    MEX_CHECK_ARG(!(-1 <= ldUV2P), MEXNAME ":arguments",
                  "Argument %d: '%s' must be greater equals -1.", cnt + 1,
                  ARG_NAMES[cnt]);
  }
  /*------------------------------------------------------------------------
   *  get pointer from input arguments
   *-----------------------------------------------------------------------*/
  exp_hH11T = MXGETPR(prhs[0]); // transpose (1,1) block of matrix exponential
  exp_hH12T = MXGETPR(prhs[1]); // transpose (1,2) block of matrix exponential
  exp_hH21T = MXGETPR(prhs[2]); // transpose (2,1) block of matrix exponential
  exp_hH22T = MXGETPR(prhs[3]); // transpose (2,2) block of matrix exponential
  Xk = MXGETPR(prhs[4]);        // current iterate

  /*------------------------------------------------------------------------
   *  copy input to output
   *-----------------------------------------------------------------------*/
  plhs[0] = mxCreateUninitNumericMatrix(n, n, mxDOUBLE_CLASS, mxREAL);
  Xkout = MXGETPR(plhs[0]);
  dlacpy("A", &n, &n, Xk, &n, Xkout, &n);

  /*------------------------------------------------------------------------
   *  call MDM_SYM_DRE_STEP functin
   *-----------------------------------------------------------------------*/
  if (bs <= 0 || bs >= n) {
    info = MDM_SYM_DRE_STEP_unblocked(exp_hH11T, exp_hH12T, exp_hH21T,
                                      exp_hH22T, Xkout, n, trf, mm, ldUV2P);
    if (info) {
      PRINT_ERROR(MEXNAME ":MDM_SYM_DRE_STEP_unblocked",
                  "MDM_SYM_DRE_STEP_unblocked returned a nonzero value %d.",
                  info);
    }
  } else {
    info = MDM_SYM_DRE_STEP_blocked(exp_hH11T, exp_hH12T, exp_hH21T, exp_hH22T,
                                    Xkout, n, bs, trf, mm, LUcpy, Lsolve,
                                    Usolve, ldUV2P);
    if (info) {
      PRINT_ERROR(MEXNAME ":MDM_SYM_DRE_STEP_blocked",
                  "MDM_SYM_DRE_STEP returned a nonzero value %d.", info);
    }
  }
}
