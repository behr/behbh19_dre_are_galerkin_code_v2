//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

/*-----------------------------------------------------------------------------
 * mextools_call    Caller for all interfaced routines.
 *
 *  # Purpose:

 *  Caller for all interfaced routines.
 *  Internal use only.
 *
 *  # Description:
 *
 *    MEXTOOLS_CALL().
 *    OUTPUT = MEXTOOLS_CALL('function', ARGS).
 *
 *   Author: Maximilian Behr
 *---------------------------------------------------------------------------*/

/* MEXTOOLS HEADER */
#include "mextools.h"

/* BANDED */
#include "banded.h"

/* QRTOOLS */
#include "qrtools.h"

/* AMD */
#include "amd.h"

/* UMFPACK */
#include "umfpack.h"

/* CHOLMOD */
#include "cholmod.h"

/* ENGINE_INTERFACE */
#include "engine_interface.h"

/* MA57 */
#include "ma57.h"

/* REGISTER AVAILABLE FUNCTIONS */
typedef mextools_int (*mextools_call_function)(int nlhs, mxArray *plhs[],
                                               int nrhs, const mxArray *prhs[]);

typedef struct mextools_call_t {
  char *package;
  char *name;
  mextools_call_function func;
  int lock;
  int unlock;
  int reset_AMD_memory_manager;
} mextools_call_t;

static const mextools_call_t mextools_calls[] = {
    /*------------------------------------------------------------------------
     *  MA57
     *-----------------------------------------------------------------------*/
    /* symbolic */
    {"MA57", "MA57AD", mex_MA57AD, 0, 0, 0},

    /* symbolic */
    {"MA57", "MA57BD", mex_MA57BD, 0, 0, 0},

    /* solving */
    {"MA57", "MA57DD", mex_MA57DD, 0, 0, 0},
    {"MA57", "MA57DD_omp", mex_MA57DD_omp, 0, 0, 0},

    /*------------------------------------------------------------------------
     *  BANDED
     *-----------------------------------------------------------------------*/
    /* general banded systems */
    {"BANDED", "GBSV", mex_GBSV, 0, 0, 0},
    {"BANDED", "GBTRF", mex_GBTRF, 0, 0, 0},
    {"BANDED", "GBTRS", mex_GBTRS, 0, 0, 0},

    /* symmetric/hermitian positive definite banded systems */
    {"BANDED", "PBSV", mex_PBSV, 0, 0, 0},
    {"BANDED", "PBTRF", mex_PBTRF, 0, 0, 0},
    {"BANDED", "PBTRS", mex_PBTRS, 0, 0, 0},

    /* general tridiagonal systems */
    {"BANDED", "GTSV", mex_GTSV, 0, 0, 0},
    {"BANDED", "GTTRF", mex_GTTRF, 0, 0, 0},
    {"BANDED", "GTTRS", mex_GTTRS, 0, 0, 0},

    /*------------------------------------------------------------------------
     *  QRTOOLS
     *-----------------------------------------------------------------------*/
    /* DGEQRF related */
    {"QRTOOLS", "DGEQRF", mex_DGEQRF, 0, 0, 0},
    {"QRTOOLS", "DORGQR", mex_DORGQR, 0, 0, 0},
    {"QRTOOLS", "DORMQR", mex_DORMQR, 0, 0, 0},

    /* DGEQRT related */
    {"QRTOOLS", "DGEQRT", mex_DGEQRT, 0, 0, 0},
    {"QRTOOLS", "DGEMQRT", mex_DGEMQRT, 0, 0, 0},

    /*------------------------------------------------------------------------
     *  UMFPACK
     *-----------------------------------------------------------------------*/
    /* symbolic */
    {"UMFPACK", "umfpack_dl_symbolic", mex_umfpack_dl_symbolic, 1, 0, 1},
    {"UMFPACK", "umfpack_zl_symbolic", mex_umfpack_zl_symbolic, 1, 0, 1},
    {"UMFPACK", "umfpack_dl_report_symbolic", mex_umfpack_dl_report_symbolic, 0,
     0, 1},
    {"UMFPACK", "umfpack_zl_report_symbolic", mex_umfpack_zl_report_symbolic, 0,
     0, 1},
    {"UMFPACK", "umfpack_dl_free_symbolic", mex_umfpack_dl_free_symbolic, 0, 1,
     1},
    {"UMFPACK", "umfpack_zl_free_symbolic", mex_umfpack_zl_free_symbolic, 0, 1,
     1},

    /* numeric */
    {"UMFPACK", "umfpack_dl_numeric", mex_umfpack_dl_numeric, 1, 0, 1},
    {"UMFPACK", "umfpack_zl_numeric", mex_umfpack_zl_numeric, 1, 0, 1},
    {"UMFPACK", "umfpack_dl_report_numeric", mex_umfpack_dl_report_numeric, 0,
     0, 1},
    {"UMFPACK", "umfpack_zl_report_numeric", mex_umfpack_zl_report_numeric, 0,
     0, 1},
    {"UMFPACK", "umfpack_dl_free_numeric", mex_umfpack_dl_free_numeric, 0, 1,
     1},
    {"UMFPACK", "umfpack_zl_free_numeric", mex_umfpack_zl_free_numeric, 0, 1,
     1},

    /* solve */
    {"UMFPACK", "umfpack_dl_solve", mex_umfpack_dl_solve, 0, 0, 1},
    {"UMFPACK", "umfpack_zl_solve", mex_umfpack_zl_solve, 0, 0, 1},
    {"UMFPACK", "umfpack_dl_solve_omp", mex_umfpack_dl_solve_omp, 0, 0, 1},
    {"UMFPACK", "umfpack_zl_solve_omp", mex_umfpack_zl_solve_omp, 0, 0, 1},

    /*------------------------------------------------------------------------
     *  CHOLMOD
     *-----------------------------------------------------------------------*/
    /* CHOLMOD analyze */
    {"CHOLMOD", "cholmod_l_analyze", mex_cholmod_l_analyze, 1, 0, 1},

    /* CHOLMOD factorize */
    {"CHOLMOD", "cholmod_l_factorize", mex_cholmod_l_factorize, 1, 0, 1},

    /* CHOLMOD solve */
    {"CHOLMOD", "cholmod_l_solve", mex_cholmod_l_solve, 0, 0, 1},
    {"CHOLMOD", "cholmod_l_solve_omp", mex_cholmod_l_solve_omp, 0, 0, 1},

    /* CHOLMOD print */
    {"CHOLMOD", "cholmod_l_print_factor", mex_cholmod_l_print_factor, 0, 0, 1},

    /* CHOLMOD free */
    {"CHOLMOD", "cholmod_l_free_factor", mex_cholmod_l_free_factor, 0, 1, 1},

    /*------------------------------------------------------------------------
     *  ENGINE INERFACE
     *-----------------------------------------------------------------------*/
    {"ENGINE", "engOpen", mex_engOpen, 1, 0, 0},
    {"ENGINE", "engClose", mex_engClose, 0, 1, 0},
    {"ENGINE", "engEvalString", mex_engEvalString, 0, 0, 0},
    {"ENGINE", "engEvalStringParallel", mex_engEvalStringParallel, 0, 0, 0},
    {"ENGINE", "engGetVariable", mex_engGetVariable, 0, 0, 0},
    {"ENGINE", "engPutVariable", mex_engPutVariable, 0, 0, 0},

    /*------------------------------------------------------------------------
     *  END
     *-----------------------------------------------------------------------*/
    {NULL, NULL, NULL, 0, 0, 0},
};

/* NAME OF THE MEX FUNCTION */
#define MEXNAME "mextools_call"

#ifdef DEBUG
/* INIT FUNCTION */
__attribute__((constructor)) void mextools_call_init() {
  PRINT_MESSAGE("LOADED: " MEXNAME ".\n");
}

/* ATEXIT FUNCTION */
static void AtExit(void) { PRINT_MESSAGE("UNLOADED: " MEXNAME "\n"); }
#endif

void set_malloc() {}

/* ENTRY POINT FOR MATLAB */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  /*------------------------------------------------------------------------
   *  compiler warning and error message
   *-----------------------------------------------------------------------*/
#ifndef MX_HAS_INTERLEAVED_COMPLEX
#pragma warning MEXNAME " is not working without interlevad MATLAB API."
  PRINT_ERROR(MEXNAME ":API", MEXNAME " need interleaved MATLAB API.");
#endif

  /*------------------------------------------------------------------------
   *  register AtExit function
   *-----------------------------------------------------------------------*/
#ifdef DEBUG
  mexAtExit(AtExit);
#endif

  /*------------------------------------------------------------------------
   *  variables
   *-----------------------------------------------------------------------*/
  mextools_int i = 0, info = 0;
  char mextools_call[128];
  const char *ARG_NAMES[] = {"mextools_call"};

  /*------------------------------------------------------------------------
   *  print all available functions
   *-----------------------------------------------------------------------*/
  if (nrhs == 0) {
    PRINT_MESSAGE("available functions:\n");
    while (mextools_calls[i].name != NULL) {
      PRINT_MESSAGE("  %s: %s\n", mextools_calls[i].package,
                    mextools_calls[i].name);
      ++i;
    }
    return;
  }

  /*------------------------------------------------------------------------
   *  check input and output arguments
   *-----------------------------------------------------------------------*/
  // check number of input arguments
  MEX_CHECK_ARG(nrhs < 1, MEXNAME ":arguments",
                "Number of input arguments be at least 1: got %d", nrhs);

  // check if first argument is string
  MEX_CHECK_ARG(
      !mxIsChar(prhs[0]) || (mxGetM(prhs[0]) != 1) || mxIsEmpty(prhs[0]),
      MEXNAME ":arguments",
      "Argument %d: '%s' must be nonempty character array.", 1, ARG_NAMES[0]);

  /*------------------------------------------------------------------------
   *  get string as function
   *-----------------------------------------------------------------------*/
  info = mxGetString(prhs[0], mextools_call, sizeof(mextools_call));
  if (info) {
    PRINT_ERROR(MEXNAME ":mxGetString",
                "mxGetString function returned nonzero value %ld.", info);
  }

  /*------------------------------------------------------------------------
   *  get current AMD memory manager routines
   *-----------------------------------------------------------------------*/
  amd_malloc_t old_amd_malloc = amd_malloc;
  amd_free_t old_amd_free = amd_free;
  amd_realloc_t old_amd_realloc = amd_realloc;
  amd_calloc_t old_amd_calloc = amd_calloc;
  amd_printf_t old_amd_printf = amd_printf;

  /*------------------------------------------------------------------------
   *  find the function and call
   *-----------------------------------------------------------------------*/
  i = 0;
  while (mextools_calls[i].name != NULL) {
    if (strcmp(mextools_calls[i].name, mextools_call) == 0) {

      // set default AMD memory manager routines
      if (mextools_calls[i].reset_AMD_memory_manager) {
        AMD_MEMORY_SET_DEFAULTS;
      }

      // call the function
#ifdef DEBUG
      PRINT_MESSAGE(MEXNAME ": call to %s\n", mextools_calls[i].name);
#endif
      info = mextools_calls[i].func(nlhs, plhs, nrhs - 1, ++prhs);

      // retset  AMD memory manager routines
      if (mextools_calls[i].reset_AMD_memory_manager) {
        amd_malloc = old_amd_malloc;
        amd_free = old_amd_free;
        amd_calloc = old_amd_calloc;
        amd_realloc = old_amd_realloc;
        amd_printf = old_amd_printf;
      }

      // check the info value
      if (info) {
        PRINT_ERROR(MEXNAME ":call", "%s function returned nonzero value %ld.",
                    mextools_calls[i].name, info);
      }

      // call lock
      if (mextools_calls[i].lock) {
        mexLock();
      }

      // call unlock
      if (mextools_calls[i].unlock) {
        mexUnlock();
      }

      return;
    }
    ++i;
  }

  /*------------------------------------------------------------------------
   *  function not callable
   *-----------------------------------------------------------------------*/
  PRINT_ERROR(MEXNAME ":call", "function %s not available.", mextools_call);
}
