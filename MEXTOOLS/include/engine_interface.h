//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef ENGINE_INTERFACE_H
#define ENGINE_INTERFACE_H

#include <engine.h>

/*-----------------------------------------------------------------------------
 *  INTERFACED FUNCTIONS
 *-----------------------------------------------------------------------------*/
mextools_int mex_engOpen(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]);

mextools_int mex_engClose(int nlhs, mxArray *plhs[], int nrhs,
                          const mxArray *prhs[]);

mextools_int mex_engEvalString(int nlhs, mxArray *plhs[], int nrhs,
                               const mxArray *prhs[]);

mextools_int mex_engEvalStringParallel(int nlhs, mxArray *plhs[], int nrhs,
                                       const mxArray *prhs[]);

mextools_int mex_engGetVariable(int nlhs, mxArray *plhs[], int nrhs,
                                const mxArray *prhs[]);

mextools_int mex_engPutVariable(int nlhs, mxArray *plhs[], int nrhs,
                                const mxArray *prhs[]);

#endif
