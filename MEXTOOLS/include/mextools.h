//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef MEXTOOLS_H
#define MEXTOOLS_H

/*-----------------------------------------------------------------------------
 * HEADERS
 *-----------------------------------------------------------------------------*/
#include <complex.h>
#include <math.h>
#include <mm_malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

// MATLAB MEX
#ifdef MATLAB_MEX_FILE
#include "mex.h"
#endif

/*-----------------------------------------------------------------------------
 * MEMORY ALIGNMENT
 *-----------------------------------------------------------------------------*/
// alignment of the allocation if available
#define ALIGNMENT 64

/*-----------------------------------------------------------------------------
 * INTEGER TYPE
 *-----------------------------------------------------------------------------*/
#ifdef MATLAB_MEX_FILE
#define mextools_int mwSignedIndex
#define mextools_uint mwIndex
#else
#define mextools_int int64_t
#define mextools_uint uint64_t
#endif

/*-----------------------------------------------------------------------------
 * PRINT, WARNING AND ERROR MESSAGE FUNCTIONS
 *-----------------------------------------------------------------------------*/
#ifdef MATLAB_MEX_FILE
#define PRINT_MESSAGE mexPrintf
#define PRINT_WARNING(ID, FMT, ...) mexWarnMsgIdAndTxt(ID, FMT, __VA_ARGS__)
#define PRINT_ERROR(ID, FMT, ...) mexErrMsgIdAndTxt(ID, FMT, __VA_ARGS__)
#else
#define PRINT_MESSAGE printf
#define PRINT_WARNING(ID, FMT, ...) printf(ID " " FMT, __VA_ARGS__)
#define PRINT_ERROR(ID, FMT, ...) fprintf(stderr, ID " " FMT, __VA_ARGS__)
#endif

/*-----------------------------------------------------------------------------
 * MEX RELATED MACROS
 *-----------------------------------------------------------------------------*/
#if MX_HAS_INTERLEAVED_COMPLEX
#define MXGETPR mxGetDoubles
#else
#define MXGETPR mxGetPr
#endif

// if cond is true, throw and error with id and formatted message
#define MEX_CHECK_ARG(COND, ID, FMT, ...)                                      \
  do {                                                                         \
    if ((COND)) {                                                              \
      PRINT_ERROR(ID, FMT, __VA_ARGS__);                                       \
    }                                                                          \
  } while (0)

/*-----------------------------------------------------------------------------
 * USEFUL MACROS
 *-----------------------------------------------------------------------------*/
// minimum / maximum
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

// Shift pointer to block (i, j) of A where BS is the blocksize
#define BLOCKIDX(A, I, J, BS, LDA) (A) + (I) * (BS) + (J) * (BS) * (LDA)

// file and line
#define STRINGIFY(X) #X
#define TOSTRING(X) STRINGIFY(X)
#define ATFILE __FILE__ ", line " TOSTRING(__LINE__)

// concatenation and evaluation
#undef __CONCAT
#define __CONCAT(X, Y) X##Y
#define CONCAT(X, Y) __CONCAT(X, Y)

// suppress unused variable warning
#define UNUSED_VARIABLE(VAR)                                                   \
  do {                                                                         \
    (void)(VAR);                                                               \
  } while (0)

// free a pointer and set to NULL
#define FREE_POINTER(PTR)                                                      \
  do {                                                                         \
    if ((PTR)) {                                                               \
      free((PTR));                                                             \
      (PTR) = NULL;                                                            \
    }                                                                          \
  } while (0)

/* if cond is true goto label */
#define COND_GOTO(COND, LABEL)                                                 \
  do {                                                                         \
    if ((COND)) {                                                              \
      goto LABEL;                                                              \
    }                                                                          \
  } while (0)

/* if cond is true print a message and goto label */
#define COND_MESSAGE_GOTO(COND, LABEL, FMT, ...)                               \
  do {                                                                         \
    if ((COND)) {                                                              \
      PRINT_MESSAGE("%s " FMT, ATFILE, __VA_ARGS__);                           \
      goto LABEL;                                                              \
    }                                                                          \
  } while (0)

/* if cond is true print a warning and goto label */
#define COND_WARNING_GOTO(COND, LABEL, ID, FMT, ...)                           \
  do {                                                                         \
    if ((COND)) {                                                              \
      PRINT_WARNING(ID, "%s " FMT, ATFILE, __VA_ARGS__);                       \
      goto LABEL;                                                              \
    }                                                                          \
  } while (0)

/*-----------------------------------------------------------------------------
 *  MEMORY_ALLOCATION
 *-----------------------------------------------------------------------------*/
int mextools_malloc(void **memptr, size_t size);

/*-----------------------------------------------------------------------------
 *  MATLAB THREADS
 *-----------------------------------------------------------------------------*/
int mextools_matlab_get_num_threads(void);
void mextools_matlab_set_num_threads(mextools_int num_threads);
void mextools_matlab_set_default_num_threads(void);

#endif
