//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef BANDED_H
#define BANDED_H

/*-----------------------------------------------------------------------------
 *  BANDED INTERFACED FUNCTIONS
 *-----------------------------------------------------------------------------*/
/* general banded systems */
mextools_int mex_GBSV(int nlhs, mxArray *plhs[], int nrhs,
                      const mxArray *prhs[]);

mextools_int mex_GBTRF(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]);

mextools_int mex_GBTRS(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]);

/* symmetric/hermitian positive definite banded systems */
mextools_int mex_PBSV(int nlhs, mxArray *plhs[], int nrhs,
                      const mxArray *prhs[]);

mextools_int mex_PBTRF(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]);

mextools_int mex_PBTRS(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]);

/* general tridiagonal systems */
mextools_int mex_GTSV(int nlhs, mxArray *plhs[], int nrhs,
                      const mxArray *prhs[]);

mextools_int mex_GTTRF(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]);

mextools_int mex_GTTRS(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]);

/* symmetric/hermitian tridiagonal systems */
mextools_int mex_PTSV(int nlhs, mxArray *plhs[], int nrhs,
                      const mxArray *prhs[]);

mextools_int mex_PTTRF(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]);

mextools_int mex_PTTRS(int nlhs, mxArray *plhs[], int nrhs,
                       const mxArray *prhs[]);

#endif
