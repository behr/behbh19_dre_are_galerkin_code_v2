//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef UMFPACK_H
#define UMFPACK_H

/* PARTS ARE OF THE CODE ARE COPIED FROM THE SUITESPARSE PROJECT!
 * REPO: https://github.com/DrTimothyAldenDavis/SuiteSparse
 * TAG: v.3.4.0
 * FILES: umfpack.h, umfpack_symbolic.h, umfpack_numeric.h, umfpack_solve.h
 *        umfpack_free_symbolic.h, umfpack_free_numeric.h,
 *        umfpack_report_symbolic.h, umfpack_report_numeric.h
 *
 * ----------------------------------------------------------------------------
 *
 *  LICENSE UMFPACK:
 *
 *  UMFPACK, Copyright (c) 1995-2006 by Timothy A.  Davis.  All Rights Reserved.
 *  UMFPACK is available under alternate licences; contact T. Davis for details.
 *
 *  Your use or distribution of UMFPACK or any modified version of
 *  UMFPACK implies that you agree to this License.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 *  USA
 *
 *  Permission is hereby granted to use or copy this program under the
 *  terms of the GNU GPL, provided that the Copyright, this License,
 *  and the Availability of the original version is retained on all copies.
 *  User documentation of any code that uses this code or any modified
 *  version of this code must cite the Copyright, this License, the
 *  Availability note, and "Used by permission." Permission to modify
 *  the code and to distribute modified code is granted, provided the
 *  Copyright, this License, and the Availability note are retained,
 *  and a notice that the code was modified is included.
 *
 * ----------------------------------------------------------------------------
 */

/* BEGIN COPY FROM SUITESPARSE PROJECT */
/*-----------------------------------------------------------------------------
 *  UMFPACK DEFINES
 *-----------------------------------------------------------------------------*/
/* Control */
#define UMFPACK_CONTROL 20

/* Contents of Control */
#define UMFPACK_PRL 0
#define UMFPACK_DENSE_ROW 1
#define UMFPACK_DENSE_COL 2
#define UMFPACK_BLOCK_SIZE 4
#define UMFPACK_STRATEGY 5
#define UMFPACK_ORDERING 10
#define UMFPACK_FIXQ 13
#define UMFPACK_AMD_DENSE 14
#define UMFPACK_AGGRESSIVE 19
#define UMFPACK_SINGLETONS 11
#define UMFPACK_PIVOT_TOLERANCE 3
#define UMFPACK_ALLOC_INIT 6
#define UMFPACK_SYM_PIVOT_TOLERANCE 15
#define UMFPACK_SCALE 16
#define UMFPACK_FRONT_ALLOC_INIT 17
#define UMFPACK_DROPTOL 18
#define UMFPACK_IRSTEP 7
#define UMFPACK_COMPILED_WITH_BLAS 8
#define UMFPACK_STRATEGY_AUTO 0
#define UMFPACK_STRATEGY_UNSYMMETRIC 1
#define UMFPACK_STRATEGY_OBSOLETE 2
#define UMFPACK_STRATEGY_SYMMETRIC 3

#define UMFPACK_SCALE_NONE 0
#define UMFPACK_SCALE_SUM 1
#define UMFPACK_SCALE_MAX 2

#define UMFPACK_ORDERING_CHOLMOD 0
#define UMFPACK_ORDERING_AMD 1
#define UMFPACK_ORDERING_GIVEN 2
#define UMFPACK_ORDERING_METIS 3
#define UMFPACK_ORDERING_BEST 4
#define UMFPACK_ORDERING_NONE 5
#define UMFPACK_ORDERING_USER 6

/* codes for solving */
#define UMFPACK_A 0   /* Ax=b    */
#define UMFPACK_At 1  /* A'x=b   */
#define UMFPACK_Aat 2 /* A.'x=b  */

#define UMFPACK_Pt_L 3  /* P'Lx=b  */
#define UMFPACK_L 4     /* Lx=b    */
#define UMFPACK_Lt_P 5  /* L'Px=b  */
#define UMFPACK_Lat_P 6 /* L.'Px=b */
#define UMFPACK_Lt 7    /* L'x=b   */
#define UMFPACK_Lat 8   /* L.'x=b  */

#define UMFPACK_U_Qt 9   /* UQ'x=b  */
#define UMFPACK_U 10     /* Ux=b    */
#define UMFPACK_Q_Ut 11  /* QU'x=b  */
#define UMFPACK_Q_Uat 12 /* QU.'x=b */
#define UMFPACK_Ut 13    /* U'x=b   */
#define UMFPACK_Uat 14   /* U.'x=b  */

/* error codes */
#define UMFPACK_OK 0
#define UMFPACK_WARNING_singular_matrix 1
#define UMFPACK_WARNING_determinant_underflow 2
#define UMFPACK_WARNING_determinant_overflow 3
#define UMFPACK_ERROR_out_of_memory -1
#define UMFPACK_ERROR_invalid_Numeric_object -3
#define UMFPACK_ERROR_invalid_Symbolic_object -4
#define UMFPACK_ERROR_argument_missing -5
#define UMFPACK_ERROR_n_nonpositive -6
#define UMFPACK_ERROR_invalid_matrix -8
#define UMFPACK_ERROR_different_pattern -11
#define UMFPACK_ERROR_invalid_system -13
#define UMFPACK_ERROR_invalid_permutation -15
#define UMFPACK_ERROR_internal_error -911
#define UMFPACK_ERROR_file_IO -17
#define UMFPACK_ERROR_ordering_failed -18

/*-----------------------------------------------------------------------------
 *  UMFPACK SYMBOLIC
 *-----------------------------------------------------------------------------*/
/* create symbolic */
extern mextools_int umfpack_dl_symbolic(mextools_int m, mextools_int n,
                                        const mextools_int *Ap,
                                        const mextools_int *Ai,
                                        const double *Ax, void **Symbolic,
                                        const double *Control, double *Info);

extern mextools_int
umfpack_zl_symbolic(mextools_int m, mextools_int n, const mextools_int *Ap,
                    const mextools_int *Ai, const double *Ax, const double *Az,
                    void **Symbolic, const double *Control, double *Info);

/* report symbolic */
extern mextools_int umfpack_dl_report_symbolic(void **Symbolic,
                                               const double *Control);
extern mextools_int umfpack_zl_report_symbolic(void **Symbolic,
                                               const double *Control);

/* free symbolic */
extern void umfpack_dl_free_symbolic(void **Symbolic);
extern void umfpack_zl_free_symbolic(void **Symbolic);

/*-----------------------------------------------------------------------------
 *  UMFPACK NUMERIC
 *-----------------------------------------------------------------------------*/
/* create numeric */
extern mextools_int umfpack_dl_numeric(const mextools_int *Ap,
                                       const mextools_int *Ai, const double *Ax,
                                       void *Symbolic, void **Numeric,
                                       const double *Control, double *Info);

extern mextools_int umfpack_zl_numeric(const mextools_int *Ap,
                                       const mextools_int *Ai, const double *Ax,
                                       const double *Az, void *Symbolic,
                                       void **Numeric, const double *Control,
                                       double *Info);

/* report numeric*/
extern mextools_int umfpack_dl_report_numeric(void **Numeric,
                                              const double *Control);
extern mextools_int umfpack_zl_report_numeric(void **Numeric,
                                              const double *Control);

/* free numeric */
extern void umfpack_dl_free_numeric(void **Numeric);
extern void umfpack_zl_free_numeric(void **Numeric);

/*-----------------------------------------------------------------------------
 *  UMFPACK SOLVE
 *-----------------------------------------------------------------------------*/
/* solve */
extern mextools_int umfpack_dl_solve(mextools_int sys, const mextools_int *Ap,
                                     const mextools_int *Ai, const double *Ax,
                                     double *X, const double *B, void **Numeric,
                                     const double *Control, double *Info);

extern mextools_int umfpack_zl_solve(mextools_int sys, const mextools_int *Ap,
                                     const mextools_int *Ai, const double *Ax,
                                     const double *Az, double *Xx, double *Xz,
                                     const double *Bx, const double *Bz,
                                     void **Numeric, const double *Control,
                                     double *Info);
/* END COPY FROM SUITESPARSE PROJECT */

/*-----------------------------------------------------------------------------
 *  UMFPACK INTERFACED FUNCTIONS
 *-----------------------------------------------------------------------------*/
/* umfpack error to human readable string */
const char *umfpack_error(mextools_int info);

/* create symbolic */
mextools_int mex_umfpack_dl_symbolic(int nlhs, mxArray *plhs[], int nrhs,
                                     const mxArray *prhs[]);
mextools_int mex_umfpack_zl_symbolic(int nlhs, mxArray *plhs[], int nrhs,
                                     const mxArray *prhs[]);

/* report symbolic */
mextools_int mex_umfpack_dl_report_symbolic(int nlhs, mxArray *plhs[], int nrhs,
                                            const mxArray *prhs[]);
mextools_int mex_umfpack_zl_report_symbolic(int nlhs, mxArray *plhs[], int nrhs,
                                            const mxArray *prhs[]);

/* free symbolic */
mextools_int mex_umfpack_dl_free_symbolic(int nlhs, mxArray *plhs[], int nrhs,
                                          const mxArray *prhs[]);
mextools_int mex_umfpack_zl_free_symbolic(int nlhs, mxArray *plhs[], int nrhs,
                                          const mxArray *prhs[]);

/* create numeric */
mextools_int mex_umfpack_dl_numeric(int nlhs, mxArray *plhs[], int nrhs,
                                    const mxArray *prhs[]);
mextools_int mex_umfpack_zl_numeric(int nlhs, mxArray *plhs[], int nrhs,
                                    const mxArray *prhs[]);

/* report numeric */
mextools_int mex_umfpack_dl_report_numeric(int nlhs, mxArray *plhs[], int nrhs,
                                           const mxArray *prhs[]);
mextools_int mex_umfpack_zl_report_numeric(int nlhs, mxArray *plhs[], int nrhs,
                                           const mxArray *prhs[]);

/* free numeric */
mextools_int mex_umfpack_dl_free_numeric(int nlhs, mxArray *plhs[], int nrhs,
                                         const mxArray *prhs[]);
mextools_int mex_umfpack_zl_free_numeric(int nlhs, mxArray *plhs[], int nrhs,
                                         const mxArray *prhs[]);

/* solve */
mextools_int mex_umfpack_dl_solve(int nlhs, mxArray *plhs[], int nrhs,
                                  const mxArray *prhs[]);
mextools_int mex_umfpack_zl_solve(int nlhs, mxArray *plhs[], int nrhs,
                                  const mxArray *prhs[]);

mextools_int mex_umfpack_dl_solve_omp(int nlhs, mxArray *plhs[], int nrhs,
                                      const mxArray *prhs[]);
mextools_int mex_umfpack_zl_solve_omp(int nlhs, mxArray *plhs[], int nrhs,
                                      const mxArray *prhs[]);

#endif
