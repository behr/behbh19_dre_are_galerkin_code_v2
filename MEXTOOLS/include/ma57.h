//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef MA57_H
#define MA57_H

/*-----------------------------------------------------------------------------
 *  FOR DOCUMENTATION SEE:
 *    http://www.hsl.rl.ac.uk/catalogue/ma57.html
 *-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
 *  SOME DEFAULT VALUES AND LENGTH OF ARRAYS FOR MA57
 *-----------------------------------------------------------------------------*/
#define MA57_CNTL 5
#define MA57_ICNTL 20
#define MA57_INFO 40
#define MA57_RINFO 20
#define MA57AD_IWORK(N) (5 * (N))

/* WORKSPACE SIZES */
#define MA57_LKEEP(N, NE) (7 * (N) + (NE) + MAX((N), (NE)) + 42)
#define MA57BD_IWORK(N) (N)

#define MA57DD_WORK(N) (4 * (N))
#define MA57DD_IWORK(N) (N)

/* IMPORTANT ENTRIES IN INFO */
#define MA57_INFO_LFACT 8
#define MA57_INFO_LIFACT 9

/* ORDERING OPTION IN ICTNL */
#define MA57_ICNTL_ORDERING 5
#define MA57_ICNTL_ORDERING_MC47 2
#define MA57_ICNTL_ORDERING_MA27 3

/* SCALE FACTOR TO MAKE LFACT AND IFACT SLIGHTLY LARGER */
#define MA57_LFACT_SCALE 1.25
#define MA57_LIFACT_SCALE 1.25

/*-----------------------------------------------------------------------------
 *  ROUTINES FROM MA57
 *-----------------------------------------------------------------------------*/
#if !defined(_WIN32)
/* MA57 */
#define ma57id ma57id_
#define ma57ad ma57ad_
#define ma57bd ma57bd_
#define ma57dd ma57dd_
#endif

/* default values for controlling paramters */
extern void ma57id(double *CNTL, mextools_int *ICNTL);

/* symbolic analysis */
extern void ma57ad(const mextools_int *N, const mextools_int *NE,
                   const mextools_int *IRN, const mextools_int *JCN,
                   const mextools_int *LKEEP, mextools_int *KEEP,
                   mextools_int *IWORK, mextools_int *ICNTL, mextools_int *INFO,
                   double *RINFO);

/* numerical factorization */
extern void ma57bd(const mextools_int *N, const mextools_int *NE, double *A,
                   double *FACT, const mextools_int *LFACT, mextools_int *IFACT,
                   mextools_int *LIFACT, const mextools_int *LKEPP,
                   const mextools_int *KEEP, mextools_int *IWORK,
                   mextools_int *ICNTL, double *CNTL, mextools_int *INFO,
                   double *RINFO);

/* solving */
extern void ma57dd(const mextools_int *JOB, const mextools_int *N,
                   const mextools_int *NE, double *A, const mextools_int *IRN,
                   const mextools_int *JCN, const double *FACT,
                   const mextools_int *LFACT, const mextools_int *IFACT,
                   const mextools_int *LIFACT, const double *RHS, double *X,
                   double *RESID, double *WORK, mextools_int *IWORK,
                   mextools_int *ICNTL, double *CNTL, mextools_int *INFO,
                   double *RINFO);

/*-----------------------------------------------------------------------------
 *  MA57 INTERFACED FUNCTIONS
 *-----------------------------------------------------------------------------*/
/* symbolic analysis */
mextools_int mex_MA57AD(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]);

/* numeric factorization */
mextools_int mex_MA57BD(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]);

/* solving */
mextools_int mex_MA57DD(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]);

mextools_int mex_MA57DD_omp(int nlhs, mxArray *plhs[], int nrhs,
                            const mxArray *prhs[]);

#endif
