//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef BLAS_LAPACK_H
#define BLAS_LAPACK_H

/*-----------------------------------------------------------------------------
 *  BLAS AND LAPACK
 *-----------------------------------------------------------------------------*/
#if !(defined(_WIN32) || defined(_WIN64))
/* matrix multiplication */
#define dsymm dsymm_
#define dgemm dgemm_

/* solving linear systems */
#define dgetrf dgetrf_
#define dgetrf2 dgetrf2_
#define dgetf2 dgetf2_
#define dgetrs dgetrs_
#define dtrsm dtrsm_

/* solving banded linear systems */
#define dgbsv dgbsv_
#define zgbsv zgbsv_
#define dgbtrf dgbtrf_
#define zgbtrf zgbtrf_
#define dgbtrs dgbtrs_
#define zgbtrs zgbtrs_

#define dpbsv dpbsv_
#define zpbsv zpbsv_
#define dpbtrf dpbtrf_
#define zpbtrf zpbtrf_
#define dpbtrs dpbtrs_
#define zpbtrs zpbtrs_

/* solving tridiagonal linear systems */
#define dgtsv dgtsv_
#define zgtsv zgtsv_
#define dgttrf dgttrf_
#define zgttrf zgttrf_
#define dgttrs dgttrs_
#define zgttrs zgttrs_

#define dptsv dptsv_
#define zptsv zptsv_
#define dpttrf dpttrf_
#define zpttrf zpttrf_
#define dpttrs dpttrs_
#define zpttrs zpttrs_

/* dgeqrf related functions */
#define dgeqrf dgeqrf_
#define dormqr dormqr_
#define dorgqr dorgqr_

/* dgeqrt related functions */
#define dgeqrt dgeqrt_
#define dgemqrt dgemqrt_

/* permutation */
#define dlaswp dlaswp_
#define dlapmt dlapmt_

/* copy */
#define dlacpy dlacpy_
#define zlacpy zlacpy_
#define dcopy dcopy_

/* set */
#define dlaset dlaset_
#endif

/* matrix-matrix multiplication */
extern void dgemm(const char *trans, const char *transb, const mextools_int *m,
                  const mextools_int *n, const mextools_int *k,
                  const double *alpha, const double *a, const mextools_int *lda,
                  const double *b, const mextools_int *ldb, const double *beta,
                  double *c, const mextools_int *ldc);

extern void dsymm(const char *side, const char *uplo, const mextools_int *m,
                  const mextools_int *n, const double *alpha, const double *a,
                  const mextools_int *lda, const double *b,
                  const mextools_int *ldb, const double *beta, double *c,
                  const mextools_int *ldc);

/* solving linear systems */
extern void dgetrf(const mextools_int *m, const mextools_int *n, double *a,
                   const mextools_int *lda, mextools_int *ipiv,
                   mextools_int *info);

extern void dgetrf2(const mextools_int *m, const mextools_int *n, double *a,
                    const mextools_int *lda, mextools_int *ipiv,
                    mextools_int *info);

extern void dgetf2(const mextools_int *m, const mextools_int *n, double *a,
                   const mextools_int *lda, mextools_int *ipiv,
                   mextools_int *info);

extern void dgetrs(const char *trans, const mextools_int *n,
                   const mextools_int *nrhs, const double *a,
                   const mextools_int *lda, const mextools_int *ipiv, double *b,
                   const mextools_int *ldb, mextools_int *info);

extern void dtrsm(const char *side, const char *uplo, const char *transa,
                  const char *diag, const mextools_int *m,
                  const mextools_int *n, const double *alpha, const double *a,
                  const mextools_int *lda, double *b, const mextools_int *ldb);

/* solving linear banded systems */
extern void dgbsv(const mextools_int *n, const mextools_int *kl,
                  const mextools_int *ku, const mextools_int *nrhs, double *AB,
                  const mextools_int *ldab, mextools_int *ipiv, double *B,
                  const mextools_int *ldb, mextools_int *info);

extern void zgbsv(const mextools_int *n, const mextools_int *kl,
                  const mextools_int *ku, const mextools_int *nrhs,
                  double complex *AB, const mextools_int *ldab,
                  mextools_int *ipiv, double complex *B,
                  const mextools_int *ldb, mextools_int *info);

extern void dgbtrf(const mextools_int *m, const mextools_int *n,
                   const mextools_int *kl, const mextools_int *ku, double *AB,
                   const mextools_int *ldab, mextools_int *ipiv,
                   mextools_int *info);

extern void zgbtrf(const mextools_int *m, const mextools_int *n,
                   const mextools_int *kl, const mextools_int *ku,
                   double complex *AB, const mextools_int *ldab,
                   mextools_int *ipiv, mextools_int *info);

extern void dgbtrs(const char *trans, const mextools_int *n,
                   const mextools_int *kl, const mextools_int *ku,
                   const mextools_int *nrhs, const double *AB,
                   const mextools_int *ldab, mextools_int *ipiv, double *B,
                   const mextools_int *ldb, mextools_int *info);

extern void zgbtrs(const char *trans, const mextools_int *n,
                   const mextools_int *kl, const mextools_int *ku,
                   const mextools_int *nrhs, const double complex *AB,
                   const mextools_int *ldab, mextools_int *ipiv,
                   double complex *B, const mextools_int *ldb,
                   mextools_int *info);

extern void dpbsv(const char *uplo, const mextools_int *n,
                  const mextools_int *kd, const mextools_int *nrhs, double *AB,
                  const mextools_int *ldab, double *B, const mextools_int *ldb,
                  mextools_int *info);

extern void zpbsv(const char *uplo, const mextools_int *n,
                  const mextools_int *kd, const mextools_int *nrhs,
                  double complex *AB, const mextools_int *ldab,
                  double complex *B, const mextools_int *ldb,
                  mextools_int *info);

extern void dpbtrf(const char *uplo, const mextools_int *n,
                   const mextools_int *kd, double *AB, const mextools_int *ldab,
                   mextools_int *info);

extern void zpbtrf(const char *uplo, const mextools_int *n,
                   const mextools_int *kd, double complex *AB,
                   const mextools_int *ldab, mextools_int *info);

extern void dpbtrs(const char *uplo, const mextools_int *n,
                   const mextools_int *kd, const mextools_int *nrhs,
                   const double *AB, const mextools_int *ldab, double *B,
                   const mextools_int *ldb, mextools_int *info);

extern void zpbtrs(const char *uplo, const mextools_int *n,
                   const mextools_int *kd, const mextools_int *nrhs,
                   const double complex *AB, const mextools_int *ldab,
                   double complex *B, const mextools_int *ldb,
                   mextools_int *info);

/* solving tridiagonal linear systems */
extern void dgtsv(const mextools_int *n, const mextools_int *nrhs, double *DL,
                  double *D, double *DU, double *B, const mextools_int *ldb,
                  mextools_int *info);

extern void zgtsv(const mextools_int *n, const mextools_int *nrhs,
                  double complex *DL, double complex *D, double complex *DU,
                  double complex *B, const mextools_int *ldb,
                  mextools_int *info);

extern void dgttrf(const mextools_int *n, double *DL, double *D, double *DU,
                   double *DU2, mextools_int *ipiv, mextools_int *info);

extern void zgttrf(const mextools_int *n, double complex *DL, double complex *D,
                   double complex *DU, double complex *DU2, mextools_int *ipiv,
                   mextools_int *info);

extern void dgttrs(const char *uplo, const mextools_int *n,
                   const mextools_int *nrhs, const double *DL, const double *D,
                   const double *DU, const double *DU2, mextools_int *ipiv,
                   double *B, const mextools_int *ldb, mextools_int *info);

extern void zgttrs(const char *uplo, const mextools_int *n,
                   const mextools_int *nrhs, const double complex *DL,
                   const double complex *D, const double complex *DU,
                   const double complex *DU2, mextools_int *ipiv,
                   double complex *B, const mextools_int *ldb,
                   mextools_int *info);

extern void dptsv(const mextools_int *n, const mextools_int *nrhs, double *D,
                  double *E, double *B, const mextools_int *ldb,
                  mextools_int *info);

extern void zptsv(const mextools_int *n, const mextools_int *nrhs, double *D,
                  double complex *E, double complex *B, const mextools_int *ldb,
                  mextools_int *info);

extern void dpttrf(const mextools_int *n, double *D, double *E,
                   mextools_int *info);

extern void zpttrf(const mextools_int *n, double *D, double complex *E,
                   mextools_int *info);

extern void dpttrs(const mextools_int *n, const mextools_int *nrhs,
                   const double *D, const double *E, double *B,
                   mextools_int *ldb, mextools_int *info);

extern void zpttrs(const char *uplo, const mextools_int *n,
                   const mextools_int *nrhs, double *D, const double complex *E,
                   double complex *B, mextools_int *ldb, mextools_int *info);

/* dgeqrf related functions */
extern void dgeqrf(const mextools_int *m, const mextools_int *n, double *a,
                   const mextools_int *lda, double *tau, double *work,
                   mextools_int *lwork, mextools_int *info);

extern void dormqr(const char *side, const char *trans, const mextools_int *m,
                   const mextools_int *n, const mextools_int *k,
                   const double *a, const mextools_int *lda, const double *tau,
                   double *c, const mextools_int *ldc, double *work,
                   mextools_int *lwork, mextools_int *info);

extern void dorgqr(const mextools_int *m, const mextools_int *n,
                   const mextools_int *k, double *a, const mextools_int *lda,
                   const double *tau, double *work, mextools_int *lwork,
                   mextools_int *info);

/* dgeqrt related functions */
extern void dgeqrt(const mextools_int *m, const mextools_int *n,
                   const mextools_int *nb, double *a, const mextools_int *lda,
                   double *T, const mextools_int *ldt, double *work,
                   mextools_int *info);

extern void dgemqrt(const char *side, const char *trans, const mextools_int *m,
                    const mextools_int *n, const mextools_int *k,
                    const mextools_int *nb, const double *v,
                    const mextools_int *ldv, const double *t,
                    const mextools_int *ldt, double *c, const mextools_int *ldc,
                    double *work, mextools_int *info);

/* permutation */
extern void dlaswp(const mextools_int *n, double *a, const mextools_int *lda,
                   const mextools_int *k1, const mextools_int *k2,
                   const mextools_int *ipiv, const mextools_int *incx);

extern void dlapmt(const mextools_int *forward, const mextools_int *m,
                   const mextools_int *n, double *x, const mextools_int *ldx,
                   const mextools_int *k);

/* copy */
extern void dlacpy(const char *uplo, const mextools_int *m,
                   const mextools_int *n, const double *a,
                   const mextools_int *lda, double *b, const mextools_int *ldb);

extern void zlacpy(const char *uplo, const mextools_int *m,
                   const mextools_int *n, const double complex *a,
                   const mextools_int *lda, double complex *b,
                   const mextools_int *ldb);

extern void dcopy(const mextools_int *n, const double *dx,
                  const mextools_int *incx, double *dy,
                  const mextools_int *incy);

/* set */
extern void dlaset(const char *uplo, const mextools_int *m,
                   const mextools_int *n, const double *alpha,
                   const double *beta, double *a, const mextools_int *lda);
/* scale */
extern void dscal(const mextools_int *n, const double *da, double *dx,
                  const mextools_int *incx);

/* axpy */
extern void daxpy(const mextools_int *n, const double *da, const double *dx,
                  const mextools_int *incx, double *dy,
                  const mextools_int *incy);

/*-----------------------------------------------------------------------------
 *  BLAS EXTENSIONS OF MKL
 *-----------------------------------------------------------------------------*/
//#if !defined(_WIN32)
//#define mkl_domatcopy mkl_domatcopy_
//#endif
// SEEMS TO BE WITH UNDERSCORE ON WINDOWS
#define mkl_domatcopy mkl_domatcopy_

/* The online documentation of Intel about mkl_domatcopy seems to be incorrect.
 *
 *  (Dec, 09, 2019):
 *  https://software.intel.com/en-us/onemkl-developer-reference-c-mkl-omatcopy
 *  https://software.intel.com/en-us/forums/intel-math-kernel-library/topic/800815
 *
 */
extern void mkl_domatcopy(const char *ordering, const char *trans,
                          const mextools_int *rowsa, const mextools_int *colsa,
                          const double *alpha, const double *a,
                          const mextools_int *lda, double *b,
                          const mextools_int *ldb);

#endif
