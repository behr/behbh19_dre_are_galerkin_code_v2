//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef QRTOOLS_H
#define QRTOOLS_H

/*-----------------------------------------------------------------------------
 *  DGEQRT HELPER FUNCTIONS
 *-----------------------------------------------------------------------------*/
void qrt_blocksize(mextools_int minmn, mextools_int *nb);

/*-----------------------------------------------------------------------------
 *  QRTOOLS INTERFACED FUNCTIONS
 *-----------------------------------------------------------------------------*/
/* dgeqrf related functions */
mextools_int mex_DGEQRF(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]);

mextools_int mex_DORGQR(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]);

mextools_int mex_DORMQR(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]);

/* dgeqrt related functions */
mextools_int mex_DGEQRT(int nlhs, mxArray *plhs[], int nrhs,
                        const mxArray *prhs[]);

mextools_int mex_DGEMQRT(int nlhs, mxArray *plhs[], int nrhs,
                         const mxArray *prhs[]);

#endif
