//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef MDM_SYM_DRE_STEP_H
#define MDM_SYM_DRE_STEP_H

/*-----------------------------------------------------------------------------
 *  SHIPPED VARIANTS OF LU DECOMPOSITION FROM NETLIB LAPACK
 *-----------------------------------------------------------------------------*/
#if !defined(_WIN32)
/* Crout LU, Left Looking LU and Toledos recursive LU */
#define cr_dgetrf cr_dgetrf_
#define ll_dgetrf ll_dgetrf_
#define rec_dgetrf rec_dgetrf_
#endif

/* solving linear systems */
extern void cr_dgetrf(const mextools_int *m, const mextools_int *n, double *a,
                      const mextools_int *lda, mextools_int *ipiv,
                      mextools_int *info);

extern void ll_dgetrf(const mextools_int *m, const mextools_int *n, double *a,
                      const mextools_int *lda, mextools_int *ipiv,
                      mextools_int *info);

extern void rec_dgetrf(const mextools_int *m, const mextools_int *n, double *a,
                       const mextools_int *lda, mextools_int *ipiv,
                       mextools_int *info);

/*-----------------------------------------------------------------------------
 *  MODIFIED DAVISON-MAKI STEP FUNCTIONS
 *-----------------------------------------------------------------------------*/
/* RELAPACK_dgetrf */
void ReLAPACK_dgetrf(const mextools_int *m, const mextools_int *n, double *A,
                     const mextools_int *ldA, mextools_int *ipiv,
                     mextools_int *info);
void ReLAPACK_dgetrfx(const mextools_int *m, const mextools_int *n, double *A,
                      const mextools_int *ldA, mextools_int *ipiv,
                      mextools_int *info, mextools_int CROSSOVER);

/* helper functions */
int MDM_dgetrf(mextools_int m, mextools_int n, double *restrict A,
               mextools_int ldA, mextools_int *restrict ipiv, mextools_int trf);
void MDM_symmetrize(mextools_int n, double *restrict a, mextools_int lda);
void MDM_copy_trans(const mextools_int m, const mextools_int n,
                    const double *restrict A, const mextools_int ldA,
                    double *restrict B, const mextools_int ldB);

// unblocked modified Davison-Maki step function
int MDM_SYM_DRE_STEP_unblocked(const double *restrict exp_hH11T,
                               const double *restrict exp_hH12T,
                               const double *restrict exp_hH21T,
                               const double *restrict exp_hH22T,
                               double *restrict Xk, mextools_int n,
                               mextools_int trf, mextools_int mm,
                               mextools_int ldUV2P);

// blocked modified Davison-Maki step function
int MDM_SYM_DRE_STEP_blocked(const double *restrict exp_hH11T,
                             const double *restrict exp_hH12T,
                             const double *restrict exp_hH21T,
                             const double *restrict exp_hH22T,
                             double *restrict Xk, mextools_int n,
                             mextools_int bs, mextools_int trf, mextools_int mm,
                             mextools_int LUcpy, mextools_int Lsolve,
                             mextools_int Usolve, mextools_int ldUV2P);

#endif
