//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef CHOLMOD_H
#define CHOLMOD_H

///////////////////////////////////////////////////////////////////////////////
//////////////////// !!!WARNING, WARNING, WANING!!! ///////////////////////////
///////////////////////////////////////////////////////////////////////////////
//  !!!ALL STRUCTS MUST BE BINARY COMPATIBLE WITH MATLABS CHOLMOD VERSION!!! //
///////////////////////////////////////////////////////////////////////////////
//////////////////// !!!WARNING, WARNING, WANING!!! ///////////////////////////
///////////////////////////////////////////////////////////////////////////////
//
// MATLAB SEEMS TO USE CHOLMOD 1.7.0, Sept 20, 2008.
//
// TESTED WITH MATLAB:
//   * R2013b
//   * R2014b
//   * R2015b
//   * R2016b
//   * R2017b
//   * R2018a
//   * R2019b
//   * R2020a
//
// THIS CHOLMOD VERSION IS IN SUITESPARSE VERSION 3.2.0.
// THE INTERNAL CHOLMOD VERSION CAN BE DETERMINED BY EXECUTING THE FOLLOWING
// MATLAB CODE:
//
//    n = 100; b = rand(n, 1); A = sprand(n, n, 0.1);
//    A = A + A' + n*n*speye(n, n);
//    spparms('spumoni',2);
//    A\b;
//
//  MATLAB SHOULD PRINT A DIAGNOSTIC OUTPUT INCLUDING THE VERSION OF CHOLMOD.
//
// A WORKAROUND WOULD BE TO DEFINE STRUCT WITH A MEMORY BUFFER LARGE
// ENOUGH TO HOLD THE CONTENT OF CHOLMODS INTERNAL STRUCTS.
// FOR EXAMPLE:
//  typedef struct cholmod_common_struct {
//    char buff[8192]; // 8 KiB
//  } cholmod_common;
//
// THE DISADVANTAGE IS THAT YOU HAVE NO (SAFE) ACCESS TO THE FIELDS OF THE
// STRUCTS.
//

/* PARTS OF THE CODE ARE COPIED FROM THE SUITESPARSE PROJECT!
 * REPO: https://github.com/DrTimothyAldenDavis/SuiteSparse
 * TAG: v.3.2.0
 * FILES: cholmod_core.h, cholmod_check.h, cholmod_cholesky.h
 *
 * ----------------------------------------------------------------------------
 *
 *  LICENSE CHOLMOD CORE MODULE:
 *
 *  CHOLMOD/Core Module.  Copyright (C) 2005-2006, Univ. of Florida.
 *  Author: Timothy A. Davis
 *  CHOLMOD is also available under other licenses; contact authors for details.
 *  http://www.cise.ufl.edu/research/sparse
 *
 *  This Module is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This Module is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this Module; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------
 *
 *  LICENSE CHOLMOD CHECK MODULE:
 *
 *  CHOLMOD/Check Module.  Copyright (C) 2005-2006, Timothy A. Davis
 *  CHOLMOD is also available under other licenses; contact authors for details.
 *  http://www.cise.ufl.edu/research/sparse
 *
 *  This Module is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This Module is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this Module; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------
 *
 *  LICENSE CHOLMOD CHOLESKY MODULE:
 *
 *  CHOLMOD/Cholesky module, Copyright (C) 2005-2006, Timothy A. Davis
 *  CHOLMOD is also available under other licenses; contact authors for details.
 *  http://www.cise.ufl.edu/research/sparse
 *
 *
 *  This Module is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This Module is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this Module; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------
 */

/* BEGIN COPY FROM SUITESPARSE PROJECT */
/*-----------------------------------------------------------------------------
 *  CHOLMOD basic defines
 *-----------------------------------------------------------------------------*/
/* itype */
#define CHOLMOD_INT 0
#define CHOLMOD_INTLONG 1
#define CHOLMOD_LONG 2

/* dtype */
#define CHOLMOD_DOUBLE 0
#define CHOLMOD_SINGLE 1

/* xtype */
#define CHOLMOD_PATTERN 0
#define CHOLMOD_REAL 1
#define CHOLMOD_COMPLEX 2
#define CHOLMOD_ZOMPLEX 3

/* others */
#define CHOLMOD_MAXMETHODS 9

/* Common->status values */
#define CHOLMOD_OK 0
#define CHOLMOD_NOT_INSTALLED -1
#define CHOLMOD_OUT_OF_MEMORY -2
#define CHOLMOD_TOO_LARGE -3
#define CHOLMOD_INVALID -4
#define CHOLMOD_NOT_POSDEF 1
#define CHOLMOD_DSMALL 2

/* sys code for solving */
#define CHOLMOD_A 0

/*-----------------------------------------------------------------------------
 *  CHOLMOD cholmod_common
 *-----------------------------------------------------------------------------*/
typedef struct cholmod_common_struct {
  double dbound;
  double grow0;
  double grow1;
  size_t grow2;

  size_t maxrank;
  double supernodal_switch;
  int supernodal;

  int final_asis;
  int final_super;
  int final_ll;
  int final_pack;
  int final_monotonic;
  int final_resymbol;

  double zrelax[3];
  size_t nrelax[3];

  int prefer_zomplex;
  int prefer_upper;
  int quick_return_if_not_posdef;

  int print;
  int precise;
  int (*print_function)(const char *, ...);

  int try_catch;

  void (*error_handler)(int status, const char *file, int line,
                        const char *message);
  int nmethods;
  int current;
  int selected;
  struct cholmod_method_struct {
    double lnz;
    double fl;
    double prune_dense;
    double prune_dense2;
    double nd_oksep;
    double other1[4];
    size_t nd_small;
    size_t other2[4];
    int aggressive;
    int order_for_lu;
    int nd_compress;
    int nd_camd;
    int nd_components;
    int ordering;
    size_t other3[4];
  } method[CHOLMOD_MAXMETHODS + 1];

  int postorder;
  void *(*malloc_memory)(size_t);
  void *(*realloc_memory)(void *, size_t);
  void (*free_memory)(void *);
  void *(*calloc_memory)(size_t, size_t);
  int (*complex_divide)(double ax, double az, double bx, double bz, double *cx,
                        double *cz);

  double (*hypotenuse)(double x, double y);
  double metis_memory;
  double metis_dswitch;
  size_t metis_nswitch;
  size_t nrow;
  mextools_int mark;
  size_t iworksize;
  size_t xworksize;

  void *Flag;
  void *Head;
  void *Xwork;
  void *Iwork;

  int itype;
  int dtype;

  int no_workspace_reallocate;
  int status;
  double fl;
  double lnz;
  double anz;

  double modfl;

  size_t malloc_count;
  size_t memory_usage;
  size_t memory_inuse;

  double nrealloc_col;
  double nrealloc_factor;
  double ndbounds_hit;

  double rowfacfl;
  double aatfl;
  double other1[12];

  double SPQR_xstat[2];
  double SPQR_grain;
  double SPQR_small;

  mextools_int SPQR_istat[10];
  mextools_int other2[6];

  int other3[10];

  int prefer_binary;
  int default_nesdis;
  int called_nd;
  int blas_ok;

  int SPQR_shrink;
  int SPQR_nthreads;

  size_t other4[16];

  void *other5[16];

} cholmod_common;

int cholmod_l_start(cholmod_common *);
int cholmod_l_finish(cholmod_common *);

/*-----------------------------------------------------------------------------
 *  CHOLMOD cholmod_sparse
 *-----------------------------------------------------------------------------*/
typedef struct cholmod_sparse_struct {
  size_t nrow;
  size_t ncol;
  size_t nzmax;
  void *p;
  void *i;
  void *nz;
  void *x;
  void *z;
  int stype;
  int itype;
  int xtype;
  int dtype;
  int sorted;
  int packed;
} cholmod_sparse;

/*-----------------------------------------------------------------------------
 *  CHOLMOD cholmod_factor
 *-----------------------------------------------------------------------------*/
typedef struct cholmod_factor_struct {
  size_t n;
  size_t minor;
  void *Perm;
  void *ColCount;
  size_t nzmax;
  void *p;
  void *i;
  void *x;
  void *z;
  void *nz;
  void *next;
  void *prev;
  size_t nsuper;
  size_t ssize;
  size_t xsize;
  size_t maxcsize;
  size_t maxesize;
  void *super;
  void *pi;
  void *px;
  void *s;
  int ordering;
  int is_ll;
  int is_super;
  int is_monotonic;
  int itype;
  int xtype;
  int dtype;
} cholmod_factor;

int cholmod_l_print_factor(cholmod_factor *, const char *, cholmod_common *);
int cholmod_l_free_factor(cholmod_factor **, cholmod_common *);
cholmod_factor *cholmod_l_copy_factor(cholmod_factor *, cholmod_common *);

/*-----------------------------------------------------------------------------
 *  CHOLMOD cholmod_factor
 *-----------------------------------------------------------------------------*/
typedef struct cholmod_dense_struct {
  size_t nrow;
  size_t ncol;
  size_t nzmax;
  size_t d;
  void *x;
  void *z;
  int xtype;
  int dtype;
} cholmod_dense;

int cholmod_l_free_dense(cholmod_dense **, cholmod_common *);

/*-----------------------------------------------------------------------------
 *  CHOLMOD other routines
 *-----------------------------------------------------------------------------*/
cholmod_factor *cholmod_l_analyze(cholmod_sparse *, cholmod_common *);
int cholmod_l_factorize(cholmod_sparse *, cholmod_factor *, cholmod_common *);
cholmod_dense *cholmod_l_solve(int, cholmod_factor *, cholmod_dense *,
                               cholmod_common *);
/* END COPY FROM SUITESPARSE PROJECT */

/*-----------------------------------------------------------------------------
 *  CHOLMOD INTERFACED FUNCTIONS
 *-----------------------------------------------------------------------------*/
/* analyze */
mextools_int mex_cholmod_l_analyze(int nlhs, mxArray *plhs[], int nrhs,
                                   const mxArray *prhs[]);

/* factorize */
mextools_int mex_cholmod_l_factorize(int nlhs, mxArray *plhs[], int nrhs,
                                     const mxArray *prhs[]);

/* free factor */
mextools_int mex_cholmod_l_free_factor(int nlhs, mxArray *plhs[], int nrhs,
                                       const mxArray *prhs[]);

/* print factor */
mextools_int mex_cholmod_l_print_factor(int nlhs, mxArray *plhs[], int nrhs,
                                        const mxArray *prhs[]);

/* solve */
mextools_int mex_cholmod_l_solve(int nlhs, mxArray *plhs[], int nrhs,
                                 const mxArray *prhs[]);

mextools_int mex_cholmod_l_solve_omp(int nlhs, mxArray *plhs[], int nrhs,
                                     const mxArray *prhs[]);

/*-----------------------------------------------------------------------------
 *  CHOLMOD COMMON DEFAULT MEMORY AND PRINT ROUTINES
 *-----------------------------------------------------------------------------*/
#define CHOLMOD_COMMON_DEFAULTS(CC)                                            \
  do {                                                                         \
    (CC).print_function = mexPrintf;                                           \
    (CC).malloc_memory = malloc;                                               \
    (CC).realloc_memory = realloc;                                             \
    (CC).free_memory = free;                                                   \
    (CC).calloc_memory = calloc;                                               \
  } while (0)

#endif
