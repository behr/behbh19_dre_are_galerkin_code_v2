//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//
// Copyright (C) Maximilian Behr
//               2018-2020
//

#ifndef AMD_H
#define AMD_H

/* THE FOLLOWING 5 LINES OF CODE ARE COPIED FROM THE SUITESPARSE PROJECT!
 * REPO: https://github.com/DrTimothyAldenDavis/SuiteSparse
 * TAG: v.3.2.0
 * FILE: amd.h
 *
 * AMD Version 2.2, Copyright (c) 2007 by Timothy A.
 * Davis, Patrick R. Amestoy, and Iain S. Duff.  All Rights Reserved.
 * AMD is available under alternate licences; contact T. Davis for details.
 *
 * AMD License:
 *
 *  Your use or distribution of AMD or any modified version of
 *  AMD implies that you agree to this License.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 *  USA
 *
 *  Permission is hereby granted to use or copy this program under the
 *  terms of the GNU LGPL, provided that the Copyright, this License,
 *  and the Availability of the original version is retained on all copies.
 *  User documentation of any code that uses this code or any modified
 *  version of this code must cite the Copyright, this License, the
 *  Availability note, and "Used by permission." Permission to modify
 *  the code and to distribute modified code is granted, provided the
 *  Copyright, this License, and the Availability note are retained,
 *  and a notice that the code was modified is included.
 */

/* BEGIN COPY FROM SUITESPARSE PROJECT */
extern void *(*amd_malloc)(size_t);          /* pointer to malloc */
extern void (*amd_free)(void *);             /* pointer to free  */
extern void *(*amd_realloc)(void *, size_t); /* pointer to realloc */
extern void *(*amd_calloc)(size_t, size_t);  /* pointer to calloc */
extern int (*amd_printf)(const char *, ...); /* pointer to printf */
/* END COPY FROM SUITESPARSE PROJECT */

/*-----------------------------------------------------------------------------
 *  MEMORY MANAGER ROUTINES USED BY UMFPACK
 *-----------------------------------------------------------------------------*/

typedef void *(*amd_malloc_t)(size_t);
typedef void (*amd_free_t)(void *);
typedef void *(*amd_realloc_t)(void *, size_t);
typedef void *(*amd_calloc_t)(size_t, size_t);
typedef int (*amd_printf_t)(const char *, ...);

#define AMD_MEMORY_SET_DEFAULTS                                                \
  do {                                                                         \
    amd_malloc = malloc;                                                       \
    amd_free = free;                                                           \
    amd_realloc = realloc;                                                     \
    amd_calloc = calloc;                                                       \
    amd_printf = mexPrintf;                                                    \
  } while (0)

#endif
