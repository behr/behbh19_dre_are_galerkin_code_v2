classdef test_example_entries_decay_DRE < matlab.unittest.TestCase
    % TEST_EXAMPLE_ENTRIES_DECAY_DRE    Test for example_entries_decay_dre.
    %
    %   TEST_EXAMPLE_ENTRIES_DECAY_DRE calls the function
    %   example_entries_decay_dre simply with different parameters.
    %
    %   TEST_EXAMPLE_ENTRIES_DECAY_DRE properties:
    %       alpha       - double, scalar number, subdiagonal and superdiagonal of A
    %       n           - double, scalar, positive, integer, size of A
    %       T           - double, scalar, positive, final time
    %       k           - double, scalar, integer, step size will be 2^k
    %       prec        - double, scalar, positive integer, control precision
    %       multiprec   - logical, turn multiprecision on/off
    %
    %   TEST_EXAMPLE_ENTRIES_DECAY_DRE methods:
    %       test        - Call example_entries_decay_dre with different parameters.
    %
    %   See also EXAMPLE_ENTRIES_DECAY_DRE and RUN_EXAMPLE_ENTRIES_DECAY_DRE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        % double, scalar number, subdiagonal and superdiagonal of A
        alpha = {1, 2, 5};
        % double, scalar, positive, integer, size of A
        n = {3};
        % double, scalar, positive, final time
        T = {1};
        % double, scalar, integer, step size will be 2^k
        k = {-4};
        % double, scalar, positive integer, control precision
        prec = {32, 64};
        % logical, turn multiprecision on/off
        multiprec = {false, (~verLessThan('matlab', '9.2') && license('test', 'Symbolic_Toolbox'))};
        % logical, verbosity
        verbose = {~isCIrun()};
    end

    methods(Test)
        function test(obj, alpha, n, k, T, multiprec, prec, verbose) %#ok<*INUSL>
            beta = -1;
            gamma = -alpha;
            example_entries_decay_DRE(alpha, beta, gamma, n, T, k, multiprec, prec, false, verbose);
        end
    end
end
