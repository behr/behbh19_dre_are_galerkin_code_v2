function example_entries_decay_DRE(alpha, beta, gamma, n, T, k, multiprec, prec, plot_visible, verbose)
%EXAMPLE_ENTRIES_DECAY_DRE      Shows the behaviour of the entries of the
%   solution of the differential Riccati equation after a change of basis.
%
%   EXAMPLE_ENTRIES_DECAY_DRE visualises the behaviour of the entries of
%   the solution of the Differential Riccati Equation
%
%       \dot{X} = A'X + XA - XBB'X + C'C, X(0)=0
%
%   after a change of basis for tridiagonal matrices
%   A = tridiag(alpha, beta, gamma), B = ones(n, 1) and C = ones(1,n)
%   on the interval [0,1]. The basis consists of the eigenvectors of the
%   solution of the algebraic Riccati equation.
%
%   EXAMPLE_ENTRIES_DECAY_DRE(ALPHA, BETA, GAMMA, N, T, K, MULTIPREC, PREC, PLOT_VISIBLE, VERBOSE)
%
%   Arguments:
%       ALPHA               - double, scalar, integer, scalar number, subdiagonal of A
%       BETA                - double, scalar, integer, scalar number, diagonal of A
%       GAMMA               - double, scalar, integer, scalar number, superdiagonal of A
%       N                   - double, scalar, positive, integer, size of A
%       T                   - double, scalar, positive, final time
%       K                   - double, scalar, integer, step size will be 2^K
%       MULTIPREC           - logical, turn multiprecision on/off
%       PREC                - double, scalar, positive, integer, control precision
%       PLOTTING_VISIBLE    - logical, turn visible plotting on/off
%       VERBOSE             - logical, verbosity on/off
%
%   If MULTIPREC is false, then standard IEEE double precision is used,
%   otherwise variable precision arithmetic (vpa) is used.
%   The number of significant digits can be controlled by PREC.
%
%   See also TEST_EXAMPLE_ENTRIES_DECAY_DRE and RUN_EXAMPLE_ENTRIES_DECAY_DRE.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% check input arguments
validateattributes(alpha, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(1));
validateattributes(beta, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(2));
validateattributes(gamma, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(3));
validateattributes(n, {'double'}, {'real', '>', 0, 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(4));
validateattributes(T, {'double'}, {'real', '>', 0, 'scalar', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(5));
validateattributes(k, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(6));
validateattributes(multiprec, {'logical'}, {}, mfilename, inputname(7));
validateattributes(prec, {'double'}, {'real', '>', 0, 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(8));
validateattributes(plot_visible, {'logical'}, {}, mfilename, inputname(9));
validateattributes(plot_visible, {'logical'}, {}, mfilename, inputname(10));

%% turn multiprec on/off, if multiprec is on it takes significantly longer
if multiprec
    multiprec_handle = @(x) vpa(x);
    digits(prec);
else
    multiprec_handle = @(x) x;
    prec = 16;
end

%% check matlab version, because of fprintf incompatbility with symbolic
if verLessThan('matlab', '9.0') && multiprec
    error('Matlab Version is too old. fprintf does not work with symbolic inputs.');
end

if ~license('test', 'Symbolic_Toolbox') && multiprec
    error('Symbolic Toolbox not available.');
end

%% set data for differential Riccati equation \dot{X} = A' X + X A - X B B' X + C'C, X(0)=0
A = multiprec_handle(full(gallery('tridiag', n, alpha, beta, gamma)));
B = multiprec_handle(ones(n, 1));
C = multiprec_handle(ones(1, n));
X0 = multiprec_handle(zeros(n, n));

% Parameters for modified Davison Maki method
T = multiprec_handle(T);
k = multiprec_handle(k);
t = multiprec_handle(0);
h = multiprec_handle(2^k);

store_data_at_t = 2^k:2^k:T;
store_pics_at_t = 2^k:2^k:T;
meps = multiprec_handle(eps('double'));

%% create filenames and directories for results
mybasename = sprintf('%s_alpha%d_T%d_k%d_n%d_prec%d_multiprec%d', mfilename, alpha, T, k, n, prec, multiprec);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
myvid = fullfile(mydir, mybasename);
mypic = fullfile(mydir, mybasename);
mkdir(mydir);

%% delete old diary and create new
delete(mydiary);
diary(mydiary);

%% setup parameters for modified Davison Maki method
if verbose
    fprintf('%s - Start Matrix Exponential Computation of Hamiltonian\n', datestr(now));
end
expmhH = expm(-h*[A, -B * B'; -C' * C, -A']);
if verbose
    fprintf('%s - Finished Matrix Exponential Computation of Hamiltonian\n', datestr(now));
end
expmhH11 = expmhH(1:n, 1:n);
expmhH12 = expmhH(1:n, (n + 1):end);
expmhH21 = expmhH((n + 1):end, 1:n);
expmhH22 = expmhH((n + 1):end, (n + 1):end);
Xk = X0;

%% solve ARE
if verbose
    fprintf('%s - Solve algebraic Riccati equation\n', datestr(now));
end
if multiprec
    [V, D] = eig([A, -B * B'; -C' * C, -A']);
    [~, I] = sort(real(diag(D)), 'ascend');
    I = I(1:n);
    V = V(:, I);
    Xinf = V((n + 1):end, 1:n) / V(1:n, 1:n);
    Xinf = real(Xinf);
    Xinf = 0.5 * (Xinf + Xinf');
else
    Xinf = care(A, B, C'*C);
end

if verbose
    fprintf('%s - Finished solving algebraic Riccati equation\n', datestr(now));
    fprintf('%s - Compute eigenvalues of solution of algebraic Riccati equation\n', datestr(now));
end

[Vinf, Dinf] = eig(Xinf);
[Dinf, Iinf] = sort(diag(abs(Dinf)), 'descend');
Vinf = Vinf(:, Iinf);
if verbose
    fprintf('%s - Finished computing eigenvalues of solution of algebraic Riccati equation\n', datestr(now))
end

%% create plot
close all;
if ~plot_visible
    set(0, 'DefaultFigureVisible', 'off');
end

t0 = t;
hs = surf(double(abs(Vinf'*Xk*Vinf)), 'Facecolor', 'interp', 'LineStyle', 'none');
view([-20, 40]);
colorbar;
colormap(jet);
shading interp;
set(gca, 'ZScale', 'log', 'colorscale', 'log');
%set(gca, 'ZScale', 'log');
cb = colorbar();
cb.Ruler.Scale = 'log';
cb.Ruler.MinorTick = 'on';

caxis([double(meps), 1e+1]);
set(gcf, 'Position', [100, 100, 1024, 1024]);
zlim([double(meps), 1e+1]);
ax = gca;
ax.YDir = 'reverse';
ax.XDir = 'reverse';
xlabel('j');
ylabel('i');
zlabel('|(V^TX(t)V))_{i,j}|');
title(sprintf('t = %.5f, t \\in [%.1f, %.1f]', t, t0, T));

%% create video, solve equation and save matrices
v = VideoWriter(myvid, 'Motion JPEG AVI');
v.FrameRate = double(round(1/h));
open(v)

ABS_Vk_T_Xk_V_trunc = {};
ABS_Vk_T_Xk_V = {};
BOUND_T = {};
tstore = [];
i = 1;
j = 1;

while t < T

    % time step modified davison maki
    Xk = (expmhH21 + expmhH22 * Xk) / (expmhH11 + expmhH12 * Xk);
    Xk = 0.5 * (Xk + Xk');
    t = t + h;
    if verbose
        fprintf('%s - Modified Davison Maki t = %f\n', datestr(now), t);
    end

    % transform solution and take absolute values
    Xtemp = abs(Vinf'*Xk*Vinf);
    doubleXtemp = double(Xtemp);
    if verbose
        fprintf('%s - Modified Davison Maki t = %f, min = %.2e, max = %.2e\n', datestr(now), t, min(min(Xtemp)), max(max(Xtemp)));
    end
    doubleXtemp(doubleXtemp <= double(meps)) = double(meps);

    % update plot
    set(hs, 'ZData', doubleXtemp);
    title(sprintf('t = %.5f, t \\in [%.1f, %.1f]', t, t0, T));
    writeVideo(v, getframe(gcf));
    i = i + 1;

    % capture picture
    if ismember(t, store_pics_at_t)
        if verbose
            fprintf('%s - Store Picture at t = %f\n', datestr(now), t);
        end
        saveas(gcf, sprintf('%s_t_%f.eps', mypic, t), 'epsc');
        saveas(gcf, sprintf('%s_t_%f.jpg', mypic, t));
    end

    % save matrices
    if ismember(t, store_data_at_t)
        if verbose
            fprintf('%s - Collect Data at t = %f\n', datestr(now), t);
        end

        %% store untruncated data
        ABS_Vk_T_Xk_V{j} = Xtemp;

        %% store truncated data
        Xtemp2 = Xtemp;
        Xtemp2(Xtemp2 <= meps) = meps;
        ABS_Vk_T_Xk_V_trunc{j} = Xtemp2;

        %% store bound
        Xtemp3 = Vinf' * Xk * Vinf;
        Xtemp4 = multiprec_handle(zeros(n, n));
        for k = 1:n
            for l = 1:n
                if k == l
                    Xtemp4(k, k) = sqrt(abs(Dinf(k)*Dinf(k)));
                else
                    Xtemp4(k, l) = sqrt(abs((Dinf(k) - Xtemp3(k, k))*(Dinf(l) - Xtemp3(l, l))));
                end
            end
        end
        BOUND_T{j} = Xtemp4;

        %% truncate values below machine eps
        tstore = [tstore, t]; %#ok<*AGROW>

        j = j + 1;
    end

end

close(v);

% reset setting
if ~plot_visible
    set(0, 'DefaultFigureVisible', 'on');
end

%% Write Results To File
if verbose
    fprintf('%s - Start writing data to tikz file\n', datestr(now));
end

% store time data
for ti = 1:length(tstore)
    if verbose
        fprintf('%s - Writing data to tikz file for t = %f\n', datestr(now), tstore(ti));
    end

    mydat = fullfile(mydir, sprintf('%s_t%.6f.dat', mybasename, tstore(ti)));

    dlogger = Datalogger(mydat, prec);
    dlogger.add_to_preamble('mfilename', mfilename);
    dlogger.add_to_preamble('n', n);
    dlogger.add_to_preamble('alpha', alpha);
    dlogger.add_to_preamble('beta', beta);
    dlogger.add_to_preamble('gamma', gamma);
    dlogger.add_to_preamble('T', T);
    dlogger.add_to_preamble('k', k);
    dlogger.add_to_preamble('prec', prec);
    header = 'i,j,ABS_Vk_T_Xk_Vk_trunc,ABS_Vk_T_Xk_Vk,BOUND';
    dlogger.set_header(header);

    % table
    datatable = multiprec_handle(zeros(n*n, 5));
    for i = 1:n
        for j = 1:n
            datatable((i - 1)*n+j, :) = [i, j, ABS_Vk_T_Xk_V_trunc{ti}(i, j), ABS_Vk_T_Xk_V{ti}(i, j), BOUND_T{ti}(i, j)];
        end
    end
    dlogger.set_table(datatable);
    dlogger.write();
end

% store eigenvalues
mydat_eval = fullfile(mydir, sprintf('%s_Xinf_evals.dat', mybasename));

dlogger = Datalogger(mydat_eval, prec);
dlogger.add_to_preamble('mfilename', mfilename);
dlogger.add_to_preamble('n', n);
dlogger.add_to_preamble('alpha', alpha);
dlogger.add_to_preamble('beta', beta);
dlogger.add_to_preamble('gamma', gamma);
dlogger.add_to_preamble('T', T);
dlogger.add_to_preamble('k', k);
dlogger.add_to_preamble('prec', prec);

header = 'i,Xinf_lambda';
dlogger.set_header(header);

% table
datatable = multiprec_handle(zeros(n, 2));
for i = 1:n
    datatable(i, :) = [i, Dinf(i)];
end
dlogger.set_table(datatable);
dlogger.write();

% store bound
mydat_bound = fullfile(mydir, sprintf('%s_Xinf_bound.dat', mybasename));

dlogger = Datalogger(mydat_bound, prec);
dlogger.add_to_preamble('mfilename', mfilename);
dlogger.add_to_preamble('n', n);
dlogger.add_to_preamble('alpha', alpha);
dlogger.add_to_preamble('beta', beta);
dlogger.add_to_preamble('gamma', gamma);
dlogger.add_to_preamble('T', T);
dlogger.add_to_preamble('k', k);
dlogger.add_to_preamble('prec', prec);

header = 'i,j,bound_no_time';
dlogger.set_header(header);

% table
datatable = multiprec_handle(zeros(n*n, 3));
for i = 1:n
    for j = 1:n
        datatable((i - 1)*n+j, :) = [i, j, sqrt(Dinf(i)*Dinf(j))];
    end
end
dlogger.set_table(datatable);
dlogger.write();

if verbose
    fprintf('%s - Finished writing tikz file\n', datestr(now));
end

%% Turn Diary Off
diary('off');
