%RUN_EXAMPLE_ENTRIES_DECAY_DRE
%
%   Calls example_entries decay_dre for different parameters.
%
%   See also TEST_EXAMPLE_ENTRIES_DECAY_DRE and EXAMPLE_ENTRIES_DECAY_DRE.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% parameters
alpha = 5;
beta = -1 * ones(length(alpha), 1);
gamma = -alpha;
n = 5;
k = -2;
T = 4;
multiprec = false;
prec = 32;
plot_visible = ~isCIrun();
verbose = ~isCIrun();

%% calls
for j = 1:length(alpha)
    for n_ = n
        for multiprec_ = multiprec
            for prec_ = prec
                for plotting_ = plot_visible
                    example_entries_decay_DRE(alpha(j), beta(j), gamma(j), n_, T, k, multiprec_, prec_, plotting_, verbose);
                end
            end
        end
    end
end
