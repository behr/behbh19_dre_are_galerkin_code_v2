#!/bin/bash
# Your job name.
#SBATCH -J RUN_EXAMPLE_EIG_DECAY_ARE
#
# Output files. .out. is for standard output .err. is for the error output.
#SBATCH -o /mechthild/home/behr/JOBOUTPUT_PHD/%x-%j.out-%N
#
# Maximum expected runtime.  ( 00 Days, 1 hour, 00 minutes, 00 seconds)
#SBATCH --time=200-0:00:00
#
# Allocate one node with 1 CPU cores
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#
# Choose Partition (Queue)
#SBATCH --partition long
#
# Mail Options
#SBATCH --mail-type=FAIL,BEGIN,END
#SBATCH --mail-user=behr@mpi-magdeburg.mpg.de
#
# Array Job
#SBATCH --array=1-25
#
# QoS
#SBATCH --qos=behr
#
# Constraint only Intel Xeon Silver with 192GB mem
#SBATCH --constraint="4110&RAM192"
#
### END OF THE SLURM SPECIFIC PART ###
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2019
#

# Setup OpenMP
if [ -n "$SLURM_CPUS_PER_TASK" ]; then
    omp_threads=$SLURM_CPUS_PER_TASK
else
    omp_threads=1
fi
export OMP_NUM_THREADS=$omp_threads
export MKL_ENABLE_INSTRUCTIONS=AVX2
# set up the environment (choose whats needed)
# load the modules system
source /etc/profile.d/modules.sh

# Load Modules
module load compiler/gcc/6.4
module load libraries/openblas/0.3.0-pthread
module load apps/matlab/2019b
module load libraries/arpack/3.6.3


# SETUP Your MATLAB environment
# The default search path for m-files.
export MATLABPATH=.

MATLAB=matlab

# PARAMETER COMBINATIONS
NS=(100)
PRECS=(512)
MULTIPREC="true"
PLOT_VISIBLE="false"

INDEX=1
for N in "${NS[@]}"; do
    for PREC in "${PRECS[@]}"; do
        # CHECK FOR SLUM TASK ID
        if [ "$INDEX" -eq "${SLURM_ARRAY_TASK_ID}" ]; then
            # RANDOM SLEEP TO AVOID HEAVY LOAD ON FILE SYSTEM
            sleep $((RANDOM % 120))
            # execute matlab command
            CODE="\
                run ../../add_to_path;                                                                                      \
                alpha = [1:10, 25, 50, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 5000, 10000, 50000];         \
                beta = -1*ones(1,length(alpha));                                                                            \
                gamma = -alpha;                                                                                             \
                example_eig_decay_ARE(alpha, beta, gamma, ${N}, ${MULTIPREC}, ${PREC}, ${PLOT_VISIBLE}, true);              \
                quit();                                                                                                     \
                "
            ${MATLAB} -nodesktop -nosplash -r "${CODE}" < /dev/null
        fi
        INDEX=$((INDEX + 1))
    done
done
