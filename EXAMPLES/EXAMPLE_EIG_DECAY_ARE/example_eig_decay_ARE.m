function example_eig_decay_ARE(alpha, beta, gamma, n, multiprec, prec, plot_visible, verbose)
%EXAMPLE_EIG_DECAY_ARE     Shows the eigenvalue decay of the symmetric
%   positive semidefinite solution of the algebraic Riccati equation.
%
%   EXAMPLE_EIG_DECAY_ARE solves several algebraic Riccati equations
%
%       A' X + X A - X B B' X + C' C = 0
%
%   for tridiagonal matrices A = tridiag(alpha, beta, gamma), B = ones(n, 1)
%   and C = ones(1,n). The eigenvalues of the solution are computed numerically
%   and the results are written to disk.
%
%   EXAMPLE_EIG_DECAY_ARE(ALPHA, BETA, GAMMA, N, MULTIPREC, PREC, PLOT_VISIBLE, VERBOSE)
%
%   Arguments:
%       ALPHA               - double, integer, vector of length k, subdiagonal of A
%       BETA                - double, integer, vector of length k, diagonal of A
%       GAMMA               - double, integer, vector of length k, superdiagonal of A
%       N                   - double, scalar, positive, integer, size of A
%       MULTIPREC           - logical, turn multiprecision on/off
%       PREC                - double, scalar, positive, integer, control precision
%       PLOTTING_VISIBLE    - logical, turn visible plotting on/off
%       VERBOSE             - logical, true for verbosity
%
%   If MULTIPREC is false, then standard IEEE double precision is used,
%   otherwise variable precision arithmetic (vpa) is used.
%   The number of significant digits can be controlled by PREC.
%
%   See also TEST_EXAMPLE_EIG_DECAY_ARE and RUN_EXAMPLE_EIG_DECAY_ARE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% check input arguments
validateattributes(alpha, {'double'}, {'real', 'vector', 'nrows', 1, 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(1));
validateattributes(beta, {'double'}, {'real', 'vector', 'nrows', 1, 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(2));
validateattributes(gamma, {'double'}, {'real', 'vector', 'nrows', 1, 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(3));
validateattributes(n, {'double'}, {'real', '>', 0, 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(4));
validateattributes(multiprec, {'logical'}, {}, mfilename, inputname(5));
validateattributes(prec, {'double'}, {'real', '>', 0, 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(6));
validateattributes(plot_visible, {'logical'}, {}, mfilename, inputname(7));
assert(isvector(alpha) && isvector(beta) && isvector(gamma), 'No vector given.');
assert(length(alpha) == length(beta), 'different length of alphas and betas');
assert(length(gamma) == length(beta), 'different length of gammas and betas');
validateattributes(verbose, {'logical'}, {}, mfilename, inputname(8))

%% turn multiprec on/off, if multiprec is on it takes significantly longer
if multiprec
    multiprec_handle = @(x) vpa(x);
    digits(prec);
else
    multiprec_handle = @(x) x;
    prec = 16;
end

%% check matlab version and license, because of fprintf incompatbility with symbolic
if verLessThan('matlab', '9.0') && multiprec
    error('Matlab Version is too old. fprintf does not work with symbolic inputs.');
end

if ~license('test', 'Symbolic_Toolbox') && multiprec
    error('Symbolic Toolbox not available.');
end

%% set data for Algebraic Riccati Equation A' X + X A - X B B' X + C' C
Afun = @(n, alpha, beta, gamma) multiprec_handle(full(gallery('tridiag', n, alpha, beta, gamma)));
Bfun = @(n) multiprec_handle(ones(n, 1));
Cfun = @(n) multiprec_handle(ones(1, n));

%% Create Filenames and Directories for Results
mybasename = sprintf('%s_n%d_prec%d_multiprec%d', mfilename, n, prec, multiprec);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mydat = fullfile(mydir, sprintf('%s.dat', mybasename));
mypic = fullfile(mydir, mybasename);
mkdir(mydir);

%% delete old diary and create new one
delete(mydiary);
diary(mydiary);

%% Solve Riccati Equation and Compute Eigenvalues
nb = length(alpha);
evals(n, nb) = multiprec_handle(0);
abs2nrm(1, nb) = multiprec_handle(0);
rel2nrm(1, nb) = multiprec_handle(0);

for j = 1:nb

    %% set up matrices
    if verbose
        fprintf('%s - Set up Matrices n=%d, alpha=%.2f, beta=%.2f, gamma=%.2f\n', datestr(now), n, alpha(j), beta(j), gamma(j));
    end
    A = Afun(n, alpha(j), beta(j), gamma(j));
    B = Bfun(n);
    C = Cfun(n);

    %% compute solution of A'X + XA - XBB'X + C'C = 0
    if verbose
        fprintf('%s - Solve algebraic Riccati equation\n', datestr(now));
    end

    if multiprec
        [V, D] = eig([A, -B * B'; -C' * C, -A']);
        [~, I] = sort(real(diag(D)), 'ascend');
        I = I(1:n);
        V = V(:, I);
        Xinf = V((n + 1):end, 1:n) / V(1:n, 1:n);
        Xinf = real(Xinf);
        Xinf = 0.5 * (Xinf + Xinf');
    else
        Xinf = care(A, B, C'*C);
    end

    if verbose
        fprintf('%s - Compute abs./rel. 2-norm residual of solution of solution of algebraic Riccati equation\n', datestr(now));
    end
    abs2nrm(j) = norm(A'*Xinf+Xinf*A-(Xinf * B)*(B' * Xinf)+C'*C);
    t = norm(C)^2;
    rel2nrm(j) = abs2nrm(j) / t;

    if verbose
        fprintf('%s - alpha = %.2f - abs./rel. 2-norm residual = %.2e/%.2e\n', datestr(now), double(alpha(j)), double(abs2nrm(j)), double(rel2nrm(j)));
    end

    %% compute eigenvalues values
    if verbose
        fprintf('%s - Compute eigenvalues of solution of algebraic Riccati equation\n', datestr(now));
    end
    [~, t] = eig(Xinf);
    t = abs(diag(t));
    t = sort(t, 'descend');
    evals(:, j) = t;
end

%% Write to file for tikz and mat
dlogger = Datalogger(mydat, prec);
dlogger.add_to_preamble('mfilename', mfilename);
dlogger.add_to_preamble('n', n);
dlogger.add_to_preamble('prec', prec);
dlogger.add_to_preamble('multiprec', multiprec);

% write alpha beta gamma
alpha_str = sprintf('%d,', alpha);
alpha_str = alpha_str(1:end-1);
dlogger.add_to_preamble('alpha', alpha_str);

beta_str = sprintf('%d,', beta);
beta_str = beta_str(1:end-1);
dlogger.add_to_preamble('beta', beta_str);

gamma_str = sprintf('%d,', gamma);
gamma_str = gamma_str(1:end-1);
dlogger.add_to_preamble('gamma', gamma_str);

nb = numel(alpha);
for i = 1:nb
    if i == 1
        header = sprintf('alpha_%d_beta_%d_gamma_%d', alpha(i), beta(i), gamma(i));
    else
        header = sprintf('%s, alpha_%d_beta_%d_gamma_%d', header, alpha(i), beta(i), gamma(i));        
    end
end
dlogger.set_header(header);
dlogger.set_table(evals);
dlogger.write();

%% turn diary off
diary('off');

%% plot results
close all;
close all hidden;
visible = get(0, 'DefaultFigureVisible');
if ~plot_visible
    set(0, 'DefaultFigureVisible', 'off');
end

list = hsv(length(alpha));
for j = 1:size(list, 1)
    semilogy(evals(:, j), 'o', 'DisplayName', sprintf('\\alpha = %d, \\beta = %d, \\gamma = %d\n', alpha(j), beta(j), gamma(j)), 'color', list(j, :))
    hold on
    set(gcf, 'Position', [100, 100, 1024, 1024]);
end
title('Eigenvalues of X for different values of \alpha.');
legend('show')
hold off

saveas(gcf, sprintf('%s_n_%d.eps', mypic, n), 'epsc');
saveas(gcf, sprintf('%s_n_%d.jpg', mypic, n));

% reset setting
set(0, 'DefaultFigureVisible', visible);
end
