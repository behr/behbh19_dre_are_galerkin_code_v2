classdef test_example_eig_decay_ARE < matlab.unittest.TestCase
    %TEST_EXAMPLE_EIG_DECAY_ARE     Test for example_eig_decay_are.
    %
    %   TEST_EXAMPLE_EIG_DECAY_ARE calls the function
    %   example_eig_decay_are simply with different parameters.
    %
    %   TEST_EXAMPLE_EIG_DECAY_ARE properties:
    %       n           - double, scalar, positive, integer, size of A
    %       prec        - double, scalar, positive integer, control precision
    %       multiprec   - logical, turn multiprecision on/off
    %
    %   TEST_EXAMPLE_EIG_DECAY_DRE methods:
    %       test        - Call example_eig_decay_are with different parameters.
    %
    %   See also EXAMPLE_EIG_DECAY_ARE and RUN_EXAMPLE_EIG_DECAY_ARE.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        % double, scalar, positive, integer, size of A
        n = {3};
        % logical, turn multiprecision on/off
        multiprec = {false, (~verLessThan('matlab', '9.0') && license('test', 'Symbolic_Toolbox'))};
        % double, scalar, positive integer, control precision
        prec = {32, 64};
        % logical
        verbose = {~isCIrun()};
    end

    methods(Test)
        function test(obj, n, multiprec, prec, verbose) %#ok<*INUSL>
            alpha = [1, 2, 5, 10, 25, 50, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 5000, 10000, 50000];
            beta = -1 * ones(1, length(alpha));
            gamma = -alpha;
            example_eig_decay_ARE(alpha, beta, gamma, n, multiprec, prec, false, verbose);
        end
    end
end
