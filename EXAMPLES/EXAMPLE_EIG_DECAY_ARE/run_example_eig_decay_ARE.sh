#!/bin/bash
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2019-2020
#


# SETUP Your MATLAB environment
MATLAB=matlab2018a

# PARAMETER COMBINATIONS
NS=(100)
PRECS=(512)
MULTIPREC="true"
PLOT_VISIBLE="false"

INDEX=1
for N in "${NS[@]}"; do
    for PREC in "${PRECS[@]}"; do
        # execute matlab command
        CODE="\
            run ../../add_to_path;                                                                                      \
            alpha = [1, 2, 5, 10, 25, 50, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 5000, 10000, 50000];  \
            beta = -1*ones(1,length(alpha));                                                                            \
            gamma = -alpha;                                                                                             \
            example_eig_decay_ARE(alpha, beta, gamma, ${N}, ${MULTIPREC}, ${PREC}, ${PLOT_VISIBLE}, true);              \
            quit();                                                                                                     \
            "
        ${MATLAB} -nodesktop -nosplash -r "${CODE}"
    done
done
