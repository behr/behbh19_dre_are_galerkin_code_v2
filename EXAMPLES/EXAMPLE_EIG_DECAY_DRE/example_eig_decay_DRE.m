function example_eig_decay_DRE(alpha, beta, gamma, n, T, k, multiprec, prec, plot_visible, verbose)
%EXAMPLE_EIG_DECAY_DRE     Shows the eigenvalue decay of the the solution
% of the differential Riccati equation.
%
%   EXAMPLE_EIG_DECAY_DRE visualises the eigenvalue behaviour of
%   the solution of the differential Riccati equation
%
%       \dot{X} = A'X + XA - XBB'X + C'C, X(0)=0
%
%   for tridiagonal matrices A = tridiag(alpha, beta, gamma), B = ones(n, 1)
%   and C = ones(1,n). A modified Davison Maki method is used to solve
%   the equation. The eigenvalues of the solution is computed numerically
%   and the results are written to disk.
%
%   EXAMPLE_EIG_DECAY_DRE(ALPHA, BETA, GAMMA, N, T, K, MULTIPREC, PREC, PLOT_VISIBLE, VERBOSE)
%
%   Arguments:
%       ALPHA               - double, scalar, integer, scalar number, subdiagonal of A
%       BETA                - double, scalar, integer, scalar number, diagonal of A
%       GAMMA               - double, scalar, integer, scalar number, superdiagonal of A
%       N                   - double, scalar, positive, integer, size of A
%       T                   - double, scalar, positive, final time
%       K                   - double, scalar, integer, step size will be 2^K
%       MULTIPREC           - logical, turn multiprecision on/off
%       PREC                - double, scalar, positive, integer, control precision
%       PLOTTING_VISIBLE    - logical, turn visible plotting on/off
%       VERBOSE             - logical, verbosity on/off
%
%   If MULTIPREC is false, then standard IEEE double precision is used,
%   otherwise variable precision arithmetic (vpa) is used.
%   The number of significant digits can be controlled by PREC.
%
%   See also TEST_EXAMPLE_EIG_DECAY_DRE and RUN_EXAMPLE_EIG_DECAY_DRE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% check input arguments
validateattributes(alpha, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(1));
validateattributes(beta, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(2));
validateattributes(gamma, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(3));
validateattributes(n, {'double'}, {'real', '>', 0, 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(4));
validateattributes(T, {'double'}, {'real', '>', 0, 'scalar', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(5));
validateattributes(k, {'double'}, {'real', 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(6));
validateattributes(multiprec, {'logical'}, {}, mfilename, inputname(7));
validateattributes(prec, {'double'}, {'real', '>', 0, 'scalar', 'integer', 'nonnan', 'finite', 'nonsparse'}, mfilename, inputname(8));
validateattributes(plot_visible, {'logical'}, {}, mfilename, inputname(9));
validateattributes(verbose, {'logical'}, {}, mfilename, inputname(10));

%% turn multiprec on/off, if multiprec is on it takes significantly longer
if multiprec
    multiprec_handle = @(x) vpa(x);
    digits(prec);
else
    multiprec_handle = @(x) x;
    prec = 16;
end

%% check matlab version and license, because of fprintf incompatbility with symbolic
if verLessThan('matlab', '9.0') && multiprec
    error('Matlab Version is too old. fprintf does not work with symbolic inputs.');
end

if ~license('test', 'Symbolic_Toolbox') && multiprec
    error('Symbolic Toolbox not available.');
end

%% set data for differential Riccati equation \dot{X} = A' X + X A - X B B' X + C'C, X(0)=0
A = multiprec_handle(full(gallery('tridiag', n, alpha, beta, gamma)));
B = multiprec_handle(ones(n, 1));
C = multiprec_handle(ones(1, n));
X0 = multiprec_handle(zeros(size(A)));

% Parameters for modified Davison Maki method
T = multiprec_handle(T);
k = multiprec_handle(k);
t = multiprec_handle(0);
h = multiprec_handle(2^k);

% Setup Parameters for Storing Eigenvalues
evals_to_store = n; % store all n largest eigenvalues
evals_store_at = 0:1 / 2:T; % store eigenvalues at these times

if n <= 30
    evals_to_mark = 1:5:n; % mark these eigenvalues in red
else
    evals_to_mark = 15:5:30; % mark these eigenvalues in red
end

%% create filenames and directories for results
mybasename = sprintf('%s_alpha%d _beta%d_gamma%d_T%d_n%d_prec%d_multiprec%d', mfilename, alpha, beta, gamma, T, n, prec, multiprec);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mfilename, mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mydiary = fullfile(mydir, sprintf('%s.log', mybasename));
mydat = fullfile(mydir, sprintf('%s.dat', mybasename));
mypic = fullfile(mydir, mybasename);
mkdir(mydir);

%% delete old diary and create new
delete(mydiary);
diary(mydiary);

%% setup parameters for modified Davison Maki method
if verbose
    fprintf('%s - Start: Compute Matrix Exponential of Hamiltonian\n', datestr(now));
end
expmhH = expm(-h*[A, -B * B'; -C' * C, -A']);
if verbose
    fprintf('%s - Finished: Compute Matrix Exponential of Hamiltonian\n', datestr(now));
end
expmhH11 = expmhH(1:n, 1:n);
expmhH12 = expmhH(1:n, (n + 1):end);
expmhH21 = expmhH((n + 1):end, 1:n);
expmhH22 = expmhH((n + 1):end, (n + 1):end);
Xk = X0;

%% solve DRE
% prepare store for eigenvalues
evals = [];
evals_t = [];

while t < T

    % make timestep using modified Davison method
    Xk = (expmhH21 + expmhH22 * Xk) / (expmhH11 + expmhH12 * Xk);
    Xk = 0.5 * (Xk + Xk');
    t = t + h;
    if verbose
        fprintf('%s - Modified Davison Maki t = %f\n', datestr(now), t);
    end

    %% Check if eigenvalue store is wanted
    if ismember(t, evals_store_at)
        % compute eigenvalues
        evals_xk = sort(abs(eig(Xk)), 'descend');
        evals_xk = evals_xk(1:evals_to_store);
        evals = [evals, evals_xk]; %#ok<*AGROW>
        evals_t = [evals_t, t];
        if verbose
            fprintf('Computed and Stored Eigenvalues at t = %5.3f\n', t);
        end
    end

end

%% plot eigenvalues and visualize selected eigencurves
close all;
close all hidden;
visible = get(0, 'DefaultFigureVisible');
if ~plot_visible
    set(0, 'DefaultFigureVisible', 'off');
end

[xx, yy] = meshgrid(evals_t, 1:size(evals, 1));
subplot(2, 1, 1)
plot3(xx, yy, evals, 'ob')
set(gca, 'zscale', 'log');
set(gca, 'FontSize', 17);
xlabel('$t$', 'Interpreter', 'latex')
ylabel('$k$', 'Interpreter', 'latex')
zlabel('$\lambda_k(X(t))$', 'Interpreter', 'latex')
%zticks([1e-30, 1e-20, 1e-10, 1e-5, 1e+0, 1e+5])
set(gca, 'ZTickLabel', [1e-30, 1e-20, 1e-10, 1e-5, 1e+0, 1e+5]);
hold on
for markedeval = evals_to_mark
    [xx, yy] = meshgrid(evals_t, markedeval);
    subplot(2, 1, 1)
    plot3(xx, yy, evals(markedeval, :)', 'r', 'LineWidth', 4)
    set(gca, 'zscale', 'log');
    set(gca, 'YDir', 'reverse');
end

saveas(gcf, sprintf('%s.eps', mypic), 'epsc');
saveas(gcf, sprintf('%s.jpg', mypic));

% reset setting
set(0, 'DefaultFigureVisible', visible);

%% Write to file for tikz and mat
dlogger = Datalogger(mydat, prec);
dlogger.add_to_preamble('mfilename', mfilename);
dlogger.add_to_preamble('n', n);
dlogger.add_to_preamble('alpha', alpha);
dlogger.add_to_preamble('beta', beta);
dlogger.add_to_preamble('gamma', gamma);
dlogger.add_to_preamble('T', T);
dlogger.add_to_preamble('k', k);
dlogger.add_to_preamble('prec', prec);
header = 'N,Time,Lambda';
dlogger.set_header(header);
% table
l_evals_t = length(evals_t);
datatable = multiprec_handle(zeros(evals_to_store*l_evals_t, 3));

for i = 1:evals_to_store
    for j = 1:l_evals_t
        datatable((j - 1)*evals_to_store+i, :) = [i, evals_t(j), evals(i, j)];
    end
end
dlogger.set_table(datatable);
dlogger.write();

fprintf('%s - Finished writing tikz file\n', datestr(now));

%% turn diary off
diary('off');

end
