%RUN_EXAMPLE_EIG_DECAY_DRE
%
%   Calls example_eig_decay_dre for different parameters.
%
%   See also TEST_EXAMPLE_EIG_DECAY_DRE and EXAMPLE_EIG_DECAY_DRE.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all

%% parameters
alpha = [1, 5, 10, 20, 50];
beta = -1 * ones(1, length(alpha));
gamma = -alpha;

n = 10;
k = -4;
T = 15;
multiprec = true;
prec = 32;
plot_visible = false;
verbose = ~isCIrun();

%% calls
for i = 1:length(alpha)
    for n_ = n
        for multiprec_ = multiprec
            for prec_ = prec
                for plot_visible_ = plot_visible
                    example_eig_decay_DRE(alpha(i), beta(i), gamma(i), n_, T, k, multiprec_, prec_, plot_visible_, verbose);
                end
            end
        end
    end
end
