%RUN_EXAMPLE_DAVISON_MAKI_FAIL
%
%   Calls example_davison_maki_fail for different parameters.
%
%   See also TEST_EXAMPLE_DAVISON_MAKI_FAIL and EXAMPLE_DAVISON_MAKI_FAIL.
%
%   Author: Maximilian Behr

% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all;

%% parameters
alpha = 5;
beta = -1;
gamma = -5;
n = 10;
k = -5;
T = 1;
multiprec = true;
prec = 32;
plot_visible = false;
verbose = ~isCIrun();

%% calls
example_davison_maki_fail(alpha, beta, gamma, n, T, k, multiprec, prec, plot_visible, verbose);
