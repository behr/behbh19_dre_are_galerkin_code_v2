#!/bin/bash
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Copyright (C) Maximilian Behr
#               2019-2020
#

# SETUP Your MATLAB environment
MATLAB=matlab2018a

# PARAMETER COMBINATIONS
ALPHAS=(5)
BETAS=(-1)
NS=(100)
TS=(1)
KS=(-5)
PRECS=(512)
for N in "${NS[@]}"; do
    for ALPHA in "${ALPHAS[@]}"; do
        for BETA in "${BETAS[@]}"; do
            for PREC in "${PRECS[@]}"; do
                for T in "${TS[@]}"; do
                    for K in "${KS[@]}"; do
                        # execute matlab command
                        ${MATLAB} -nodesktop -nosplash -r "run ../../add_to_path; example_davison_maki_fail(${ALPHA}, ${BETA}, -${ALPHA}, ${N}, ${T}, ${K}, true, ${PREC}, false, true); quit();"
                    done
                done
            done
        done
    done
done
