classdef test_example_davison_maki_fail < matlab.unittest.TestCase
    %TEST_EXAMPLE_DAVISON_MAKI_FAIL     Test for example_davison_maki_fail.
    %
    %   TEST_EXAMPLE_DAVISON_MAKI_FAIL calls the function
    %   example_davison_maki_fail simply with different parameters.
    %
    %   TEST_EXAMPLE_DAVISON_MAKI_FAIL properties:
    %       alpha       - double, scalar number, subdiagonal of A
    %       beta        - double, scalar number, diagonal of A
    %       gamma       - double, scalar number, superdiagonal of A
    %       n           - double, scalar, positive, integer, size of A
    %       T           - double, scalar, positive, final time
    %       k           - double, scalar, integer, step size will be 2^k
    %       prec        - double, scalar, positive integer, control precision
    %       multiprec   - logical, turn multiprecision on/off
    %
    %   TEST_EXAMPLE_DAVISON_MAKI_FAIL methods:
    %       test        - Call example_davison_maki_fail with different parameters.
    %
    %   See also EXAMPLE_DAVISON_MAKI_FAIL and RUN_EXAMPLE_DAVISON_MAKI_FAIL.
    %
    %   Author: Maximilian Behr

    %
    % This program is free software; you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation; either version 2 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program; if not, see <http://www.gnu.org/licenses/>.
    %
    % Copyright (C) Maximilian Behr
    %               2019-2020
    %
    properties(TestParameter)
        % double, scalar number, subdiagonal of A
        alpha = {5};
        % double, scalar number, diagonal of A
        beta = {-1};
        % double, scalar number, superdiagonal of A
        gamma = {-5};
        % double, scalar, nonnegative, integer, size of A
        n = {3};
        % double, scalar, positive, final time
        T = {1};
        % double, scalar, integer, step size will be 2^k
        k = {-5};
        % double, scalar, nonegative integer, control precision
        prec = {32, 64};
        % logical, turn multiprecision on/off
        multiprec = {false, (~verLessThan('matlab', '9.0') && license('test', 'Symbolic_Toolbox'))};
        % logical, verbose
        verbose = {~isCIrun()};
    end

    methods(Test)
        function test(obj, alpha, beta, gamma, n, T, k, multiprec, prec, verbose) %#ok<*INUSL>
            example_davison_maki_fail(alpha, beta, gamma, n, T, k, multiprec, prec, false, verbose);
        end
    end
end
