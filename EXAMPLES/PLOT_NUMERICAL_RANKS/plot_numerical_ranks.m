%PLOT_NUMERICAL_RANKS
%
%   Script computes numerical ranks of the solution of a differential
%   Riccati equation. The script uses the benchmark examples from the project.
%   The numerical solution is computed by the
%
%   See also MDM_SYM_DRE.
%
%   Author: Maximilian Behr

%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) Maximilian Behr
%               2019-2020
%

%% clear
clearvars, clc, close all;

%% benchmark examples from slicot, rail example and fss example
verbose = ~isCIrun();
if isCIrun()
    problems = { ...
        struct('instance', 'beam', 'T', 2^(-1), 'k_mod_dm', -8, 'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'beam', 'T', 2^(2), 'k_mod_dm', -8, 'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        };
else
    problems = { ...
        struct('instance', 'build',             'T', 2^0,       'k_mod_dm', -8,     'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'CDplayer',          'T', 2^(-4),    'k_mod_dm', -15,    'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'eady',              'T', 2^(-10),   'k_mod_dm', -20,    'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'fom',               'T', 2^(-6),    'k_mod_dm', -12,    'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'heat-cont',         'T', 2^5,       'k_mod_dm', -8,     'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'iss',               'T', 2^5,       'k_mod_dm', -6,     'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'pde',               'T', 2^(-5),    'k_mod_dm', -14,    'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'random',            'T', 2^(-10),   'k_mod_dm', -18,    'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'rail_371',          'T', 2^8,       'k_mod_dm', -2,     'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'fss_K50M2Q2_100',   'T', 2^4,       'k_mod_dm', -8,     'X0', '@(n) zeros(n, n)', 'X0string', 'zeros', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'beam',              'T', 2^2,       'k_mod_dm', -8,     'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'build',             'T', 2^3,       'k_mod_dm', -4,     'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'CDplayer',          'T', 2^(-4),    'k_mod_dm', -15,    'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'eady',              'T', 2^(-10),   'k_mod_dm', -20,    'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'fom',               'T', 2^(-6),    'k_mod_dm', -12,    'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'heat-cont',         'T', 2^5,       'k_mod_dm', -8,     'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'iss',               'T', 2^5,       'k_mod_dm', -6,     'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'pde',               'T', 2^(-5),    'k_mod_dm', -14,    'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'random',            'T', 2^(-10),   'k_mod_dm', -18,    'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'rail_371',          'T', 2^8,       'k_mod_dm', -2,     'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'fss_K50M2Q2_100',   'T', 2^4,       'k_mod_dm', -8,     'X0', '@(n) eye(n, n)', 'X0string', 'eye', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'beam',              'T', 2^2,       'k_mod_dm', -8,     'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'build',             'T', 2^3,       'k_mod_dm', -4,     'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'CDplayer',          'T', 2^(-4),    'k_mod_dm', -15,    'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'eady',              'T', 2^(-10),   'k_mod_dm', -20,    'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'fom',               'T', 2^(-6),    'k_mod_dm', -12,    'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'heat-cont',         'T', 2^5,       'k_mod_dm', -8,     'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'iss',               'T', 2^5,       'k_mod_dm', -6,     'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'pde',               'T', 2^(-5),    'k_mod_dm', -14,    'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'random',            'T', 2^(-10),   'k_mod_dm', -18,    'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'rail_371',          'T', 2^8,       'k_mod_dm', -2,     'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        struct('instance', 'fss_K50M2Q2_100',   'T', 2^4,       'k_mod_dm', -8,     'X0', '@(n) ones(n, n)', 'X0string', 'ones', 'save_mod', 0.25, 'ranktol', 1e-11) ...
        };
end
%% create filenames and directories for results
mybasename = sprintf('%s', mfilename);
[mydir, ~, ~] = fileparts(mfilename('fullpath'));
mydir = fullfile(mydir, 'results', mybasename);
[status, message, messageid] = rmdir(mydir, 's');
if ~status
    fprintf('%s - %s:%s-%s\n', datestr(now), mfilename, message, messageid);
end
mkdir(mydir);

%% benchmark iterate over benchmark examples
for problem_i = 1:length(problems)

    %% load problem instance
    problem = problems{problem_i};
    if verbose
        fprintf('\n');
        fprintf('----------------------------------------------\n');
    end
    instance = problem.instance;
    data = load(instance);
    A = full(data.A);
    B = full(data.B);
    C = full(data.C);
    n = size(A, 1);
    X0_str = problem.X0string;
    X0_fh = str2func(problem.X0);
    X0 = X0_fh(n);
    T = problem.T;
    ranktol = problem.ranktol;

    %% create files for logging
    mydat = fullfile(mydir, sprintf('%s_%s_%s.dat', mybasename, instance, X0_str));
    mypic = fullfile(mydir, sprintf('%s_%s_%s', mybasename, instance, X0_str));

    %% get period for saving numerical ranks
    save_mod = problem.save_mod;

    %% modified davison maki solver options
    mdm_opt = MDM_DRE_options(true);
    mod_dm = MDM_SYM_DRE(A, [], B*B', C'*C, X0, mdm_opt, 2^problem.k_mod_dm, problem.T);
    mod_dm.prepare();

    %% iterate over time and logging data
    t = 0;
    if verbose
        fprintf('instance = %s, X0 = %s, T = %.8f\n', instance, X0_str, T);
    end
    time_run = tic;
    save_mod_counter = 0;
    percent = 0;
    log_t = [];
    log_norm2X = [];
    log_rank = [];
    while mod_dm.t < T

        % time steps with davison maki
        mod_dm.time_step();

        %% compute numerical rank, store and print
        percent = mod_dm.t / T * 100;
        if percent >= save_mod_counter && verbose
            % get solutions and compute numerical rank and 2-norm largest absolute eigenvalue
            X_mod_dm = mod_dm.get_solution();
            trank = rank(X_mod_dm, ranktol);
            tnorm2X = two_norm_sym_handle(@(x) X_mod_dm*x, n);
            fprintf('t = %f, %6.2f %%, mod_dm step size = %f, numerical rank: eps/nrank/maxrank = %.2e / %4d / %4d, 2-norm X = %.2e\n', ...
                mod_dm.t, mod_dm.t/T*100, mod_dm.h, ranktol, trank, n, tnorm2X);

            % log values
            log_t = [log_t; mod_dm.t]; %#ok<*AGROW>
            log_norm2X = [log_norm2X; tnorm2X];
            log_rank = [log_rank; trank];

            save_mod_counter = save_mod_counter + save_mod;
        end

    end
    time_run = toc(time_run);
    if verbose
        fprintf('Test Runtime: %s, %s %f secs.\n', instance, X0_str, time_run);
        fprintf('Computational Time: %s, Step Size = %f, %f secs.\n', upper(class(mod_dm)), mod_dm.h, mod_dm.wtime_prepare+mod_dm.wtime_time_step);
        fprintf('----------------------------------------------\n\n');
    end

    %% write to file for tikz
    dlogger = Datalogger(mydat);
    dlogger.add_to_preamble('mfilename', mfilename);
    dlogger.add_to_preamble('n', n);
    dlogger.add_to_preamble('instance', instance);
    dlogger.add_to_preamble('X0_str', X0_str);
    dlogger.add_to_preamble('time_run', time_run);
    dlogger.add_to_preamble('T', mod_dm.T);
    dlogger.add_to_preamble('h', mod_dm.h);
    dlogger.add_to_preamble('ranktol', ranktol);
    dlogger.add_to_preamble('wtime_prepare', mod_dm.wtime_prepare);
    dlogger.add_to_preamble('wtime_time_step', mod_dm.wtime_time_step);

    header = 't,nrank,norm2X';
    dlogger.set_header(header);
    dlogger.set_table([log_t, log_rank, log_norm2X]);
    dlogger.write();

    %% write plot of the numerical rank
    close all;
    close all hidden;
    visible = get(0, 'DefaultFigureVisible');
    set(0, 'DefaultFigureVisible', 'off');

    plot(log_t, log_rank);
    title(sprintf('Numerical rank of X(t), Example: %s, X0=%s, T=%f.', instance, X0_str, T));
    hold off

    saveas(gcf, mypic, 'epsc');
    saveas(gcf, mypic, 'jpg');

    % reset setting
    set(0, 'DefaultFigureVisible', visible);

end
